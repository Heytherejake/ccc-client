package com.studio878.characters;

import java.util.HashMap;
import java.util.Map.Entry;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SpriteSheet;

import com.studio878.abilities.Ability;

public abstract class PlayerCharacter {

	public static HashMap<Integer, PlayerCharacter> characterList = new HashMap<Integer, PlayerCharacter>();
	
	int id = 0;
	
	String fullName, shortName, lore;
	
	
	String[] tags, tips;
	
	int rangedDamage = 75, meleeDamage = 75;
	double moveSpeed = 3.4;
	
	int strength = 10, dexterity = 10, intelligence = 10;
	
	int health = 100, mana = 100;
	
	int healthPerLevel = 100, manaPerLevel = 50;
	
	int armor = 15, magicDefence = 15;
	
	int healthRegen = 5, manaRegen = 5;
	
	int lifeSteal = 0;

	
	Ability passive;
	
	Ability[] abilities = new Ability[4];
	int[] levels = new int[4];
	
	SpriteSheet blueSheet, redSheet;
	Image hand, classIcon;
	
	public int getId() {
		return id;
	}
	
	public String getFullName() {
		return fullName;
	}
	
	public String getShortName() {
		return shortName;
	}
	
	
	public String getLore() {
		return lore;
	}
	
	public String[] getTags() {
		return tags;
	}
	
	public String[] getTips() {
		return tips;
	}
	
	public Image getClassIcon() {
		return classIcon;
	}
	
	public int getRangedDamage() {
		return rangedDamage;
	}
	
	public int getMeleeDamage() {
		return meleeDamage;
	}
	
	public int getHealth() {
		return health;
	}
	
	public double getMoveSpeed() {
		return moveSpeed;
	}
	
	public int getStrength() {
		return strength;
	}
	
	public int getDexterity() {
		return dexterity;
	}
	
	public int getIntelligence() {
		return intelligence;
	}
	
	public Ability getAbility(int position) {
		return abilities[position];
	}
	
	public int getAbilityLevel(int position) {
		return levels[position];
	}
	
	public SpriteSheet getRedSpriteSheet() {
		return redSheet;
	}
	
	public SpriteSheet getBlueSpriteSheet() {
		return blueSheet;
	}
	
	public Image getHandImage() {
		return hand;
	}
	
	public Ability getPassiveAbility() {
		return passive;
	}

	public void updateCharacter(int rd, int md, double speed, int str, int dex, int intel, int hp, int mp, int hpl, int mpl, int armor, int magicdef, int hpregen, int mpregen, int lifesteal) {
		this.rangedDamage = rd;
		this.meleeDamage = md;
		this.moveSpeed = speed;
		this.strength = str;
		this.dexterity = dex;
		this.intelligence = intel;
		this.health = hp;
		this.mana = mp;
		this.healthPerLevel = hpl;
		this.manaPerLevel = mpl;
		this.armor = armor;
		this.magicDefence = magicdef;
		this.healthRegen = hpregen;
		this.manaRegen = mpregen;
		this.lifeSteal = lifesteal;
	}
	
	
	public void run(Graphics g, GameContainer c) {
		
	}
	
	public void draw(Graphics g, GameContainer c) {
		
	}
	
	public static void populateCharacterList() {
		characterList.put(1, new Excavator());
		characterList.put(2, new Researcher());
		characterList.put(3, new Tactitian());
	}
	
	public static PlayerCharacter lookupId(Integer i) {
		for(Entry<Integer, PlayerCharacter> c : characterList.entrySet()) {
			if(c.getKey() == i) {
				return c.getValue();
			}
		}
		return null;
	}
	
	
}
