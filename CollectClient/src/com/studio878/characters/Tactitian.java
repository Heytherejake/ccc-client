package com.studio878.characters;

import com.studio878.abilities.Ability;
import com.studio878.abilities.Tactitian1Momentum;
import com.studio878.abilities.Tactitian2HollowPoint;
import com.studio878.abilities.Tactitian3FullMetalJacket;
import com.studio878.abilities.Tactitian4RainOfBullets;
import com.studio878.abilities.TactitianPassive;
import com.studio878.util.Sheets;

public class Tactitian extends PlayerCharacter {

	public Tactitian() {
		id = 3;
		fullName = "Talon, the Xeliarian Tactitian";
		shortName = "Tactitian";
		lore = "\"We lost two platoons? Unacceptable. How many did they lose? Fifteen? Eh, I've done better.\"<br>\t- Talon, upon winning a battle fought with severe terrain and size disadvantage<br><br>The head general under the Xeliar for twenty years, Talon has been the most invaluable asset to Xeliarian control in the kingdom's history. Having won more battles than any other general in the history of Agema, Talon is admired as a hero and an inspiration to his people, some of whom believe he holds more power than the Xeliar himself. Rumors of a possible takeover spread following Talon's monumental victory over the Regency at Faastrum Ridge, however to this date, Talon appears adamantely loyal to the Xeliar.<br><br>Primarily as a result of his unparalleled record in battle, the Xeliarian kingdom is currently experiencing peace unmatched throughtout all of his history. As a result, Talon has taken to assisting mercenary groups in land grabs, hoping to further sharpen the scope of his strategical knowledge, as long as doing so doesn't hinder the Xeliar.";
		tags = new String[]{"Carry, Strength"};
		tips = new String[]{"Full Metal Jacket and Momement can be chained to escape.", "Rain of Bullets deals attack damage as well as true damage. Use it to finish off enemies.", "Use Hollow Point before moving in on an enemy."};
	
		abilities = new Ability[]{new Tactitian1Momentum(), new Tactitian2HollowPoint(), new Tactitian3FullMetalJacket(), new Tactitian4RainOfBullets()};
		passive = new TactitianPassive();
		
		classIcon = Sheets.CHARACTER_SPLASH.getSubImage(2, 0);
		redSheet = Sheets.SOLDIER_RED;
		blueSheet = Sheets.SOLDIER_BLUE;
		hand = Sheets.HANDS.getSubImage(0, 2);
	}
}
