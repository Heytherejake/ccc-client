package com.studio878.characters;

import com.studio878.abilities.Ability;
import com.studio878.abilities.Excavator1MetalDetector;
import com.studio878.abilities.Excavator2Exhume;
import com.studio878.abilities.Excavator3DynamiteExcavation;
import com.studio878.abilities.Excavator4StonesThrow;
import com.studio878.abilities.ExcavatorPassive;
import com.studio878.util.Sheets;

public class Excavator extends PlayerCharacter {

	public Excavator() {
		id = 1;
		fullName = "Koen, the Arcane Excavator";
		shortName = "Excavator";
		lore = "\"I've seen some of his 'discoveries'. I'm fairly sure my rift devices aren't ancient treasure.\"<br>\t- Yawln, accusing Koen of theivery";
		
		tags = new String[]{"Miner, Intelligence, Scout"};
		tips = new String[]{"Exhume is always on and has no mana cost. Upgrade it early to earn quick money and have enough stone for Stone's Throw.", "Archeologist's Favor allows Koen to easily escape through caves.", "Stone's Throw's damage increases linearly with the amount of stone in your inventory. Either mine or purchase stone to use this ultimate optimally."};
		
		abilities = new Ability[]{new Excavator1MetalDetector(), new Excavator2Exhume(), new Excavator3DynamiteExcavation(), new Excavator4StonesThrow()};
		passive = new ExcavatorPassive();
		classIcon = Sheets.CHARACTER_SPLASH.getSubImage(0, 0);
		redSheet = Sheets.EXCAVATOR_RED;
		blueSheet = Sheets.EXCAVATOR_BLUE;
		hand = Sheets.HANDS.getSubImage(0, 0);
	}
}
