package com.studio878.characters;

import com.studio878.abilities.Ability;
import com.studio878.abilities.Excavator1MetalDetector;
import com.studio878.abilities.Excavator2Exhume;
import com.studio878.abilities.Excavator3DynamiteExcavation;
import com.studio878.abilities.Excavator4StonesThrow;
import com.studio878.abilities.Researcher1StatusField;
import com.studio878.abilities.Researcher2PowerLeech;
import com.studio878.abilities.Researcher3DimensionVault;
import com.studio878.abilities.Researcher4Stargate;
import com.studio878.abilities.ResearcherPassive;
import com.studio878.util.Sheets;

public class Researcher extends PlayerCharacter {

	public Researcher() {
		id = 2;
		fullName = "Yawln, the Researcher of the Rifts";
		shortName = "Researcher";
		lore = "\"Evidentally, I just started world wars in five dimensions. My bad.\"<br>\t- Yawln, on a Tuesday<br><br>As a student under the brilliant scientist Ian Nytten, Yawln was quickly recognized as exempliary among his peers, due to his natural grasp of physics and mathematics. Considered a genius, Yawln was showered with affection and praise by his fellow scientists; ultimately, this pandering resulted in Yawln relying too heavily on his natural talent, and faltering in his studies.<br><br>Having become careless and arrogant, Nytten grew tiresom of Yawln, and began looking for a reason to release Yawln from his pupilship without drawing heavy public disapproval. During one of his experiments, Yawln failed to notice a faltering rift connection, and the resulting energy spike locked a dimension, preventing all cross-dimensional travel to and from said dimension. Aware of the heavy legal implications this held in Xeliarian society, Yawln fled the unversity, ultimately joining the mercinaries, where he uses his knowledge of rift manipulation to aide his team.";
		tags = new String[]{"Support, Intelligence"};
		tips = new String[]{"Dimension Vault allows Yawln to move in for a kill, or escape quickly.", "Stargate's ultimate still does damage if Yawln is at the surface.", "Scientific Prodigy allows Yawln to buff his team at little expense. Balance personal and research expense to use Yawln optimally."};
		
		abilities = new Ability[]{new Researcher1StatusField(), new Researcher2PowerLeech(), new Researcher3DimensionVault(), new Researcher4Stargate()};
		passive = new ResearcherPassive();
		
		classIcon = Sheets.CHARACTER_SPLASH.getSubImage(1, 0);
		redSheet = Sheets.RESEARCHER_RED;
		blueSheet = Sheets.RESEARCHER_BLUE;
		hand = Sheets.HANDS.getSubImage(0, 1);
	}
}
