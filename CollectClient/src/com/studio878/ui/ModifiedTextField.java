package com.studio878.ui;

import java.lang.reflect.Field;

import org.lwjgl.input.Keyboard;
import org.newdawn.slick.Color;
import org.newdawn.slick.Font;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.gui.GUIContext;
import org.newdawn.slick.gui.TextField;

import com.studio878.input.KeyBindings;

public class ModifiedTextField extends TextField {

	public String watermark;
	public int width, height, curve;
	public int x, y, sx;
	boolean password = false;
	Font font;
	boolean isVisible = true;
	public boolean center = true;
	Color bg, fc;
	int cursorPosition = 0;
	int limit = 40;
	long lastDeleteTime = 0, holdTime = 0;
	public ModifiedTextField(GUIContext container, Font font, int x, int y, int width, int height, int curve, String watermark, boolean password, boolean center) {
		super(container, font, -2000, -2000, width, height);
		this.width = width;
		this.height = height;
		this.watermark = watermark;
		this.password = password;
		this.curve = curve;
		this.font = font;
		this.x = x;
		this.y = y;
		sx = this.x;
		this.center = center;
		bg = Color.white;
		fc = Color.black;
	}

	public void setLimit(int limit) {
		this.limit = limit; 
	}
	public void update(GameContainer c) {
		if(center) {
			this.x = c.getWidth() / 2 - width/2 + sx;
		}

	}

	public void setPosition(int x, int y, int width, int height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}

	public void setVisibility(boolean visible) {
		this.isVisible = visible;
	}

	public void setY(int y) {
		this.y = y;
	}

	public void setWidth(int width) {
		this.width = width;
	}
	public void setForegroundColor(Color c) {
		fc = c;
	}

	public void setBackgroundColor(Color c) {
		bg = c;
	}
	public void draw(GameContainer c, Graphics g) {
		int cursorPos = -1;
		String td = this.getText();
		if(password) {
			td = "";
			for(int i = 0; i < this.getText().length(); i++) {
				td += "�";
			}
		}
		if(c.getInput().isMouseButtonDown(0) && isVisible) {
			int mx = c.getInput().getMouseX();
			int my = c.getInput().getMouseY();
			if(mx >= x && mx <= x + width && my >= y && my <= y + height) {
				this.setFocus(true);
			} else {
				this.setFocus(false);
			}
		}


		if(this.hasFocus()) {
			try {
				Class cd = Class.forName("org.newdawn.slick.gui.TextField");
				Field o = cd.getDeclaredField("cursorPos");
				o.setAccessible(true);
				cursorPos = o.getInt(this);
				cursorPosition = o.getInt(this);
			}
			catch (Throwable e) {
				cursorPos = 0;
			}
			td = new StringBuffer(td).insert(cursorPos, "|").toString();
		}

		if(c.getInput().isKeyPressed(Keyboard.KEY_BACK)) {
			holdTime = System.currentTimeMillis();
		}
		if(this.hasFocus() && c.getInput().isKeyDown(Keyboard.KEY_BACK)) {
			if(cursorPos > 0 && (holdTime != 0 && System.currentTimeMillis() - holdTime >= 500) && System.currentTimeMillis() - lastDeleteTime >= 80) {
				lastDeleteTime = System.currentTimeMillis();
				this.setText(this.getText().substring(0, cursorPos - 1)  + this.getText().substring(cursorPos));
			}
		} else {
			holdTime = 0;
		}

		g.setLineWidth(1.0f);
		g.setColor(bg);
		g.fillRoundRect(x, y, width, height, curve);
		g.setColor(Color.black);
		g.drawRoundRect(x, y, width, height, curve);
		g.setColor(fc);
		g.setFont(font);
		if(td.length() == 0) {
			g.setColor(new Color(160,160,160));
			g.setFont(font);
			g.drawString(watermark, (x) + (width)/2 - font.getWidth(watermark)/2, y + 10);
			g.setColor(Color.black);
		} 

		if(this.getText().length() > limit) {
			this.setText(this.getText().substring(0, limit));
		}
		Rectangle oldClip = g.getClip();
		g.setWorldClip(x, y, width, height);
		int cpos;
		try {
			cpos = font.getWidth(td.substring(0, cursorPosition));
		} catch (Exception e) {
			cursorPosition = 0;
			cpos = font.getWidth(td.substring(0, cursorPosition));

		}
		int tx = 0;
		if (cpos > width) {
			tx = width - cpos - font.getWidth("|");
		}

		g.translate(tx, 0);
		if(this.center) {
			g.drawString(td, (x) + (width)/2 - font.getWidth(td)/2, y + height/2 - font.getHeight(td)/2);
		} else {
			g.drawString(td, x + 2, y + 2);
		}
		g.resetTransform();
		g.clearWorldClip();
		g.setWorldClip(oldClip);
	}

	public void mouseReleased(int button, int x, int y) {
	}
}
