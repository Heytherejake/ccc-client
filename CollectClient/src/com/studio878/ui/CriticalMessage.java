package com.studio878.ui;

import java.util.concurrent.CopyOnWriteArrayList;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;

import com.studio878.inventory.ItemID;

public class CriticalMessage {

	public static CopyOnWriteArrayList<CriticalMessage> messages = new CopyOnWriteArrayList<CriticalMessage>();
	
	public static CopyOnWriteArrayList<CriticalMessage> getMessages() {
		return messages;
	}
	
	public static void addMessage(CriticalMessage m) {
		messages.add(m);
	}
	
	double x, y;
	float opacity = 1.0f;
	String value;
	public CriticalMessage(String val, int x, int y) {
		this.x = x;
		this.y = y;
		this.value = val;
	}
	
	public double getX() {
		return x;
	}
	
	public double getY() {
		return y;
	}
	
	public String getValue() {
		return value;
	}
	
	public void move() {
		this.y -= 0.4;
		opacity -= 0.005f;
	}
	
	public void draw(Graphics g) {
		if(value.indexOf("|") == -1) {
		Fonts.drawWithColor(value, Fonts.ChatFont, g, x, y, 500, opacity);
		} else {
			String[] split = value.split("\\|");
			int fx = 0;
			for(int i = 0; i < split.length; i++) {
				if(i % 2 == 0) {
					g.setFont(Fonts.ChatFont);
					g.setColor(new Color(255, 255, 255, opacity));
					g.drawString(split[i], fx, (float) y + 1);
					Fonts.drawWithColor(split[i], Fonts.ChatFont, g, fx, y, 500, opacity);
					fx += Fonts.ChatFont.getWidth(split[i]);

				} else {
					String[] parts = split[i].split("=");
					if(parts.length > 0 && parts[0].equalsIgnoreCase("item")) {
						ItemID id = ItemID.lookupName(parts[1]);
						if(id != null) {
							Image s = id.getImage().copy();
							s.setAlpha(opacity);
							g.drawImage(s, (float) fx, (float) y - 4);
							fx += id.getImage().getWidth();
						}
					}
				}
			}
		}
		if(opacity <= 0) {
			messages.remove(this);
		}
	}
}
