package com.studio878.ui;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

import com.studio878.objects.Player;
import com.studio878.util.GraphicsUtil;
import com.studio878.util.Images;
import com.studio878.util.Settings;
import com.studio878.util.Sheets;

public class PlayerListItem extends TableListItem{

	Player player;
	int latency;
	boolean isEnemy = false;
	public PlayerListItem(Player player, int latency, boolean enemy) {
		super(player.getName(), 70, Settings.MainWindow.folder.team);
		this.player = player;
		this.latency = latency;
		isEnemy = enemy;
	}

	public void draw(Graphics g, int i) {
		g.setColor(new Color(185, 185, 185));
		g.fillRect(0, i, parent.width, 70);
		g.setColor(new Color(240, 240, 240));
		//	g.drawLine(0, i + 70, parent.width, i+70);
		g.setFont(Fonts.LargeFont);
		g.drawImage(player.getCharacter().getClassIcon(), 20, i + 20);


		GraphicsUtil.drawWithShadow(g, player.getName(), 70, i + 20, new Color(70, 70, 70), Color.white);		
		g.setFont(Fonts.ChatFont);
		GraphicsUtil.drawWithShadow(g, "Kills", 500, i + 10, new Color(70, 70, 70), Color.white);		
		GraphicsUtil.drawWithShadow(g, ""+player.getKills(), 500 - Fonts.ChatFont.getWidth(""+player.getKills())/2 + Fonts.ChatFont.getWidth("Kills")/2, i + 35, new Color(70, 70, 70), Color.white);		

		GraphicsUtil.drawWithShadow(g, "Deaths", 600, i + 10, new Color(70, 70, 70), Color.white);		
		GraphicsUtil.drawWithShadow(g, ""+player.getDeaths(), 600 - Fonts.ChatFont.getWidth(""+player.getDeaths())/2 + Fonts.ChatFont.getWidth("Deaths")/2, i + 35, new Color(70, 70, 70), Color.white);		

		GraphicsUtil.drawWithShadow(g, "Latency", 700, i + 10, new Color(70, 70, 70), Color.white);		
		GraphicsUtil.drawWithShadow(g, ""+latency, 700 - Fonts.ChatFont.getWidth(""+latency)/2 + Fonts.ChatFont.getWidth("Latency")/2, i + 35, new Color(70, 70, 70), Color.white);		

	}

}
