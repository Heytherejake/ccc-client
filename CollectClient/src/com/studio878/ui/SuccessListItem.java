package com.studio878.ui;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

import com.studio878.objects.EntityManager;
import com.studio878.util.GraphicsUtil;
import com.studio878.util.Images;
import com.studio878.util.Settings;

public class SuccessListItem extends TableListItem{

	String name;
	int points;
	public SuccessListItem(String name, int points) {
		super(name, 70, Settings.MainWindow.successView.table);
		this.name = name;
		this.points = points;
	}

	public void draw(Graphics g, int i) {
		g.setColor(new Color(185, 185, 185));
		g.fillRect(0, i, parent.width, 70);
		g.setColor(new Color(240, 240, 240));
		//	g.drawLine(0, i + 70, parent.width, i+70);
		g.setFont(Fonts.LargeFont);
		GraphicsUtil.drawWithShadow(g, name, 70, i + 20, new Color(70, 70, 70), Color.white);		
		g.setFont(Fonts.ChatFont);
		GraphicsUtil.drawWithShadow(g, "Score", 600, i + 10, new Color(70, 70, 70), Color.white);		
		g.setFont(Fonts.ChatFont);
		GraphicsUtil.drawWithShadow(g, ""+points, 600 - Fonts.ChatFont.getWidth(""+points)/2 + Fonts.ChatFont.getWidth("Score")/2, i + 35, new Color(70, 70, 70), Color.white);		
	}

}
