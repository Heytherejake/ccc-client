package com.studio878.ui;


import java.io.File;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.font.effects.ColorEffect;

import com.studio878.util.Console;
import com.studio878.util.Settings;

//Shitty font api is shitty.
@SuppressWarnings("unchecked")

public class Fonts {
	public static UnicodeFont TitleFont;
	public static UnicodeFont DefaultFont;
	public static UnicodeFont ChatFont;
	public static UnicodeFont LargeFont;
	public static UnicodeFont SmallFont;
	public static UnicodeFont ChapterFont;
	public static UnicodeFont LargeItalicFont;
	public static UnicodeFont BoldFont;
	public static UnicodeFont TypewriterFont;
	public static UnicodeFont SmallTypewriterFont;
	public static UnicodeFont LargeTypewriterFont;
	



	public static void loadFonts() {
		Console.write("Generating fonts...");
		try {
		Console.write(ChatColors.Gray + "On Mac OS X and Linux this can take awhile...");
		Console.write("Generating lucon.ttf (Console)...");
		DefaultFont = registerFont(Settings.SystemFolder.getAbsolutePath() + File.separator + "assets" + File.separator + "fonts" + File.separator + "lucon.ttf", 12, false, false);
		
		Console.write("Generating Telex.ttf (Main - 16)...");
		TitleFont = registerFont(Settings.SystemFolder.getAbsolutePath() + File.separator + "assets" + File.separator + "fonts" + File.separator + "Telex.ttf", 16, false, false);
		
		Console.write("Generating Telex.ttf (Main - 14)...");
		ChatFont = registerFont(Settings.SystemFolder.getAbsolutePath() + File.separator + "assets" + File.separator + "fonts" + File.separator + "Telex.ttf", 18,false,false);
		
		Console.write("Generating Telex.ttf (Main - 18)...");
		LargeFont = registerFont(Settings.SystemFolder.getAbsolutePath() + File.separator + "assets" + File.separator + "fonts" + File.separator + "Telex.ttf", 24,false,false);
		
		Console.write("Generating Telex.ttf (Main - 12)...");
		SmallFont = registerFont(Settings.SystemFolder.getAbsolutePath() + File.separator + "assets" + File.separator + "fonts" + File.separator + "Telex.ttf", 12, false, false);
		
		Console.write("Generating Telex.ttf (Italic - 18)...");
		LargeItalicFont = registerFont(Settings.SystemFolder.getAbsolutePath() + File.separator + "assets" + File.separator + "fonts" + File.separator + "Telex.ttf", 18, false, true);
		
		Console.write("Generating Telex.ttf (Bold - 22)...");
		BoldFont = registerFont(Settings.SystemFolder.getAbsolutePath() + File.separator + "assets" + File.separator + "fonts" + File.separator + "Telex.ttf", 22, true, false);
		
		Console.write("Generating Telex.ttf (Main - 28)...");
		ChapterFont = registerFont(Settings.SystemFolder.getAbsolutePath() + File.separator + "assets" + File.separator + "fonts" + File.separator + "Telex.ttf", 28, false, false);
		
		Console.write("Generating KIN668.TTF (Typewriter - 18)...");
		SmallTypewriterFont = registerFont(Settings.SystemFolder.getAbsolutePath() + File.separator + "assets" + File.separator + "fonts" + File.separator + "KIN668.TTF", 12, false, false);
		
		Console.write("Generating KIN668.TTF (Typewriter - 24)...");
		TypewriterFont = registerFont(Settings.SystemFolder.getAbsolutePath() + File.separator + "assets" + File.separator + "fonts" + File.separator + "KIN668.TTF", 16, false, false);
		
		Console.write("Generating KIN668.TTF (Typewriter - 32)...");
		LargeTypewriterFont = registerFont(Settings.SystemFolder.getAbsolutePath() + File.separator + "assets" + File.separator + "fonts" + File.separator + "KIN668.TTF", 32, false, false);
		
		Console.write("Fonts generated with no errors."); 
		} catch (Exception e) {
			Console.write("Error generating fonts, not good.");
		}
	}

	
	public static UnicodeFont registerFont(String font, int size, boolean bold, boolean italics) {
			UnicodeFont u = null;
			try {
				u = new UnicodeFont(font, size, bold, italics);
			} catch (SlickException e) {
				Console.error("Failed to register font: " + font);
			}
			u.addAsciiGlyphs();
			u.addGlyphs("•δε");
			//u.addGlyphs(400, 600);
			u.getEffects().add(new ColorEffect(java.awt.Color.WHITE));
			try {
				u.loadGlyphs();
			} catch (SlickException e) {
				Console.error("Failed to load glyphs for font: " + font);

			}
		return u;
	}
	public static void drawWithColor(String s, UnicodeFont font, Graphics g, double x, double y, int width) { 
		double xPos = x;
		double yPos = y;
		String[] split = s.split("•");
		if(split.length != 1) {
			for(int i = 0; i < split.length; i++) {

				if(split[i].length() > 0) {
					String st = split[i].substring(1);

					Color c;
					char t = split[i].substring(0, 1).toCharArray()[0];
					switch(t) {
					case '0':
						c = Color.black;
						break;
					case '1':
						c = Color.blue;
						break;
					case '2':
						c = Color.cyan;
						break;
					case '3':
						c = new Color(153, 255, 255);
						break;
					case '4':
						c = Color.magenta;
						break;
					case '5':
						c = Color.red;						
						break;
					case '6':
						c = Color.pink;
						break;
					case '7':
						c = new Color(0, 214, 71);
						break;
					case '8':
						c = Color.green;
						break;
					case '9':
						c = Color.yellow;
						break;
					case 'a':
						c = new Color(200, 200, 200);
						break;
					case 'b':
						c = Color.white;
						break;
					case 'c':
						c = new Color(80, 80, 80);
						break;
					default:
						c = Color.black;
					}
					
					g.setColor(c);
					g.setFont(font);
					g.drawString(st, (float) xPos, (float) yPos);
					xPos += font.getWidth(st);

				}
					
			}

		} else {
			font.drawString((float) x, (float) y, s, Color.black);
		}
	}
	public static void drawWithColor(String s, UnicodeFont font, Graphics g, double x, double y, int width, float opacity) { 
		double xPos = x;
		double yPos = y;
		String[] split = s.split("•");
		if(split.length != 1) {
			for(int i = 0; i < split.length; i++) {

				if(split[i].length() > 0) {
					String st = split[i].substring(1);

					Color c;
					char t = split[i].substring(0, 1).toCharArray()[0];
					switch(t) {
					case '0':
						c = new Color(0, 0, 0, opacity);
						break;
					case '1':
						c = new Color(0, 0, 1.0f, opacity);
						break;
					case '2':
						c = new Color(0, 1.0f, 1.0f, opacity);;
						break;
					case '3':
						c = new Color(153, 255, 255, opacity);
						break;
					case '4':
						c =  new Color(255, 0, 255, opacity);
						break;
					case '5':
						c =  new Color(255, 0, 0, opacity);						
						break;
					case '6':
						c = new Color(255, 175, 175, opacity);
						break;
					case '7':
						c = new Color(0, 214, 71, opacity);
						break;
					case '8':
						c = new Color(0, 255, 0, opacity);
						break;
					case '9':
						c = new Color(255, 255, 0, opacity);
						break;
					case 'a':
						c = new Color(200, 200, 200, opacity);
						break;
					case 'b':
						c = new Color(255, 255, 255, opacity);
						break;
					case 'c':
						c = new Color(80, 80, 80, opacity);
						break;
					default:
						c = new Color(0, 0, 0, opacity);
					}
					
					g.setColor(c);
					g.setFont(font);
					g.drawString(st, (float) xPos, (float) yPos);
					xPos += font.getWidth(st);

				}
					
			}

		} else {
			font.drawString((float) x, (float) y, s, new Color(0, 0, 0, opacity));
		}
	}
}
