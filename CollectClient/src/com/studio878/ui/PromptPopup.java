package com.studio878.ui;

import org.newdawn.slick.Font;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class PromptPopup extends Popup {
	
	public static final int ShopPurchase = 1;
	public static final int DeleteCharacter = 2;
	public static final int DeleteItem = 3;
	public static final int UseItem = 4;
	
	Button option1, option2;
	int mode = 0;
	public PromptPopup(String s, String option1, String option2, int x, int y, int width, int height, Font f, boolean centered) {
		super(s, x, y, width, height, f, centered);
		this.option1 = new Button(x + width/2 - 100, y + height - 50, option1, false);
		this.option2 = new Button(x + width/2 + 100, y + height - 50, option2, false);
		this.compress = false;
	}
	
	public void draw(Graphics g, GameContainer c) {
		super.draw(g, c);
		option1.setX(x + width/2 - 250);
		option1.setY(y + height - 70);
		option2.setX(x + width/2 + 50);
		option2.setY(y + height - 70);
		option1.draw(g, c);
		option1.check(c);
		option2.draw(g, c);
		option2.check(c);

	}
	
	public void setPanelMode(int mode) {
		this.mode = mode;
	}
	
	public int getPanelMode() {
		return mode;
	}
	
	public void setOption1Value(String value) {
		option1.setValue(value);
	}
	
	public void setOption2Value(String value) {
		option2.setValue(value);
	}
	
	public boolean isOption1Selected() {
		return option1.isSelected();
	}
	
	public boolean isOption2Selected() {
		return option2.isSelected();
	}


}
