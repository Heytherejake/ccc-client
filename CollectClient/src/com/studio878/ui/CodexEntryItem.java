package com.studio878.ui;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

import com.studio878.util.GraphicsUtil;

public class CodexEntryItem extends TableListItem {

	String title, value;
	public CodexEntryItem(String title, String value, TableList parent) {
		super(title, 30, parent);
		this.title = title;
		this.value = value;
	}

	public String getTitle() {
		return title;
	}
	
	public String getValue() {
		return value;
	}
	
	public void draw(Graphics g, int i) {
		if(!isSelected()) {
			g.setColor(new Color(235, 235, 235));
		} else {
			g.setColor(new Color(200, 200, 200));
		}
		g.setFont(Fonts.ChatFont);
		g.fillRect(0, i, parent.width, i + 30);
		g.setColor(Color.black);
		
		String sf = title;
		if(Fonts.ChatFont.getWidth(title) > parent.width) {
			sf = "";
			for(int j = 0; j < title.length(); j++) {
				if(Fonts.ChatFont.getWidth(title.substring(0, j)) > parent.width) {
					try {
					sf = title.substring(0, j-3) + "...";
					} catch (Exception e) {
						
					}
					break;
				}
			}
		}
		GraphicsUtil.drawWithShadow(g, sf, 0, i+4, Color.black, Color.white);
		g.setColor(Color.black);
		g.drawLine(0, i + 29, parent.width, i + 29);

	}
}
