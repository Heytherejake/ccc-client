package com.studio878.ui;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

import com.studio878.util.GraphicsUtil;

public class CondensedServerListItem extends ServerListItem {

	
	
	public CondensedServerListItem(String ip, int port, String name, long time, int players, int maxPlayers, TableList parent) {
		super(ip, port, name, time, players, maxPlayers, parent);
		this.height = 25;
	}

	public void draw(Graphics g, int i) {
		g.setFont(Fonts.ChatFont);
		if(!this.isSelected()) {
		g.setColor(new Color(200, 200, 200));
		} else {
			g.setColor(new Color(240,240,240));
		}
		g.fillRect(0, i, parent.width, height);
		g.setColor(Color.black);
		g.drawString(this.getMessage(), 10, i);
		String pst = ip + ":" + port;
		GraphicsUtil.drawWithShadow(g, pst, parent.width - Fonts.ChatFont.getWidth(pst) - 350, i, new Color(70, 70, 70), Color.white);
		String latency = "Latency: " + time + "ms";
		if(time == 99999) {
			latency = "Unresponsive";
		}
		GraphicsUtil.drawWithShadow(g, latency, parent.width - 150, i, new Color(70, 70, 70), Color.white);

		String ps = "Players: " + players + "/" + maxPlayers;
		GraphicsUtil.drawWithShadow(g, ps, parent.width - 300, i, new Color(70, 70, 70), Color.white);
 	}

}
