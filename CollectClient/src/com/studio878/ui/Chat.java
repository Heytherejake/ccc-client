package com.studio878.ui;

import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.CopyOnWriteArrayList;

import org.newdawn.slick.UnicodeFont;

public class Chat {
	public static boolean ShowChat = false;
	static CopyOnWriteArrayList<MessageObject> messages = new CopyOnWriteArrayList<MessageObject>();
	
	public static void addMessage(String m) {
		ArrayList<String> parts = splitBetweenLines(m, Fonts.ChatFont, 500);
		for(int i = 0; i < parts.size(); i++) {
		messages.add(new MessageObject(parts.get(i), new Date()));
		}
	}
	
	public static void clearMessages() {
		messages.clear();
	}
	
	public static CopyOnWriteArrayList<MessageObject> getMessages() {
		return messages;
	}
	
	public static ArrayList<MessageObject> getMessagesWithinTime(int s) {
		ArrayList<MessageObject> results = new ArrayList<MessageObject>();
		Date now = new Date();
		for(int i = messages.size() - 1; i >= 0; i--) {
			if((now.getTime() - messages.get(i).getTime().getTime()) / 1000 <= s && results.size() < 3) {
				results.add(messages.get(i));
			} else {
				return results;
			}
		}
		return results;
		
	}

	public static ArrayList<String> splitBetweenLines(String s, UnicodeFont f, int w) {
		ArrayList<String> results = new ArrayList<String>();
		if(f.getWidth(s) < w) {
			results.add(s);
			return results;
		} else {
			String left = s;
			for(int i = 0; i < left.length(); i++) {
				if(f.getWidth(left.substring(0, i)) > w) {
					int lastSpace = left.substring(0, i).lastIndexOf(' ');
					int msp = left.substring(0, i).lastIndexOf('�');
					char nbp = left.charAt(msp + 1);
					if(lastSpace != -1) {
						results.add(left.substring(0, lastSpace).trim());
						left = "�" + nbp + (left.substring(lastSpace).trim());

					} else {
						results.add(left.substring(0, i).trim());
						left = "�" + nbp + (left.substring(i).trim());

					}


				}
			}
			results.add(left.trim());
		}
		return results;

	}



}
