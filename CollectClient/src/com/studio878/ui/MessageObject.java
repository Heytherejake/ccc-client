package com.studio878.ui;

import java.util.Date;

public class MessageObject {

	Date posted;
	String message;
	
	public MessageObject (String message, Date time) {
		this.message = message;
		posted = time;
	}
	
	public Date getTime() {
		return posted;
	}
	
	public String getMessage() {
		return message;
	}
}
