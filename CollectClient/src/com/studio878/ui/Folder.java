package com.studio878.ui;

import java.util.concurrent.CopyOnWriteArrayList;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.fills.GradientFill;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.geom.Transform;

import com.studio878.crafting.BonusItem.BonusItemType;
import com.studio878.inventory.Inventory;
import com.studio878.inventory.Item;
import com.studio878.util.Damage;
import com.studio878.util.FilterableImage;
import com.studio878.util.GraphicsUtil;
import com.studio878.util.Images;
import com.studio878.util.Settings;
import com.studio878.util.Sheets;

public class Folder {

	int cx = 0;
	int cy = 0;

	CopyOnWriteArrayList<FolderTab> tabs = new CopyOnWriteArrayList<FolderTab>();
	int selected = 0;
	Image player_info, top_secret, bonus_text;

	public TableList bonuses = new TableList(-180, 400, 545, 200, Fonts.TypewriterFont, true, 5);
	public TableList team = new TableList(-500, 100, 800, 450, Fonts.TypewriterFont, true, 2);
	public TableList enemy = new TableList(-500, 100, 800, 450, Fonts.TypewriterFont, true, -2);
	public Folder() {
		tabs.add(new FolderTab("Enemy", 3, this));

		tabs.add(new FolderTab("Team", 2, this));
		tabs.add(new FolderTab("Stuff", 1, this));
		tabs.add(new FolderTab("Info", 0, this));
		try {
			player_info = new FilterableImage(500, 500, FilterableImage.FILTER_LINEAR);
			top_secret = new FilterableImage(200, 100, FilterableImage.FILTER_LINEAR);
			bonus_text = new FilterableImage(500, 200, FilterableImage.FILTER_LINEAR);

		} catch (SlickException e) {
			e.printStackTrace();
		}
		refreshStats();

	}

	public void setSelected(int i) {
		selected = i;
	}

	public int getSelected() {
		return selected;
	}

	public void draw(GameContainer c, Graphics g) {
		refreshStats();

		cx = (c.getWidth() - Images.FolderBackground.getWidth())/2;
		cy =  (c.getHeight() - Images.FolderBackground.getHeight())/2;
		for(FolderTab t : tabs) {
			if(!(Settings.StoryMode && t.title.equals("Enemy"))) {
				t.draw(c, g);
			}
		}
		if(selected == 0) {
			Image ics = Images.FolderClipBack.copy();
			ics.setRotation(-80);
			g.drawImage(ics, c.getWidth()/2 - 429, c.getHeight()/2 - 216);

		} else if (selected == 2) {
			Image ics = Images.FolderClipBack.copy();
			ics.setRotation(80);
			g.drawImage(ics, c.getWidth()/2 + 387, c.getHeight()/2 + 87);
		} else if (selected == 3) {
			Image ics = Images.FolderClipBack.copy();
			ics.setRotation(-80);
			g.drawImage(ics, c.getWidth()/2 - 419, c.getHeight()/2 + 13);
		}
		g.drawImage(Images.FolderBackground, cx, cy);
		switch(selected) {
		case 0:
			Graphics gs;
			try {
				gs = player_info.getGraphics();
				gs.clear();
				GraphicsUtil.drawShadowBox(gs, 0, 0, 400, 150, 0);
				gs.setFont(Fonts.LargeTypewriterFont);
				gs.setColor(Color.black);
				if(Settings.Username != null) {
					gs.drawString(Settings.Username, 10, 10);
				}
				gs.setFont(Fonts.TypewriterFont);
				if(Settings.ActivePlayer.getCharacter() != null) {
					gs.drawString("Licensed " + Settings.ActivePlayer.getCharacter().getFullName(), 10, 50);
				}
				String ks = Settings.ActivePlayer.getKills() + " kills";
				if(Settings.ActivePlayer.getKills() == 1) {
					ks = ks.substring(0, ks.length() - 1);
				}
				String ds = Settings.ActivePlayer.getDeaths() + " deaths";
				if(Settings.ActivePlayer.getDeaths()  == 1) {
					ds = ds.substring(0, ds.length() - 1);
				}
				gs.drawString(ks + ", " + ds + ", and 0 points", 10, 110);
				player_info.setRotation(-10);
				Graphics gcv = top_secret.getGraphics();
				gcv.clear();
				gcv.setColor(new Color(90, 0, 0));
				gcv.setFont(Fonts.LargeTypewriterFont);
				gcv.drawString("TOP SECRET", 0, 0);
				top_secret.setRotation(10);
			} catch (SlickException e) {
				e.printStackTrace();
			}

			g.drawImage(top_secret, c.getWidth()/2 + 200, c.getHeight()/2 - 230);	
			g.drawImage(player_info, c.getWidth()/2 - 420, c.getHeight()/2 - 250);
			Image fc = Images.FolderClipFront.copy();
			fc.setRotation(-80);
			g.drawImage(fc, c.getWidth()/2 - 440, c.getHeight()/2 - 230);
			bonus_text.setRotation(5);
			try {
				Graphics gv = bonus_text.getGraphics();
				gv.clear();
				gv.setColor(Color.black);
				gv.setFont(Fonts.LargeTypewriterFont);
				gv.drawString("Stats", 0, 0);
			} catch (SlickException e) {
				e.printStackTrace();
			}
			GraphicsUtil.drawShadowBox(g, c.getWidth()/2 - 200, c.getHeight()/2 - 60, 600, 300, 5);
			//g.drawImage(bonus_box, c.getWidth()/2 - 230, c.getHeight()/2 - 30);
			//bonus_box.setRotation(5);
			bonuses.update(c);
			bonuses.draw(g);
			bonus_text.draw(c.getWidth()/2 - 170, c.getHeight()/2 - 30);
			Image ts1 = Images.FolderTape.copy();
			ts1.setRotation(90);
			g.drawImage(ts1, c.getWidth()/2 - 230, c.getHeight()/2 - 100);
			ts1.setRotation(100);
			g.drawImage(ts1, c.getWidth()/2 + 340, c.getHeight()/2 + 220);
			bonuses.setY(c.getHeight()/2 + 30);

			break;
		case 1:
			GraphicsUtil.drawShadowBox(g, c.getWidth()/2 - 425, c.getHeight()/2 - 215 , 280, 400, 0);

			InventoryUI.setPositon(c.getWidth()/2, c.getHeight()/2 - 40);
			InventoryUI.draw(g, c);
			g.setColor(Color.black);
			g.setFont(Fonts.LargeTypewriterFont);
			g.drawString("Inventory", c.getWidth()/2 - Fonts.LargeTypewriterFont.getWidth("Inventory")/2 - 425 + 140, c.getHeight()/2 - 190);
			Image t1 = Images.FolderTape.copy();
			t1.setRotation(100);
			g.drawImage(t1, c.getWidth() / 2 - 460, c.getHeight()/2 - 255);
			Image t2 = Images.FolderTape.copy();
			t2.setRotation(60);
			g.drawImage(t2, c.getWidth() / 2 + 370, c.getHeight()/2 + 130);
			break;
		case 2:
			team.setX(c.getWidth()/2 - 400);
			team.setY(c.getHeight()/2 - 180);
			team.drawBackground = false;
			GraphicsUtil.drawShadowBox(g, c.getWidth()/2 - 413, c.getHeight()/2 - 255, 830, 528, 2);
			try {
				Graphics gcv = top_secret.getGraphics();
				gcv.clear();
				gcv.setColor(Color.black);
				gcv.setFont(Fonts.LargeTypewriterFont);
				gcv.drawString("Team", 0, 0);
			} catch (SlickException e) {
				e.printStackTrace();
			}
			top_secret.setRotation(2);
			g.drawImage(top_secret, c.getWidth()/2 - 30, c.getHeight()/2 - 230);

			team.draw(g);
			Image fdc = Images.FolderClipFront.copy();
			fdc.setRotation(80);
			g.drawImage(fdc, c.getWidth()/2 + 375, c.getHeight()/2 + 100);

			break;
		case 3:
			enemy.setX(c.getWidth()/2 - 397);
			enemy.setY(c.getHeight()/2 - 185);
			enemy.drawBackground = false;
			GraphicsUtil.drawShadowBox(g, c.getWidth()/2 - 413, c.getHeight()/2 - 245, 830, 528, -2);
			try {
				Graphics gcv = top_secret.getGraphics();
				gcv.clear();
				gcv.setColor(Color.black);
				gcv.setFont(Fonts.LargeTypewriterFont);
				gcv.drawString("Opponents", 0, 0);
			} catch (SlickException e) {
				e.printStackTrace();
			}
			top_secret.setRotation(-2);
			g.drawImage(top_secret, c.getWidth()/2 - Fonts.LargeTypewriterFont.getWidth("Opponents")/2, c.getHeight()/2 - 230);
			enemy.draw(g);
			Image fec = Images.FolderClipFront.copy();
			fec.setRotation(-80);
			g.drawImage(fec, c.getWidth()/2 - 430, c.getHeight()/2);

		}		

	}

	public void refreshStats() {
		bonuses.clearItems();
		for(int i = 0; i < BonusItemType.values().length; i++) {
			BonusItemType t = BonusItemType.values()[i];
			try {
				bonuses.addItem(new StatListItem(Sheets.STAT_ICONS.getSubImage(i, 0), t, bonuses));
			} catch (Exception e) {

			}
		}
	}


}
