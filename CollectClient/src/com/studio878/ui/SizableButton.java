package com.studio878.ui;

import org.lwjgl.input.Mouse;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;

import com.studio878.util.GraphicsUtil;
import com.studio878.util.Images;
import com.studio878.util.Sheets;
import com.studio878.util.SoundEffects;

public class SizableButton {

	Image standard = Sheets.BUTTONS.getSubImage(0, 0);
	Image rollover = Sheets.BUTTONS.getSubImage(0, 1);
	Image click = Sheets.BUTTONS.getSubImage(0, 2);
	Image toDisplay;

	protected int x, y;
	int sx;
	int width, height;
	int offset;
	int colorProfile = 0;
	String name;

	boolean disabled = false;

	public boolean isSelected = false;

	boolean centered;

	public SizableButton(int x, int y, int width, int height, String name, boolean centered) {
		toDisplay = standard;
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.name = name;
		this.centered = centered;
		sx = x;
	}

	public boolean isSelected() {
		if(isSelected) {
			isSelected = false;
			return true;
		}
		return false;
	}

	public void setX(int x) {
		this.x = x;
		this.sx = x;
	}

	public void setValue(String v) {
		this.name = v;
	}

	public void setY(int y) {
		this.y = y;
	}
	
	public void setWidth(int width) {
		this.width = width;
	}
	
	public void setHeight(int height) {
		this.height = height;
	}

	public String getValue() {
		return name;
	}
	
	public void check(GameContainer c) {
		if(this.isDisabled()) {
			toDisplay = standard;
			return;
		}
		if(toDisplay == click && !c.getInput().isMouseButtonDown(0)) {
			isSelected = true;
		}
		double mx = c.getInput().getMouseX();
		double my = c.getInput().getMouseY();
		if(mx >= x && my >= y && mx <= x + width && my <= y + height) {
			if(c.getInput().isMouseButtonDown(0)){
				toDisplay = click;
			} else {
				toDisplay = rollover;
			}
		} else {
			toDisplay = standard;
		}
		if(!this.isDisabled()) {
			if(mx >= x && mx <= x + width && my >= y && my <= y + height) {
				if(c.getInput().isMouseButtonDown(0)){
					if(offset != 100) {
						SoundEffects.playSound(SoundEffects.Click);
					}
					offset = 100;

				} else {
					offset = 50;

				}
			} else {
				offset = 0;
			}
		} else {
			offset = 0;
		}

	}

	public boolean isDisabled() {
		return disabled;
	}

	public void isDisabled(boolean b) {
		disabled = b;
	}
	
	public void setColorProfile(int i) {
		colorProfile = i;
	}

	public void draw(GameContainer c, Graphics g) {
		int mx = c.getInput().getMouseX();
		int my = c.getInput().getMouseY();

		Image is = Images.button;
		Color cs = is.getColor(3 + 13*colorProfile, 20 + offset);

		Image i1 = is.getSubImage(0 + 13*colorProfile, offset, 6, 6);
		Image i2 = is.getSubImage(7 + 13*colorProfile, offset, 6, 6);
		Image i3 = is.getSubImage(0 + 13*colorProfile, 44 + offset, 6, 6);
		Image i4 = is.getSubImage(7 + 13*colorProfile, 44+ offset, 6, 6);
		Image isd = is.getSubImage(6 + 13*colorProfile, 0+ offset, 1, 6);
		Image ise = is.getSubImage(6 + 13*colorProfile, 44+ offset, 1, 6);
		Image isf = is.getSubImage(0 + 13*colorProfile, 12+ offset, 6, 1);
		Image isg = is.getSubImage(7 + 13*colorProfile, 12+ offset, 6, 1);
		float alpha = 1.0f;
		if(this.isDisabled()) {
			isd.setAlpha(0.5f);
			ise.setAlpha(0.5f);
			isf.setAlpha(0.5f);
			isg.setAlpha(0.5f);
			i1.setAlpha(0.5f);
			i2.setAlpha(0.5f);
			i3.setAlpha(0.5f);
			i4.setAlpha(0.5f);
			alpha = 0.5f;
		}

		g.setColor(new Color(cs.r, cs.g, cs.b, alpha));
		g.fillRect(x + 6, y + 6, width - 12, height-12);
		g.drawImage(i1, x, y);
		g.drawImage(i2, x + width - 6, y);
		g.drawImage(i3, x, y + height - 6);
		g.drawImage(i4, x + width - 6, y + height - 6);
		for(int i = 0; i < width - 12; i++) {
			g.drawImage(isd, x + i + 6, y);
			g.drawImage(ise, x + i + 6, y + height - 6);
		}

		for(int i = 0; i < height - 12; i++) {
			g.drawImage(isf, x, y + i + 6);
			g.drawImage(isg, x + width - 6, y + i + 6);
		}
		g.setColor(new Color(0, 0, 0, alpha));
		g.setFont(Fonts.ChatFont);
		g.drawString(name, x + width/2 -  Fonts.ChatFont.getWidth(name)/2, y + height/2 - Fonts.ChatFont.getHeight(name)/2 - 2);

	}
}
