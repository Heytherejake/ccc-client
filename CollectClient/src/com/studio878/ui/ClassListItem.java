package com.studio878.ui;

import org.newdawn.slick.Animation;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SpriteSheet;

import com.studio878.characters.PlayerCharacter;
import com.studio878.util.Settings;
import com.studio878.util.TeamManager.Team;

public class ClassListItem extends TableListItem {

	
	PlayerCharacter character;
	
	public ClassListItem(PlayerCharacter character, TableList parent) {
		super(character.getShortName(), 160, parent);

	}

	public PlayerCharacter getCharacter() {
		return character;
	}
	

	
	public void draw(Graphics g, int i) {
		boolean isBlue = false;
		try {
			isBlue = (Settings.ActivePlayer.getTeam() == null || Settings.ActivePlayer.getTeam() == Team.Blue);
		} catch (Exception e) {
		}
		Image is;
		if(isBlue) {
			is = character.getBlueSpriteSheet().getSubImage(0, 0).getScaledCopy(2.0f);
		} else {
			is = character.getRedSpriteSheet().getSubImage(0, 0).getScaledCopy(2.0f);
		}

		is.draw(50, i);
		Image isv = character.getHandImage().getScaledCopy(2.0f);
		isv.rotate(0);
		isv.draw(74, i + 60);
	}
}
