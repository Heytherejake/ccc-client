package com.studio878.ui;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;

import com.studio878.crafting.BonusItem;
import com.studio878.crafting.BonusItem.BonusItemType;

public class StatListItem extends TableListItem {
	
	Image image;
	BonusItemType type;
	TableList parent;
	double amount;
	
	public StatListItem(Image i, BonusItemType type, TableList parent) {
		super(type.name(), 70, parent);
		image = i;
		this.type = type;
		amount = BonusItem.calculateBonus(type);
	}
	
	public void draw(Graphics g, int i) {
		g.drawImage(image, 3 , 3 + i);
		g.setFont(Fonts.LargeFont);
		g.setColor(Color.black);
		g.drawString(type.getName(), 80, i + 15);
		g.setColor(new Color(90, 90, 90));
		if(amount < 1) {
			g.setColor(new Color(200, 0, 0));
		} else if (amount > 1) {
			g.setColor(new Color(0, 140, 0));
		}
		g.drawString(type.format(amount), 350, i + 15);
		
	}
}
