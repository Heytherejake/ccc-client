package com.studio878.ui;

import java.util.ArrayList;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.UnicodeFont;

import com.studio878.crafting.CraftingRecipe;
import com.studio878.displays.MainGame;
import com.studio878.inventory.Inventory;
import com.studio878.inventory.Item;
import com.studio878.inventory.ItemID;
import com.studio878.util.Images;

public class ShopListItem extends TableListItem{
	CraftingRecipe recipe1;
	CraftingRecipe recipe2;
	CraftingRecipe recipe3;
	static int Height = 55;

	public ShopListItem(CraftingRecipe c1, CraftingRecipe c2, CraftingRecipe c3, TableList parent) {
		super("", 55, parent);
		this.recipe1 = c1;
		this.recipe2 = c2;
		this.recipe3 = c3;
	}

	public CraftingRecipe getRecipe1() {
		return recipe1;
	}

	public CraftingRecipe getRecipe2() {
		return recipe2;
	}

	public CraftingRecipe getRecipe3() {
		return recipe3;
	}

	public void draw(Graphics g, int i) {
		CraftingRecipe acr = recipe1;
		int mx = Mouse.getX();
		int my = Display.getDisplayMode().getHeight() - Mouse.getY();
		for(int j = 0; j < 3; j++) {
			if(j == 1) {
				acr = recipe2;
			} else if(j == 2) {
				acr = recipe3;
			}
			if(acr != null) {
				boolean hasMaterials = true;
				for(Item iv: acr.getRequirements()) {
					if(!Inventory.contains(iv.getType(), iv.getAmount())) {
						hasMaterials = false;
					}
				}
				if(Inventory.getAmount(ItemID.lookupName("Coins")) < acr.getBuyCost()) {
					hasMaterials = false;
				}

				int xoffset = 1, yoffset = 1;

				if(MainGame.activeRecipe != acr) {
					if(Inventory.contains(acr.getResult().getType())) {
						xoffset = 0;
						yoffset = 0;
					} else {
						xoffset = hasMaterials ? 1 : 0;
						yoffset = hasMaterials ? 0 : 1;
					}
				}

				g.drawImage(Images.GridItem.getSubImage(xoffset*50, yoffset*50, 50, 50), 55*j, i);

				g.drawImage(acr.getResult().getType().getImage(), 9 + 55*j, i + 9);

				if(Mouse.isButtonDown(0) && mx >= parent.x + j*55 && mx <= parent.x + j*55 + 50 && my >= i && my <= i + 50) {
					MainGame.activeRecipe = acr;
				}

			}



		}



		/*int wdt = parent.width - 10;

		Rectangle r = new Rectangle(0, i, wdt, Height + 1);
		GradientFill f = new GradientFill(0, 0, new Color(111, 182, 255), wdt, Height, new Color(77,152,253));
		g.fill(r, f);
		g.setColor(Color.white);
		g.drawRect(5, i+5, wdt - 11, Height - 12);
		g.drawImage(recipe1.getResult().getType().getImage().getScaledCopy(4.0f), 20, i + 10);
		if(recipe1.getResult().getAmount() != 1) {
			g.setFont(Fonts.MediumBlueprintFont);
			g.drawString("x" + recipe1.getResult().getAmount(), 125, i + 115);
		}
		g.setFont(Fonts.BlueprintFont);
		GraphicsUtil.drawWithShadow(g, recipe1.getName(), 160, i + 10, Color.white, new Color(50, 50, 50));
		g.setFont(Fonts.SmallBlueprintFont);
		int sy = i + 50;
		ArrayList<String> parts = splitBetweenLines(recipe1.getDescription(), Fonts.SmallBlueprintFont, 330);
		for(String s : parts) {
			GraphicsUtil.drawWithShadow(g, s, 160, sy, Color.white, new Color(50, 50, 50));
			sy += 15;
		}

		int sx = 150;
		sy += 5;
		for(Item id : recipe1.getRequirements()) {
			g.drawImage(id.getType().getAntiAliasedImage(), sx, sy);
			GraphicsUtil.drawWithShadow(g, "x" + id.getAmount(), sx + 33, sy + 5, Color.white, new Color(50, 50, 50));
			sx += 38 + Fonts.SmallBlueprintFont.getWidth("x" + id.getAmount());
			/*int amt = Inventory.getAmount(id.getType()) - id.getAmount();
			Color c = new Color(180, 180, 180);
			String st = "("+amt + ")";
			if(amt > 0) {
				c = new Color(0, 200, 0);
				st = "(+" + amt + ")";
			} else if (amt < 0) {
				c = new Color(180, 0, 0);
			}
			GraphicsUtil.drawWithShadow(g, st, sx + 33, i + 95, c, new Color(50, 50, 50));
			sx += 38 + Fonts.SmallBlueprintFont.getWidth(st);*/

		/*}
		sy = i + 10;
		g.setFont(Fonts.MediumBlueprintFont);
		if(recipe1.getUpsides() != null) {
			for(String s : recipe1.getUpsides()) {
				ArrayList<String> pts = splitBetweenLines(s, Fonts.MediumBlueprintFont, 220);
				for(String p : pts) {
					GraphicsUtil.drawWithShadow(g,p, 500, sy, new Color(0, 255, 0), Color.black);
					sy += 25;
				}
			}
		}
		sy += 10;
		if(recipe1.getDownsides() != null) {
			for(String s: recipe1.getDownsides()) {
				ArrayList<String> pts = splitBetweenLines(s, Fonts.MediumBlueprintFont, 220);
				for(String p : pts) {
					GraphicsUtil.drawWithShadow(g,p, 500, sy, new Color(255, 125, 0), Color.black);
					sy += 25;
				}
			}
		}

		boolean hasMaterials = true;
		for(Item iv: recipe1.getRequirements()) {
			if(!Inventory.contains(iv.getType(), iv.getAmount())) {
				hasMaterials = false;
			}
		}
		if ((recipe1.getResult().getType().isWeapon() && Inventory.contains(recipe1.getResult().getType()))) {
			g.setColor(new Color(255, 0, 0, 0.5f));
			g.fillRect(0, i, parent.width - 11, Height);
			g.setFont(Fonts.BoldFont);
			String msg = "Weapon already in inventory";

			GraphicsUtil.drawWithShadow(g, msg, parent.width/2 - Fonts.BlueprintFont.getWidth(msg)/2, i + 115, Color.black, new Color(255, 255, 255, 0.5f));
		} else if(!hasMaterials) {
			g.setColor(new Color(255, 0, 0, 0.5f));
			g.fillRect(0, i, parent.width - 11, Height);
			g.setFont(Fonts.BoldFont);
			String msg = "Insufficient resources";
			GraphicsUtil.drawWithShadow(g, msg, parent.width/2 - Fonts.BlueprintFont.getWidth(msg)/2,i + 115, Color.black, new Color(255, 255, 255, 0.5f));

		} else {
			if(!isSelected()) {
				g.setColor(new Color(0, 0, 0, 0.2f));
				g.fillRect(0, i, parent.width - 11, Height);

			}
		}*/

	}


	private ArrayList<String> splitBetweenLines(String s, UnicodeFont f, int w) {
		ArrayList<String> results = new ArrayList<String>();
		if(f.getWidth(s) < w) {
			results.add(s);
			return results;
		} else {
			String left = s;
			for(int i = 0; i < left.length(); i++) {
				if(f.getWidth(left.substring(0, i)) > w) {
					int lastSpace = left.substring(0, i).lastIndexOf(' ');
					results.add(left.substring(0, lastSpace).trim());
					left = left.substring(lastSpace);
				}
			}
			results.add(left.trim());
		}
		return results;

	}

}
