package com.studio878.ui;

public class ChatColors {
	public static String Black = "�0";
	public static String Blue = "�1";
	public static String Cyan = "�2";
	public static String LightBlue = "�3";
	public static String Purple = "�4";
	public static String Red = "�5";
	public static String LightRed = "�6";
	public static String Green = "�7";
	public static String LightGreen = "�8";
	public static String Yellow = "�9";
	public static String Gray = "�a";
	public static String White = "�b";
	public static String DarkGray = "�c";
}
