package com.studio878.ui;


import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

import com.studio878.util.Sheets;

public class Checkbox {

	boolean isSelected = false;
	int x;
	int y;
	int sx;
	boolean pressed = false;
	boolean centered;
	public Checkbox(int x, int y, boolean def, boolean centered) {
		this.x = x;
		this.y = y;
		this.sx = x;
		isSelected = def;
		this.centered = centered;
	}
	
	public boolean isSelected() {
		return isSelected;
	}
	
	public void process(GameContainer c) {
		if(centered) {
			x = sx + c.getWidth()/2;
		}
		int mx = c.getInput().getMouseX();
		int my = c.getInput().getMouseY();
		if(c.getInput().isMouseButtonDown(0) && mx >= x && mx <= x + 20 && my >= y && my <= y + 20) {
			pressed = true;
		}
		if(pressed && !c.getInput().isMouseButtonDown(0)) {
			if( mx >= x && mx <= x + 20 && my >= y && my <= y + 20) {
				isSelected = !isSelected;
			}
				pressed = false;
		}
	}
	
	public void draw(Graphics g) {
	if(!isSelected) {
		g.drawImage(Sheets.CHECKBOXES.getSubImage(0, 0), x, y);
	} else {
		g.drawImage(Sheets.CHECKBOXES.getSubImage(1, 0), x, y);
	}
	}
}
