package com.studio878.ui;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.fills.GradientFill;
import org.newdawn.slick.geom.Rectangle;

import com.studio878.displays.MainGame;
import com.studio878.inventory.Inventory;
import com.studio878.inventory.Item;
import com.studio878.inventory.ItemID;
import com.studio878.util.GraphicsUtil;
import com.studio878.util.Images;

public class SellListItem extends TableListItem {

	Item item;
	
	public SellListItem(Item i, TableList parent) {
		super(i.getType().getReferenceName(), 55, parent);
		this.item = i;
	}
	
	public Item getItem() {
		return item;
	}
	
	public void draw(Graphics g, int i) {
		int xoffset = 0, yoffset = 0;
		if(MainGame.activeSellItem == item.getType()) {
			xoffset = 1;
			yoffset = 1;
		}

		g.drawImage(Images.GridItem.getSubImage(xoffset*50, yoffset*50, 50, 50), 0, i);
		
		g.drawImage(item.getType().getImage(), 9, i + 9);
		g.setFont(Fonts.ChatFont);
		GraphicsUtil.drawWithShadow(g, item.getType().getSellCost() + "", 55, i + 14, Color.white, Color.black);
		g.drawImage(ItemID.lookupName("Coins").getImage(), 57 + Fonts.ChatFont.getWidth(item.getType().getSellCost() + ""), i + 8);
		
		int mx = Mouse.getX();
		int my = Display.getDisplayMode().getHeight() - Mouse.getY();
		if(Mouse.isButtonDown(0) && mx >= parent.x && mx <= parent.x + parent.width && my >= i && my <= i + 55) {
			MainGame.activeSellItem = item.getType();
			MainGame.activeRecipe = null;
			MainGame.activeRecipeItem = null;
		}

	}
	
	

}
