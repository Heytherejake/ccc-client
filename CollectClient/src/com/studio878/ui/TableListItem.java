package com.studio878.ui;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

public class TableListItem {

	String message;
	int height;
	protected TableList parent;
	public TableListItem(String s, int height, TableList parent) {
		message = s;
		this.height = height;
		this.parent = parent;
	}
	
	public boolean isSelected() {
		return parent.selected == parent.items.indexOf(this);
	}
	
	public String getMessage() {
		return message;
	}
	public void draw(Graphics g, int i) {
		if(isSelected()) {
			g.setColor(new Color(0, 0, 0, 0.4f));
		} else {
			g.setColor(new Color(255, 255, 255, 0.2f));
		}
		g.fillRect(0, i, parent.width - parent.scrollWidth, height);
		g.setColor(Color.black);
		g.drawString(message, 5 , i);
	}
}
