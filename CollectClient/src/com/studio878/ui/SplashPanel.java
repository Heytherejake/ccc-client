package com.studio878.ui;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

import org.lwjgl.LWJGLUtil;

import com.studio878.assets.AssetLoader;

public class SplashPanel extends JPanel{
	public static java.awt.Image splash;
	public String message = "Loading...";
	
	public SplashPanel() {
		try {
			splash = ImageIO.read(AssetLoader.class.getResource("s878_logo.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void paintComponent(Graphics g) {
		  Graphics2D g2d=(Graphics2D) g;
		  g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		  g2d.setBackground(Color.black);
		  g2d.setColor(Color.black);
		  g2d.fillRect(0, 0, getWidth(), getHeight());
		g2d.drawImage(splash, (this.getWidth() - splash.getWidth(this))/2, (this.getHeight() - splash.getHeight(this))/2 - 20, this);
		g2d.setFont(new Font("Trebuchet MS", Font.PLAIN, 20));
		FontMetrics m = g2d.getFontMetrics();
		g2d.setColor(Color.white);
		g2d.drawString(message, (this.getWidth() - m.stringWidth(message))/2 , (this.getHeight() - splash.getHeight(this))/2 + 350);
		if(LWJGLUtil.getPlatform() == LWJGLUtil.PLATFORM_MACOSX) {
			String m2 = "(This may take a while)";
			g2d.drawString(m2, (this.getWidth() - m.stringWidth(m2))/2 , (this.getHeight() - splash.getHeight(this))/2 + 380);

		}
	}
}
