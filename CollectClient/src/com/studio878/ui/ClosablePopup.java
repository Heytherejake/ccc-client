package com.studio878.ui;


import org.newdawn.slick.Font;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class ClosablePopup extends Popup {

	Button button;
	public ClosablePopup(String s, String btn, int x, int y, int width, int height, Font f, boolean centered) {
		super(s, x, y, width, height, f, centered);
		button = new Button(x + width/2 - 100, y + height - 50, btn, false);
		this.compress = false;
	}
	

	public void draw(Graphics g, GameContainer c) {
		super.draw(g, c);
		button.setX(x + width/2 - 100);
		button.setY(y + height - 70);
		button.draw(g, c);
		button.check(c);
		if(button.isSelected()) {
			this.isOpen = false;
		}
	}

}
