package com.studio878.ui;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

import com.studio878.inventory.Inventory;
import com.studio878.inventory.Item;
import com.studio878.inventory.ItemID;
import com.studio878.util.Sheets;

public class InventoryUI {
	
	static int x = 0,  y = 0;
	
	public static void setPositon(int xs, int ys) {
		x = xs;
		y = ys;
	}
	
	public static int getX() {
		return x;
	}
	
	public static int getY() {
		return y;
	}
	public static void draw(Graphics g, GameContainer c) {

		g.setFont(Fonts.SmallFont);
		int yoff = y - 100;
		int disp = 82;
		int xdisp = 800;
		for(int k = 0; k < Inventory.Slots - 1; k++) {
				int i = k % 3;
				int j = k / 3;
				g.setColor(new Color(190, 190 , 190));
				g.fillRect((2*x - xdisp)/2 + i*disp, yoff + j*disp, 62, 62);
				g.setColor(new Color(245, 245, 245));
				g.fillRect((2*x - xdisp)/2 + i*disp, yoff + j*disp, 60, 60);
				g.setColor(new Color(215, 215, 215));
				g.fillRect((2*x- xdisp)/2 + i*disp + 2, yoff + j*disp + 2, 58, 58);
			}

		for(Item i : Inventory.getItems()) {
			int ist = i.getSlot() - 1;
			if(ist >= 0) {
				g.drawImage(i.getType().getAntiAliasedImage(),(2*x - xdisp)/2 + (ist % 3)*disp + 15, yoff+15 + ((int) Math.floor(ist/3))*disp);
				if(i.getType().isWeapon()) {
					g.drawImage(Sheets.ITEM_FLAIR.getSubImage(i.getLevel()%3, (int) Math.floor(i.getLevel()/3)), (2*x - xdisp)/2 + (ist % 3)*disp + 40, yoff+38 + ((int) Math.floor(ist/3))*disp);
				}
				if(i.getAmount() > 1) {
					g.setColor(Color.black);
					g.drawString(""+i.getAmount(), (2*x - xdisp)/2 + (ist%3)*disp + 55 - Fonts.SmallFont.getWidth("" + i.getAmount()), yoff + 42 + ((int) Math.floor(ist/3))*disp);
				}


			}
		}
		for(int i = 0; i < Inventory.Slots - 1; i++) {
			int tx = (2*x - xdisp)/2 + (i % 3)*disp;
			int ty = yoff + ((int) Math.floor(i/3))*disp;
			if(c.getInput().getMouseX() >= tx && c.getInput().getMouseX() <= tx + 62 && c.getInput().getMouseY() >= ty && c.getInput().getMouseY() <= ty + 62) {
				g.setColor(new Color(255, 255, 255, 0.6f));
				g.fillRect(tx, ty, 62, 62);
				g.setColor(new Color(255, 222, 173));
				Item ti = Inventory.getItemFromSlot(i + 1);
				if(ti != null) {
					String title = ti.getType().getNames()[ti.getLevel()];
					g.setFont(Fonts.ChatFont);
					g.setColor(new Color(255, 255, 255, 0.6f));
					g.fillRect(c.getInput().getMouseX() - (Fonts.ChatFont.getWidth(title) + 10)/2, c.getInput().getMouseY() - 30, (Fonts.ChatFont.getWidth(title) + 10), 30);
					g.setColor(Color.black);
					g.drawString(title, c.getInput().getMouseX() - (Fonts.ChatFont.getWidth(title) + 10)/2 + 5, c.getInput().getMouseY() - 25);
					g.setLineWidth(1.0f);
					g.drawRect(c.getInput().getMouseX() - (Fonts.ChatFont.getWidth(title) + 10)/2, c.getInput().getMouseY() - 30, (Fonts.ChatFont.getWidth(title) + 10), 30);
				}
			}
		}
		String amt = "x " + insertCommas(""+Inventory.getBalance());
		int wdt = Fonts.ChatFont.getWidth(amt) + 32;
		g.setColor(Color.black);
		g.drawImage(ItemID.lookupName("Coins").getImage(), x - wdt/2 - 255, y + 160);
		g.setFont(Fonts.ChatFont);
		g.drawString(amt, x - wdt/2 + 32 - 255, y + 170);
	
	}
	
    private static String insertCommas(String str)  {
        if(str.length() < 4){
            return str;
        }
        return insertCommas(str.substring(0, str.length() - 3)) + "," + str.substring(str.length() - 3, str.length());
    }

}
