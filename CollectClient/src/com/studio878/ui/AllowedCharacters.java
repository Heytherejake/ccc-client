package com.studio878.ui;

import java.util.ArrayList;
import java.util.Arrays;

public class AllowedCharacters {
	private static Character[] letters = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '1', '2', '3', '4', '5', '6', '7', '8', '9', '9', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '~', '`', '{' ,'}', '[', ']', '-', '_', '+', '=', '|', '\\', ':', ';', '\'', '"', '?', '/', '.', '>', '<', ',', '�'};
	public static ArrayList<Character>  Characters = (ArrayList<Character>) Arrays.asList(letters);

	public static boolean isAllowed(String s) {
		boolean isAllowed = true;
		for(int i = 0; i < s.length(); i++) {
			if(!Characters.contains(s.charAt(i))) {
				isAllowed = false;
			}
		}
		return isAllowed;
	}
	
}
