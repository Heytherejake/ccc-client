package com.studio878.ui;

import java.util.concurrent.CopyOnWriteArrayList;

import org.newdawn.slick.Color;
import org.newdawn.slick.Font;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.geom.Transform;

import com.studio878.util.FilterableImage;

public class TableList {

	Font font;
	public int x, y;
	public int height, width;

	public CopyOnWriteArrayList<TableListItem> items = new CopyOnWriteArrayList<TableListItem>();

	int selected = 0;

	Image sg;
	Image scrollbar;
	int scrollWidth = 0;
	boolean enableScroll = false;
	public double scrollPosition = 0;
	boolean isScrolling = false;
	double scrollHeight = 0;
	double previousY = 0;
	boolean centered;
	int sx;
	boolean drawBackground = true;
	double rotation = 0;
	public TableList(int x, int y, int width, int height, Font font, boolean centered, double rotation) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.font = font;
		this.centered = centered;
		sx = x;
		this.rotation = rotation;
		try {
			if(rotation != 0) {
				sg = new FilterableImage(1000, 1000, FilterableImage.FILTER_LINEAR);
			}
			scrollbar = new FilterableImage(width, height, FilterableImage.FILTER_LINEAR);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void setX(int x) {
		this.x = x;
		this.sx = x;
	}


	public int getTotalHeight() {
		int tht = 0;
		for(TableListItem i : items) {
			tht += i.height;
		}
		return tht;
	}
	public void setHeight(int h) {
		this.height = h;
	}
	public TableListItem getSelected() {
		try {
			TableListItem s = items.get(selected);
			return s;
		} catch (IndexOutOfBoundsException e) {
			return null;
		}
	}

	public void setDrawBackground(boolean f) {
		drawBackground = f;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getSelectedId() {
		return selected;
	}
	public void addItem(TableListItem value) {
		items.add(value);
	}

	public void removeItem(String value) {
		if(items.contains(value)) {
			items.remove(value);
		}
	}

	public void updateScrollPosition(int interval) {
		if(enableScroll) {
			scrollPosition += interval;
			previousY += interval;
			if(scrollPosition > height - scrollHeight - 4) {
				scrollPosition = height  - scrollHeight - 4;
			} else if (scrollPosition < 0) {
				scrollPosition = 0;
			}
		}
	}
	public void setItemValue(int id, TableListItem value) {
		items.remove(id);
		items.add(id, value);
	}

	public void setSelected(int i) {
		this.selected = i;
	}
	public Font getFont() {
		return font;
	}

	public void clearItems() {
		items.clear();
	}

	public void update(GameContainer c) {
		if(centered) {
			this.x = c.getWidth() / 2 + sx;
		}
		double mx = c.getInput().getMouseX();
		double my = c.getInput().getMouseY();

		int center = x + width/2;
		double cx = mx - center;
		double sin = cx*Math.sin(Math.toRadians(rotation));
		int centery = y + height/2;
		double cy = my - centery;
		double cos = cy*Math.sin(Math.toRadians(rotation));
		if(c.getInput().isMouseButtonDown(0) && mx >= x - cos && mx <= x + width - cos - scrollWidth && my >= y + sin&& my <= y + height + sin) {
			int pos = 0;
			for(TableListItem i : items) {
				if(my - y  - sin + getTotalHeight()*(scrollPosition/height) >= pos && my - y - sin <= pos + i.height + getTotalHeight()*(scrollPosition/height) ) {
					selected = items.indexOf(i);
				}
				pos += i.height;
			}
		}
		int th = 0;
		for(TableListItem i : items) {
			th += i.height;
		}
		if(th > height) {
			enableScroll = true;
			scrollWidth = 10;

		} else {
			enableScroll = false;
		}
		if(enableScroll && c.getInput().isMouseButtonDown(0) && mx >= x + width - scrollWidth && mx <= x + width && my >= y && my <= y + scrollHeight) {
			isScrolling = true;
			previousY = my;
		}
		if(isScrolling) {
			if(c.getInput().isMouseButtonDown(0)) {
				scrollPosition = my - y;
				if(scrollPosition > height - scrollHeight - 4) {
					scrollPosition = height  - scrollHeight - 4;
				} else if (scrollPosition < 0) {
					scrollPosition = 0;
				}
			} else {
				isScrolling = false;
			}
		}




	}

	public void draw(Graphics gs) {
		try {
			Graphics gb = scrollbar.getGraphics();
			if(rotation != 0) {
				sg.setRotation((float) rotation);
			}
			scrollbar.setRotation((float) rotation);
			gb.clear();

			gs.setColor(Color.white);
			if(drawBackground && getTotalHeight() != 0) {
				int h = getTotalHeight();
				if(h > height) {
					h = height;
				}
				Shape r = new Rectangle(x, y, width - 12, h);
				r = r.transform(Transform.createRotateTransform((float) Math.toRadians(rotation), x + (width-12)/2, y + h/2));
				gs.fill(r);
			}
			if(rotation == 0) {
				Rectangle oldClip = gs.getClip();
				int offset = (int)( y + -1*(scrollPosition/height) * (getTotalHeight()));//-(int) ((scrollPosition/height) * (getTotalHeight())) + y + (int) (Math.sin(rotation)*-1*(items.get(0).height)/2);
				gs.setWorldClip(new Rectangle(x, y, width, height));
				//gs.setWorldClip((int) (x - width*Math.cos(rotation)) , (int) (y + height*Math.cos(rotation)), width, height);
				gs.translate((int) (x), 0);
				gs.rotate(x + width/2, y + height/2, (float) (rotation));
				for(int i = 0; i < items.size(); i++) {

					items.get(i).draw(gs, offset);

					offset += items.get(i).height;

				}
				gs.rotate(x + width/2, y + height/2, (float) (-1*rotation));
				gs.translate((int) -x, 0);
				gs.clearWorldClip();
				gs.setClip(oldClip);
			} else {
				Graphics g = sg.getGraphics();
				g.clear();
				int offset = 0;
				for(int i = 0; i < items.size(); i++) {
					try {
						items.get(i).draw(g, offset);
						offset += items.get(i).height;
					} catch (ArrayIndexOutOfBoundsException e) {

					}
				}
				Image igs = sg.getSubImage(0, (int) ((scrollPosition/height) * (getTotalHeight())), width, height);
				igs.setRotation((float) rotation);
				gs.drawImage(igs, x, y);
			}
			if(enableScroll) {
				int th = 0;
				for(TableListItem i : items) {
					th += i.height;
				}

				gb.setColor(new Color(80, 80, 80));
				gb.fillRect(width - scrollWidth, 0, scrollWidth, height);
				scrollHeight = (((double) height) / ((double) th)) * height - 4;
				if(scrollHeight > height - 4) {
					scrollHeight = height - 4;
				}
				gb.setColor(new Color(150, 150, 150));
				gb.fillRoundRect(width - scrollWidth + 2, (float) scrollPosition + 2, scrollWidth - 4, (float) scrollHeight, 2);

			}
			gs.drawImage(scrollbar, x, y);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
