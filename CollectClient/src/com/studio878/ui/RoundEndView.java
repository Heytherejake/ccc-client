package com.studio878.ui;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

import com.studio878.util.GraphicsUtil;
import com.studio878.util.TeamManager.Team;

public class RoundEndView {

	Image image;
	public TableList table;

	Team team;
	public int time;
	public long start;

	public RoundEndView() {
		try {
			image = new Image(2000, 2000);
			table = new TableList(50, 75, 700, 200, Fonts.ChatFont, false, 0);
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}
	public void update(GameContainer c) {

	}

	public void setTeam(Team team) {
		this.team = team;
	}

	public Team getTeam() {
		return team;
	}

	public void setNextRoundTime(int time) {
		this.time = time;
	}

	public void setEndTime(long time) {
		this.start = time;
	}

	public void draw(Graphics g, GameContainer c) {
		try {
			Graphics gs = image.getGraphics();
			gs.clear();
			GraphicsUtil.drawShadowBox(gs, 0, 0, 800, 300, 0);
			gs.setFont(Fonts.LargeFont);
			gs.setColor(Color.black);
			if(team != null) {
				String fs = team.name() + " Team wins!";
				gs.drawString(fs, 400 - Fonts.LargeFont.getWidth(fs)/2, 10);
			}
			gs.setFont(Fonts.ChatFont);
			long ts = System.currentTimeMillis() - start;
			int sec = time - (int) (Math.ceil(ts/1000));
			String str = "A new round will start in " + sec + " seconds";

			gs.drawString(str, 400 - Fonts.ChatFont.getWidth(str)/2, 40);
			table.draw(gs);

		} catch (SlickException e) {
		}
		image.setRotation(0.5f);
		g.drawImage(image, c.getWidth()/2 - 400, c.getHeight() - 325);


	}
}
