package com.studio878.ui;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;

import com.studio878.util.GraphicsUtil;
import com.studio878.util.Sheets;
import com.studio878.util.SoundEffects;

public class Button {

	protected Image standard = Sheets.BUTTONS.getSubImage(0, 0);
	protected Image rollover = Sheets.BUTTONS.getSubImage(0, 1);
	protected Image click = Sheets.BUTTONS.getSubImage(0, 2);
	Image toDisplay;
	
	int x, y;
	int sx;
	String name;
	
	boolean disabled = false;
	
	public boolean isSelected = false;
	
	boolean centered;
	
	boolean allowSelect = false;

	public Button(int x, int y, String name, boolean centered) {
		toDisplay = standard;
		this.x = x;
		this.y = y;
		this.name = name;
		this.centered = centered;
		sx = x;
	}
	
	public boolean isSelected() {
		if(isSelected) {
			isSelected = false;
			return true;
		}
		return false;
	}
	
	public void setX(int x) {
		this.x = x;
		this.sx = x;
	}
	
	public void setY(int y) {
		this.y = y;
	}
	
	public void setValue(String v) {
		this.name = v;
	}
	
	public String getValue() {
		return name;
	}
	public void check(GameContainer c) {
		if(disabled) {
			return;
		}
		if(toDisplay == click && allowSelect && !c.getInput().isMouseButtonDown(0)) {
			isSelected = true;
			allowSelect = false;
		}
		double mx = c.getInput().getMouseX();
		double my = c.getInput().getMouseY();
		if(mx >= x && my >= y && mx <= x + toDisplay.getWidth() && my <= y + toDisplay.getHeight()) {
			if(c.getInput().isMouseButtonDown(0)){
				if(toDisplay != click) {
					SoundEffects.playSound(SoundEffects.Click);
				}
				toDisplay = click;
			} else {
				toDisplay = rollover;
			}
			allowSelect = true;
		} else {
			allowSelect = false;
			toDisplay = standard;
		}
	
	}

	public boolean isDisabled() {
		return disabled;
	}
	public void isDisabled(boolean b) {
		disabled = b;
	}
	public void draw(Graphics g, GameContainer c) {
		if(centered) {
			this.x = (c.getWidth() / 2) + sx;
		}
		
		float op = 1.0f;
		if(disabled) {
			toDisplay.setAlpha(0.5f);
			op = 0.5f;
		} else {
			toDisplay.setAlpha(1.0f);
		}
		g.drawImage(toDisplay, x, y);
		g.setFont(Fonts.ChatFont);
		int wx = Fonts.ChatFont.getWidth(name);
		int wy = Fonts.ChatFont.getHeight(name);
		g.setColor(Color.black);
		Color fg = new Color(220, 220, 220, op);
		if(toDisplay == click){
			fg = new Color(140, 140, 140, op);
		} else if (toDisplay == rollover) {
			fg = new Color(255, 255, 255, op);
		}
		GraphicsUtil.drawWithShadow(g, name, x + (toDisplay.getWidth() - wx)/2, y + (toDisplay.getHeight() - wy)/2, new Color(0, 0, 0, op), fg);


	}
}
