package com.studio878.ui;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;

import com.studio878.displays.MainGame;
import com.studio878.util.Sheets;

public class WireModeButton extends Button {
	
	int position = 0;
	Image overImage;
	
	public WireModeButton(int x, int y, int position) {
		super(x, y, "", false);
		standard = Sheets.WIRE_OVERLAY_ICONS.getSubImage(0, 0);
		rollover = Sheets.WIRE_OVERLAY_ICONS.getSubImage(2, 0);
		click = Sheets.WIRE_OVERLAY_ICONS.getSubImage(1, 0);
		overImage = Sheets.WIRE_OVERLAY_ICONS.getSubImage(0, position);
		
		this.position = position;
	}
	
	public void check(GameContainer c) {
		super.check(c);
		if(MainGame.WirePlacementType == position) {
			toDisplay = click;
		}
	}
	
	public void draw(Graphics g, GameContainer c) {
		super.draw(g, c);
		g.drawImage(overImage, x, y);
		if(toDisplay == rollover) {
			String tsp = "Wires";
			switch(position) {
			case 2:
				tsp = "Latches";
				break;
			case 3:
				tsp = "Turrets";
				break;
			case 4:
				tsp = "Input";
				break;
			case 5:
				tsp = "Close";
				break;
			}
			g.setColor(new Color(90, 90, 90, 0.5f));
			g.setFont(Fonts.SmallFont);
			g.fillRect(x + 16 - Fonts.SmallFont.getWidth(tsp)/2 - 10 , y + 40, Fonts.SmallFont.getWidth(tsp) + 20 , 20);
			g.setColor(Color.black);
			g.drawString(tsp, x + 16 - Fonts.SmallFont.getWidth(tsp)/2, y + 42);
			g.drawRect(x + 16 - Fonts.SmallFont.getWidth(tsp)/2 - 10 , y + 40, Fonts.SmallFont.getWidth(tsp) + 20 , 20);

		}
	}

}
