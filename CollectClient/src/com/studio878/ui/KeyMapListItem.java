package com.studio878.ui;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

import com.studio878.util.GraphicsUtil;

public class KeyMapListItem extends TableListItem {
	
	String key, value;
	TableList parent;
	
	public KeyMapListItem(String key, String value, TableList parent) {
		super(key, 25, parent);
		this.key = key;
		this.value = value;
		this.parent = parent;
	}
	
	public String getKey() {
		return key;
	}
	
	public void draw(Graphics g, int i) {
		g.setFont(Fonts.ChatFont);
		if(!this.isSelected()) {
		g.setColor(new Color(200, 200, 200));
		} else {
			g.setColor(new Color(240,240,240));
		}
		g.fillRect(0, i, parent.width, height);
		g.setColor(Color.black);
		GraphicsUtil.drawWithShadow(g, key, 10, i + 2, new Color(70, 70, 70), Color.white);

		GraphicsUtil.drawWithShadow(g, value, parent.width - Fonts.ChatFont.getWidth(value) - 20, i + 2, new Color(70, 70, 70), Color.white);
		g.setColor(Color.black);
		g.setLineWidth(1.0f);
		g.drawLine(0, i + height - 1, parent.width, i + height - 1);
	}

}
