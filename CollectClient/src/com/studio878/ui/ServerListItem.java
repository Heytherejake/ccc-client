package com.studio878.ui;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

import com.studio878.util.GraphicsUtil;

public class ServerListItem extends TableListItem{

	String ip;
	int port, players, maxPlayers;
	long time;
	public ServerListItem(String ip, int port, String name, long time, int players, int maxPlayers, TableList parent) {
		super(name, 70, parent);
		this.port = port;
		this.time = time;
		this.ip = ip;
		this.players = players;
		this.maxPlayers = maxPlayers;
	}

	public int getPort() {
		return port;
	}

	public String getIp() {
		return ip;
	}

	public void draw(Graphics g, int i) {
		g.setFont(Fonts.LargeFont);
		if(!this.isSelected()) {
			g.setColor(new Color(200, 200, 200));
		} else {
			g.setColor(new Color(240,240,240));
		}
		g.fillRect(0, i, parent.width, height);
		g.setColor(Color.black);
		g.drawString(this.getMessage(), 10, i + 10);
		g.setColor(new Color(50, 50, 50));
		g.setFont(Fonts.ChatFont);
		String pst = ip + ":" + port;
		GraphicsUtil.drawWithShadow(g, pst, 10, i + 40, new Color(70, 70, 70), Color.white);
		String latency = "Latency: " + time + "ms";
		if(time == 99999) {
			latency = "Unresponsive";
		}
		GraphicsUtil.drawWithShadow(g, latency, parent.width - Fonts.ChatFont.getWidth(latency) - 20, i + 40, new Color(70, 70, 70), Color.white);

		String ps = "Players: " + players + "/" + maxPlayers;
		GraphicsUtil.drawWithShadow(g, ps, parent.width - Fonts.ChatFont.getWidth(ps) - 20, i + 10, new Color(70, 70, 70), Color.white);
		g.setLineWidth(1.0f);
		g.setColor(Color.black);
		g.drawLine(0, i + height - 1, parent.width, i + height - 1);

	}

}
