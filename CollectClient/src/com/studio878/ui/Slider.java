package com.studio878.ui;


import org.newdawn.slick.Color;
import org.newdawn.slick.Font;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

import com.studio878.util.Console;
import com.studio878.util.Images;

public class Slider {

	int x, y, width, sx;
	boolean centered;
	boolean showAmount;
	Font font;
	Image slider;
	int spx;
	boolean isDragging = false;
	int position = 0;
	public Slider(int x, int y, int width, Font font, boolean centered, boolean showAmount) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.font = font;
		this.centered = centered;
		this.showAmount = showAmount;
		sx = x;
		try {
		slider = new Image(10, 20);
	
		} catch (SlickException e) {
			Console.write("Well that's problematic. Failed to create a slider.");
		}
	}
	
	public void setPosition(int posx) {
		spx = (int) (posx*(((double) width-5)/100));
	}
	public int getPosition() {
		return (int)  (spx/(((double) width - 5)/100));
	}
	
	public boolean isDragging() {
		return isDragging;
	}
	public void update(GameContainer c) {
		if(centered) {
			this.x = c.getWidth()/2 + sx - width/2;
		}
		
		if(c.getInput().isMouseButtonDown(0)) {
			int mx = c.getInput().getMouseX();
			int my = c.getInput().getMouseY();
			if(mx >= x+spx && mx <= x + spx + 10 && my >= y - 5 && my <= my + 15) {
				isDragging = true;
			}
		} else {
			isDragging = false;
		}
		if(isDragging) {
			int mx = c.getInput().getMouseX();
			spx = mx - x;
			if(spx <= 0) {
				spx = 0;
			} else if (spx >= width - 5) {
				spx = width - 5;
			}
		}
	}
	
	public void draw(GameContainer c, Graphics g) {		  
	
		g.setColor(new Color(190, 190, 190));
		g.drawImage(Images.slider.getSubImage(5, 0, 1, 13).getScaledCopy(width - 8, 13), x + 5, y);
		g.drawImage(Images.slider.getSubImage(0, 0, 5, 13), x, y);
		g.drawImage(Images.slider.getSubImage(6, 0, 5, 13), x + width - 4, y);
		g.setColor(Color.black);
		g.drawImage(Images.slider.getSubImage(1, 15, 10, 19), x + spx, y - 3);
		g.drawString(""+getPosition(), x + width/2 - font.getWidth(""+getPosition())/2, y + 10);

	}
}
