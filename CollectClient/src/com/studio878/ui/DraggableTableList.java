package com.studio878.ui;

import org.lwjgl.input.Mouse;
import org.newdawn.slick.Color;
import org.newdawn.slick.Font;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.geom.Transform;

public class DraggableTableList extends TableList{


	public int selY;

	int preX = -1;
	int dxsp = 0;
	double dx;

	boolean isPressed = false;
	public DraggableTableList(int x, int y, int width, int height, Font font, int goalY, boolean centered, double rotation) {
		super(x, y, width, height, font, centered, rotation);

		this.selY = goalY;

	}

	public void setX(int x) {
		this.x = x;
		this.sx = x;
	}


	public int getTotalHeight() {
		int tht = 0;
		for(TableListItem i : items) {
			tht += i.height;
		}
		return tht;
	}
	public void setHeight(int h) {
		this.height = h;
	}
	public TableListItem getSelected() {
		try {
			TableListItem s = items.get(selected);
			return s;
		} catch (IndexOutOfBoundsException e) {
			return null;
		}
	}

	public void setDrawBackground(boolean f) {
		drawBackground = f;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getSelectedId() {
		return selected;
	}
	public void addItem(TableListItem value) {
		items.add(value);
	}

	public void removeItem(String value) {
		if(items.contains(value)) {
			items.remove(value);
		}
	}

	public void updateScrollPosition(int interval) {
		if(enableScroll) {
			scrollPosition += interval;
			previousY += interval;
			if(scrollPosition > height - scrollHeight - 4) {
				scrollPosition = height  - scrollHeight - 4;
			} else if (scrollPosition < 0) {
				scrollPosition = 0;
			}
		}
	}
	public void setItemValue(int id, TableListItem value) {
		items.remove(id);
		items.add(id, value);
	}

	public Font getFont() {
		return font;
	}

	public void clearItems() {
		items.clear();
	}

	public void update(GameContainer c) {
		int mx = Mouse.getX();
		int my = c.getHeight() - Mouse.getY();
		if(preX != -1) {
			if(c.getInput().isMouseButtonDown(0)) {
				dx = preX - my;
				isPressed = true;
				dxsp += Math.abs(dx);
				if(mx >= x && mx <= x + width && my >= y && my <= y + height) {
					this.scrollPosition += 0.8*dx;
				}
			} else {

				if(this.scrollPosition < 0) {
					this.scrollPosition += this.scrollPosition*-0.15;
					if(this.scrollPosition > -0.002) {
						this.scrollPosition = 0;
					}
				}
				int sm = height;
				if(height > getTotalHeight()) {
					sm = getTotalHeight();
				}
				if(this.scrollPosition >= getTotalHeight() - sm) {
					this.scrollPosition -= this.scrollPosition*0.10;
					if(this.scrollPosition < getTotalHeight() - sm + 0.002) {
						this.scrollPosition = getTotalHeight() - sm;
					}
				}
				if(isPressed && dxsp < 10) {
					int center = x + width/2;
					double cx = mx - center;
					double sin = cx*Math.sin(Math.toRadians(rotation));
					int centery = y + height/2;
					double cy = my - centery;
					double cos = cy*Math.sin(Math.toRadians(rotation));
					if(mx >= x - cos && mx <= x + width - cos - scrollWidth && my >= y + sin&& my <= y + height + sin) {
						int pos = 0;
						for(TableListItem i : items) {
							if(my - y  - sin + scrollPosition >= pos && my - y - sin <= pos + i.height + scrollPosition ) {
								selected = items.indexOf(i);
							}
							pos += i.height;
						}
					}
				}
				isPressed = false;
				dxsp = 0;
			}
				
		}

		preX = my;


	}

	public void draw(Graphics gs) {
		try {
			Graphics gb = scrollbar.getGraphics();
			if(rotation != 0) {
				sg.setRotation((float) rotation);
			}
			scrollbar.setRotation((float) rotation);
			gb.clear();

			gs.setColor(Color.white);
			if(drawBackground && getTotalHeight() != 0) {
				int h = getTotalHeight();
				if(h > height) {
					h = height;
				}
				Shape r = new Rectangle(x, y, width - 12, h);
				r = r.transform(Transform.createRotateTransform((float) Math.toRadians(rotation), x + (width-12)/2, y + h/2));
				gs.fill(r);
			}
			if(rotation == 0) {
				Rectangle oldClip = gs.getClip();
				int offset = (int)(y + -1*scrollPosition);//-(int) ((scrollPosition/height) * (getTotalHeight())) + y + (int) (Math.sin(rotation)*-1*(items.get(0).height)/2);
				int ts = height;
				if(height > getTotalHeight()) {
					ts = getTotalHeight();
				}
				gs.setWorldClip(new Rectangle(x, y, width, ts));
				//gs.setWorldClip((int) (x - width*Math.cos(rotation)) , (int) (y + height*Math.cos(rotation)), width, height);
				gs.translate((int) (x), 0);
				//gs.rotate(x + width/2, y + height/2, (float) (rotation));
				
				for(int i = 0; i < items.size(); i++) {
					items.get(i).draw(gs, offset);

					offset += items.get(i).height;

				}
				//gs.rotate(x + width/2, y + height/2, (float) (-1*rotation));
				gs.translate((int) -x, 0);
				gs.clearWorldClip();
				gs.setClip(oldClip);
			} else {
				Graphics g = sg.getGraphics();
				g.clear();
				int offset = 0;
				for(int i = 0; i < items.size(); i++) {
					items.get(i).draw(g, offset);

					offset += items.get(i).height;

				}
				Image igs = sg.getSubImage(0, (int) ((scrollPosition/height) * (getTotalHeight())), width, height);
				igs.setRotation((float) rotation);
				gs.drawImage(igs, x, y);
			}
			if(enableScroll) {
				int th = 0;
				for(TableListItem i : items) {
					th += i.height;
				}

				gb.setColor(new Color(80, 80, 80));
				gb.fillRect(width - scrollWidth, 0, scrollWidth, th);
				scrollHeight = (((double) height) / ((double) th)) * height - 4;
				if(scrollHeight > height - 4) {
					scrollHeight = height - 4;
				}
				gb.setColor(new Color(150, 150, 150));
				gb.fillRoundRect(width - scrollWidth + 2, (float) scrollPosition + 2, scrollWidth - 4, (float) scrollHeight, 2);

			}
			gs.drawImage(scrollbar, x, y);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
