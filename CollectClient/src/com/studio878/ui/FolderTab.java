package com.studio878.ui;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

import com.studio878.util.Images;

public class FolderTab implements Comparable<FolderTab>{

	String title;
	int pos;
	Folder parent;
	int dy = 0;
	public FolderTab(String title, int pos, Folder parent) {
		this.title = title;
		this.pos = pos;
		this.parent = parent;
	}

	public boolean isSelected() {
		return parent.getSelected() == pos;
	}
	public void draw(GameContainer c, Graphics g) {
		int lx = parent.cx + 45 + (pos*160);
		int ly = parent.cy - 30;
		if(this.isSelected()) {
			dy = -10;
		}
		g.drawImage(Images.FolderTab, lx , ly + dy);
		g.setFont(Fonts.TypewriterFont);
		g.setColor(new Color(248,245,225));
		g.drawString(title, lx + (Images.FolderTab.getWidth() - Fonts.TypewriterFont.getWidth(title))/2, parent.cy - 17 + dy);

		int mx = c.getInput().getMouseX();
		int my = c.getInput().getMouseY();
		if(mx >= lx && mx <= lx + Images.FolderTab.getWidth() && my >= ly && my <= ly + Images.FolderTab.getHeight()) {
			if(c.getInput().isMouseButtonDown(0)) {
				parent.setSelected(pos);
			}
			if(dy >= -10) {
				dy--;
			}
		} else {
			if(dy <= 0) {
				dy++;
			}
		}



	}

	public int compareTo(FolderTab o) {
		FolderTab f = (FolderTab) o;
		if(f.isSelected()) {
			return 1;
		}else {
			if(f.pos > pos) {
				return 1;
			} else if (f.pos == pos) {
				return 0;
			}
			return -1;
		}
	}
}

