package com.studio878.ui;

import java.util.ArrayList;

import org.newdawn.slick.Color;
import org.newdawn.slick.Font;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

import com.studio878.util.GraphicsUtil;

public class Popup {

	String msg;
	int x, y, sx, sy, width, height;
	boolean centered;
	Font font;
	ArrayList<String> lines = new ArrayList<String>();
	boolean isOpen = false;
	boolean compress = true;
	public Popup(String s, int x, int y, int width, int height, Font f, boolean centered) {
		this.msg = s;
		this.x = x;
		this.y = y;
		sx = x;
		sy = y;
		this.width = width;
		this.height = height;
		this.centered = centered;
		font = f;
	}

	public void check(GameContainer c) {
		if(centered) {
			this.x = (c.getWidth() / 2) + sx;
		}
	}
	
	public void setX(int x) {
		this.x = x;
	}
	
	public void setY(int y) {
		this.y = y;
	}

	public void setMessage(String s) {
		msg = s;
		lines.clear();
		for(int i = 0; i < s.length(); i++) {
			if(font.getWidth(s.substring(0, i)) > width - 20) {
				int ls = s.substring(0, i).lastIndexOf(" ");
				if(ls == -1) {
					ls = i;
				}
				lines.add(s.substring(0, ls));
				s = s.substring(ls);
				i = 0;

			}
		}
		lines.add(s);
		System.out.println(lines);
	}

	public void setOpen(boolean open) {
		this.isOpen = open;
	}
	
	public boolean isOpen() {
		return isOpen;
	}
	
	
	public void draw(Graphics g, GameContainer c) {
		g.setColor(new Color(0, 0, 0, 0.4f));
		g.fillRect(0, 0, c.getWidth(), c.getHeight());
		if(compress) {
			height = lines.size()*40;
		}
		g.setColor(new Color(255, 255, 255, 0.4f));
		g.fillRect(x - 20, y - 20, width + 40, height + 40);
		g.setColor(Color.white);
		g.fillRect(x, y, width, height);
		g.setFont(font);
		g.setColor(Color.black);
		for(int i = 0; i < lines.size(); i++) {
			String sm = lines.get(i);
			int ofst = lines.size() - i - 1;
			if(!compress) {
				ofst++;
			}
			g.drawString(sm, x + width/2 - font.getWidth(sm)/2 , y + height/2 - 32 - (ofst)*40 + (lines.size() * 20));
		}
	}
}
