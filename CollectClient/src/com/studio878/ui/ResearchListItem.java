package com.studio878.ui;

import java.util.ArrayList;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.fills.GradientFill;
import org.newdawn.slick.geom.Circle;
import org.newdawn.slick.geom.Rectangle;

import com.studio878.crafting.CraftingRecipe;
import com.studio878.inventory.Inventory;
import com.studio878.inventory.Item;
import com.studio878.inventory.ItemID;
import com.studio878.util.GraphicsUtil;
import com.studio878.util.Settings;

public class ResearchListItem extends TableListItem{
	CraftingRecipe recipe;
	static int Height = 150;

	public ResearchListItem(CraftingRecipe c, TableList parent) {
		super("", Height, parent);
		this.recipe = c;
	}

	public void draw(Graphics g, int i) {
		int wdt = parent.width - 10;

		Rectangle r = new Rectangle(0, i + 2, wdt, Height + 1);
		Circle c = new Circle(100, i, 10);
		g.setColor(new Color(0, 0, 0, 0.5f));
		g.fill(r);
		r.setX(0);
		r.setY(i);
		GradientFill f = new GradientFill(0, 0, new Color(255, 255, 255), wdt, Height, new Color(240, 240, 240));
		g.fill(r, f);
		g.setColor(Color.black);
		g.drawRect(0, i, wdt, Height + 1);
		g.drawImage(recipe.getImage(), 20, i + 10);
		g.setFont(Fonts.TypewriterFont);
		g.setColor(Color.black);
		g.drawString(recipe.getName(), 160, i+10);
		g.setFont(Fonts.SmallTypewriterFont);
		int sy = i + 30;
		ArrayList<String> parts = splitBetweenLines(recipe.getDescription(), Fonts.SmallTypewriterFont, 350);
		for(String s : parts) {
			g.drawString(s, 160, sy);
			sy += 15;
		}

		int sx = 150;
		if(recipe.getRequirements() != null) {
			for(Item id : recipe.getRequirements()) {
				g.drawImage(id.getType().getAntiAliasedImage(), sx, i + 115);
				g.drawString("x" + id.getAmount(), sx + 35, i + 128);
				sx += 38 + Fonts.SmallTypewriterFont.getWidth("x" + id.getAmount());
			}
		}
		sy = i + 20;
		g.setColor(new Color(0, 90, 0));
		g.setFont(Fonts.TypewriterFont);
		for(String s : recipe.getUpsides()) {
			ArrayList<String> ptss = splitBetweenLines(s, Fonts.SmallFont, 155);
			for(String sv : ptss) {
				g.drawString(sv, 530, sy);
				sy += 15;
			}
			sy += 10;
		}
		sy += 10;
		g.setColor(new Color(90, 0, 0));
		if(recipe.getDownsides() != null) {
			for(String s: recipe.getDownsides()) {
				ArrayList<String> ptss = splitBetweenLines(s, Fonts.SmallFont, 155);
				for(String sv : ptss) {
					g.drawString(sv, 530, sy);
					sy += 15;
				}
				sy += 10;
			}
		}
		boolean hasMaterials = true;
		if(recipe.getRequirements() != null) {
			for(Item iv: recipe.getRequirements()) {
				if(!Inventory.contains(iv.getType(), iv.getAmount())) {
					hasMaterials = false;
				}
			}
		}
		
		if(Inventory.getAmount(ItemID.lookupName("Coins")) > recipe.getBuyCost()) {
			hasMaterials = true;
		}
		
		
		if (Inventory.hasBonus(recipe)) {
			g.setColor(new Color(255, 0, 0, 0.5f));
			g.fillRect(0, i, parent.width - 10, Height);
			g.setFont(Fonts.BoldFont);
			String msg = "Research already completed";

			GraphicsUtil.drawWithShadow(g, msg, parent.width/2 - Fonts.SmallFont.getWidth(msg)/2,i + 115, Color.black, new Color(255, 255, 255, 0.5f));
		} else if(!hasMaterials) {
			g.setColor(new Color(255, 0, 0, 0.5f));
			g.fillRect(0, i, parent.width - 10, Height);
			g.setFont(Fonts.BoldFont);
			String msg = "Insufficient resources";
			GraphicsUtil.drawWithShadow(g, msg, parent.width/2 - Fonts.SmallFont.getWidth(msg)/2,i + 115, Color.black, new Color(255, 255, 255, 0.5f));

		} else {
			if(!isSelected()) {
				g.setColor(new Color(0, 0, 0, 0.2f));
				g.fillRect(0, i, parent.width - 10, Height);

			}
		}

	}

	public CraftingRecipe getRecipe() {
		return recipe;
	}
	private ArrayList<String> splitBetweenLines(String s, UnicodeFont f, int w) {
		ArrayList<String> results = new ArrayList<String>();
		if(f.getWidth(s) < w) {
			results.add(s);
			return results;
		} else {
			String left = s;
			for(int i = 0; i < left.length(); i++) {
				if(f.getWidth(left.substring(0, i)) > w) {
					int lastSpace = left.substring(0, i).lastIndexOf(' ');
					results.add(left.substring(0, lastSpace).trim());
					left = left.substring(lastSpace);
				}
			}
			results.add(left.trim());
		}
		return results;

	}

}
