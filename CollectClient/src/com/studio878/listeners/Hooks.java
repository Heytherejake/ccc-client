package com.studio878.listeners;

import java.util.ArrayList;

import com.studio878.block.Block;
import com.studio878.objects.Player;

public class Hooks {
	public static enum Hook {
		BLOCK_DESTROY,
		BLOCK_DAMAGED,
		BLOCK_CREATED,
		;
	}
	public static boolean call(Hook hook, Object... p) {
		boolean result = true;
		ArrayList<EventListener> l = ListenerList.getListeners();
		for(int i = 0; i < l.size(); i++) {
			EventListener e = l.get(i);

			switch(hook) {
			case BLOCK_DESTROY:
				if(HookRegistry.isHookRegistered(e, Hook.BLOCK_DESTROY)) {
					if(!e.onBlockDestroyed((Player) p[0], (Block) p[1])) {
						result = false;
					}
				}
				break;
			case BLOCK_DAMAGED:
				if(HookRegistry.isHookRegistered(e, Hook.BLOCK_DAMAGED)) {
					if(!e.onBlockDamaged((Player) p[0], (Block) p[1], (Integer) p[2])) {
						result = false;
					}
				}
				break;
			case BLOCK_CREATED:
				if(HookRegistry.isHookRegistered(e, Hook.BLOCK_CREATED)) {
					if(!e.onBlockCreated((Player) p[0], (Block) p[2])) {
						result = false;
					}
				}
				break;
			}
		}
		return result;
	}
}
