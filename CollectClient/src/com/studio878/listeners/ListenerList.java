package com.studio878.listeners;

import java.util.ArrayList;

public class ListenerList {
	public static ArrayList<EventListener> listeners = new ArrayList<EventListener>();
	public static void addListener(EventListener e) {
		listeners.add(e);
	}
	
	public static void removeListener(EventListener e) {
		listeners.remove(e);
	}
	
	public static ArrayList<EventListener> getListeners() {
		return listeners;
	}
	
}
