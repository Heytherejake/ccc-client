package com.studio878.listeners;

import com.studio878.block.Block;
import com.studio878.objects.Player;

public interface EventListener {
	public boolean onBlockDestroyed(Player player, Block block);
	public boolean onBlockDamaged(Player player, Block block, int damage);
	public boolean onBlockCreated(Player player, Block block);
}
