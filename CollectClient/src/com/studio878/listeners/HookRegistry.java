package com.studio878.listeners;

import java.util.ArrayList;
import java.util.HashMap;

import com.studio878.listeners.Hooks.Hook;

public class HookRegistry {
	private static HashMap<EventListener, ArrayList<Hook>> hooks = new HashMap<EventListener, ArrayList<Hook>>();
	public static void registerHook(EventListener l, Hook h) {
		ArrayList<Hook> p = hooks.get(l);
		if(p == null) {
			hooks.put(l, new ArrayList<Hook>());
			hooks.get(l).add(h);
		} else {
			p.add(h);
		}
	}
	
	public static ArrayList<Hook> getHooks(EventListener l) {
		return hooks.get(l);
	}
	public static boolean isHookRegistered(EventListener l, Hook h) {
		return(hooks.get(l).contains(h));
	}
	
	
}
