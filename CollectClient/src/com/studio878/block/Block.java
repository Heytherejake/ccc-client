package com.studio878.block;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Sound;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.geom.Rectangle;

import com.studio878.displays.Menu;
import com.studio878.listeners.Hooks;
import com.studio878.listeners.Hooks.Hook;
import com.studio878.objects.Entity;
import com.studio878.objects.PhysicsParticle;
import com.studio878.objects.Player;
import com.studio878.util.Location;
import com.studio878.util.Settings;
import com.studio878.util.Sheets;
import com.studio878.util.SoundEffects;
import com.studio878.util.TeamManager.Team;
import com.studio878.world.Grid;


public class Block {


	private static final byte Durability_Lowest = 1;
	private static final byte Durability_Lower = 2;
	private static final byte Durability_Low = 3;
	private static final byte Durability_Medium = 4;
	private static final byte Durability_High = 5;
	private static final byte Durability_Higher = 6;
	private static final byte Durability_Highest = 7;
	private static final byte Durability_Indestructable = -1;

	static final short LightRadius = 150;
	int x;
	int y;
	BlockType type, decorationType, foregroundType, wireType;
	int dat, wireData;
	byte condition = 10;
	short ticks = 0;
	byte squareLocation;
	byte strength;
	Image blockImage, backgroundImage, midImage, decorationImage, foregroundImage, conditionImage, wireImage;
	boolean overlay, surface;
	short light;

	int updateTicks = 0;
	//Square Location:
	// 0 1
	// 2 3
	BlockType bgType;


	public Block(BlockType t, int d, int x, int y, byte sql) {
		type = t;
		dat = d;
		this.x = x;
		this.y = y;
		squareLocation = sql;
		strength = t.getDurability();
		bgType = t;
		setBlockImage();
		setBackgroundImage();
		setCondition(condition);

	}
	public Block(Block b) {
		type = b.getType();
		dat = b.getData();
		setCondition(b.getCondition());
		x = b.getX();
		y = b.getY();
		ticks = b.getTicks();
		strength = b.getDurability();
		squareLocation = b.getSquareLocation();
		bgType = b.getBackgroundType();
		setBlockImage();
		setBackgroundImage();
		light = b.light;
	}


	public void setBlockImage() {
		int offset = 0;
		if(squareLocation == 2 || squareLocation == 3) {
			offset = 1;
		}
		midImage = null;
		int ost = 0;

		if(type.isRounded() && Settings.RoundBlocks) {
			boolean[][] grid = new boolean[3][3];
			for(int i = -1; i <= 1; i++) {
				for(int j = -1; j <= 1; j++) {
					Block bd = getBlockAdjacent(i, j);
					if(bd != null && (bd.getType() == type || bd.getType() != BlockType.lookupName("Air"))) {
						grid[i + 1][j + 1] = true;
					} else {
						grid[i + 1][j + 1] = false;
					}
				}
			}
			Block bs = null;
			boolean l = grid[0][1], r = grid[2][1], t = grid[1][0], b = grid[1][2];
			if(t && r && !b && !l) {
				bs = getBlockAdjacent(0, 1);
				if(bs != null) {
					ost = 3;
					BlockType st = bs.getType();

					int oft = bs.getSquareLocation();
					int ss = 0;
					if(oft == 2 || oft == 3) {
						ss = 1;
					}
					midImage =  Sheets.BLOCK_TEXTURES.getSprite((int) (oft*2), (int) ((st.getPosition()* 2) + ss));
				}
			} else if (t && l && !r && !b) {
				bs = getBlockAdjacent(0, 1);
				if(bs != null) {
					ost = 4;
					BlockType st = bs.getType();

					int oft = bs.getSquareLocation();
					int ss = 0;
					if(oft == 2 || oft == 3) {
						ss = 1;
					}
					midImage =  Sheets.BLOCK_TEXTURES.getSprite((int) (oft*2), (int) ((st.getPosition()* 2) + ss));
				}

			} else if(r && !b && !t && !l) {
				bs = getBlockAdjacent(-1, 0);
				if(bs != null) {
					ost = 7;
					BlockType st = bs.getType();

					int oft = bs.getSquareLocation();
					int ss = 0;
					if(oft == 2 || oft == 3) {
						ss = 1;
					}
					midImage =  Sheets.BLOCK_TEXTURES.getSprite((int) (oft*2), (int) ((st.getPosition()* 2) + ss));
				}
			} else if(!r && !b && !t && l) {
				bs = getBlockAdjacent(1, 0);
				if(bs != null) {
					ost = 8;
					BlockType st = bs.getType();

					int oft = bs.getSquareLocation();
					int ss = 0;
					if(oft == 2 || oft == 3) {
						ss = 1;
					}
					midImage =  Sheets.BLOCK_TEXTURES.getSprite((int) (oft*2), (int) ((st.getPosition()* 2) + ss));
				}
			} else if (b && r && !t && !l) {
				bs = getBlockAdjacent(0, -1);
				if(bs != null) {
					ost = 1;
					BlockType st = bs.getType();

					int oft = bs.getSquareLocation();
					int ss = 0;
					if(oft == 2 || oft == 3) {
						ss = 1;
					}
					midImage =  Sheets.BLOCK_TEXTURES.getSprite((int) (oft*2), (int) ((st.getPosition()* 2) + ss));
				}

			} else if (b && l && !t && !r) {
				bs = getBlockAdjacent(0, -1);
				if(bs != null) {
					ost = 2;
					BlockType st = bs.getType();

					int oft = bs.getSquareLocation();
					int ss = 0;
					if(oft == 2 || oft == 3) {
						ss = 1;
					}
					midImage =  Sheets.BLOCK_TEXTURES.getSprite((int) (oft*2), (int) ((st.getPosition()* 2) + ss));
				}

			} else if(!r && b && !t && !l) {
				bs = getBlockAdjacent(0, -1);
				if(bs != null) {
					ost = 5;
					BlockType st = bs.getType();

					int oft = bs.getSquareLocation();
					int ss = 0;
					if(oft == 2 || oft == 3) {
						ss = 1;
					}
					midImage =  Sheets.BLOCK_TEXTURES.getSprite((int) (oft*2), (int) ((st.getPosition()* 2) + ss));
				}
			} else if(!r && !b && t && !l) {
				bs = getBlockAdjacent(0, 1);
				if(bs != null) {
					ost = 6;
					BlockType st = bs.getType();

					int oft = bs.getSquareLocation();
					int ss = 0;
					if(oft == 2 || oft == 3) {
						ss = 1;
					}
					midImage =  Sheets.BLOCK_TEXTURES.getSprite((int) (oft*2), (int) ((st.getPosition()* 2) + ss));
				}
			}
			if(bs != null) {
				if(bs.getBackgroundType() == BlockType.lookupName("Air")) {
					surface = true;
				}
			}
		} else {
			ost = 0;
		}

		blockImage = Sheets.BLOCK_TEXTURES.getSprite((int) (squareLocation%2 + ost*2), (int) ((type.getPosition() * 2) + offset));
		checkDecoration(BlockType.lookupName("Steel Pipe"), BlockType.lookupName("Vertical Steel Pipe"), Sheets.PIPE_CORNERS, 1);

	}

	public Rectangle getBoundingBox() {
		return new Rectangle(x*16, y*16, 16, 16);
	}
	public void setDecorationType(BlockType t) {
		this.decorationType = t;
		if(decorationType != null) {
			setDecorationImage();
			ArrayList<Block> neighbors = getSurroundingBlocks();
			for(Block b : neighbors) {
				if(b != null && b.getDecorationType() != null) {
					b.setDecorationImage();
				}
			}
		}

	}

	public void setForegroundType(BlockType t) {
		this.foregroundType = t;
		if(foregroundType != null) {
			setForegroundImage();
			ArrayList<Block> neighbors = getSurroundingBlocks();
			for(Block b : neighbors) {
				if(b != null && b.getForegroundType() != null) {
					b.setForegroundImage();
				}
			}
		}

	}

	public void setDecorationImage() {
		int offset = 0;
		if(squareLocation == 2 || squareLocation == 3) {
			offset = 1;
		}
		this.decorationImage = Sheets.BLOCK_TEXTURES.getSprite((int) (squareLocation%2), (int) ((decorationType.getPosition() * 2) + offset));
		checkDecoration(BlockType.lookupName("Steel Pipe"), BlockType.lookupName("Vertical Steel Pipe"), Sheets.PIPE_CORNERS, 2);

	}

	public void setForegroundImage() {
		int offset = 0;
		if(squareLocation == 2 || squareLocation == 3) {
			offset = 1;
		}
		this.foregroundImage = Sheets.BLOCK_TEXTURES.getSprite((int) (squareLocation%2), (int) ((foregroundType.getPosition() * 2) + offset));
		checkDecoration(BlockType.lookupName("Steel Pipe"), BlockType.lookupName("Vertical Steel Pipe"), Sheets.PIPE_CORNERS, 3);

	}

	public BlockType getForegroundType() {
		return foregroundType;
	}

	public BlockType getDecorationType() {
		return decorationType;
	}
	public int getLightLevel() {
		return light;
	}

	public void incrementLightLevel() {
		light++;

	}

	public void decrementLightLevel() {
		if(light > 0) {
			light--;
		}
	}

	public void setLightLevel(byte i) {
		light = i;
	}

	public void setBackgroundImage() {
		int offset = 0;
		if(squareLocation == 2 || squareLocation == 3) {
			offset = 1;
		}
		backgroundImage = Sheets.BLOCK_TEXTURES.getSprite((int) (squareLocation%2), (int) ((bgType.getPosition() * 2) + offset));
	}
	public void setBlockImage(BlockType t) {
		BlockType ts = type;
		type = t;
		setBlockImage();
		ArrayList<Block> neighbors = getSurroundingBlocks();
		for(Block b : neighbors) {
			if(b != null) {
				b.setBlockImage();
			}
		}
		type = ts;

	}

	public ArrayList<Block> getSurroundingBlocks() {
		ArrayList<Block> result = new ArrayList<Block>();
		result.add(this.getBlockAdjacent(1, 0));
		result.add(this.getBlockAdjacent(-1, 0));
		result.add(this.getBlockAdjacent(0, 1));
		result.add(this.getBlockAdjacent(0, -1));
		return result;
	}

	public void setOverlay(boolean t) {
		overlay = t;
	}
	public void setType(BlockType t) {
		BlockType ts = type;
		type = t;
		if(t != BlockType.lookupName("Air")){
			setBlockImage(t);
		} else {
			setBlockImage(bgType);
		}

		if(ts.getLuminocity() != 0) {
			String sp = ts.getName();
			if(sp.equals("Floodlight")) {
				//triangular path;
				for(int i = 0; i < ts.getLuminocity(); i++) {
					for(int j = x - i; j <= x + i; j++) {
						Block b = Grid.getBlockAt(j, y + i);
						if(b != null) {
							b.setLightLevel((byte) (b.getLightLevel() - Math.abs((ts.getLuminocity() - i - (int) ((Math.abs(x-j))/2)))));
						}

					}
				}
			} else if (sp.equals("Room-Fill Light Source")) {
				roomFillLightCheck(new ArrayList<Block>(), false);
			} else {
				//round path
				for(int i = 0; i < ts.getLuminocity(); i++) {
					ArrayList<Block> blocks = Grid.getCircle(x, y, i);
					for(Block b : blocks) {
						if(b != null) {
							b.decrementLightLevel();

						}

					}
				}

			}

		}
		if(t.getLuminocity() != 0) {
			String sp = ts.getName();
			if(sp.equals("Floodlight")) {
				//triangular path;
				for(int i = 0; i < t.getLuminocity(); i++) {
					for(int j = x - i; j <= x + i; j++) {
						Block b = Grid.getBlockAt(j, y + i);
						if(b != null) {
							b.setLightLevel((byte) (b.getLightLevel() + Math.abs((t.getLuminocity() - i- (int) ((Math.abs(x-j)/2))))));
						}

					}
				}
			} else if (sp.equals("Room-Fill Light Source")) {
				roomFillLightCheck(new ArrayList<Block>(), true);
			} else {
				//round path
				for(int i = 0; i < t.getLuminocity(); i++) {
					ArrayList<Block> blocks = Grid.getCircle(x, y, i);
					for(Block b : blocks) {
						if(b != null) {
							b.incrementLightLevel();

						}
					}
				}

			}
		}
		if(this.type == BlockType.lookupName("Air") && this.bgType == BlockType.lookupName("Air")) {
			light = 10;
			for(int i = 0; i < 10; i++) {
				ArrayList<Block> blocks = Grid.getCircle(x, y, i);
				for(Block b : blocks) {
					if(b != null) {

						b.incrementLightLevel();
					}
				}

			}
		}
		if(this.type == BlockType.lookupName("Air") && this.bgType == BlockType.lookupName("Air")) {
			light = 10;
		}


	}

	protected void roomFillLightCheck(ArrayList<Block> source, Boolean inc) {
		ArrayList<Block> blocks = getAdjacentAndDiagonalBlocks();
		boolean hasEdge = false;
		byte fill = (inc ? (byte) 10 : (byte) 0);
		this.light = fill;
		if(this.type == BlockType.lookupName("Air") || this.type == BlockType.lookupName("Room-Fill Light Source")) {
			for(Block b : blocks) {
				if(!source.contains(b)) {
					try {
						source.add(b);
						b.roomFillLightCheck(source, inc);
					} catch (StackOverflowError e) {
						return;
					}
				}
			}
		} else {
			fill = (inc ? (byte) 8 : (byte) 0);
			this.light = fill;
		}
	}

	public ArrayList<Block> getAdjacentAndDiagonalBlocks() {
		ArrayList<Block> result = new ArrayList<Block>();
		for(int i = -1; i <= 1; i++) {
			for(int j = -1; j <= 1; j++) {
				Block b = getBlockAdjacent(i, j);
				if(b != null) {
					result.add(b);
				}
			}
		}
		return result;
	}

	public byte getSquareLocation() {
		return squareLocation;
	}
	public byte getDurability() {
		return strength;
	}
	public Location getLocationOnGrid() {
		return new Location(x, y);
	}
	public short getTicks() {
		return ticks;
	}
	public void shiftDown(int shift) {
		Grid.setBlockAt(x, y + shift, this);
	}
	public Block getBlockAdjacent(int x, int y) {
		Location t = getLocationOnGrid();
		try {
			return (Grid.getGrid()[t.getX() + x][t.getY() + y]);
		} catch(ArrayIndexOutOfBoundsException e) {
			return null;
		}
	}
	
	public Image getBackgroundImage() {
		return backgroundImage;
	}



	public void setData(int d) {
		dat = d;
	}
	public void setCondition(byte c) {
		condition = c;
		try {
			conditionImage = Sheets.BLOCK_BREAK_TEXTURES.getSubImage(10 - condition, 0);
		} catch (Exception e) {

		}

	}
	public BlockType getType() {
		return type;
	}
	public int getData() {
		return dat;
	}
	public void setSquareLocation(byte s) {
		squareLocation = s;
	}
	public byte getCondition() {
		return condition;
	}
	public int getX() { 
		return x;
	}
	public int getY() {
		return y;
	}

	public void setX(int x) {
		this.x = x;
	}
	public void setY(int y) {
		this.y = y;
	}

	public int getForegroundId() {
		try {
			return foregroundType.getId();
		} catch (NullPointerException e) {
			return -1;
		}
	}

	public int getDecorationId() {
		try {
			return decorationType.getId();
		} catch (NullPointerException e) {
			return -1;
		}
	}

	public void create(Player p) {
		if(!Hooks.call(Hook.BLOCK_CREATED, p, this)) {
			this.setType(BlockType.lookupName("Air"));
		}
	}

	public void setBackgroundType(BlockType b) {
		bgType = b;
		setBackgroundImage();
	}
	
	public Image getMainImage() {
		return blockImage;
	}

	public BlockType getBackgroundType() {
		return bgType;
	}

	public int getWireData() {
		return wireData;
	}

	public void setWireData(int data) {
		int pd = wireData;
		wireData = data;
		setWireImage();
	}

	public BlockType getWireType() {
		return wireType;
	}

	public void setWireType(BlockType type) {
		BlockType wt = wireType;
		this.wireType = type;
		setWireImage();
		for(Block b : getAdjacentWires()) {
			b.setWireImage();
		}

	}

	public ArrayList<Block> getAdjacentWires() {
		ArrayList<Block> result = new ArrayList<Block>();
		Block b = getBlockAdjacent(0, -1);
		if(b != null && b.getWireType() != null && b.getWireType().isElectric()) {
			result.add(b);
		}
		b = getBlockAdjacent(0, 1);
		if(b != null && b.getWireType() != null && b.getWireType().isElectric()) {
			result.add(b);
		}
		b = getBlockAdjacent(1, 0);
		if(b != null && b.getWireType() != null && b.getWireType().isElectric()) {
			result.add(b);
		}
		b = getBlockAdjacent(-1, 0);
		if(b != null && b.getWireType() != null && b.getWireType().isElectric()) {
			result.add(b);
		}
		return result;
	}

	public void setWireImage() {
		int offset = 0;
		if(squareLocation == 2 || squareLocation == 3) {
			offset = 1;
		}
		int ost = 0;
		boolean isWire = false;
		if(wireType == BlockType.lookupName("Wire")|| wireType == BlockType.lookupName("Vertical Wire")) {
			ost = wireData;
			isWire = true;
		}

		if(wireType != null) {
			wireImage = Sheets.BLOCK_TEXTURES.getSprite((int) (squareLocation%2) + ost*2, (int) ((wireType.getPosition() * 2) + offset));

			if(isWire) {
				if(ost == 0) {
					checkDecoration(BlockType.lookupName("Wire"), BlockType.lookupName("Vertical Wire"), Sheets.WIRE_CORNERS, 4);
				} else if (ost == 1) {
					checkDecoration(BlockType.lookupName("Wire"), BlockType.lookupName("Vertical Wire"), Sheets.LIGHT_WIRE_CORNERS, 4);

				}
			}
		}
	}

	public void checkDecoration(BlockType t1, BlockType t2, SpriteSheet sheet, int layer) {
		BlockType tst = null;
		switch(layer) {
		case 1:
			tst = type;
			break;
		case 2:
			tst = decorationType;
			break;
		case 3:
			tst = foregroundType;
			break;
		}

		if(!(tst == t1 || tst == t2)) return;
		boolean[][] grid = new boolean[3][3];
		for(int i = -1; i <= 1; i++) {
			for(int j = -1; j <= 1; j++) {
				Block bd = getBlockAdjacent(i, j);
				if(bd != null) {
					BlockType svl = null;
					switch(layer) {
					case 1:
						svl = bd.getType();
						break;
					case 2:
						try {
							svl = BlockType.lookupId(bd.getDecorationId());
						} catch (Exception e) {
						}
						break;
					case 3:
						try {
							svl = BlockType.lookupId(bd.getForegroundId());
						} catch (Exception e) {
						}
						break;
					}
					if(svl == t1 || svl == t2) {
						grid[i + 1][j + 1] = true;
					} else {
						grid[i + 1][j + 1] = false;
					}
				} else {
					grid[i + 1][j + 1] = false;
				}

			}
		}

		boolean t = grid[1][0];
		boolean b = grid[1][2];
		boolean l = grid[0][1];
		boolean r = grid[2][1];

		int sx = -1;
		int sy = -1;
		if(t && b && l && r) {
			sx = 1;
			sy = 1;
		} else if (t && b && l && !r) {
			sx = 2;
			sy = 1;
		} else if (t && b && !l && r) {
			sx = 0;
			sy = 1;
		} else if (t && !b && l && r) {
			sx = 1;
			sy = 2;
		} else if (!t && b && l && r) {
			sx = 1;
			sy = 0;
		} else if (!t && b && !l && r) {
			sx = 0;
			sy = 0;
		} else if (!t && b && l && !r) {
			sx = 2;
			sy = 0;
		} else if (t && !b && l && !r) {
			sx = 2;
			sy = 2;
		} else if (t && !b && !l && r) {
			sx = 0;
			sy = 2;
		}

		if(sx != -1 && sy != -1) {
			switch(layer) {
			case 1:
				blockImage = sheet.getSubImage(sx, sy);
				break;
			case 2:
				decorationImage = sheet.getSubImage(sx, sy);
				break;
			case 3:
				foregroundImage = sheet.getSubImage(sx, sy);
				break;
			}
		}
	}

	public void drawForeground(Graphics g) {
		int sl = light;


		int ll = 10 - sl;
		if(Settings.CameraPlayer != null) {
			if(Math.sqrt(Math.pow(this.x*16 - Settings.CameraPlayer.getX(), 2) + Math.pow(this.y*16 - Settings.CameraPlayer.getY(), 2)) <= LightRadius) {
				ll -= 10 - (int) (10*((double)Math.sqrt(Math.pow(this.x*16 - Settings.CameraPlayer.getX(), 2) + Math.pow(this.y*16 - Math.round(Settings.CameraPlayer.getY()), 2))/(double)LightRadius));
			}
		}

		if(ll != 10 || !Settings.ShowLight) {
			if(foregroundType != null) {
				g.drawImage(foregroundImage, x*16, y*16);
			}
			if(type.isFluid()) {
				g.drawImage(blockImage.getSubImage(0, 16-dat, 16, dat), x*16, y*16 + (16-dat));

			}
		}
	}


	public void drawLightMap(Graphics g) {
		int sl = light;


		int ll = 10 - sl;
		if(Settings.CameraPlayer != null) {
			if(Math.sqrt(Math.pow(this.x*16 - Settings.CameraPlayer.getX(), 2) + Math.pow(this.y*16 - Settings.CameraPlayer.getY(), 2)) <= LightRadius) {
				ll -= 10 - (int) (10*((double)Math.sqrt(Math.pow(this.x*16 - Settings.CameraPlayer.getX(), 2) + Math.pow(this.y*16 - Math.round(Settings.CameraPlayer.getY()), 2))/(double)LightRadius));
			}
		}

		if(ll < 0) {
			ll = 0;
		}

		g.setColor(new Color(0, 0, 0, ll/10.0f));
		g.fillRect(x*16, y*16, 16, 16);

	}

	public void draw(Graphics g) {

		short sl = light;
		if(!(Settings.MainWindow.d instanceof Menu)) {
			if(Settings.CameraPlayer != null) {
				if(Math.sqrt(Math.pow(this.x*16 - Settings.CameraPlayer.getX(), 2) + Math.pow(this.y*16 - Math.floor(Settings.CameraPlayer.getY()), 2)) <= LightRadius) {
					sl = 10;
				}
			}
		}
		if(sl != 0 || !Settings.ShowLight) {
			try {
				if((type != null && type.isTransparent() && bgType != BlockType.lookupName("Air")) || (midImage != null && !surface)) {
					if(bgType != BlockType.lookupName("Air")) {
						g.drawImage(backgroundImage, x*16, y*16);
					}
					if(decorationType != null) {
						g.drawImage(decorationImage, x*16, y*16);
					}
					if(!bgType.getName().equals("Air")) {
						g.setColor(new Color(0, 0, 0, 0.6f));
						g.fillRect(x*16, y*16, 16, 16);
					}
					if(midImage != null) {
						g.drawImage(midImage, x*16, y*16);

					}
				}

				if(type != BlockType.lookupName("Air")) {
					try {
						if(!type.isFluid()) {
							g.drawImage(blockImage, x*16, y*16);
						} else {
							g.drawImage(blockImage.getSubImage(0, 16-dat, 16, dat), x*16, y*16 + (16-dat));

						}
						if(condition != 10 && condition != 0) {
							g.drawImage(conditionImage, x*16, y*16);
						}
					} catch (Exception e) {
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}


	}



}
