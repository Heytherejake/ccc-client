package com.studio878.block;

import java.util.ArrayList;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import org.newdawn.slick.Sound;

import com.studio878.objects.Entity;
import com.studio878.objects.PhysicsParticle;
import com.studio878.objects.Player;
import com.studio878.util.Console;
import com.studio878.util.SoundEffects.SoundEffectSets;
import com.studio878.util.TeamManager.Team;

public class BlockType {

	private static final ConcurrentHashMap<Integer, BlockType> lookupId = new ConcurrentHashMap<Integer, BlockType>();
	private static final ConcurrentHashMap<String, BlockType> lookupName = new ConcurrentHashMap<String, BlockType>();
	public static int nextHighestId = 0;
	private int id;
	private String name;
	private boolean physicsEnabled;
	private byte d;
	private boolean ps;
	private ArrayList<Sound> sfx;
	private int position;
	private boolean transparent;
	private boolean rounded;
	private boolean fluid;
	private boolean powered;
	private int luminocity;
	private String drop;
	
	public BlockType(int id, String name, boolean p, byte d, boolean permiable, ArrayList<Sound> sfx, int position, boolean transparency, boolean rounded, boolean fluid, boolean powered, int luminocity, String drop) {
		this.id = id;
		this.name = name;
		this.physicsEnabled = p;
		this.d = d;
		this.ps = permiable;
		this.sfx = sfx;
		this.position = position;
		this.transparent = transparency;
		this.rounded = rounded;
		this.fluid = fluid;
		this.powered = powered;
		this.luminocity = luminocity;
		this.drop = drop;
	}

	public int getLuminocity() {
		return luminocity;
	}

	public Sound getStepSound() {
		if(sfx != null ) {
			return sfx.get((int) (Math.random()*sfx.size()));
		}
		return null;
	}
	
	public String getName() {
		return name;
	}
	
	public int getId() {
		return id;
	}

	public boolean isFluid() {
		return fluid;
	}

	public boolean isTransparent() {
		return transparent;
	}

	public byte getDurability() {
		return d;
	}

	public boolean isElectric() {
		return powered;
	}

	public boolean isRounded() {
		return rounded;
	}

	public int getPosition() {
		return position;
	}

	
	public boolean isPhysicsEnabled() {
		return physicsEnabled;
	}

	public boolean getPermiability(Entity e) {
		if(this.getName().equals("Red Barrier")) {
			if(e instanceof Player) {
				Player p = (Player) e;
				if(p.getTeam() == Team.Red) {
					return true;
				} else {
					return false;
				}
			}
			if(e instanceof PhysicsParticle) {
				return true;
			}
		}
		if(this.getName().equals("Blue Barrier")) {
			if(e instanceof Player) {
				Player p = (Player) e;
				if(p.getTeam() == Team.Blue) {
					return true;
				} else {
					return false;
				}
			}
			if(e instanceof PhysicsParticle) {
				return true;
			}
		}

		return ps;
	}
	public static BlockType lookupId(int id) {
		return lookupId.get(id);
	}

	public static BlockType lookupName(String s) {
		for(Entry<String, BlockType> sc: lookupName.entrySet()) {
			if(s.equalsIgnoreCase(sc.getKey())) {
				return sc.getValue();
			}
		}
		return null;
	}
	
	public static void addBlock(int id, String name, boolean p, byte d, boolean permiable, int sounds, int position, boolean transparent, boolean rounded, boolean fluid, boolean electric, int luminocity, String drop) {
		BlockType block = new BlockType(id, name, p, d, permiable, SoundEffectSets.lookupId(sounds), position, transparent, rounded, fluid, electric, luminocity, drop);
		lookupId.put(id, block);
		lookupName.put(name, block);
		Console.write("Block registered: " + name + " (" + id + ")");
	}
	private static void addInitialBlock(String name, boolean p, byte d, boolean permiable, int sounds, int position, boolean transparent, boolean rounded, boolean fluid, boolean electric, int luminocity, String drop) {
		addBlock(nextHighestId, name, p, d, permiable, sounds, position, transparent, rounded, fluid, electric, luminocity, drop);
		nextHighestId++;
	}
	
	public static void clearBlocks() {
		lookupId.clear();
		lookupName.clear();
	}
	
	public static void populateDefaultBlocks() {
		addInitialBlock("Air", false, Durability_Indestructable, true, 0, 0, true, false, false, false, 0, null);
		addInitialBlock("Stone", false, Durability_Medium, false, 2, 1, false, true, false, false, 0, "Stone");
		addInitialBlock("Dirt", true, Durability_Lower, false, 1,2, false, true, false, false, 0, "Dirt");
		addInitialBlock("Grass", true, Durability_Lower, false, 1,3, false, true, false, false, 0, "Dirt");
		addInitialBlock("Lava", false, Durability_Indestructable, true, 0, 4, true, false, true, false, 3, null);
		addInitialBlock("Iron", false, Durability_Higher, false, 2, 5, false, true, false, false, 0, "Iron");
		addInitialBlock("Titanium Block", false, Durability_Indestructable, false, 2, 7, false, false, false, false, 0, null);
		addInitialBlock("Red Barrier", false, Durability_Indestructable, false, 0, 9, true, false, false, false, 0, null);
		addInitialBlock("Blue Barrier", false, Durability_Indestructable, false, 0, 8, true, false, false, false, 0, null);
		addInitialBlock("Sulfur", false, Durability_High, false, 2, 6, false, true, false, false, 0, "Sulfur"); 
		addInitialBlock("Sand", false, Durability_Lower, false, 1, 10, false, true, false, false, 0, "Sand");
		addInitialBlock("Coal", false, Durability_Higher, false, 2, 12, false, true, false, false, 0, "Coal");
		addInitialBlock("Copper", false, Durability_Higher, false, 2, 11, false, true, false, false, 0, "Copper");
		addInitialBlock("Wood", false, Durability_Higher, false, 2, 13, false, true, false, false, 0, "Wood");
		addInitialBlock("Mossy Stone", false, Durability_Medium, false, 2, 14, false, true, false, false, 0, null);
		addInitialBlock("Torch", false, Durability_Lowest, true, 0, 15, true, true, false, false, 10, "Torch");
		addInitialBlock("Steel Girder",  false, Durability_Highest, false, 2, 16, true, true, false, false, 0, null);
		addInitialBlock("Vertical Steel Girder", false, Durability_Highest, false, 2, 17, true, true, false, false, 0, null);
		addInitialBlock("Steel Pipe", false, Durability_Highest, true, 2, 18, true, true, false, false, 0, null);
		addInitialBlock("Vertical Steel Pipe", false, Durability_Highest, true, 2, 19, true, true, false, false, 0, null);
		addInitialBlock("Floodlight", false, Durability_Higher, true, 0, 20, true, false, false, false, 15, null);
		addInitialBlock("Chainlink Fence", false, Durability_Highest, true, 2, 21, false, false, false, false, 0, null);
		addInitialBlock("Gray Background", false, Durability_Indestructable, false, 2, 22, false, false, false, false, 0, null);
		addInitialBlock("Room-Fill Light Source", false, Durability_Indestructable, false, 0, 20, true, false, false, false, 10, null);
		addInitialBlock("Tree Stump", false, Durability_Medium, true, 0, 23, true, false, false, false, 0, "Wood");
		addInitialBlock("Tree Trunk", false, Durability_Medium, true, 0, 24, true, false, false, false, 0, "Wood");
		addInitialBlock("Tree Top", false, Durability_Medium, true, 0, 25, true, false, false, false, 0, "Wood");
		addInitialBlock("Brick", false, (byte) 30, false, 2, 26, false, true, false, false, 0, "Brick");
		addInitialBlock("Crate", false, Durability_High,false, 0, 27, false, false, false, false, 0, null);
		addInitialBlock("Water", false, Durability_Indestructable, true, 0, 28, true, false, true, false, 0, null);
		addInitialBlock("Vertical Wire", false, Durability_Indestructable, true, 0, 29, true, false, false, true, 0, null);
		addInitialBlock("Water", false, Durability_Indestructable, true, 0, 30, true, false, false, true, 0, null);
		addInitialBlock("And Gate", false, Durability_Indestructable, true, 0, 31, true, false, false, true, 0, null);
		addInitialBlock("Or Gate", false, Durability_Indestructable, true, 0, 32, true, false, false, true, 0, null);
		addInitialBlock("Not Gate", false, Durability_Indestructable, true, 0, 33, true, false, false, true, 0, null);
		addInitialBlock("Nor Gate", false, Durability_Indestructable, true, 0, 34, true, false, false, true, 0, null);
		addInitialBlock("Xor Gate", false, Durability_Indestructable, true, 0, 35, true, false, false, true, 0, null);
		addInitialBlock("Battery", false, Durability_Indestructable, true, 0, 36, true, false, false, true,  0, null);
	}
	

	private static final byte Durability_Lowest = 1;
	private static final byte Durability_Lower = 2;
	private static final byte Durability_Low = 3;
	private static final byte Durability_Medium = 4;
	private static final byte Durability_High = 5;
	private static final byte Durability_Higher = 6;
	private static final byte Durability_Highest = 7;
	private static final byte Durability_Indestructable = -1;
}
