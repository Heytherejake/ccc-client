package com.studio878.crafting;

import org.newdawn.slick.Image;

import com.studio878.characters.PlayerCharacter;
import com.studio878.crafting.CraftingRecipeList.RecipeType;
import com.studio878.inventory.Inventory;
import com.studio878.inventory.Item;


public class CraftingRecipe {
	
	int id;
	String name, description;
	String[] downsides;
	String[] upsides;
	Item[] resources;
	Item result;
	Image image;
	RecipeType type;
	BonusItem[] bonuses;
	PlayerCharacter[] specs;
	int buyCost;
	
	public CraftingRecipe(int id, String name, String description, Item result, Image img, String[] up, String[] down, Item[] resources, BonusItem[] bonuses, RecipeType type, PlayerCharacter[] specs, int buyCost) {
		this.id = id;
		this.name = name;
		this.description = description;
		downsides = down;
		upsides = up;
		this.result = result;
		this.resources = resources;
		this.image = img;
		this.type = type;
		this.bonuses = bonuses;
		this.specs = specs;
		this.buyCost = buyCost;
		
	}
	
	public int getId() {
		return id;
	}
	
	public int getBuyCost() {
		return buyCost;
	}
	
	public int getCalculatedBuyCost() {
		int cost = buyCost;
		for(Item i : resources) {
			if(Inventory.contains(i.getType(), i.getAmount())) {
				cost -= i.getType().getBuyCost();
			}
		}
		return cost;
	}
	
	public PlayerCharacter[] getInvalidClasses() {
		return specs;
	}
	
	public boolean isValidForClass(PlayerCharacter cs) {
		if(specs == null) {
			return true;
		}
		for(PlayerCharacter s : specs) {
			if(cs.getClass() == s.getClass()) {
				return false;
			}
		}
		return true;
	}
	
	public BonusItem[] getBonuses() {
		return bonuses;
	}
	
	public RecipeType getType() {
		return type;
	}
	
	public Image getImage() {
		return image;
	}
	
	public Item getResult() {
		return result;
	}
	
	public String getDescription() {
		return description;
	}
	
	public String getName() {
		return name;
	}
	
	public String[] getUpsides() {
		return upsides;
	}
	
	public String[] getDownsides() {
		return downsides;
	}
	
	public Item[] getRequirements() {
		return resources;
	}
}
