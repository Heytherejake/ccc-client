package com.studio878.crafting;

import java.util.HashMap;
import java.util.Map.Entry;
import java.util.concurrent.CopyOnWriteArrayList;

import org.newdawn.slick.Image;

import com.studio878.characters.PlayerCharacter;
import com.studio878.inventory.Item;
import com.studio878.util.Console;

public class CraftingRecipeList {

	public enum RecipeType {
		Weapon (0),
		Material (1),
		Research (2);

		int id;
		
		private static HashMap<Integer, RecipeType> lookupId = new HashMap<Integer, RecipeType>();
		private static HashMap<String, RecipeType> lookupName =  new HashMap<String, RecipeType>();

		private RecipeType(int id) {
			this.id = id;
		}

		public int getId() {
			return id;
		}
		
		public static RecipeType lookupId(int id) {
			return lookupId.get(id);
		}
		
		public static RecipeType lookupName(String name) {
			for(Entry<String, RecipeType> sc: lookupName.entrySet()) {
				if(name.equalsIgnoreCase(sc.getKey())) {
					return sc.getValue();
				}
			}
			return null;
		}

		static {
			for(RecipeType i : RecipeType.values()) {
				lookupId.put(i.getId(), i);
				lookupName.put(i.name(), i);
			}
		}
	}

	public static CopyOnWriteArrayList<CraftingRecipe> Recipes = new CopyOnWriteArrayList<CraftingRecipe>();

	public static CraftingRecipe getRecipe(int id) {
		for(CraftingRecipe r : Recipes) {
			if(r.getId() == id) {
				return r;
			}
		}
		return null;
	}
	
	public static void addRecipe(int id, String name, String description, Item result, Image img, String[] up, String[] down, Item[] resources, BonusItem[] bonuses, RecipeType type, PlayerCharacter[] specs, int buyCost) {
		Recipes.add(new CraftingRecipe(id, name, description, result, img, up, down, resources, bonuses, type, specs, buyCost));
		Console.write("Recipe registered: " + name + " (" + id + ")");

	}


	static {
		/* Change this to be grabbed server-side.
		Recipes.add(new CraftingRecipe(0, "Pistol", "A standard pistol. Fires fast and accurately.", new Item(ItemID.Pistol, 1), null, new String[]{"Medium rate of fire", "Low spread"}, new String[]{"Small clip"},  new Item[]{new Item(ItemID.Iron, 10), new Item(ItemID.Sulfur, 5)}, null, RecipeType.Weapon, null));
		Recipes.add(new CraftingRecipe(1, "Submachine Gun", "Fires lots of bullets in a wide spread. Powers deep-sea exploration vehicles.", new Item(ItemID.SMG, 1), null, new String[]{"High rate of fire", "Large clip"}, new String[]{"Very inaccurate"}, new Item[]{new Item(ItemID.Iron, 15),  new Item(ItemID.Sulfur, 10)}, null, RecipeType.Weapon, null));
		Recipes.add(new CraftingRecipe(2, "Rocket Launcher", "Fires powerful missiles that damage blocks, as well as very small rocks.", new Item(ItemID.RocketLauncher, 1), null, new String[]{"Heavy damage", "Large radius"}, new String[]{"Small clip", "Slow rate of fire"}, new Item[]{new Item(ItemID.Grenade, 4),  new Item(ItemID.Iron, 25), new Item(ItemID.Sulfur, 20)}, null, RecipeType.Weapon, new Specialization[]{Specialization.Soldier}));
		Recipes.add(new CraftingRecipe(3, "1,3-Diethylbenzene", "Discovered in 1924, \"Endurine\" was advertised as an energy drink. After the discovery that it caused abnormal teeth growth, it was pulled off the market.", null, Sheets.RESEARCH_IMAGES.getSubImage(0, 0), new String[]{"Increases movement speed by 10%"}, new String[]{"Affects only you"}, new Item[]{new Item(ItemID.Coins, 100)}, new BonusItem[]{new BonusItem(BonusItemType.Movement, 0.1)}, RecipeType.Research, new Specialization[]{Specialization.Researcher}));
		Recipes.add(new CraftingRecipe(4, "Triphenylphosphane", "\"Phenyltwitchane\" was originally used by the Ancient Romans as artifical snow during ski season. The Roman strategist Killus Maximus discovered in 123 BCE that it caused uncontrollable arm spasms, and since then it has been used to increase reaction time." , null, Sheets.RESEARCH_IMAGES.getSubImage(1, 0), new String[]{"Increases rate of fire by 10%"}, new String[]{"Affects only you"}, new Item[]{new Item(ItemID.Coins, 100)}, new BonusItem[]{new BonusItem(BonusItemType.RateOfFire, 0.1)}, RecipeType.Research, new Specialization[]{Specialization.Researcher}));
		Recipes.add(new CraftingRecipe(5, "2-Amino-1-Nitropropane-1-One", "\"Vertino\", engineered in 2002, quickly became notorious for its popularity with professional athletes. It's use was banned with the Perpendicular Limitation Act of 2007, a bill famous for it's popularity with both major political parties", null, Sheets.RESEARCH_IMAGES.getSubImage(2, 0), new String[]{"Increases jump height by 20%"}, new String[]{"Reduces movement speed by 5%", "Affects only you"}, new Item[]{new Item(ItemID.Coins, 100)}, new BonusItem[]{new BonusItem(BonusItemType.JumpHeight, 0.2), new BonusItem(BonusItemType.Movement, -0.05)}, RecipeType.Research,  new Specialization[]{Specialization.Researcher}));
		Recipes.add(new CraftingRecipe(6, "1,1,1,1-Methanetetrayltetrabenzene", "\"1111\" was the first compound designed entirely by computer. Translated from its native Binary, it translates roughly to \"We Are Your New Robot Overlords, And You Will Obey Us-ene\". Strangely, it speeds up the neural abilities of a person", null, Sheets.RESEARCH_IMAGES.getSubImage(3, 0), new String[]{"Increases rate of fire by 5%", "Increases move speed by 5%"}, new String[]{"Affects only to you"}, new Item[]{new Item(ItemID.Coins, 250)},  new BonusItem[]{new BonusItem(BonusItemType.RateOfFire, 0.05), new BonusItem(BonusItemType.Movement, 0.05)}, RecipeType.Research,  new Specialization[]{Specialization.Researcher}));
		//Recipes.add(new CraftingRecipe(7, "Digonimum (Temporary)", "When it was first introduced in 1952, \"[Name]\" was quickly forced off the markets by the FDA due to it's tendencies to cause hallucinations of objects in mirror being exactly as close as they appear. It also had the side effect of weapons being more effective than theoretically possible.", null, Sheets.RESEARCH_IMAGES.getSubImage(3, 0), new String[]{"Increases dig radius by 1 meter"}, new String[]{"Affects only you"},new Item[]{new Item(ItemID.Coins, 250)}, new BonusItem[]{new BonusItem(BonusItemType.DigRadius, 1)}, RecipeType.Research, , new Specialization[]{Specialization.Researcher}));
		Recipes.add(new CraftingRecipe(8, "Copper Wire", "A spool of copper wire. It's great for conducting electricity and stoping crime.", new Item(ItemID.CopperWire, 2), null, new String[]{"Used to craft electronics"}, null, new Item[]{new Item(ItemID.Copper, 1)}, null, RecipeType.Material, null));
		Recipes.add(new CraftingRecipe(9, "Integrated Circuit", "A tiny circuit that does the function of a larger circuit. It's value is Circuit * x + c.", new Item(ItemID.IntegratedCircuit, 1), null, new String[]{"Used to craft electronics"}, null, new Item[]{new Item(ItemID.CopperWire, 1), new Item(ItemID.Silicon, 1)}, null, RecipeType.Material, null));
		Recipes.add(new CraftingRecipe(10, "Grenade", "A timed explosive device that damages a group of blocks. Made with 100 percent real gren.", new Item(ItemID.Grenade, 3), null, new String[]{"Damages a large area", "Very powerful"}, new String[]{"Timed detonation", "One-time use"}, new Item[]{new Item(ItemID.Sulfur, 6), new Item(ItemID.IntegratedCircuit, 1)}, null, RecipeType.Weapon, null));
		Recipes.add(new CraftingRecipe(11, "Landmine", "An explosive device that goes off when someone approaches it. The opposite of a quarry under the sea.", new Item(ItemID.Landmine, 4), null, new String[]{"Detonates when a player walks over it", "Very powerful"}, new String[]{"One-time use"}, new Item[]{new Item(ItemID.Sulfur, 10), new Item(ItemID.IntegratedCircuit, 4)}, null, RecipeType.Weapon, new Specialization[]{Specialization.Researcher, Specialization.Soldier}));
		Recipes.add(new CraftingRecipe(12, "Shotgun", "A high-damage, close-range firearm. For passenger-side use only.", new Item(ItemID.Shotgun, 1), null, new String[]{"Heavy damage"}, new String[]{"Slow rate of fire", "Very inaccurate"}, new Item[]{new Item(ItemID.Iron, 25), new Item(ItemID.Sulfur, 10), new Item(ItemID.Coins, 100)}, null, RecipeType.Weapon, new Specialization[]{Specialization.Soldier}));
		Recipes.add(new CraftingRecipe(13, "Flintlock Pistol", "An antique pistol that does heavy damage, but has to be reloaded after every shot. It belongs in a museum.", new Item(ItemID.FlintlockPistol, 1), null, new String[]{"Heavy damage"}, new String[]{"Very small clip"}, new Item[]{new Item(ItemID.Iron, 50), new Item(ItemID.Sulfur, 25), new Item(ItemID.Coins,200)}, null, RecipeType.Weapon, new Specialization[]{Specialization.Excavator}));
		Recipes.add(new CraftingRecipe(14, "Wrench", "A metal tool used to turn things. It's better for bashing in heads.", new Item(ItemID.Wrench, 1), null, new String[]{"Heavy damage", "Melee Weapon"}, new String[]{"Slow attack rate"}, new Item[]{new Item(ItemID.Iron, 20)}, null, RecipeType.Weapon, new Specialization[]{Specialization.Researcher}));
		Recipes.add(new CraftingRecipe(15, "Hatchet", "Used to cut trees down and limbs off. It's a miniature hatch.", new Item(ItemID.Hatchet, 1), null, new String[]{"Medium damage", "Faster swing", "Melee Weapon"}, new String[]{""}, new Item[]{new Item(ItemID.Iron, 20)}, null, RecipeType.Weapon, new Specialization[]{Specialization.Soldier, Specialization.Researcher}));
		Recipes.add(new CraftingRecipe(16, "Torch", "A lighting device crafted from torch wood. It's outside the government and beyond the police.", new Item(ItemID.Torch, 5), null, new String[]{"Lights an area"}, new String[]{"Small lighting radius"}, new Item[]{new Item(ItemID.Coal, 1)}, null, RecipeType.Material, null));
	*/
	}
}
