package com.studio878.crafting;

import java.util.HashMap;
import java.util.Map.Entry;

import com.studio878.inventory.Inventory;
import com.studio878.multiplayer.MultiplayerAbilityListItem;
import com.studio878.ui.TableListItem;
import com.studio878.util.Settings;


public class BonusItem {

	public enum BonusItemType {
		Movement (0, "Movement Speed", "[Value]x"),
		RateOfFire (1, "Rate of Fire", "[Value]x"),
		JumpHeight (2, "Jump Height", "[Value]x"),
		DigRadius (3, "Dig Radius", "[Value] m."),
		BuildRadius (4, "Build Radius", "[Value] m."),
		CurrencyOutput (5, "Income", "[Value]x"),
		RangedDamage (6, "Ranged Damage", "[Value]"),
		MeleeDamage(7, "Melee Damage", "[Value]"),
		Strength(8, "Strength", "[Value]"),
		Dexterity(9, "Dexterity", "[Value]"),
		Intelligence(10, "Intelligence", "[Value]"),
		Health(11, "Health", "[Value]"),
		Mana(12, "Mana", "[Value]"),
		Armor(13, "Armor", "[Value]"),
		MagicDefence(14, "Magic Defence", "[Value]"),
		HealthRegen(15, "Health Regeneration", "[Value]"),
		ManaRegen(16, "Mana Regeneration", "[Value]"),
		LifeSteal(17, "Life Steal", "[Value]"),
		
		;
		
		int id;
		String name, format;
		
		private static HashMap<Integer, BonusItemType> lookupId = new HashMap<Integer, BonusItemType>();
		private static HashMap<String, BonusItemType> lookupName =  new HashMap<String, BonusItemType>();
		
		private BonusItemType(int id, String name, String format) {
			this.id = id;
			this.name = name;
			this.format = format;

		}
		
		public int getId() {
			return id;
		}
		
		public String format(double value) {
			return format.replace("[Value]", ""+value);
		}
		
		public String getName() {
			return name;
		}
		
		public static BonusItemType lookupId(int id) {
			return lookupId.get(id);
		}
		
		public static BonusItemType lookupName(String name) {
			for(Entry<String, BonusItemType> sc: lookupName.entrySet()) {
				if(name.equalsIgnoreCase(sc.getKey())) {
					return sc.getValue();
				}
			}
			return null;
		}

		static {
			for(BonusItemType i : BonusItemType.values()) {
				lookupId.put(i.getId(), i);
				lookupName.put(i.name(), i);
			}
		}
	}
	
	BonusItemType type;
	double amount;
	
	public BonusItem(BonusItemType t, double a) {
		type = t;
		amount = a;
	}
	
	public double getAmount() {
		return amount;
	}
	
	public BonusItemType getType() {
		return type;
	}

	public static double calculateBonus(BonusItemType t) {
		double amt = 1.0;

		for(CraftingRecipe c : Inventory.getBonuses()) {
			for(BonusItem i: c.getBonuses()) {
				if(i.getType() == t) {
					amt += i.getAmount();
				}
			}
		}
		for(TableListItem i: Settings.MainWindow.multiplayerAbilityList.items) {
			MultiplayerAbilityListItem li = (MultiplayerAbilityListItem) i;
			if(li.getType().getType() == t) {
				amt += li.getMultiplier();
			}
		}
		return amt;
	}

}
