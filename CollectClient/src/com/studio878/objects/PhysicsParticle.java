package com.studio878.objects;

import org.newdawn.slick.Image;

public class PhysicsParticle extends ClientSideEntity {

	long createTime;
	int length;
	public PhysicsParticle(double x, double y, Image i, int length) {
		super(x, y, i);
		createTime = System.currentTimeMillis();
		this.length = length;
		height = i.getHeight();
		width = i.getWidth();
	}
	
	public void step() {
		super.step();
		if(System.currentTimeMillis() - createTime >=  length) {
			EntityManager.unregisterEntity(this.getId());
		}
	}
	
	

}
