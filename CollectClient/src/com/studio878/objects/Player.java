package com.studio878.objects;


import java.util.ArrayList;

import org.newdawn.slick.Animation;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

import com.studio878.block.Block;
import com.studio878.characters.PlayerCharacter;
import com.studio878.crafting.BonusItem;
import com.studio878.crafting.BonusItem.BonusItemType;
import com.studio878.io.NetObjects;
import com.studio878.packets.Packet59SendAbsolutePosition;
import com.studio878.ui.Fonts;
import com.studio878.util.Settings;
import com.studio878.util.Sheets;
import com.studio878.util.TeamManager.Team;
import com.studio878.weapons.EmptyHand;
import com.studio878.weapons.Shovel;
import com.studio878.weapons.Weapon;
import com.studio878.world.Grid;

public class Player extends HealthEntity{

	public static int MoveSpeed = 2;
	boolean hasJumped = false;
	Animation a;

	public double gunRotation = 0;
	Image st, hand;

	String name;
	int height = 64;
	int width = 32;
	Image playerHand;
	Weapon equipped = new Shovel();
	long lastFire = 0;
	long lastBullet = 0;
	Team team = Team.Red;

	long lastKilled = 0, lastJumpTime = 0;
	int respawnTime = 0;

	int kills = 0;
	int deaths = 0;

	boolean isFluid = false;


	String lastKiller = "";

	PlayerCharacter playerClass;

	static final int TerminalVelocity = 16;

	Image currentEmote;
	long lastEmoteTime = 0;

	public Player(double x, double y, String data, int id) {
		super(x, y, Sheets.EXCAVATOR_RED.getSprite(0, 0), id);
		String[] parts = data.split(",");
		this.name = parts[0];
		this.health = 100;
		a = new Animation(Sheets.EXCAVATOR_RED, 70);
		st = Sheets.EXCAVATOR_RED.getSubImage(0, 0);
		hand = Sheets.HANDS.getSubImage(0, 0);

	}
	public String getName() {
		return name;
	}

	public void setTeam(Team t) {
		if(this.team != t) {
			this.team = t;
			if(team == Team.Red){
				a = new Animation(playerClass.getRedSpriteSheet(), 70);
				st = playerClass.getRedSpriteSheet().getSubImage(0, 0);
			} else {
				a = new Animation(playerClass.getBlueSpriteSheet(), 70);
				st = playerClass.getBlueSpriteSheet().getSubImage(0, 0);
			}
		}
	}




	public Team getTeam() {
		return team;
	}

	public Weapon getWeapon() {
		return equipped;
	}

	public void setWeapon(Weapon w) {
		equipped = w;
	}

	public void setLastFireTime(long s) {
		lastFire = s;
	}

	public void setLastBulletTime(long s) {
		lastBullet = s;
	}

	public void setLastKiller(String s) {
		lastKiller = s;
	}

	public String getLastKiller() {
		return lastKiller;
	}
	public Image getPlayerHand() {
		return playerHand;
	}

	public boolean isDead() {
		if(this.health < 0) {
			return true;
		}
		return false;
	}

	public void setKills(int k) {
		kills = k;
	}

	public void setDeaths(int d) {
		deaths = d;
	}

	public int getKills() {
		return kills;
	}

	public int getDeaths() {
		return deaths;
	}

	public void setLastKilledTime(long l) {
		lastKilled = l;
	}

	public long getLastKilledTime() {
		return lastKilled;
	}

	public int getRespawnTime() {
		return respawnTime;
	}

	public void setEmote(Image img) {
		this.currentEmote = img;
	}

	public void setLastEmoteTime(long time) {
		this.lastEmoteTime = time;
	}

	public PlayerCharacter getCharacter() {
		return playerClass;
	}

	public void setCharacter(PlayerCharacter c) {
		playerClass = c;
	}

	public boolean isLocationValid(double x, double y, int dir) throws NullPointerException {
		double dx, dy;
		if(dir == 1) {
			dx = ((x + xspeed) - ((x+xspeed)%16)) + 32;
			dy = y - (y%16);
		} else {
			dx = ((x + xspeed) - ((x + xspeed)%16));
			dy = y - (y%16); 
		}
		Block brd = Grid.getClosestBlock((int)dx,(int) dy + height);
		if(brd == null) {
			if(y + height <= Grid.HighestLeftPoint) {
				return true;
			}
			return false;
		}

		if(x >= Settings.WorldWidth*16 - width - 10) {
			return true;
		}

		b =  brd.getBlockAdjacent(0, -1);

		for(int i = 0; i < ((int) (Math.ceil(height / 16))); i++) {
			if(b != null) {
				if(!b.getType().getPermiability(this)) {

					return false;
				}
			}
			b = b.getBlockAdjacent(0, -1);
		}
		return true;
	}

	public void step() {
		try {
			this.doesStep = false;
			boolean doMove = true;
			int dx = (int) (x - x%16);
			int dy = (int) (y - y%16);
			if(facingDirection == 0 || facingDirection == 1) {
				dx += 16;
			}

			float dv = 1.0f;
			boolean fluid = false;
			ArrayList<Block> cs = getCollidingBlocks();

			for(int i = 0; i < cs.size(); i++) {
				if(fluid == false && i > cs.size() - 3 && cs.get(i).getType().isFluid()) {
					dv = 1.0f;
					fluid = true;
					isFluid = true;
					i = cs.size();
				} else {
					if(cs.get(i).getType().isFluid()) {
						fluid = true;
						isFluid = true;
						if(this instanceof Player) {
							((Player) this).hasJumped = false;
						}
						String ns = b.getType().getName();
						if(ns.equals("Lava") || ns.equals("Water")) {
							dv = 0.02f;
						} else {
							dv = 0.05f;
						}
					}
				}
			}
			if(isFluid && dv == 1.0f) {
				isFluid = false;
				xspeed = 0;
			}

			b =  Grid.getClosestBlock(dx, dy + height);
			if(b != null && (x <= Settings.WorldWidth*16 - width && x > 0)) {
				Block ba = b.getBlockAdjacent(1, 0);
				if(ba == null) {
					this.x += -2*xspeed;
					xspeed = 0;
					return;
				}
				if(b != null && ba != null &&  (!b.getType().getPermiability(this) || !ba.getType().getPermiability(this)) && yspeed > 0) {
					doMove = false;
				} 
			}
			if((x < 0 && y + height >= Grid.HighestLeftPoint) || (x > Settings.WorldWidth*16 - width && y + height >= Grid.HighestRightPoint)) {
				doMove = false;
			}

			if(doMove) {
				yspeed += Settings.Gravity;

			} else {
				if(this instanceof Player) {
					((Player) this).hasJumped = false;

				}
				y -= (y - dy);


				yspeed = 0;

			}
			b = Grid.getClosestBlock(dx, dy);


			if(yspeed < 0.0 && b != null && (!b.getType().getPermiability(this) || !b.getBlockAdjacent(1, 0).getType().getPermiability(this))) {
				y -= 2*yspeed;
				yspeed = 0;
			} else {
				if(yspeed > TerminalVelocity) {
					y += TerminalVelocity*dv;
				} else {
					y += yspeed*dv;
				}
			}

			if(xspeed != 0) {
				if(isLocationValid(x, y, (int) (Math.abs(xspeed)/xspeed)) || (x < 0 || x > Settings.WorldWidth* 16)) {
					x += xspeed*dv;
					if(yspeed == 0) {
						if(Math.abs(xspeed) >= Settings.Friction) {
							if(xspeed < 0) {
								xspeed += Settings.Friction;
							} else {
								xspeed -= Settings.Friction;
							}

						} else {
							xspeed = 0;
						}
					}
				} else {
					xspeed = 0;
				}
			}

			NetObjects.Sender.sendPacket(new Packet59SendAbsolutePosition());

		} catch (Exception e) {
		}
	}

	public void jump() {
		boolean fluid = false;
		boolean exit = false;
		ArrayList<Block> cs = getCollidingBlocks();

		for(int i = 0; i < cs.size(); i++) {
			if(fluid == false && i > cs.size() - 3 && cs.get(i).getType().isFluid()) {
				exit = true;
				fluid = true;
				i = cs.size();
			} else {
				if(cs.get(i).getType().isFluid()) {
					fluid = true;
				}
			}
		}
		if(!hasJumped && (yspeed == 0 || fluid) && System.currentTimeMillis() - lastJumpTime >= 50) {
			y -= 3;
			yspeed = fluid ? -0.1*BonusItem.calculateBonus(BonusItemType.JumpHeight):-5*BonusItem.calculateBonus(BonusItemType.JumpHeight);
			yspeed = exit ? yspeed : -5*BonusItem.calculateBonus(BonusItemType.JumpHeight);
			hasJumped = true;
			lastJumpTime = System.currentTimeMillis();
		}

	}

	public void setPlayerCharacter(PlayerCharacter s) {
		playerClass = s;
		hand = playerClass.getHandImage();

		if(team == Team.Red){
			a = new Animation(playerClass.getRedSpriteSheet(), 70);
			st = playerClass.getRedSpriteSheet().getSubImage(0, 0);
		} else {
			a = new Animation(playerClass.getBlueSpriteSheet(), 70);
			st = playerClass.getBlueSpriteSheet().getSubImage(0, 0);
		}
	}
	public void setRespawnTime(int s) {
		respawnTime = s;
	}

	public long getLastFireTime() {
		return lastFire;
	}
	public void draw(Graphics g) {
		if(health == 0 || id < 0) return;
		a.setSpeed(1.3f);
		double sx = Math.round(x);
		double sy = Math.round(y);
		if(playerHand == null) {
			try {
				playerHand = new Image(200, 200);
			} catch (SlickException e) {
				e.printStackTrace();
			}
		}
		double dx, dy;
		int dir = facingDirection;
		if(dir == 1) {
			dx = (sx + width) - (sx%16);
			dy = sy - (sy%16);
		} else {
			dx = (sx - 2) - ((sx-2)%16);// + 16;
			dy = sy - (sy%16); 
		}
		if(Settings.DebugMode) {
			g.setColor(Color.black);
			g.setLineWidth(1.0f);
			g.drawRect((float) sx, (float) sy, img.getWidth(), img.getHeight());
			g.drawRect((float)dx,(float) dy, 16, 64);
			g.setFont(Fonts.SmallFont);
			g.drawString(""+id,(float) (sx + (width/4)) - Fonts.SmallFont.getWidth(""+id)/2, (float)(sy - 30));
		}
		if(facingDirection == -1) {
			a.draw((float)(Math.round(sx) + st.getWidth()), (float) Math.round(sy), -st.getWidth(), 64);
		} else if (facingDirection == 0) {
			st.draw((float)Math.round(sx), (float)Math.round(sy));
		} else if (facingDirection == 1) {
			a.draw((float)Math.round(sx), (float)Math.round(sy));
		} else {
			st.getFlippedCopy(true, false).draw((float)Math.round(sx), (float)Math.round(sy));
		} 

		Image wep;
		boolean fireImage = false;
		if(equipped == null) {
			equipped = new EmptyHand();
		}
		if(System.currentTimeMillis() - lastBullet <= 50) {
			wep = equipped.getFiringImage();
			fireImage = true;
		} else {
			wep = equipped.getImage();
		}

		Image handbase = hand;
		int hx = 13;
		int hy = 28;
		playerHand.setCenterOfRotation(hx + playerHand.getWidth() / 2, hy + handbase.getHeight()/2);
		equipped.reconfigure();
		float deg = (float) Math.toDegrees(gunRotation) + 90;
		if(facingDirection == 2 || facingDirection == -1) {
			deg *= 1;
		}
		playerHand.setRotation(deg);

		try {
			Graphics gh = playerHand.getGraphics();
			gh.clear();
			gh.drawImage(wep, (float) 100 + hx + handbase.getWidth() + equipped.getXOffset(), (float) hy + equipped.getYOffset()); 
			/*if(fireImage && equipped.doesLightUp()) {
				b =  Grid.getClosestBlock((int)dx,(int) dy + height/2);
				if(b != null && b.getType() == BlockType.Air && b.getBackgroundType() != BlockType.Air) {
					gh.setColor(new Color(255, 255, 255, 0.5f));
					gh.fillOval((float) (100 + hx + equipped.getXOffset() + equipped.getImage().getWidth()*.75), (float) (hy + equipped.getYOffset() - equipped.getImage().getHeight() - handbase.getHeight()/2), 50, 50);
				}
			}*/

			gh.drawImage(handbase, (float) 100 + hx, (float) hy);
		} catch (Exception e) {
			e.printStackTrace();
		}

		if(facingDirection == 2 || facingDirection == -1) {
			Image tbd = playerHand.getFlippedCopy(false, true);
			tbd.setRotation(deg);
			tbd.setCenterOfRotation(hx + playerHand.getWidth() / 2, hy + 41 + handbase.getHeight() / 2 + 100);
			g.drawImage(tbd, (float) sx + 6 - playerHand.getWidth()/2,  (float) sy - 141);

		} else {
			g.drawImage(playerHand, (float) sx - playerHand.getWidth() / 2, (float) sy);
		}

		long dsp = System.currentTimeMillis() - lastEmoteTime;
		if(currentEmote != null && dsp <= 5000) {
			float a = (float) (500 - dsp)/500.0f;
			float ty = -35;
			if(a >= 0) {
				currentEmote.setAlpha(1.0f - a);
				ty = -35.0f + (15.0f*a);
			}

			g.drawImage(currentEmote, (float) sx + 10, (float) sy + ty);
		}
		if(!name.equals(Settings.Username)) {
			g.setColor(new Color(0, 0, 0, 0.1f));
			g.setFont(Fonts.SmallFont);
			g.fillRect((float) sx  - (width / 4) - (Fonts.SmallFont.getWidth(name)/2) + 10,(float) sy - 20, Fonts.SmallFont.getWidth(name) + 10, Fonts.SmallFont.getHeight(name) + 10);
			g.setColor(Color.black);
			g.drawString(name, (float) sx + (width / 4) - (Fonts.SmallFont.getWidth(name) / 2),(float) sy - 15);

			if(getTeam() == Settings.ActivePlayer.getTeam()) {
				g.setColor(new Color(0, 90, 0, 0.5f));
				g.fillRect((float) sx  + (width / 4) - 20, (float) sy - 30, (getHealth()/(float)100)*40, 7);
				g.setColor(new Color(90, 0, 0, 0.5f));
				g.fillRect((float) sx  + (width / 4) - 20 + (getHealth()/(float)100)*40, (float) sy - 30, 40-(getHealth()/(float)100)*40, 7);

				g.setColor(Color.black);
				g.setLineWidth(1.0f);
				g.drawRect((float) sx + (width/4) - 20, (float) sy - 30, 40, 7);
			}
		}



	}

}
