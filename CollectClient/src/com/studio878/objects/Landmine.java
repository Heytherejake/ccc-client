package com.studio878.objects;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;

import com.studio878.inventory.ItemID;
import com.studio878.objects.EntityManager.EntityName;
import com.studio878.util.Sheets;
import com.studio878.util.SoundEffects;

public class Landmine extends HealthEntity {

	Image on, off;
	long lastBeep = 0;
	public boolean hasHealthChanged = false;
	public Landmine(double x, double y, int id) {
		super(x, y, ItemID.lookupName("Landmine").getAntiAliasedImage(), id);
		on = ItemID.lookupName("Landmine").getAntiAliasedImage();
		off = Sheets.ITEMS.getSubImage(0, 3);
		this.type = EntityName.Landmine;
		this.doesEmitBlood = false;
	}
	
	public void draw(Graphics g) {
		super.draw(g);
		if(state == 0) {
			this.img = off;
		} else if (state == 1) {
			if(((int)(System.currentTimeMillis()/1000))%2 == 0) {
				this.img = on;
				if(System.currentTimeMillis() - lastBeep >= 1000) {
					SoundEffects.playSound(SoundEffects.Landmine, (int) x, (int) y, 0.3f);
					lastBeep = System.currentTimeMillis();
				}
			} else {
				this.img = off;
			}
		
		} else if(state == 2) {
			this.img = on;
			if(System.currentTimeMillis() - lastBeep >= 250) {
				SoundEffects.playSound(SoundEffects.Landmine, (int) x, (int) y, 0.3f);
				lastBeep = System.currentTimeMillis();
			}
		}
		if(this.health != 0) {
			g.setColor(new Color(0, 90, 0, 0.6f));
			g.fillRect((float) x - width/2, (float) y - 10,  (int) (50*(this.health/25.0)), 4);
			g.setColor(new Color(90, 0, 0, 0.6f));
			g.fillRect((float) x - width/2 + (int) (50*(this.health/25.0)), (float) y - 10, 50 - (int) (50*(this.health/25.0)), 4);
			g.setColor(Color.black);
			g.setLineWidth(1.0f);
			g.drawRect((float) x - width/2, (float) y - 10, 50, 4);
			hasHealthChanged = true;
		}
	}

}
