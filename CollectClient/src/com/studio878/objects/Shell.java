package com.studio878.objects;

import com.studio878.util.Images;

public class Shell extends ClientSideEntity {

	long created;

	public Shell(double x, double y) {
		super(x, y, Images.shell);
		created = System.currentTimeMillis();
	}

	public void step() {
		super.step();
		if(yspeed > 0.5) {
			if(xspeed < 0) {
				rotation -= 2;
			} else {
				rotation += 2;
			}
		}
		if(System.currentTimeMillis() - created >=  20000) {
			EntityManager.unregisterEntity(this.getId());
		}
	}



}
