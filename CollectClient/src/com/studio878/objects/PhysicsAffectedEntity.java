package com.studio878.objects;


import org.newdawn.slick.Image;

import com.studio878.block.Block;
import com.studio878.util.Settings;


public class PhysicsAffectedEntity extends Entity {
	double yspeed = 0, xspeed = 0;
	Block b;
	public long timeSinceLastStep = 0;
	public boolean stepSound = true;
	double sx, sy;
	int step = 0;
	boolean doesStep = true;
	
	public PhysicsAffectedEntity(double x, double y, Image img, int id) {
		super(x, y, img, id);
		sx = x;
		sy = y;
	}

	public double getGoalX() {
		return sx;
	}

	public double getGoalY() {
		return sy;
	}
	
	public double getXSpeed() {
		return xspeed;
	}
	
	public double getYSpeed() {
		return yspeed;
	}
	
	public void setXSpeed(double d) {
		this.xspeed = d;
	}

	public void setYSpeed(double y) {
		this.yspeed = y;
	}
	
	public void stepTowardsGoal() {
		if(doesStep) {
			if(getStep() == 0) {
				this.x += (this.x - this.sx)/2;
				this.y += (this.y - this.sy)/2;
			} else {
				this.x = this.sx;
				this.y = this.sy;
			}
		}
	}

	public void setGoal(double x, double y) {
		this.sx = x;
		this.sy = y;
	}

	public int getStep() {
		int st = step;
		step++;
		if(step == 2) {
			step = 0;
		}
		return st;
	}


}
