package com.studio878.objects;

import org.newdawn.slick.Animation;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;

import com.studio878.ui.Fonts;
import com.studio878.util.Settings;

public class NPC extends HealthEntity {

	Animation movement;
	public NPC(double x, double y, Image i, Animation a, int id) {
		super(x, y, i, id);
		movement = a;
	}
	
	public void draw(Graphics g) {
		if(Settings.DebugMode) {
			g.setColor(Color.black);

			g.setFont(Fonts.SmallFont);
			g.drawString(""+id,(float) (sx + (width/4)) - Fonts.SmallFont.getWidth(""+id)/2, (float)(sy - 30));
		}
		if(facingDirection == -1) {
			movement.draw((float)(Math.round(sx) + img.getWidth()), (float) Math.round(sy), -img.getWidth(), 64);
		} else if (facingDirection == 0) {
			img.draw((float)Math.round(sx), (float)Math.round(sy));
		} else if (facingDirection == 1) {
			movement.draw((float)Math.round(sx), (float)Math.round(sy));
		} else {
			img.getFlippedCopy(true, false).draw((float)Math.round(sx), (float)Math.round(sy));
		} 
	}

}
