package com.studio878.objects;

import org.newdawn.slick.Graphics;

import com.studio878.objects.EntityManager.EntityName;
import com.studio878.ui.Fonts;
import com.studio878.util.Sheets;

public class Grenade extends PhysicsAffectedEntity {

	private long time;
	public Grenade(double x, double y, int id) {
		super(x, y, Sheets.PROJECTILES.getSubImage(0, 0), id);
		time = System.currentTimeMillis();
		stepSound = true;
		this.type = EntityName.Grenade;
	}
	
	public void draw(Graphics g) {
		super.draw(g);
		g.setFont(Fonts.SmallFont);
		g.drawString("" + ((int) (3 - Math.ceil(System.currentTimeMillis() - time)/1000) + 1), (float) x + 10, (float) y - 20);
	}

}
