package com.studio878.objects;


import com.studio878.block.Block;
import com.studio878.util.Images;
import com.studio878.util.Settings;
import com.studio878.world.Grid;

public class Bullet extends ClientSideEntity {

	public Bullet(double x, double y) {
		super(x, y, Images.bullet);
		this.width = 10;
		this.height = 1;
	}
	
	public void step() {
		boolean isDone = false;
			x += xspeed;
			y += yspeed;
			for(HealthEntity e: EntityManager.getHealthEntities()) {
				if(x >= e.getX() && x <= e.getX() + e.getWidth() && y >= e.getY() && y <= e.getY() + e.getHeight()) {
					if(e instanceof Player) {
						Player p = (Player) e;
						if(!p.getName().equals(Settings.ActivePlayer.getName())) {
							isDone = true;
						}
					} else {
						isDone = true;
					}
				}
			}

			Block b = Grid.getClosestBlock((int)  x, (int) y);
			if(b != null && !b.getType().getPermiability(this)){
				isDone = true;
			}
	
			if(x < 0 || y < 0 || x > Settings.WorldHeight*16 || y > Settings.WorldWidth*16) {
				isDone = true;
			}
		
			if(isDone) {
				EntityManager.unregisterEntity(this.id);
			}

	}

}
