package com.studio878.objects;

import org.newdawn.slick.Image;

public class HealthEntity extends PhysicsAffectedEntity {
	public HealthEntity(double x, double y, Image i, int id) {
		super(x, y, i, id);	
	}

	int health;
	boolean doesEmitBlood = true;

	public void damage(int amount) {
		health -= amount;
		if(health <= 0) {
			EntityManager.unregisterEntity(id);
			
		}
	}
	
	public void heal(int amount) {
		health += amount;
	}
	
	public void setHealth(int amount) {
		health = amount;
		if(health <= 0 && !(this instanceof Player)) {
			EntityManager.unregisterEntity(id);
		}
	}

	public boolean doesEmitBlood() {
		return doesEmitBlood;
	}
	public int getHealth() {
		return health;
	}
	
}
