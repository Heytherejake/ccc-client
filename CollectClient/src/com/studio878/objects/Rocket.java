package com.studio878.objects;


import org.newdawn.slick.Animation;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;

import com.studio878.block.BlockType;
import com.studio878.objects.EntityManager.EntityName;
import com.studio878.util.Sheets;
import com.studio878.world.Grid;

public class Rocket extends Entity {

	Animation a = new Animation(Sheets.ROCKET, 70);

	public Rocket(double x, double y, int id) {
		super(x, y, Sheets.ROCKET.getSubImage(0, 0), id);
		this.width = 32;
		this.height = 32;
		this.type = EntityName.Rocket;
	}


	public void draw(Graphics g) {
		Image i = a.getCurrentFrame();
		if(facingDirection == -1) {
			i = i.getFlippedCopy(true, false);
		}
		i.setRotation((float) Math.toDegrees(rotation) + 90);
		i.draw((float) x, (float) y);
		b =  Grid.getClosestBlock((int)x,(int) y);
		if(b != null && b.getType() == BlockType.lookupName("Air") && b.getBackgroundType() != BlockType.lookupName("Air")) {
			g.setColor(new Color(255, 255, 255, 0.1f));
			g.fillOval((float)x - 16, (float) y - 16, 50, 50);
		}


	}

}
