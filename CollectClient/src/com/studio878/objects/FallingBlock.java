package com.studio878.objects;

import com.studio878.block.BlockType;
import com.studio878.util.Sheets;
import com.studio878.world.Grid;

public class FallingBlock extends PhysicsAffectedEntity {

	BlockType block;
	
	public FallingBlock(double x, double y, int id, BlockType block) {
		super(x, y, Sheets.BLOCK_TEXTURES.getSubImage(0, (int)block.getPosition()*2), id);
		int xloc = (int) ((int)(x/16) % 2);
		int yloc = (int) ((int)(y/16) % 2);
		this.img = Grid.getClosestBlock((int) x, (int) y).getMainImage();
		//this.img = Sheets.BLOCK_TEXTURES.getSubImage(xloc, (int)block.getPosition()*2 + yloc);
		this.block = block;
	}


}
