package com.studio878.objects;



import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Rectangle;

import com.studio878.displays.MainGame;
import com.studio878.input.KeyBindings;
import com.studio878.ui.Fonts;
import com.studio878.util.Images;
import com.studio878.util.Settings;
import com.studio878.world.Camera;

public class Workbench extends Entity implements UsableEntity {

	public Workbench(double x, double y, int id) {
		super(x, y, Images.Workbench, id);
	}



	public void onSelected() {
		if(Math.sqrt(Math.pow(Settings.ActivePlayer.getX() - x, 2) + Math.pow(Settings.ActivePlayer.getY() - y, 2)) <= 150) {
			if(MainGame.isSelling) {
				MainGame.populateSellingList();
			} else {
				MainGame.populateCraftingList(MainGame.openCraftingType);
			}
			if(MainGame.openCraftingMenu == 0) {
				MainGame.openCraftingMenu = 1;
			} else if (MainGame.openCraftingMenu == 1) {
				MainGame.openCraftingMenu = 0;
			}
		}
	}

	public void draw(Graphics g) {
		super.draw(g);
		if(Math.sqrt(Math.pow(Settings.ActivePlayer.getX() - x, 2) + Math.pow(Settings.ActivePlayer.getY() - y, 2)) <= 150) {
			Rectangle r = new Rectangle((int) (x + Camera.X_OFFSET), (int) (y + Camera.Y_OFFSET), img.getWidth(), img.getHeight());
			if(r.contains(Mouse.getX(), Display.getParent().getHeight() - Mouse.getY())) {
				g.setColor(new Color(0, 130, 130));
				g.fillRect((float) x + img.getWidth()/2 - 16, (float) y - 50, 32, 32);
				g.setColor(new Color(230, 230, 230));
				g.fillRect((float) x + img.getWidth()/2 - 14, (float) y - 48, 28, 28);
				g.setFont(Fonts.ChatFont);
				g.setColor(Color.black);
				g.drawString(KeyBindings.getKeyName(KeyBindings.Use), (float) x + img.getWidth()/2 -  5.5f, (float) y - 44f);
				g.setColor(new Color(200, 200, 200, 0.75f));
				g.setLineWidth(1.0f);
				g.fillRect((float) x + img.getWidth()/2 - Fonts.ChatFont.getWidth("Workbench")/2 - 4, (float) y - 13, Fonts.ChatFont.getWidth("Workbench") + 8, 20);
				g.setColor(Color.black);

				g.drawRect((float) x + img.getWidth()/2 - Fonts.ChatFont.getWidth("Workbench")/2 - 4, (float) y - 13, Fonts.ChatFont.getWidth("Workbench") + 8, 20);

				g.setColor(Color.black);
				g.drawString("Workbench", (float) x + img.getWidth()/2 - Fonts.ChatFont.getWidth("Workbench")/2, (float) y - 13);
				//replace with use key
			}
		}
	}

}
