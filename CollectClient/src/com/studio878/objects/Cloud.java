package com.studio878.objects;

import org.newdawn.slick.Graphics;

import com.studio878.util.Images;

public class Cloud {

	float x, y;
	
	public Cloud(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public void step() {
		this.x += 0.01;
	}
	
	public float getX() {
		return x;
	}
	
	public void draw(Graphics g) {
		g.drawImage(Images.cloud, x, y);
	}
}
