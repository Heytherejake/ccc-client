package com.studio878.objects;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import org.newdawn.slick.Animation;

import com.studio878.block.BlockType;
import com.studio878.inventory.ItemID;
import com.studio878.util.Settings;
import com.studio878.util.Sheets;




public class EntityManager {

	public enum EntityName {
		Player (0),
		Grenade (1),
		Item (2),
		Rocket (3),
		Generator (4),
		FallingBlock (5),
		Landmine (6),
		Workbench (7),
		ResearchBench (8),
		GenericNPC (9),
		Turret (10),
		;

		private static HashMap<Integer, EntityName> idLookup = new HashMap<Integer, EntityName>();

		private int id;
		private EntityName(int i) {
			id = i;
		}

		public int getId() {
			return id;
		}

		public static EntityName lookupId(int i) {
			return idLookup.get(i);
		}
		static {
			for(EntityName t: values()) {
				idLookup.put(t.getId(), t);
			}
		}		
	}

	static ConcurrentHashMap<Integer, Entity> entities = new ConcurrentHashMap<Integer, Entity>();

	static int nextClientId = 999999;

	public static void registerEntity(Entity e, int id) {
		entities.put(id, e);
	}


	public static ArrayList<ClientSideEntity> getClientSideEntities() {
		ArrayList<ClientSideEntity> results = new ArrayList<ClientSideEntity>();

		for(Entry<Integer, Entity> e : entities.entrySet()) {
			if(e.getValue() instanceof ClientSideEntity) {
				results.add((ClientSideEntity) e.getValue());

			}
		}
		return results;
	}

	public static ArrayList<PhysicsAffectedEntity> getPhysicsAffectedEntities() {
		ArrayList<PhysicsAffectedEntity> results = new ArrayList<PhysicsAffectedEntity>();

		for(Entry<Integer, Entity> e : entities.entrySet()) {
			if(e.getValue() instanceof PhysicsAffectedEntity) {
				results.add((PhysicsAffectedEntity) e.getValue());

			}
		}
		return results;
	}
	public static int getNextHighestClientSideID() {
		return nextClientId++;
	}

	public static Player matchPlayer(String name) {
		ArrayList<Player> matchedPlayers = new ArrayList<Player>();
		ArrayList<Player> players = getPlayers();
		for(int i = 0; i < players.size(); i++) {
			String tgn = players.get(i).getName();
			if (tgn.length() > 0) {
				if (name.equalsIgnoreCase(tgn)) {
					return players.get(i);
				}
				if (tgn.toLowerCase().indexOf(name) != -1) {
					matchedPlayers.add(players.get(i));
				}
			}
		}
		try {
			return matchedPlayers.get(0);
		} catch (Exception e) {
			return null;
		}
	}


	public static ArrayList<HealthEntity> getHealthEntities() {
		ArrayList<HealthEntity> results = new ArrayList<HealthEntity>();
		for(int i = 0; i < entities.size(); i++) {
			if(entities.get(i) instanceof HealthEntity) {
				results.add((HealthEntity) entities.get(i));

			}
		}
		return results;
	}

	public static void removeEntity(int id) {
		entities.remove(id);
	}
	public static Entity getEntity(int id) {
		return entities.get(id);
	}
	public static void removePlayer(Player p) {
		entities.remove(p);
	}

	public static void clearEntities() {
		entities.clear();
	}

	public static void unregisterEntity(int e) {
		if(entities.get(e) != null) {
			entities.remove(e);
		}
	}
	public static ArrayList<Entity> getEntities() {
		ArrayList<Entity> results = new ArrayList<Entity>();
		for (Integer key : entities.keySet()) {
			results.add(entities.get(key));
		}
		return results;
	}

	public static void emitBlood(HealthEntity e, double x, double y, int amount) {
		if(e.doesEmitBlood) {
			for(int i = 0; i < amount; i++) {
				PhysicsParticle s = new PhysicsParticle(x + (Math.random()*20 - 20), y + (Math.random()*20 - 20) , Sheets.PARTICLES.getSubImage(0, 0), 10000 + ((int)  (Math.random()*1000)));
				s.setYSpeed(-1*(4 + ((int) Math.random()*4)));
				int dir = 1;
				if(Math.random() >= .5) {
					dir = -1;
				}
				s.setXSpeed(dir * (Math.random()*4 + 3));
				registerEntity(s, s.getId());
			}
		}
	}
	public static Entity cloneEntity(EntityName type, double x, double y, int id, String data) {
		try {
			switch(type) {
			case Player: 
				Entity e = new Player(x, y, data, id);
				if(data.equalsIgnoreCase(Settings.Username)) {
					Settings.ActivePlayer = (Player) e;
					Settings.CameraPlayer = Settings.ActivePlayer;
				}
				return e;
			case FallingBlock:
				return new FallingBlock(x, y, id, BlockType.lookupId(Integer.parseInt(data)));
			case Generator:
				return new Generator(x, y, data, id);
			case Rocket:
				return new Rocket(x, y, id);
			case Item:
				if(data.indexOf(";") != -1) {
					return new ItemEntity(x, y, ItemID.lookupId(Integer.parseInt(data.split(";")[0].split(",")[0])), Integer.parseInt(data.split(";")[0].split(",")[1]), id);
				} else {
					int amount;
					try {
						amount = Integer.parseInt(data.split(",")[1]);
					} catch (IndexOutOfBoundsException es) {
						amount = 1;
					}
					return new ItemEntity(x, y, ItemID.lookupId(Integer.parseInt(data.split(",")[0])), amount, id);
				}
			case Grenade:
				return new Grenade(x, y, id);
			case Landmine:
				return new Landmine(x, y, id);
			case ResearchBench:
				return new ResearchBench(x, y, id);
			case Workbench:
				return new Workbench(x, y, id);
			case GenericNPC:
				return new NPC(x, y, Sheets.RESEARCHER_BLUE.getSubImage(0, 0), new Animation(Sheets.RESEARCHER_BLUE, 70), id); 
			case Turret:
				return new Turret(x, y, id, data);
			}
		} catch (Exception e) {
			System.out.println("Invalid entity of type " + type + " with data " + data);
		}
		return null;
	}
	public static ArrayList<Player> getPlayers() {
		ArrayList<Player> results = new ArrayList<Player>();
		for(Entity e : getEntities()) {
			if(e instanceof Player) {
				results.add((Player) e);
			}
		}
		return results;
	}





}
