package com.studio878.objects;

import com.studio878.inventory.ItemID;
import com.studio878.objects.EntityManager.EntityName;

public class ItemEntity extends PhysicsAffectedEntity {

	ItemID ign;
	public ItemEntity(double x, double y, ItemID ign, int level, int id) {
		super(x, y, ign.getAntiAliasedImage(), id);
		this.img = ign.getAntiAliasedImage().getScaledCopy(0.75f);
		this.type = EntityName.Item;
	}


}
