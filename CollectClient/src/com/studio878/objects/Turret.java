package com.studio878.objects;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Polygon;
import org.newdawn.slick.geom.Rectangle;

import com.studio878.block.BlockType;
import com.studio878.ui.Fonts;
import com.studio878.util.Settings;
import com.studio878.util.Sheets;
import com.studio878.world.Camera;

public class Turret extends HealthEntity {

	Image base, head;
	String data;
	public Turret(double x, double y, int id, String data) {
		super(x, y, Sheets.BLOCK_TEXTURES.getSubImage(2, BlockType.lookupName("Crate").getPosition()*2), id);
		base = Sheets.BLOCK_TEXTURES.getSubImage(32, BlockType.lookupName("Crate").getPosition()*32 + 16, 16, 16);
		head = Sheets.BLOCK_TEXTURES.getSubImage(32,BlockType.lookupName("Crate").getPosition()*32, 32, 16).copy();
		head.setCenterOfRotation(16, 8);
		this.health = 500;
		this.data = data;
		this.doesEmitBlood = false;

	}

	public void draw(Graphics g) {
		head.setCenterOfRotation(9, 8);

		head.setRotation((float) rotation);
		g.drawImage(base, (float) sx + 5, (float) sy + 8);
		g.drawImage(head, (float) sx, (float) sy);
		if(Math.sqrt(Math.pow(Settings.ActivePlayer.getX() - x, 2) + Math.pow(Settings.ActivePlayer.getY() - y, 2)) <= 150) {
			Rectangle r = new Rectangle((int) (x + Camera.X_OFFSET), (int) (y + Camera.Y_OFFSET), img.getWidth(), img.getHeight());
			if(r.contains(Mouse.getX(), Display.getParent().getHeight() - Mouse.getY())) {
				g.setLineWidth(2.0f);
				Polygon ns = new Polygon();
				ns.addPoint((float) sx + 8, (float) sy - 5);
				ns.addPoint((float) sx + 12, (float) sy - 10);
				ns.addPoint((float) sx + 60, (float) sy - 10);
				ns.addPoint((float) sx + 60, (float) sy - 130);
				ns.addPoint((float) sx - 44, (float) sy - 130);
				ns.addPoint((float) sx - 44, (float) sy - 10);
				ns.addPoint((float) sx + 4, (float) sy - 10);
				
				g.setColor(new Color(160, 160, 160, 130));
				g.fill(ns);
				g.setColor(new Color(90, 90, 90, 140));
				g.draw(ns);
				g.setColor(Color.black);
				g.setFont(Fonts.SmallFont);
				String str = "Turret";
				g.drawString(str, (float)sx + 8 - Fonts.SmallFont.getWidth(str)/2, (float) sy - 125);
				g.setLineWidth(1.0f);
				g.drawLine((float)sx + 8 - Fonts.SmallFont.getWidth(str)/2, (float) sy - 112, (float)sx + 8 + Fonts.SmallFont.getWidth(str)/2, (float) sy - 112);
				str = "Health";
				g.drawString(str, (float)sx + 8 - Fonts.SmallFont.getWidth(str)/2, (float) sy - 105);
				double tsp = ((double)this.health/500.0);
				g.setColor(new Color(255, 255, 255, 0.6f));
				g.fillRect((float) sx - 40, (float) sy - 85, 96*(float)tsp, 10);
				g.setColor(Color.black);
				g.drawRect((float) sx - 40, (float) sy - 85, 96, 10);
				
				str = "Ammunition";
				g.drawString(str, (float)sx + 8 - Fonts.SmallFont.getWidth(str)/2, (float) sy - 70);
				tsp = ((double) Integer.parseInt(this.data)/200.0);
				g.setColor(new Color(255, 255, 255, 0.6f));
				g.fillRect((float) sx - 40, (float) sy - 50, 96*(float)tsp, 10);
				g.setColor(Color.black);
				g.drawRect((float) sx - 40, (float) sy - 50, 96, 10);

			}
		}

	}

}
