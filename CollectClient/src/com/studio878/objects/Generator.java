package com.studio878.objects;


import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;

import com.studio878.ui.Fonts;
import com.studio878.util.Settings;
import com.studio878.util.Sheets;

public class Generator extends HealthEntity{
	int height = 101;
	int width = 227;

	public Generator(double x, double y, String data, int id) {
		super(x, y, Sheets.GENERATOR.getSprite(0, 0), id);
		this.health = Integer.parseInt(data);
		this.doesEmitBlood = false;

	}

	public void draw(Graphics g) {

	
		Image tsd = img.copy();
		tsd.rotate((float) rotation);
		g.setColor(new Color(0, 120, 0, 0.6f));
		g.fillRect((float) x + (width/2) - 100, (float) y - 30, (float) (200*((double) this.health / (double) 2000)), 10);
		g.setColor(new Color(190, 0, 0, 0.6f));
		g.fillRect((float) x + (width/2) - 100 + (float) (200*((double) this.health / (double) 2000)), (float) y - 30, 200 - (float) (200*((double) this.health / (double) 2000)), 10);
		g.setColor(new Color(0, 0, 0, 0.8f));
		g.drawRect((float) x + (width/2) - 100, (float) y - 30, 200, 10);
		if(facingDirection > -1) {
			g.drawImage(tsd,(float) Math.round(x), (float)Math.round(y));
		} else {
			g.drawImage(tsd.getFlippedCopy(true, false),(float) Math.round(x), (float) Math.round(y));
		}
		if(Settings.DebugMode) {
			g.setFont(Fonts.SmallFont);
			g.drawString(""+id, (float) x + (width/2) - Fonts.SmallFont.getWidth(""+id)/2, (float) y - 30);
		}
	}
}
