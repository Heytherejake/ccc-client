package com.studio878.objects;

import org.newdawn.slick.Image;

public class FloatingParticle extends ClientSideEntity {

	long createTime;
	int length;
	public FloatingParticle(double x, double y, Image i, int length) {
		super(x, y, i);
		createTime = System.currentTimeMillis();
		this.length = length;
	}
	
	public void step() {
		this.y -= yspeed;
		if(System.currentTimeMillis() - createTime >=  length*1000) {
			EntityManager.unregisterEntity(this.getId());
		}
	}
	
	

}
