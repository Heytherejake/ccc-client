package com.studio878.objects;

import org.newdawn.slick.Animation;
import org.newdawn.slick.Graphics;

import com.studio878.util.Sheets;

public class Explosion extends ClientSideEntity{

	Animation a = new Animation(Sheets.EXPLOSION, 20);
	public Explosion(double x, double y) {
		super(x, y, Sheets.EXPLOSION.getSubImage(0, 0));
		a.setLooping(false);
	}
	
	public void draw(Graphics g) {
		a.draw((float) x, (float) y);
		if(a.isStopped()) {
			EntityManager.unregisterEntity(this.getId());
		}
	}

}
