package com.studio878.objects;



import java.util.ArrayList;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Rectangle;

import com.studio878.block.Block;
import com.studio878.objects.EntityManager.EntityName;
import com.studio878.ui.Fonts;
import com.studio878.util.Settings;
import com.studio878.world.Camera;
import com.studio878.world.Grid;

public class Entity {
	double x = -99999;
	double y = -99999;
	Image img;
	int id;
	Block b;
	int height;
	int width;
	int MoveSpeed = 2;
	int facingDirection = 0;
	int state = 0;
	double rotation = 0;
	String data = "";
	EntityName type;
	public Entity(double x, double y, Image i, int id) {
		this.x = x;
		this.y = y;
		img = i;
		EntityManager.registerEntity(this, id);
		this.id = id;
		height = img.getHeight();
		width = img.getWidth();
	}

	public boolean isLocationValid(int x, int y, int dir) {
		int dx, dy;
		if(dir == 1) {
			dx = (x + MoveSpeed) - ((x+MoveSpeed)%16);
			dy = y - (y%16);
		} else {
			dx = (x-MoveSpeed)- ((x-MoveSpeed)%16);
			dy = y - (y%16); 
		}
		b =  Grid.getClosestBlock(dx, dy + height).getBlockAdjacent(0, -1);
		if(dir == 1) {
			b = b.getBlockAdjacent(1,0);
		}
		for(int i = 0; i < ((int) (Math.ceil(height / 16))); i++) {
			if(b != null) {
				if(!b.getType().getPermiability(this)) {

					return false;
				}
			}
			b = b.getBlockAdjacent(0, -1);
		}
		return true;
	}

	public void setState(int s) {
		this.state = s;
	}
	
	public int getState() {
		return state;
	}

	public double getRotation() {
		return rotation;
	}
	
	public void setRotation(double t) {
		rotation = t;
	}
	public int getWidth() {
		return width;
	}
	
	public int getHeight() {
		return height;
	}
	public int getId() {
		return id;
	}
	public void draw(Graphics g) {
		if(this.id < 0) {
			return;
		}
		if(Settings.DebugMode) {
			g.setFont(Fonts.SmallFont);
			g.drawString(""+id, (float) x + (width/4) - Fonts.SmallFont.getWidth(""+id)/2, (float) y - 30);
		}
		Image tsd = img.copy();
		tsd.rotate((float) rotation);
		if(facingDirection > -1) {
			g.drawImage(tsd,(float) Math.round(x), (float)Math.round(y));
		} else {
			g.drawImage(tsd.getFlippedCopy(true, false),(float) Math.round(x), (float) Math.round(y));
		}
	}


	public void setFacingDirection(int d) {
		facingDirection = d;
	}
	
	public int getFacingDirection() {
		return facingDirection;
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}
	
	public ArrayList<Block> getCollidingBlocks() {
		ArrayList<Block> result = new ArrayList<Block>();
		int dx = (int) (x - (x%16));
		int dy = (int) (y - (y%16));

		for(int j = dy; j < dy + height; j+= 16) {
			for(int i = dx; i < dx + width; i+= 16) {
				Block bs = Grid.getClosestBlock(i, j);
				if(bs != null) {
					result.add(bs);
				}
			}
		}
		return result;
	}

	public void setX(double x) {
		this.x = x;
	}

	public void setY(double y) {
		this.y = y;
	}
	
	public EntityName getType() {
		return type;
	}
	
	public String getData() {
		return data;
	}
	
	public void setData(String s) {
		this.data = s;
	}
	
	public boolean isRollover() {
		Rectangle r = new Rectangle((int) (x + Camera.X_OFFSET), (int) (y + Camera.Y_OFFSET), img.getWidth(), img.getHeight());
		return r.contains(Mouse.getX(), Display.getParent().getHeight() - Mouse.getY());
	}

}
