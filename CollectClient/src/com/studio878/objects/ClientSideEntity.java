package com.studio878.objects;

import java.util.ArrayList;

import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Rectangle;

import com.studio878.block.Block;
import com.studio878.io.NetObjects;
import com.studio878.packets.Packet59SendAbsolutePosition;
import com.studio878.util.Settings;
import com.studio878.world.Grid;



public class ClientSideEntity extends Entity{

	double yspeed = 0;
	double xspeed = 0;
	Block b;
	int facingDirection;
	static final int TerminalVelocity = 10;
	double prevx = 0;
	double prevy = 0;
	boolean doesBounce = false;
	boolean isFluid = false;

	public ClientSideEntity(double x, double y, Image i) {
		super(x, y, i, EntityManager.getNextHighestClientSideID());
	}

	public void setFacingDirection(int d) {
		facingDirection = d;
	}

	public int getFacingDirection() {
		return facingDirection;
	}


	public void setVelocity(double x, double y) {
		xspeed = x;
		yspeed = y;
	}
	public double getXSpeed() {
		return xspeed;
	}
	public double getYSpeed() {
		return yspeed;
	}

	public void setXSpeed(double x) {
		xspeed = x;
	}

	public void setYSpeed(double y) {
		yspeed = y;
	}
	public boolean isLocationValid(double x, double y, int dir) throws NullPointerException {
		double dx, dy;
		if(dir == 1) {
			dx = ((x + xspeed) - ((x+xspeed)%16)) + 32;
			dy = y - (y%16);
		} else {
			dx = ((x + xspeed) - ((x + xspeed)%16));
			dy = y - (y%16); 
		}

		b =  Grid.getClosestBlock((int)dx,(int) dy + height).getBlockAdjacent(0, -1);
		for(int i = 0; i < ((int) (Math.ceil(height / 16))); i++) {
			if(b != null) {
				if(!b.getType().getPermiability(this)) {
					return false;

				}
			}
			b = b.getBlockAdjacent(0, -1);
		}
		return true;
	}

	public void moveTo(double xs, double ys) {
		this.x = xs;
		this.y = ys;
	}
	public void step() {
		try {
			boolean doMove = true;
			boolean offMap = false;
			int dx = (int) (x - x%16);
			int dy = (int) (y - y%16);
			if(facingDirection == 0 || facingDirection == 1) {
				dx += 16;
			}

			float dv = 1.0f;
			boolean fluid = false;
			ArrayList<Block> cs = getCollidingBlocks();

			for(int i = 0; i < cs.size(); i++) {
				if(fluid == false && i > cs.size() - 3 && cs.get(i).getType().isFluid()) {
					dv = 1.0f;
					fluid = true;
					isFluid = true;
					i = cs.size();
				} else {
					if(cs.get(i).getType().isFluid()) {
						fluid = true;
						isFluid = true;

						String ns = b.getType().getName();
						if(ns.equals("Lava") || ns.equals("Water")) {
							dv = 0.02f;
						} else {
							dv = 0.05f;
						}
					}
				}
			}
			if(isFluid && dv == 1.0f) {
				isFluid = false;
				xspeed = 0;
			}

			b =  Grid.getClosestBlock(dx, dy + height);
			if(b != null && (x <= Settings.WorldWidth*16 - width && x > 0)) {
				Block ba = b.getBlockAdjacent(1, 0);
				if(ba == null) {
					this.x += -2*xspeed;
					xspeed = 0;
					return;
				}
				if(b != null && ba != null &&  (!b.getType().getPermiability(this) || !ba.getType().getPermiability(this)) && yspeed > 0) {
					doMove = false;
				} 
			}
			if((x < 0 && y + height >= Grid.HighestLeftPoint) || (x > Settings.WorldWidth*16 - width && y + height >= Grid.HighestRightPoint)) {
				doMove = false;
				offMap = true;
			}

			if(doMove) {
				yspeed += Settings.Gravity;

			} else {
				if(!offMap) {
					y -= (y - dy);
				}

				yspeed = 0;

			}
			b = Grid.getClosestBlock(dx, dy);


			if(yspeed < 0.0 && b != null && (!b.getType().getPermiability(this) || !b.getBlockAdjacent(1, 0).getType().getPermiability(this))) {
				y -= 2*yspeed;
				yspeed = 0;
			} else {
				if(yspeed > TerminalVelocity) {
					y += TerminalVelocity*dv;
				} else {
					y += yspeed*dv;
				}
			}

			if(xspeed != 0) {
				if(isLocationValid(x, y, (int) (Math.abs(xspeed)/xspeed)) || (x < 0 || x > Settings.WorldWidth* 16)) {
					x += xspeed*dv;
					if(yspeed == 0) {
						if(Math.abs(xspeed) >= Settings.Friction) {
							if(xspeed < 0) {
								xspeed += Settings.Friction;
							} else {
								xspeed -= Settings.Friction;
							}

						} else {
							xspeed = 0;
						}
					}
				} else {
					xspeed = 0;
				}
			}

			NetObjects.Sender.sendPacket(new Packet59SendAbsolutePosition());

		} catch (Exception e) {
		}
	}


	public boolean intersect(Rectangle r1, Rectangle r2) {
		int tx1 = (int) r1.getX();
		int ty1 = (int) r1.getY();
		int rx1 = (int) r2.getX();
		int ry1 = (int) r2.getY();
		long tx2 = tx1; tx2 += r1.getWidth();
		long ty2 = ty1; ty2 += r1.getHeight();
		long rx2 = rx1; rx2 += r2.getWidth();
		long ry2 = ry1; ry2 += r2.getHeight();
		if (tx1 < rx1) tx1 = rx1;
		if (ty1 < ry1) ty1 = ry1;
		if (tx2 > rx2) tx2 = rx2;
		if (ty2 > ry2) ty2 = ry2;
		tx2 -= tx1;
		ty2 -= ty1;
		if (tx2 < Integer.MIN_VALUE) tx2 = Integer.MIN_VALUE;
		if (ty2 < Integer.MIN_VALUE) ty2 = Integer.MIN_VALUE;
		Rectangle r =  new Rectangle  (tx1, ty1, (int) tx2, (int) ty2);
		return (r.getWidth() != 0 && r.getHeight() != 0);
	}
}
