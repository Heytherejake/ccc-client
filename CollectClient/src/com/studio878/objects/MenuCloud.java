package com.studio878.objects;

public class MenuCloud {

	float x, speed;
	float y = 2000;
	public MenuCloud(float x, float speed) {
		this.x = x;
		this.speed = speed;
	}
	
	public void move() {
		if(this.y > -500) {
		this.y -= speed;
		}
	}
	
	public float getX() {
		return x;
	}
	
	public float getY() {
		return y;
	}
}
