package com.studio878.threads;

import com.studio878.util.Settings;

public class PlayerMovementThread extends Thread{

	public void run() {
		while(true) {
			try {
				if(Settings.ActivePlayer != null && Settings.CalculateMovement) {
					Settings.ActivePlayer.step();
				}
				this.sleep(10);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
