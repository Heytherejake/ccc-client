package com.studio878.threads;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.SocketException;
import java.net.URL;
import java.net.URLClassLoader;

import com.studio878.displays.Menu;
import com.studio878.util.Console;
import com.studio878.util.Settings;

public class ServerThread extends Thread{

	boolean hasRun = false;
	private URLClassLoader ucl = null;
	public boolean succeeded = true;
	public void run() {
		while(!isInterrupted()) {
			if(!hasRun) {
				try {
					try {
						ucl = new URLClassLoader(new URL[]{new File(Settings.SystemFolder, "ccc_server.jar").toURI().toURL()}, Thread.currentThread().getContextClassLoader());



					} catch (MalformedURLException mce) {
						Menu.setDisplayMessage("Invalid ccc_server.jar. Delete the file, then try again.");
						return;
					}
					Class ctc;
					try {
					ctc = ucl.loadClass("com.studio878.server.app.Main");
					ctc.newInstance();
					} catch (Exception e) {
						Menu.setDisplayMessage("Invalid ccc_server.jar. Delete the file, then try again.");
						return;
					}
					try {
						FileWriter fw = new FileWriter(new File(Settings.SystemFolder, "ops.txt"));
						BufferedWriter w = new BufferedWriter(fw);
						w.write(Settings.Username);
						w.newLine();
						w.close();
						fw.close();
					} catch (IOException e) {
						Console.write("An error occured saving the operator list.");
					}

					Class csd = ucl.loadClass("com.studio878.server.logging.Console");

					Method mc = csd.getDeclaredMethod("setMessageQueue", Class.forName("java.util.concurrent.CopyOnWriteArrayList"));
					Object oc = mc.invoke(csd, Console.read());
					Method m = ctc.getDeclaredMethod("main", new Class[]{Class.forName("[Ljava.lang.String;")});
					String[] sts = {""};
					Object o = m.invoke(ctc.newInstance(), new Object[]{sts});


				} catch (NullPointerException e) {
					Menu.setDisplayMessage("Server port already in use");
					this.interrupt();
					succeeded = false;
				} catch (Exception e) {
					e.printStackTrace();
					Menu.setDisplayMessage("Error launching server: " + e.getCause().toString());
					this.interrupt();
					succeeded = false;
				}
			}
		}
	}


	public void shutdown() {
		try {
			Class ctc = ucl.loadClass("com.studio878.server.app.Main");
			ctc.newInstance();
			Method m = ctc.getDeclaredMethod("softShutdown", new Class[]{});
			String[] sts = {""};
			Object o = m.invoke(ctc.newInstance(), new Object[]{});
			Console.write("Shutting down server...");
		} catch (Exception e) {

		}
	}
}
