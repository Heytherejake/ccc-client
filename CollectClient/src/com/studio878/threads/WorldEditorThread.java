package com.studio878.threads;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.SocketException;
import java.net.URL;
import java.net.URLClassLoader;

import com.studio878.app.Main;
import com.studio878.displays.Menu;
import com.studio878.util.Console;
import com.studio878.util.Settings;

public class WorldEditorThread extends Thread{

	boolean hasRun = false;
	private URLClassLoader ucl = null;
	public boolean succeeded = true;
	public void run() {
		while(!isInterrupted()) {
			if(!hasRun) {
				try {
					try {
						ucl = new URLClassLoader(new URL[]{new File(Settings.SystemFolder, "CCCWorldEditor.jar").toURI().toURL()}, Thread.currentThread().getContextClassLoader());



					} catch (MalformedURLException mce) {
						Menu.setDisplayMessage("Invalid CCCWorldEditor.jar. Delete the file, then try again.");
						mce.printStackTrace();

						return;
					}
					Class ctc;
					try {

					ctc = ucl.loadClass("com.studio878.editor.Main");
					ctc.newInstance();
			//		Main.canvas.dispose();
					Main.frame.dispose();
					Method m = ctc.getDeclaredMethod("main", new Class[]{Class.forName("[Ljava.lang.String;")});
					String[] sts = {""};

					Object o = m.invoke(ctc.newInstance(), new Object[]{sts});
					System.out.println("6");

					} catch (Exception e) {
						Menu.setDisplayMessage("Invalid CCCWorldEditor.jar. Delete the file, then try again.");
						e.printStackTrace();

						return;
					}

				} catch (Exception e) {
					e.printStackTrace();
					Menu.setDisplayMessage("Error launching world editor " + e.getCause().toString());
					this.interrupt();
					succeeded = false;

				}
			}
		}
	}


	public void shutdown() {
		try {
		/*	Class ctc = ucl.loadClass("com.studio878.server.app.Main");
			ctc.newInstance();
			Method m = ctc.getDeclaredMethod("softShutdown", new Class[]{});
			String[] sts = {""};
			Object o = m.invoke(ctc.newInstance(), new Object[]{});
			Console.write("Shutting down server...");*/
		} catch (Exception e) {

		}
	}
}
