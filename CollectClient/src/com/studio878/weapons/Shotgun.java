package com.studio878.weapons;

import org.newdawn.slick.GameContainer;

import com.studio878.inventory.ItemID;
import com.studio878.io.NetObjects;
import com.studio878.packets.Packet23FireWeapon;
import com.studio878.util.Sheets;

public class Shotgun extends Weapon{

	public Shotgun() {
		xOffset = -10;
		yOffset = -5;
		type = ItemID.lookupName("Shotgun");
		img = Sheets.WEAPONS.getSubImage(1, 1).getSubImage(0, 0, 64, 21).copy();
		fireImage = Sheets.WEAPONS_FIRING.getSubImage(1, 1).getSubImage(0, 0, 64, 21).copy();

		time = 2000;
	}
	
	public void onFire(double angle, int msx, int msy, GameContainer c) {
		NetObjects.Sender.sendPacket(new Packet23FireWeapon(angle));
	}
	public void reconfigure() {
		xOffset = -10;
		yOffset = -5;
	}
}
