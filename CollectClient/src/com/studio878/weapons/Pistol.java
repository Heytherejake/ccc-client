package com.studio878.weapons;

import org.newdawn.slick.GameContainer;

import com.studio878.inventory.ItemID;
import com.studio878.io.NetObjects;
import com.studio878.packets.Packet23FireWeapon;
import com.studio878.util.Sheets;

public class Pistol extends Weapon{

	public Pistol() {
		xOffset = -10;
		yOffset = -5;
		type = ItemID.lookupName("Pistol");
		img = Sheets.WEAPONS.getSubImage(0, 0).getSubImage(0, 0, 30, 16).copy();
		fireImage = Sheets.WEAPONS_FIRING.getSubImage(0, 0).getSubImage(0, 0, 30, 16).copy();
		time = 500;
	}
	
	public void onFire(double angle, int msx, int msy, GameContainer c) {
		NetObjects.Sender.sendPacket(new Packet23FireWeapon(angle));
	}
	public void reconfigure() {
		xOffset = -12;
		yOffset = -5;
	}
}
