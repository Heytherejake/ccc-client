package com.studio878.weapons;

import org.newdawn.slick.GameContainer;

import com.studio878.inventory.ItemID;
import com.studio878.util.Sheets;

public class Hatchet extends Weapon{

	public Hatchet() {
		xOffset = -7;
		yOffset = -24;
		type = ItemID.lookupName("Hatchet");
		time = 5;
		img = Sheets.WEAPONS.getSubImage(1, 4).copy();
		fireImage = Sheets.WEAPONS.getSubImage(1, 4).copy();
		doesLight = false;
		isMelee = true;

	}
	public void onFire(double angle, int msx, int msy, GameContainer c) {		
	}
	
	public void reconfigure() {
		xOffset = -7;
		yOffset = -24;
	}
}
