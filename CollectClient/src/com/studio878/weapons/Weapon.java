package com.studio878.weapons;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Image;

import com.studio878.inventory.ItemID;



public class Weapon {

	int xOffset = 0;
	int yOffset = 0;
	int time = 50;
	Image img, fireImage;
	ItemID type;

	boolean isMelee = false;

	boolean doesLight = true;
	boolean doesDropShell = true;

	public int getXOffset() {
		return xOffset;
	}

	public ItemID getType() {
		return type;
	}

	public Image getImage() {
		return img;
	}

	public boolean doesLightUp() {
		return doesLight;
	}

	public boolean isMelee() {
		return isMelee;
	}

	public int getYOffset() {
		return yOffset;
	}

	public boolean doesDropShell() {
		return doesDropShell;
	}
	public Image getFiringImage() {
		return fireImage;
	}

	public long getTimeOffset() {
		return time;
	}

	public void reconfigure() {

	}

	public void onFire(double angle, int msx, int msy, GameContainer c) {

	}

	public static Weapon matchWeapon(int i) {
		String name = ItemID.lookupId(i).getReferenceName();
		if(name.equals("Pistol")) {
			return new Pistol();
		} else if (name.equals("Shovel")) {
			return new Shovel();
		} else if (name.equals("Submachine Gun")) {
			return new SMG();
		} else if (name.equals("Shotgun")) {
			return new Shotgun();
		} else if (name.equals("Sniper Rifle")) {
			return new SniperRifle();
		} else if (name.equals("Rocket Launcher")) {
			return new RocketLauncher();
		} else if (name.equals("Flintlock Pistol")) {
			return new FlintlockPistol();
		} else if (name.equals("Wrench")) {
			return new Wrench();
		} else if (name.equals("Sword")) {
			return new Sword();
		} else if (name.equals("Hatchet")) {
			return new Hatchet();
			/*case VelocityGun:
			return new VelocityGun();*/
		} else {
			if(ItemID.lookupId(i).isBlock()) {
				return new BlockWeapon();
			}
			return null;
		}
	}

}
