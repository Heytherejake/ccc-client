package com.studio878.weapons;

import org.newdawn.slick.GameContainer;

import com.studio878.inventory.ItemID;
import com.studio878.util.Sheets;

public class Wrench extends Weapon{

	public Wrench() {
		xOffset = -10;
		yOffset = -5;
		type = ItemID.lookupName("Wrench");
		time = 5;
		img = Sheets.WEAPONS.getSubImage(1, 3).copy();
		fireImage = Sheets.WEAPONS.getSubImage(1, 3).copy();
		doesLight = false;
		isMelee = true;

	}
	public void onFire(double angle, int msx, int msy, GameContainer c) {
	/*	int mx = (int) Camera.convertXToCamera(msx);
		int my = (int) Camera.convertYToCamera(c, msy);
		Block b = Grid.getClosestBlock(mx, my);
		if(b != null) {
			NetObjects.Sender.sendPacket(new Packet06SendBlockDamage(b));
		}*/
		
	}
	public void reconfigure() {
		xOffset = -5;
		yOffset = -4;
	}
}
