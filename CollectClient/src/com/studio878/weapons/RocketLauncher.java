package com.studio878.weapons;

import org.newdawn.slick.GameContainer;

import com.studio878.inventory.ItemID;
import com.studio878.io.NetObjects;
import com.studio878.packets.Packet23FireWeapon;
import com.studio878.util.Sheets;

public class RocketLauncher extends Weapon{

	public RocketLauncher() {
		xOffset = -10;
		yOffset = -5;
		type = ItemID.lookupName("Rocket Launcher");
		img = Sheets.WEAPONS.getSubImage(0, 2).copy();
		fireImage = Sheets.WEAPONS_FIRING.getSubImage(0, 2).copy();
		time = 2000;
		doesDropShell = false;
	}
	
	public void onFire(double angle, int msx, int msy, GameContainer c) {
		NetObjects.Sender.sendPacket(new Packet23FireWeapon(angle));
	}
	public void reconfigure() {
		xOffset = -35;
		yOffset = -9;
	}
}
