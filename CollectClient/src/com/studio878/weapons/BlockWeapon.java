package com.studio878.weapons;

import org.newdawn.slick.Image;

import com.studio878.block.BlockType;
import com.studio878.util.Sheets;

public class BlockWeapon extends Weapon {

	BlockType type;

	public BlockWeapon() {
		this.xOffset = -10;
		this.yOffset = -3;
		type = BlockType.lookupName("Stone");
	}

	public void setType(BlockType type) {
		this.type = type;
	}

	public Image getImage() {
		if(type != null) {
			return Sheets.BLOCK_TEXTURES.getSubImage(0, (int) type.getPosition()*2);
		}
		return Sheets.BLOCK_TEXTURES.getSubImage(0, 0);
	}
}
