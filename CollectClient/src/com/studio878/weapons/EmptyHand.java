package com.studio878.weapons;

import org.newdawn.slick.GameContainer;

import com.studio878.util.Sheets;

public class EmptyHand extends Weapon{

	public EmptyHand() {
		xOffset = 0;
		yOffset = 0;
		type = null;
		img = Sheets.ITEMS.getSubImage(0, 0).getSubImage(0, 0, 1, 1);
		fireImage = Sheets.ITEMS.getSubImage(0, 0).getSubImage(0, 0, 1, 1);
		time = 99999;
	}
	
	public void onFire(double angle, int msx, int msy, GameContainer c) {
	}
	
	public void reconfigure() {
	}
}
