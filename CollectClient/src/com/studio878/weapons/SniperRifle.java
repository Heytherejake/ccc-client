package com.studio878.weapons;

import org.newdawn.slick.GameContainer;

import com.studio878.inventory.ItemID;
import com.studio878.io.NetObjects;
import com.studio878.packets.Packet23FireWeapon;
import com.studio878.util.Sheets;

public class SniperRifle extends Weapon{

	public SniperRifle() {
		xOffset = -14;
		yOffset = -5;
		type = ItemID.lookupName("Sniper Rifle");
		img = Sheets.WEAPONS.getSubImage(1, 2).copy();
		fireImage = Sheets.WEAPONS_FIRING.getSubImage(1, 2).copy();

		time = 2500;
	}
	
	public void onFire(double angle, int msx, int msy, GameContainer c) {
		NetObjects.Sender.sendPacket(new Packet23FireWeapon(angle));
	}
	public void reconfigure() {
		xOffset = -26;
		yOffset = -10;
	}
}
