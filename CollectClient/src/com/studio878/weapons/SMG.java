package com.studio878.weapons;

import org.newdawn.slick.GameContainer;

import com.studio878.inventory.ItemID;
import com.studio878.io.NetObjects;
import com.studio878.packets.Packet23FireWeapon;
import com.studio878.util.Sheets;

public class SMG extends Weapon{

	public SMG() {
		xOffset = -14;
		yOffset = -5;
		type = ItemID.lookupName("Submachine Gun");
		img = Sheets.WEAPONS.getSubImage(0, 1).getSubImage(0, 0, 30, 21).copy();
		fireImage = Sheets.WEAPONS_FIRING.getSubImage(0, 1).getSubImage(0, 0, 30, 21).copy();

		time = 150;
	}
	
	public void onFire(double angle, int msx, int msy, GameContainer c) {
		NetObjects.Sender.sendPacket(new Packet23FireWeapon(angle));
	}
	public void reconfigure() {
		xOffset = -14;
		yOffset = -5;
	}
}
