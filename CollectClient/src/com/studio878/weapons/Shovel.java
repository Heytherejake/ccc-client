package com.studio878.weapons;

import org.newdawn.slick.GameContainer;

import com.studio878.block.Block;
import com.studio878.inventory.ItemID;
import com.studio878.io.NetObjects;
import com.studio878.packets.Packet06SendBlockDamage;
import com.studio878.util.Sheets;
import com.studio878.world.Camera;
import com.studio878.world.Grid;

public class Shovel extends Weapon{

	public Shovel() {
		xOffset = -10;
		yOffset = -5;
		type = ItemID.lookupName("Shovel");
		time = 5;
		img = Sheets.WEAPONS.getSubImage(1, 0).getSubImage(0, 0, 43, 16).copy();
		fireImage = Sheets.WEAPONS.getSubImage(1, 0).getSubImage(0, 0, 43, 16).copy();
		doesLight = false;
		isMelee = true;

	}
	public void onFire(double angle, int msx, int msy, GameContainer c) {
		int mx = (int) Camera.convertXToCamera(msx);
		int my = (int) Camera.convertYToCamera(c, msy);
		Block b = Grid.getClosestBlock(mx, my);
		if(b != null) {
			NetObjects.Sender.sendPacket(new Packet06SendBlockDamage(b));
		}
		
	}
	public void reconfigure() {
		xOffset = -10;
		yOffset = -5;
	}
}
