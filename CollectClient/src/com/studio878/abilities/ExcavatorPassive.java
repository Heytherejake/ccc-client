package com.studio878.abilities;

import com.studio878.util.Images;
import com.studio878.util.Sheets;

public class ExcavatorPassive extends Ability {

	public ExcavatorPassive() {
		super("Archeologist's Favor", Sheets.ABILITY_ICONS.getSubImage(4, 0), 10, new int[]{-2, -2, -2, -2}, "Koen gains increased movement speed when below the surface.");
	}
}
