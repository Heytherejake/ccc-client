package com.studio878.abilities;

import com.studio878.util.Sheets;

public class Tactitian2HollowPoint extends Ability {

	public Tactitian2HollowPoint() {
		super("Hollow Point", Sheets.ABILITY_ICONS.getSubImage(1, 2), 10, new int[]{50, 60, 70, 80}, "Talon's attacks deal |5%/10%/15%/20%| additional damage for four seconds.");
	}
}
