package com.studio878.abilities;

import com.studio878.util.Images;
import com.studio878.util.Sheets;

public class Excavator4StonesThrow extends Ability {

	public Excavator4StonesThrow() {
		super("Stone's Throw", Sheets.ABILITY_ICONS.getSubImage(3, 0), 15, new int[]{160, 180, 200, 240}, "Koen launches all the stone he is carrying at an enemy, dealing |10/15/20| damage per block. If an enemy character is killed this way, Koen gains |1/1/2| coins for each piece of stone fired.");
		
	}
}
