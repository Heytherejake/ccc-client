package com.studio878.abilities;

import com.studio878.util.Sheets;

public class Tactitian1Momentum extends Ability {

	public Tactitian1Momentum() {
		super("Momentum", Sheets.ABILITY_ICONS.getSubImage(0, 2), 10, new int[]{50, 60, 70, 80}, "Talon's next shot knocks back an enemy.");
	}
}
