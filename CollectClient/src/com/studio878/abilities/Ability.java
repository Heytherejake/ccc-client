package com.studio878.abilities;

import org.newdawn.slick.Image;

public class Ability {
	
	String name;
	String desc;
	Image img;
	int cooldown;
	int[] manacost;
	long lastCast;
	
	public Ability(String name, Image img, int cooldown, int[] manacost, String desc) {
		this.name = name;
		this.desc = desc;
		this.img = img;
		this.cooldown = cooldown;
		this.manacost = manacost;
	}
	
	public void onCast() {
		
	}
	
	public String getName() {
		return name;
	}
	
	public String getDescription() {
		return desc;
	}
	
	public Image getImage() {
		return img;
	}
	
	public int getCooldown() {
		return cooldown;
	}
	
	public long getLastCastTime() {
		return lastCast;
	}
	
	public void setLastCastTime(long s) {
		lastCast = s;
	}
	
	
	public int getManaCost(int level) {
		return manacost[level];
	}

}
