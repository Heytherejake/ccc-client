package com.studio878.abilities;

import com.studio878.util.Sheets;

public class Researcher4Stargate extends Ability {

	public Researcher4Stargate() {
		super("Stargate", Sheets.ABILITY_ICONS.getSubImage(3, 1), 10, new int[]{50, 60, 70, 80}, "Yawln opens a rift, instantly transporting himself to the surface and dealing |100/200/300| (+0.3 INT) damage to all nearby enemies, while stunning them for 2 seconds.");
	}
}
