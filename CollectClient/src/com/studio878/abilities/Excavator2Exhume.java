package com.studio878.abilities;

import com.studio878.util.Images;
import com.studio878.util.Sheets;

public class Excavator2Exhume extends Ability {

	public Excavator2Exhume() {
		super("Exhume", Sheets.ABILITY_ICONS.getSubImage(1, 0), -1, new int[]{-1, -1, -1, -1}, "(Passive) Koen's dig speed is increased by |10%/20%/30%/40%|, and his digging radius is increased by |1/1/2/2| blocks.");
	}
}
