package com.studio878.abilities;

import com.studio878.util.Images;
import com.studio878.util.Sheets;

public class Researcher3DimensionVault extends Ability {

	public Researcher3DimensionVault() {
		super("Dimension Vault", Sheets.ABILITY_ICONS.getSubImage(2, 1), 10, new int[]{50, 60, 70, 80}, "Yawln teleports in a direction, destroying all blocks in his way. If Yawln collides with an enemy, Dimension Vault deals |50/100/150/200| damage and stund for 2.4 seconds, but has it's cooldown increased by 10 seconds.<br><br>Radius: |4/6/8/10| blocks.");
	}
}
