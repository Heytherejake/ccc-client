package com.studio878.abilities;

import com.studio878.util.Sheets;

public class Tactitian4RainOfBullets extends Ability {

	public Tactitian4RainOfBullets() {
		super("Rain of Bullets", Sheets.ABILITY_ICONS.getSubImage(3, 2), 10, new int[]{50, 60, 70, 80}, "Talon empties his entire magazine in one shot, dealing that magazine's total damage in addition to |300/450/550| (+0.3 STR) true damage.");
	}
}
