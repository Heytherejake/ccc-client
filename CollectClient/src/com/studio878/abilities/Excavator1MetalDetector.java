package com.studio878.abilities;

import com.studio878.util.Images;
import com.studio878.util.Sheets;

public class Excavator1MetalDetector extends Ability {

	public Excavator1MetalDetector() {
		super("Metal Detector", Sheets.ABILITY_ICONS.getSubImage(0, 0), 10, new int[]{50, 60, 70, 80}, "Koen locates precious metals, reavling them regardless of nearby lighting for 4 seconds. <br><br> Radius: |15/20/35/30| blocks");
	}
}
