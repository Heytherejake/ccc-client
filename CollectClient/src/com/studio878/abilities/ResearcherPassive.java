package com.studio878.abilities;

import com.studio878.util.Sheets;

public class ResearcherPassive extends Ability {

	public ResearcherPassive() {
		super("Scientific Prodigy", Sheets.ABILITY_ICONS.getSubImage(4, 1), 10, new int[]{-2, -2, -2, -2}, "Yawln can research for 50% coin cost.");
	}
}
