package com.studio878.abilities;

import com.studio878.util.Images;
import com.studio878.util.Sheets;

public class Excavator3DynamiteExcavation extends Ability {

	public Excavator3DynamiteExcavation() {
		super("Dynamite Excavation", Sheets.ABILITY_ICONS.getSubImage(2, 0), 15, new int[]{100, 115, 130, 150}, "Koen places a detonating charge, which has |50/100/100/150| health and detonates after 10 seconds, leaving behind a tall cater. Dynamite Excavation deals |200/250/300/350| damage to enemies or Koen caught in the blast charge, and stuns them for 1.5 seconds.");
		
	}
}
