package com.studio878.abilities;

import com.studio878.util.Sheets;

public class TactitianPassive extends Ability {

	public TactitianPassive() {
		super("Surefire Shot", Sheets.ABILITY_ICONS.getSubImage(4, 2), 10, new int[]{-2, -2, -2, -2}, "Talon's abilities can pierce through up to three layers of blocks.");
	}
}
