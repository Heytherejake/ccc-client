package com.studio878.abilities;

import com.studio878.util.Sheets;

public class Tactitian3FullMetalJacket extends Ability {

	public Tactitian3FullMetalJacket() {
		super("Full Metal Jacket", Sheets.ABILITY_ICONS.getSubImage(2, 2), 10, new int[]{50, 60, 70, 80}, "Talon fires a piercing shot, dealing |100/200/300/350| (+0.3 STR) damage. The resulting recoil launches him backwards.");
	}
}
