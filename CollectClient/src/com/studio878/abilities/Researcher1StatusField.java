package com.studio878.abilities;

import com.studio878.util.Images;
import com.studio878.util.Sheets;

public class Researcher1StatusField extends Ability {

	public Researcher1StatusField() {
		super("Status Field", Sheets.ABILITY_ICONS.getSubImage(0, 1), 10, new int[]{50, 60, 70, 80}, "Yawln places down a forcefield, which slows enemy movement by |10%/20%/30%/40%|, and deals |5/10/15/20| damage per second.<br><br>Radius: |5/7/10/12| blocks.");
	}
}
