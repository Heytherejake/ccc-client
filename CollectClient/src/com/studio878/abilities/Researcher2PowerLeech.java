package com.studio878.abilities;

import com.studio878.util.Images;
import com.studio878.util.Sheets;

public class Researcher2PowerLeech extends Ability {

	public Researcher2PowerLeech() {
		super("Power Leech", Sheets.ABILITY_ICONS.getSubImage(1, 1), 10, new int[]{50, 60, 70, 80}, "Yawln launches an inhibitor at an enemy character that decreases attack damage by |10%/10%/15%/15%| and movement speed by |10%/15%/15%/20%| for |2/3/3/4| seconds.");
	}
}
