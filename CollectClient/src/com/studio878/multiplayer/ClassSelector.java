package com.studio878.multiplayer;

import java.util.ArrayList;

import org.json.simple.JSONObject;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

import com.studio878.characters.PlayerCharacter;
import com.studio878.ui.Button;
import com.studio878.ui.ClassListItem;
import com.studio878.ui.Fonts;
import com.studio878.ui.ModifiedTextField;
import com.studio878.util.API;
import com.studio878.util.GraphicsUtil;
import com.studio878.util.Settings;

public class ClassSelector {

	Button cancelClass;
	Button selectClass;

	ModifiedTextField nameInput;

	boolean isOpen = false;

	public ClassSelector(GameContainer c) {
		cancelClass = new Button(100, 100, "Close", false);
		selectClass = new Button(100, 100, "Select", false);
		nameInput = new ModifiedTextField(c, Fonts.TitleFont, 0, 0, 300, 30, 4, "", false, true);
		nameInput.setLimit(20);
	}

	public void setOpen(boolean open) {
		isOpen = open;
	}


	public void draw(Graphics g, GameContainer c) {
		if(!isOpen) return;
		GraphicsUtil.drawShadowBox(g, c.getWidth()/2 - 400, c.getHeight()/2 - 300, 800, 600, 0);
		Settings.MainWindow.characterSelect.draw(g);
		g.setFont(Fonts.LargeFont);
		g.setColor(Color.black);
		g.drawString("Specialization Selection", c.getWidth()/2 - Fonts.LargeFont.getWidth("Specialization Selection")/2, c.getHeight()/2 - 290);
		if(Settings.MainWindow.characterSelect.getSelected() != null) {
			ClassListItem cs = (ClassListItem) Settings.MainWindow.characterSelect.getSelected();
			g.setFont(Fonts.BoldFont);
			g.drawString(cs.getCharacter().getFullName(), c.getWidth()/2 - 200, c.getHeight()/2 - 230);
			g.setFont(Fonts.ChatFont);
			ArrayList<String> lines = GraphicsUtil.splitBetweenLines(cs.getCharacter().getLore(), Fonts.ChatFont, 550);
			int fy = c.getHeight()/2 - 200;
			for(String s : lines) {
				Fonts.drawWithColor(s, Fonts.ChatFont, g, c.getWidth()/2 - 200, fy, 560);
				fy += 20;
			}

			fy += 50;
			int sfy = fy;
	

			cancelClass.draw(g, c);
			selectClass.draw(g, c);
			g.drawString("Name", c.getWidth()/2 + 50 - Fonts.TitleFont.getWidth("Name")/2, c.getHeight()/2 + 120);
			nameInput.draw(c, g);
		}
	}

	public void update(GameContainer c) {
		if(!isOpen) return;
		Settings.MainWindow.characterSelect.update(c);
		Settings.MainWindow.characterSelect.setX(c.getWidth()/2 - 400);
		Settings.MainWindow.characterSelect.setY(c.getHeight()/2 - 225);
		Settings.MainWindow.characterSelect.setHeight(525);
		cancelClass.setX(c.getWidth()/2 - 200);
		selectClass.setX(c.getWidth()/2 + 100);
		cancelClass.setY(c.getHeight()/2 + 200);
		selectClass.setY(c.getHeight()/2 + 200);
		cancelClass.check(c);
		nameInput.setPosition(c.getWidth()/2 - 100, c.getHeight()/2 + 150, 300, 30);

		selectClass.check(c);

		if(cancelClass.isSelected()) {
			isOpen = false;
		}

		if(selectClass.isSelected()) {
			isOpen = false;
			Settings.MainWindow.alertPopup.setMessage("Working...");
			Settings.MainWindow.alertPopup.setOpen(true);
			new Thread() {
				public void run() {
					String name = nameInput.getText();
					if(name.matches("[a-zA-Z0-9_.\\-]{5,20}")) {
						PlayerCharacter spec = ((ClassListItem) Settings.MainWindow.characterSelect.getSelected()).getCharacter();
						JSONObject result = API.getObject("http://api.studio878software.com/ccc/network/user/" + Settings.Username + "/characters/create/" + name + "/" + spec.getId() + "/" + Settings.Password);
						try {
							MultiplayerSettings.updateCharacters();
						} catch (Exception e) {
						}
						Settings.MainWindow.alertPopup.setOpen(false);
						Settings.MainWindow.eventPopup.setOpen(true);
						if(result != null) {
							String output = (String) result.get("message");
							if(output.equalsIgnoreCase("success")) {
								Settings.MainWindow.eventPopup.setMessage("Created " + name + ", the " + spec.getShortName());
							} else if (output.equalsIgnoreCase("name taken")) {
								Settings.MainWindow.eventPopup.setMessage("A character with this name already exists");
							} else if (output.equalsIgnoreCase("invalid login")) {
								Settings.MainWindow.eventPopup.setMessage("Unable to authenticate account credentials. Please restart Collect, Construct, Conquer!");
							}
						} else {
							Settings.MainWindow.eventPopup.setMessage("Unable to connect to the Studio878 API. Please try again later.");

						}
					} else {
						Settings.MainWindow.alertPopup.setOpen(false);
						Settings.MainWindow.eventPopup.setOpen(true);
						Settings.MainWindow.eventPopup.setMessage("Character name must be 5-20 characters, and may not contain some special characters or spaces.");

					}
				}
			}.start();
		}
	}

}
