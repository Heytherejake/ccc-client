package com.studio878.multiplayer;

import org.newdawn.slick.Animation;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SpriteSheet;

import com.studio878.characters.PlayerCharacter;
import com.studio878.multiplayer.LoadoutInventory.LoadoutItem;
import com.studio878.ui.Fonts;
import com.studio878.ui.TableList;
import com.studio878.ui.TableListItem;

public class MultiplayerCharacterListItem extends TableListItem {

	String name;
	boolean active;
	PlayerCharacter type;


	int loadoutSize = 3;

	public LoadoutItem[] Loadout;
	Image still, hand;
	Animation animation;

	public MultiplayerCharacterListItem(String name, boolean active, PlayerCharacter type, TableList parent) {
		super(name, 150, parent);
		this.name = name;
		this.type = type;
		this.active = active;
		animation = new Animation(new SpriteSheet(type.getRedSpriteSheet().getScaledCopy(2.0f), 64, 128), 50);
		still = type.getRedSpriteSheet().getSubImage(0, 0).getScaledCopy(2.0f);
		hand = type.getHandImage().getScaledCopy(2.0f);
		hand.setRotation(90);
		Loadout = new LoadoutItem[loadoutSize];
	}

	public void setLoadout(String loadout) {
		String[] split = loadout.split(",");
		for(int i = 0; i < split.length; i++) {
			System.out.println(split[i]);
			if(split[i].length() > 0) {
				Loadout[i] = LoadoutItem.lookupId(Integer.parseInt(split[i]));
			}
		}
	} 

	public String getName() {
		return name;
	}

	public PlayerCharacter getType() {
		return type;
	}

	public int getLoadoutSize() {
		return loadoutSize;
	}

	public void draw(Graphics g, int i) {
		if(isSelected()) {
			g.setColor(new Color(0, 90, 0, 0.1f));
			g.fillRect(0, i, parent.width, 150);
			g.setColor(Color.black);
			g.setLineWidth(1.0f);
			g.drawRect(0, i, parent.width - 1, 150);
			g.drawAnimation(animation, 10, i);
		} else {
			g.drawImage(still, 10, i);
		}
		g.drawImage(hand, 16, i + 74);
		g.setColor(Color.white);
		g.setFont(Fonts.ChatFont);
		g.drawString(name, 100, i + 30);
		g.setFont(Fonts.TitleFont);
		String tsd = type.getFullName();
		if(active) {
			tsd += " - Active";
		}
		g.drawString(tsd, 100, i + 60);

	}
}
