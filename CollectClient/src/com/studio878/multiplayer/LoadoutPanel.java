package com.studio878.multiplayer;

import java.awt.Point;
import java.text.DecimalFormat;
import java.util.Arrays;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

import com.studio878.app.Window;
import com.studio878.displays.MainGame;
import com.studio878.multiplayer.LoadoutInventory.LoadoutItem;
import com.studio878.ui.Button;
import com.studio878.ui.Fonts;
import com.studio878.ui.PromptPopup;
import com.studio878.ui.SizableButton;
import com.studio878.util.API;
import com.studio878.util.Images;
import com.studio878.util.Settings;
import com.studio878.util.Sheets;

public class LoadoutPanel {




	DecimalFormat fmt = new DecimalFormat("000,000,000");

	int openMenu = 0;
	int selectedStoreItem = 0;
	int queuedApiCall = 0;
	Button invButton, charButton, abilityButton, storeButton, newCharacter;
	SizableButton nextButton, prevButton, deleteCharacter, deleteItem, useItem, loadoutButton, setActiveCharacter, saveLoadout;

	MultiplayerCharacterListItem selectedCharacter; 

	ClassSelector classSelector;

	LoadoutInventory inventory = new LoadoutInventory(400, 100, 4, 5);


	public LoadoutPanel(GameContainer c) {
		invButton = new Button(100, 20, "Inventory", false);
		loadoutButton = new SizableButton(100, 140, 100, 50, "Loadout", false);
		charButton = new Button(100, 140, "Characters", false);
		abilityButton = new Button(100, 260, "Abilities", false);
		storeButton = new Button(100, 380, "Store", false);
		nextButton = new SizableButton(600, 650, 40, 25, "-->", false);
		prevButton = new SizableButton(400, 650, 40, 25, "<--", false);
		classSelector = new ClassSelector(c);
		newCharacter = new Button(700, 650, "New Character", false);
		deleteCharacter = new SizableButton(700, 650, 100, 50, "Delete", false);
		deleteItem = new SizableButton(700, 650, 100, 50, "Delete", false);
		useItem = new SizableButton(700, 650, 100, 50, "Use", false);
		setActiveCharacter = new SizableButton(100, 140, 200, 50, "Set Active", false);
		saveLoadout = new SizableButton(100, 140, 100, 50, "Save", false);

	}

	public void draw(Graphics g, GameContainer c) {

		for(int i = 400; i < c.getWidth(); i += 500) {
			for(int j = 0; j < c.getHeight(); j += 500) {
				g.drawImage(Images.StripeBackground, i, j);
			}
		}

		invButton.draw(g, c);
		charButton.draw(g, c);
		abilityButton.draw(g, c);
		storeButton.draw(g, c);

		int msx = c.getInput().getMouseX();
		int msy = c.getInput().getMouseY();
		switch(openMenu) {
		case 0: 
			nextButton.draw(c, g);
			prevButton.draw(c, g);

			useItem.draw(c, g);
			deleteItem.draw(c, g);
			inventory.draw(g, c);

			String fst = fmt.format(MultiplayerSettings.Money);

			for(int i = 0; i < fst.length(); i++) {
				int pos = 0;
				switch(fst.charAt(i)) {
				case '0':
					pos = 9;
					break;
				case  ',':
					pos = 10;
					break;
				default:
					pos = Integer.parseInt(fst.substring(i, i+1)) - 1;
				}

				g.drawImage(Sheets.FLIP_COUNTER.getSubImage(pos, 0), c.getWidth() - (30*10 -(i)*30) - 180, 20);

			}

			g.setColor(Color.white);
			g.setFont(Fonts.ChapterFont);
			g.drawString("$", c.getWidth() - 505, 22);
			g.drawString("Inventory", 450, 22);
			int size = (int) (Math.ceil(MultiplayerSettings.Inventory.size()/(double) (inventory.getRows() * inventory.getColumns())));
			if(size == 0) size = 1;
			g.setFont(Fonts.ChatFont);
			String pgn = inventory.getInventoryPage() + " / " + size;
			g.drawString(pgn, 400 + ((c.getWidth() - 400)/2) - Fonts.ChatFont.getWidth(pgn)/2, c.getHeight() - 91);
			break;
		case 1:
			g.setColor(Color.white);
			g.setFont(Fonts.ChapterFont);
			g.drawString("Characters", 450, 22);
			newCharacter.draw(g, c);
			deleteCharacter.draw(c, g);
			setActiveCharacter.draw(c, g);
			loadoutButton.draw(c, g);
			Settings.MainWindow.multiplayerCharacterList.draw(g);

			classSelector.draw(g, c); 
			break;
		case 2:
			g.setColor(Color.white);
			g.setFont(Fonts.ChapterFont);
			g.drawString("Abilities", 450, 22);
			try {
				Settings.MainWindow.multiplayerAbilityList.draw(g);
			} catch (ArrayIndexOutOfBoundsException e) {
			}


			break;
		case 3:
			fst = fmt.format(MultiplayerSettings.Money);

			for(int i = 0; i < fst.length(); i++) {
				int pos = 0;
				switch(fst.charAt(i)) {
				case '0':
					pos = 9;
					break;
				case  ',':
					pos = 10;
					break;
				default:
					pos = Integer.parseInt(fst.substring(i, i+1)) - 1;
				}

				g.drawImage(Sheets.FLIP_COUNTER.getSubImage(pos, 0), c.getWidth() - (30*10 -(i)*30) - 180, 20);

			}
			g.setColor(Color.white);
			g.setFont(Fonts.ChapterFont);
			g.drawString("$", c.getWidth() - 505, 22);
			g.drawString("Store", 450, 22);
			int i = 0;
			for(StoreItemButton item : MultiplayerSettings.StoreButtons) {
				item.setX(((c.getWidth() - 400) - (220*2) - 200)/2 + 400 + ((i%3)*220));
				item.setY(100 + (int) (Math.floor(i/3))*320);
				item.draw(c, g);
				i++;
			}
			break;
		case 4:
			g.setColor(Color.white);
			g.setFont(Fonts.ChapterFont);
			g.drawString("Loadout", 450, 22);
 
			g.drawImage(selectedCharacter.getType().getBlueSpriteSheet().getSubImage(0, 0).getScaledCopy(2.0f), 450, 75);
			g.setFont(Fonts.ChatFont);
			g.drawString(selectedCharacter.getName() + ", the " + selectedCharacter.getType().getShortName(), 520, 90);
			nextButton.draw(c, g);
			prevButton.draw(c, g);
			g.setColor(Color.white);
			g.setFont(Fonts.ChatFont);
			size = (int) (Math.ceil(MultiplayerSettings.Inventory.size()/(double) (inventory.getRows() * inventory.getColumns())));
			if(size == 0) size = 1;
			pgn = inventory.getInventoryPage() + " / " + size;
			g.drawString(pgn, 400 + ((c.getWidth() - 400)/2) - Fonts.ChatFont.getWidth(pgn)/2, c.getHeight() - 71);

			for(i = 0; i < selectedCharacter.loadoutSize; i++) {
				g.setColor(Color.white);
				g.setFont(Fonts.TitleFont);
				g.drawString("" + (selectedCharacter.loadoutSize - i), ((c.getWidth() - 400) - (120*(4)) - 100)/2 + 400 + (4*120) - (i*120) + 80, 190);

				g.drawImage(Sheets.ITEM_INSET_BOXES.getSubImage(0, 0), ((c.getWidth() - 400) - (120*(4)) - 100)/2 + 400 + (4*120) - (i*120), 115);
				if(selectedCharacter.Loadout[selectedCharacter.loadoutSize - i - 1] != null) {
					g.drawImage(selectedCharacter.Loadout[selectedCharacter.loadoutSize - i - 1].getImage(), ((c.getWidth() - 400) - (120*(4)) - 100)/2 + 400 + (4*120) - (i*120) + 18, 100 + 23);

					g.drawImage(Sheets.CLOSE_BUTTONS.getSubImage(0, 0), ((c.getWidth() - 400) - (120*(4)) - 100)/2 + 400 + (4*120) - (i*120) + 10, 100 + 25);
					if(msx >= ((c.getWidth() - 400) - (120*(4)) - 100)/2 + 400 + (4*120) - (i*120) + 10 && msx <= ((c.getWidth() - 400) - (120*(4)) - 100)/2 + 400 + (4*120) - (i*120) + 23 && msy >= 125 && msy <= 123 + 15) {
						if(c.getInput().isMouseButtonDown(0)) {
							selectedCharacter.Loadout[selectedCharacter.loadoutSize - i - 1] = null;
						}
					}
				}

			}
			saveLoadout.draw(c, g);
			inventory.draw(g, c);
			break;
		case 5:
			g.setColor(Color.white);
			g.setFont(Fonts.ChapterFont);
			g.drawString("Purchase", 450, 22);
			for(StoreUnlockCard cd : MultiplayerSettings.PreviousUnlocks) {
				cd.draw(g);
			}
		}
	}

	public void update(GameContainer c) {
		invButton.check(c);
		charButton.check(c);
		abilityButton.check(c);
		storeButton.check(c);
		prevButton.check(c);
		nextButton.check(c);
		loadoutButton.isDisabled(false);
		invButton.isDisabled(false);
		charButton.isDisabled(false);
		abilityButton.isDisabled(false);
		storeButton.isDisabled(false);
		if(inventory.getInventoryPage() == 1) {
			prevButton.isDisabled(true);
		} else {
			prevButton.isDisabled(false);
		}
		int size = (int) (Math.ceil(MultiplayerSettings.Inventory.size()/(double) (inventory.getRows() * inventory.getColumns())));
		if(size == 0) size = 1;

		if(inventory.getInventoryPage() == size) {
			nextButton.isDisabled(true);
		} else {
			nextButton.isDisabled(false);
		}
		if(prevButton.isSelected()) {
			inventory.setInventoryPage(inventory.getInventoryPage() - 1);
			inventory.setSelectedItem(-1);
		} else if(nextButton.isSelected()) {
			inventory.setInventoryPage(inventory.getInventoryPage() + 1);
			inventory.setSelectedItem(-1);

		}
		switch(openMenu) {
		case 0:
			try {
				inventory.DisableList.clear();

				invButton.isDisabled(true);
				prevButton.setX(400 + ((c.getWidth() - 400)/2) - 20 - 100);
				nextButton.setX(400 + ((c.getWidth() - 400)/2) - 20 + 100);
				prevButton.setY(c.getHeight() - 92);
				nextButton.setY(c.getHeight() - 92);
				deleteItem.setY(c.getHeight() - 100);
				deleteItem.setX(400 + ((c.getWidth() - 400)/2) - 20 + 200);
				deleteItem.check(c);
				useItem.setY(c.getHeight() - 100);
				useItem.setX(400 + ((c.getWidth() - 400)/2) - 20 - 250);
				useItem.check(c);
				useItem.isDisabled(true);
				if(inventory.getSelectedItem() != -1) {
					if(MultiplayerSettings.Inventory.get(inventory.getSelectedItem() + (inventory.getInventoryPage() - 1)*(inventory.getRows() * inventory.getColumns())).getSellValue() > 0) {
						deleteItem.setValue("Sell");
					} else {
						deleteItem.setValue("Delete");
					}
				}
				if(MultiplayerSettings.Inventory.size() <= inventory.getSelectedItem() + (inventory.getInventoryPage() - 1)*(inventory.getRows() * inventory.getColumns()) || inventory.getSelectedItem() == -1) {
					deleteItem.isDisabled(true);
				} else {
					if(inventory.getSelectedItem() != -1 && MultiplayerSettings.Inventory.get(inventory.getSelectedItem() + (inventory.getInventoryPage() - 1)*(inventory.getRows() * inventory.getColumns())).getSource().getUsableItems() != null) {
						useItem.isDisabled(false);
					}
					if(inventory.usingItem == null) {
						deleteItem.isDisabled(false);
					} else {
						deleteItem.isDisabled(true);
					}
				}
				if(inventory.usingItem != null ||  (inventory.getSelectedItem() + (inventory.getInventoryPage() - 1)*(inventory.getRows() * inventory.getColumns()) != -1 && MultiplayerSettings.Inventory.get(inventory.getSelectedItem() + (inventory.getInventoryPage() - 1)*(inventory.getRows() * inventory.getColumns())).getSource().getType() == 2)) {
					useItem.isDisabled(false);
				}

				if(useItem.isSelected()) {
					if(inventory.usingItem == null) {
						inventory.usingItem =  MultiplayerSettings.Inventory.get(inventory.getSelectedItem() + (inventory.getInventoryPage() - 1)*(inventory.getRows() * inventory.getColumns()));
						if(inventory.usingItem.getSource().getType() == 2) {
							Window.optionPopup.setMessage("Are you sure you would like to use a " + inventory.usingItem.getName() + "?");
							Window.optionPopup.setOption1Value("Cancel");
							Window.optionPopup.setOption2Value("Use");
							Window.optionPopup.setPanelMode(PromptPopup.UseItem);
							Window.optionPopup.setOpen(true);
						} else {
							useItem.setValue("Cancel");
							inventory.setSelectingDisabled(true);
							
						}
					} else {
						useItem.setValue("Use");
						inventory.usingItem = null;
						inventory.setSelectingDisabled(false);

					}

				}
				if(inventory.usingItem == null) {
					useItem.setValue("Use");
				}

				if(inventory.usingItem != null && inventory.usingItem.getSource().getUsableItems() != null) {
					Arrays.sort(inventory.usingItem.getSource().getUsableItems());
					for(LoadoutItem i : LoadoutItem.values()) {
						int index = Arrays.binarySearch(inventory.usingItem.getSource().getUsableItems(), i.getId());  
						if(index < 0) {
							inventory.DisableList.add(i);
						}
					}

				}

				if(deleteItem.isSelected()) {
					if(MultiplayerSettings.Inventory.get(inventory.getSelectedItem() + (inventory.getInventoryPage() - 1)*(inventory.getRows() * inventory.getColumns())).getSellValue() == 0) {
						Window.optionPopup.setMessage("Are you sure you would like to delete this " + MultiplayerSettings.Inventory.get(inventory.getSelectedItem() + (inventory.getInventoryPage() - 1)*(inventory.getRows() * inventory.getColumns())).getName() + "?");
						Window.optionPopup.setOption2Value("Delete");
					} else {
						Window.optionPopup.setMessage("Are you sure you would like to sell this " + MultiplayerSettings.Inventory.get(inventory.getSelectedItem() + (inventory.getInventoryPage() - 1)*(inventory.getRows() * inventory.getColumns())).getName() + " for $" + MultiplayerSettings.Inventory.get(inventory.getSelectedItem() + (inventory.getInventoryPage() - 1)*(inventory.getRows() * inventory.getColumns())).getSellValue() + "?");
						Window.optionPopup.setOption2Value("Sell");
					}
					Window.optionPopup.setOption1Value("Cancel");
					Window.optionPopup.setPanelMode(PromptPopup.DeleteItem);
					Window.optionPopup.setOpen(true);
				}
				if(Window.optionPopup.getPanelMode() == PromptPopup.DeleteItem) {
					if(Window.optionPopup.isOption1Selected()) {
						Window.optionPopup.setOpen(false);
					} else if (Window.optionPopup.isOption2Selected()) {
						Window.optionPopup.setOpen(false);
						Window.alertPopup.setOpen(true);
						Window.alertPopup.setMessage("Working...");
						new Thread() {
							public void run() {
								try {
									JSONObject item = API.getObject("http://api.studio878software.com/ccc/network/user/" + Settings.Username + "/inventory/sell/" + MultiplayerSettings.Inventory.get(inventory.getSelectedItem() + (inventory.getInventoryPage() - 1)*(inventory.getRows() * inventory.getColumns())).getId() + "/" + Settings.Password);
									String result = (String) item.get("message");
									MultiplayerSettings.updateInventory();
									MultiplayerSettings.updateCredits();
									Window.alertPopup.setOpen(false);
									Window.optionPopup.setOpen(false);
									Window.eventPopup.setOpen(true);
									if(result.equalsIgnoreCase("deleted")) {
										Window.eventPopup.setMessage("Item deleted.");
									} else if (result.indexOf("Sold for") != -1) {
										Window.eventPopup.setMessage("Item sold for $" + result.split(" ")[2] + ".");
									} else if (result.equalsIgnoreCase("missing item")) {
										Window.eventPopup.setMessage("This item is no longer in your inventory. It may have already been sold.");
									} else if (result.equalsIgnoreCase("invalid item")) {
										Window.eventPopup.setMessage("This is not a valid item.");
									} else if (result.equalsIgnoreCase("invalid login")) {
										Window.eventPopup.setMessage("Unable to authenticate account credentials. Please restart Collect, Construct, Conquer!");
									} else {
										Window.eventPopup.setMessage("An error occured deleting this character. Please try again later.");
									}
								} catch (Exception e) {
									Window.optionPopup.setOpen(false);
									Window.eventPopup.setOpen(true);
									Window.eventPopup.setMessage("Unable to connect to the Studio878 API. Please try again later.");
									Window.alertPopup.setOpen(false);
									e.printStackTrace();


								}

							}
						}.start();
					}
				}
			} catch (IndexOutOfBoundsException e) {
			}
			break;
		case 1:
			charButton.isDisabled(true);
			newCharacter.setY(c.getHeight() - 100);
			newCharacter.setX(c.getWidth() - 400);
			newCharacter.check(c);
			deleteCharacter.setY(c.getHeight() - 100);
			deleteCharacter.setX(c.getWidth() - 150);
			deleteCharacter.check(c);
			classSelector.update(c);
			Settings.MainWindow.multiplayerCharacterList.setDrawBackground(false);
			Settings.MainWindow.multiplayerCharacterList.setY(100);
			Settings.MainWindow.multiplayerCharacterList.setHeight(c.getHeight() - 120);
			Settings.MainWindow.multiplayerCharacterList.width = 300;
			Settings.MainWindow.multiplayerCharacterList.update(c);

			loadoutButton.setX(c.getWidth() - 150);
			loadoutButton.setY(c.getHeight() - 175);
			setActiveCharacter.setX(c.getWidth() - 400);
			setActiveCharacter.setY(c.getHeight() - 175);
			if(Settings.MainWindow.multiplayerCharacterList.getSelected() == null) {
				setActiveCharacter.isDisabled(true);
				loadoutButton.isDisabled(true);
			} else {
				setActiveCharacter.isDisabled(false);
				loadoutButton.isDisabled(false);
				setActiveCharacter.check(c);
				loadoutButton.check(c);

				if(loadoutButton.isSelected()) {
					selectedCharacter = ((MultiplayerCharacterListItem) Settings.MainWindow.multiplayerCharacterList.getSelected());
					inventory.setCol(5);
					inventory.setRows(3);
					inventory.setY(c.getHeight() - 450);
					inventory.setInventoryPage(1);
					inventory.setSelectedItem(-1);
					inventory.setUsageMode(LoadoutInventory.DragMode);
					inventory.setEventManager(new LoadoutInventoryEvent() {

						public void onDraggableItemRelease(MultiplayerInventoryObject object, int msx, int msy, GameContainer c) {

							for(int i = 0; i < selectedCharacter.loadoutSize; i++) {
								if(msx >= ((c.getWidth() - 400) - (120*(4)) - 100)/2 + 400 + (4*120) - (i*120) && msx <= ((c.getWidth() - 400) - (120*(4)) - 100)/2 + 400 + (4*120) - (i*120) + 100 && msy >= 115 && msy <= 215) {
									LoadoutPanel.this.selectedCharacter.Loadout[selectedCharacter.loadoutSize - i - 1] = object.getSource();

								}


							}

						}
					});

					openMenu = 4;
				}
				if(setActiveCharacter.isSelected()) {
					Window.alertPopup.setOpen(true);
					Window.alertPopup.setMessage("Working...");
					new Thread() {
						public void run() {
							try {
								JSONObject item = API.getObject("http://api.studio878software.com/ccc/network/user/" + Settings.Username + "/characters/active/set/" + ((MultiplayerCharacterListItem) Settings.MainWindow.multiplayerCharacterList.getSelected()).getName() + "/" + Settings.Password);
								String result = (String) item.get("message");
								MultiplayerSettings.updateCharacters();
								Window.alertPopup.setOpen(false);
								Window.optionPopup.setOpen(false);
								Window.eventPopup.setOpen(true);
								if(result.equalsIgnoreCase("success")) {
									Window.eventPopup.setOpen(false);
								} else if (result.equalsIgnoreCase("invalid login")) {
									Window.eventPopup.setMessage("Unable to authenticate account credentials. Please restart Collect, Construct, Conquer!");
								} else {
									Window.eventPopup.setMessage("An error occured deleting this character. Please try again later.");
								}
							} catch (Exception e) {
								e.printStackTrace();
								Window.optionPopup.setOpen(false);
								Window.eventPopup.setOpen(true);
								Window.eventPopup.setMessage("Unable to connect to the Studio878 API. Please try again later.");
								Window.alertPopup.setOpen(false);

							}

						}
					}.start();
				}
			}



			if(newCharacter.isSelected()) {
			//	MainGame.popualteClassList();
				classSelector.setOpen(true);
			}
			if(Settings.MainWindow.multiplayerCharacterList.items.size() > 1) {
				deleteCharacter.isDisabled(false);
			} else {
				deleteCharacter.isDisabled(true);
			}

			if(deleteCharacter.isSelected()) {
				Window.optionPopup.setMessage("Are you sure you would like to delete " + ((MultiplayerCharacterListItem) Settings.MainWindow.multiplayerCharacterList.getSelected()).getName() + "?");
				Window.optionPopup.setOption1Value("Cancel");
				Window.optionPopup.setOption2Value("Delete");
				Window.optionPopup.setPanelMode(PromptPopup.DeleteCharacter);
				Window.optionPopup.setOpen(true);
			}
			if(Window.optionPopup.getPanelMode() == PromptPopup.DeleteCharacter) {
				if(Window.optionPopup.isOption1Selected()) {
					Window.optionPopup.setOpen(false);
				} else if (Window.optionPopup.isOption2Selected()) {
					Window.optionPopup.setOpen(false);
					Window.alertPopup.setOpen(true);
					Window.alertPopup.setMessage("Working...");
					new Thread() {
						public void run() {
							try {
								JSONObject item = API.getObject("http://api.studio878software.com/ccc/network/user/" + Settings.Username + "/characters/delete/" + ((MultiplayerCharacterListItem) Settings.MainWindow.multiplayerCharacterList.getSelected()).getName() + "/" + Settings.Password);
								String result = (String) item.get("message");
								MultiplayerSettings.updateCharacters();
								Window.alertPopup.setOpen(false);
								Window.optionPopup.setOpen(false);
								Window.eventPopup.setOpen(true);
								if(result.equalsIgnoreCase("success")) {
									Window.eventPopup.setMessage("Character deleted.");
								} else if (result.equalsIgnoreCase("invalid login")) {
									Window.eventPopup.setMessage("Unable to authenticate account credentials. Please restart Collect, Construct, Conquer!");
								} else {
									Window.eventPopup.setMessage("An error occured deleting this character. Please try again later.");
								}
							} catch (Exception e) {
								Window.optionPopup.setOpen(false);
								Window.eventPopup.setOpen(true);
								Window.eventPopup.setMessage("Unable to connect to the Studio878 API. Please try again later.");
								Window.alertPopup.setOpen(false);

							}

						}
					}.start();
				}
			}
			break;
		case 2:
			abilityButton.isDisabled(true);
			Settings.MainWindow.multiplayerAbilityList.setDrawBackground(false);
			Settings.MainWindow.multiplayerAbilityList.setY(100);
			Settings.MainWindow.multiplayerAbilityList.setHeight(c.getHeight() - 100);
			Settings.MainWindow.multiplayerAbilityList.width = c.getWidth() - 440;
			Settings.MainWindow.multiplayerAbilityList.update(c);
			break;
		case 3:
			storeButton.isDisabled(true);
			for(StoreItemButton item : MultiplayerSettings.StoreButtons) {
				if(item.getItem().getCost() > MultiplayerSettings.Money) {
					item.isDisabled(true);
				} else {
					item.isDisabled(false);
				}
				if(!Window.optionPopup.isOpen() && !Window.alertPopup.isOpen() &&  !Window.eventPopup.isOpen()) {
					item.check(c);
					if(item.isSelected()) {
						selectedStoreItem = item.getItem().getId();
						Window.optionPopup.setMessage("Are you sure you would like to purchase " + item.getItem().getName() + "?");
						Window.optionPopup.setOption1Value("Cancel");
						Window.optionPopup.setOption2Value("Purchase");
						Window.optionPopup.setPanelMode(PromptPopup.ShopPurchase);
						Window.optionPopup.setOpen(true);
					}
				}
			}
			break;
		case 4:
			loadoutButton.isDisabled(true);

			prevButton.setX(400 + ((c.getWidth() - 400)/2) - 20 - 100);
			nextButton.setX(400 + ((c.getWidth() - 400)/2) - 20 + 100);
			prevButton.setY(c.getHeight() - 72);
			nextButton.setY(c.getHeight() - 72);
			saveLoadout.setX(c.getWidth() - 150);
			saveLoadout.setY(c.getHeight() - 80);
			saveLoadout.check(c);

			inventory.DisableList.clear();
			for(LoadoutItem o : selectedCharacter.Loadout) {
				if(o != null) {
					inventory.DisableList.add(o);
				}
			}
			for(LoadoutItem i : LoadoutItem.values()) {
				if(!inventory.DisableList.contains(i) && i.getType() != 1) {
					inventory.DisableList.add(i);
				}
			}


			if(saveLoadout.isSelected()) {
				Window.alertPopup.setOpen(true);
				Window.alertPopup.setMessage("Working...");
				new Thread() {
					public void run() {
						try {
							String pts = "";
							for(LoadoutItem o : selectedCharacter.Loadout) {
								if(o != null) {
									pts += o.getId() + ",";
								}
							}
							pts = pts.substring(0, pts.length() - 1);
							JSONObject item = API.getObject("http://api.studio878software.com/ccc/network/user/" + Settings.Username + "/characters/loadout/set/" + ((MultiplayerCharacterListItem) Settings.MainWindow.multiplayerCharacterList.getSelected()).getName() + "/" + pts + "/" + Settings.Password);
							String result = (String) item.get("message");
							MultiplayerSettings.updateCharacters();
							Window.alertPopup.setOpen(false);
							Window.optionPopup.setOpen(false);
							Window.eventPopup.setOpen(true);
							if(result.equalsIgnoreCase("success")) {
								Window.eventPopup.setOpen(false);
								openMenu = 1;
							} else if (result.equalsIgnoreCase("invalid login")) {
								Window.eventPopup.setMessage("Unable to authenticate account credentials. Please restart Collect, Construct, Conquer!");
							} else if (result.equalsIgnoreCase("duplicate")) {
								Window.eventPopup.setMessage("Duplicates of the same item may not be placed in the same loadout.");
							} else if (result.equalsIgnoreCase("slot overload")) {
								Window.eventPopup.setMessage("This character does not have enough loadout slots to hold this many items.");
							} else if (result.equalsIgnoreCase("inventory mismatch")) {
								Window.eventPopup.setMessage("Some of the items in this loadout could not be found in your inventory.");
							} else {
								Window.eventPopup.setMessage("An error occured deleting this character. Please try again later.");
							}
						} catch (Exception e) {
							e.printStackTrace();
							Window.optionPopup.setOpen(false);
							Window.eventPopup.setOpen(true);
							Window.eventPopup.setMessage("Unable to connect to the Studio878 API. Please try again later.");
							Window.alertPopup.setOpen(false);

						}

					}
				}.start();

			}

			break;
		}

		if(Window.optionPopup.isOpen()) {
			switch(Window.optionPopup.getPanelMode()) {
			case PromptPopup.ShopPurchase:
				if(openMenu != 3) {
					Window.optionPopup.setOpen(false);
					selectedStoreItem = 0;

				} else {
					if(Window.optionPopup.isOption1Selected()) {
						Window.optionPopup.setOpen(false);
						selectedStoreItem = 0;
					} else if (Window.optionPopup.isOption2Selected()) {
						Window.optionPopup.setOpen(false);
						Window.alertPopup.setOpen(true);
						Window.alertPopup.setMessage("Working...");
						Thread call = new Thread() {
							public void run() {
								try {
									MultiplayerSettings.PreviousUnlocks.clear();
									JSONArray items = API.getArray("http://api.studio878software.com/ccc/network/user/" + Settings.Username + "/store/buy/" + selectedStoreItem + "/" + Settings.Password);

									if(items != null) {
										int i = 0;
										for(Object o : items) {
											JSONObject j = (JSONObject) o;
											MultiplayerSettings.PreviousUnlocks.add(new StoreUnlockCard(LoadoutItem.lookupId(Integer.parseInt((String) j.get("id"))), (String) j.get("name"), (String) j.get("description"), Integer.parseInt((String) j.get("color_id")), new Point(i%3, (int) Math.floor(i/3))));
											i++;
										}
										MultiplayerSettings.updateCredits();
										MultiplayerSettings.updateInventory();
										openMenu = 5;
										Window.alertPopup.setOpen(false);
									} else {
										Window.optionPopup.setOpen(false);
										Window.eventPopup.setOpen(true);
										Window.eventPopup.setMessage("An error occured purchasing your item. Please try again later.");
										Window.alertPopup.setOpen(false);

									}
								} catch (Exception e) {
									Window.optionPopup.setOpen(false);
									Window.eventPopup.setOpen(true);
									Window.eventPopup.setMessage("An error occured purchasing your item. Please try again later.");
									Window.alertPopup.setOpen(false);
									e.printStackTrace();

								}
								selectedStoreItem = 0;
							}
						};
						call.start();
					}
				}
				break;
			}
		}

		if(invButton.isSelected()) {
			inventory.usingItem = null;
			inventory.setSelectingDisabled(false);
			openMenu = 0;
			inventory.setCol(5);
			inventory.setRows(4);
			inventory.setInventoryPage(1);
			inventory.setSelectedItem(-1);
			inventory.setY(100);
			inventory.setUsageMode(LoadoutInventory.HighlightMode);

		}
		if(charButton.isSelected()) {
			inventory.usingItem = null;
			inventory.setSelectingDisabled(false);
			openMenu = 1;
		}
		if(abilityButton.isSelected()) {
			inventory.usingItem = null;	
			inventory.setSelectingDisabled(false);
			openMenu = 2;
		}
		if(storeButton.isSelected()) {
			inventory.usingItem = null;
			inventory.setSelectingDisabled(false);
			openMenu = 3;
		}

	}
}
