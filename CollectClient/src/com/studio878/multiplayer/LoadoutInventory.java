package com.studio878.multiplayer;

import java.awt.Point;
import java.util.ArrayList;
import java.util.HashMap;

import org.json.simple.JSONObject;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;

import com.studio878.app.Window;
import com.studio878.ui.Fonts;
import com.studio878.ui.PromptPopup;
import com.studio878.util.API;
import com.studio878.util.GraphicsUtil;
import com.studio878.util.Settings;
import com.studio878.util.Sheets;

public class LoadoutInventory {

	public enum LoadoutItem {
		PistolMkI(1, 1, new Point(0, 0), null),
		PistolMkII(2, 1, new Point(1, 0), null),
		PistolMkIII(3, 1, new Point(2, 0), null),
		PistolMkIV(4, 1, new Point(3, 0), null),
		PistolMkV(5, 1, new Point(0, 1), null),
		SmgMkI(6, 1, new Point(0, 2), null),
		SmgMkII(7, 1, new Point(1, 2), null),
		SmgMkIII(8, 1, new Point(2, 2), null),
		SmgMkIV(9, 1, new Point(3, 2), null),
		SmgMkV(10, 1, new Point(0, 3), null),
		PistolKit(11, 3, new Point(1, 1), new int[]{1,2,3,4}),
		SmgKit(12, 3, new Point(1, 3), new int[]{6,7,8,9}),
		RewardsCard(13, 2, new Point(0, 4), null),
		HollowPointAmmo(17, 2, new Point(1, 4), null),
		ShotgunMkI(20, 1, new Point(0, 5), null),
		ShotgunMkII(21, 1, new Point(1, 5), null),
		ShotgunMkIII(22, 1, new Point(2, 5), null),
		ShotgunMkIV(23, 1, new Point(3, 5), null),
		ShotgunMkV(24, 1, new Point(0, 6), null),
		RocketLauncherMkI(25, 1, new Point(0, 7), null),
		RocketLauncherMkII(26, 1, new Point(1, 7), null),
		RocketLauncherMkIII(27, 1, new Point(2, 7), null),
		RocketLauncherMkIV(28, 1, new Point(3, 7), null),
		RocketLauncherMkV(29, 1, new Point(0, 8), null),
		SniperRifleMkI(30, 1, new Point(0, 9), null),
		SniperRifleMkII(31, 1, new Point(1, 9), null),
		SniperRifleMkIII(32, 1, new Point(2, 9), null),
		SniperRifleMkIV(33, 1, new Point(3, 9), null),
		SniperRifleMkV(34, 1, new Point(0, 10), null),
		FlintlockPistolMkI(35, 1, new Point(0, 11), null),
		FlintlockPistolMkII(36, 1, new Point(1, 11), null),
		FlintlockPistolMkIII(37, 1, new Point(2, 11), null),
		FlintlockPistolMkIV(38, 1, new Point(3, 11), null),
		FlintlockPistolMkV(39, 1, new Point(0, 12), null),
		ShotgunKit(40, 3, new Point(1, 6), new int[]{20,21,22,23}),
		SniperRifleKit(41, 3, new Point(1, 10), new int[]{30,31,32,33}),
		RocketLauncherKit(42, 3, new Point(1, 8), new int[]{25,26,27,28}),
		FlintlockPistolKit(43, 3, new Point(1, 12), new int[]{35,36,37,38}),
		Wrench(44, 1, new Point(0, 13), null),
		Sword(45, 1, new Point(1, 13), null),
		Hatchet(46, 1, new Point(2, 13), null),
		;

		int id, type;
		Image image;
		int[] usableItems;
		private static HashMap<Integer, LoadoutItem> idLookup = new HashMap<Integer, LoadoutItem>();

		private LoadoutItem(int id, int type, Point position, int[] usableItems) {
			this.id = id;
			this.type = type;
			this.usableItems = usableItems;
			this.image = Sheets.LOADOUT_ITEMS.getSubImage(position.x, position.y);
		}

		public int getId() {
			return id;
		}

		public int[] getUsableItems() {
			return usableItems;
		}

		public int getType() {
			return type;
		}

		public Image getImage() {
			return image;
		}

		public static LoadoutItem lookupId(int i) {
			return idLookup.get(i);
		}

		static {
			for(LoadoutItem t: values()) {
				idLookup.put(t.getId(), t);
			}
		}

	}
	
	public final static int HighlightMode = 0;
	public final static int DragMode = 1;

	int x, y, row, col;

	int invPage = 1, selectedItem = -1;

	int usageMode = 0;

	boolean selectingDisabled = false;

	MultiplayerInventoryObject draggingObject, usingItem, destItem;
	LoadoutInventoryEvent eventManager;

	public static ArrayList<LoadoutItem> DisableList = new ArrayList<LoadoutItem>();

	public LoadoutInventory(int x, int y, int row, int col) {
		this.x = x;
		this.y = y;
		this.row = row;
		this.col = col;
	}

	public int getInventoryPage() {
		return invPage;
	}

	public void setSelectingDisabled(boolean disabled) {
		selectingDisabled = disabled;
	}

	public void setInventoryPage(int page) {
		invPage = page;
	}

	public void setX(int x) {
		this.x = x;
	}

	public void setY(int y) {
		this.y = y;
	}

	public void setRows(int row) {
		this.row = row;
	}

	public void setCol(int col) {
		this.col = col;
	}

	public void setSelectedItem(int id) {
		selectedItem = id;
	}

	public int getSelectedItem() {
		return selectedItem;
	}

	public int getRows() {
		return row;
	}

	public int getColumns() {
		return col;
	}

	public void setEventManager(LoadoutInventoryEvent event) {
		this.eventManager = event;
	}

	public void setUsageMode(int mode) {
		this.usageMode = mode;
	}

	public void draw(Graphics g, GameContainer c) {
		int msx = c.getInput().getMouseX();
		int msy = c.getInput().getMouseY();
		for(int i = 0; i < col; i++) {
			for(int j = 0; j < row; j++) {

				float alpha = 1.0f;
				try {
					if(MultiplayerSettings.Inventory.size() > (invPage - 1)*(row*col) + i + j*col) {
						if(MultiplayerSettings.Inventory.size() != 0 && DisableList.contains(MultiplayerSettings.Inventory.get((invPage - 1)*(row*col) + i + j*col).getSource())) {
							alpha = 0.4f;
						}
					}
				} catch (ArrayIndexOutOfBoundsException e) {

				}
				if(i + j*5 == selectedItem) {
					g.drawImage(Sheets.ITEM_INSET_BOXES.getSubImage(1,0), ((c.getWidth() - x) - (120*(col-1)) - 100)/2 + x + (i*120), y + (j)*120);
				} else {
					Image is = Sheets.ITEM_INSET_BOXES.getSubImage(0,0).copy();
					is.setAlpha(alpha);

					g.drawImage(is, ((c.getWidth() - x) - (120*(col-1)) - 100)/2 + x + (i*120), y + (j)*120);
				}
				try {
					if(MultiplayerSettings.Inventory.size() > (invPage - 1)*(row*col) + i + j*col) {
						Image is = MultiplayerSettings.Inventory.get((invPage - 1)*(row*col) + i + j*col).getImage().copy();
						is.setAlpha(alpha);
						g.drawImage(is, ((c.getWidth() - x)  - (120*(col-1)) - 100)/2 + x + (i*120) + 18, y + (j)*120 + 18);
					}
				} catch (IndexOutOfBoundsException e) {
				}


			}
		}
		
		if(Window.optionPopup.isOpen() && Window.optionPopup.getPanelMode() == PromptPopup.UseItem) {
			if(Window.optionPopup.isOption1Selected()) {
				Window.optionPopup.setOpen(false);
				usingItem = null;

			} else if (Window.optionPopup.isOption2Selected()) {
				Window.optionPopup.setOpen(false);
				useItem();
			}
		}
		if(!Window.optionPopup.isOpen() && !Window.alertPopup.isOpen() &&  !Window.eventPopup.isOpen()) {
			for(int i = 0; i < col; i++) {
				for(int j = 0; j < row; j++) {
					if(MultiplayerSettings.Inventory.size() > i + j*col + (invPage - 1)*(row*col)) {
						if(msx > ((c.getWidth() - x) - (120*col))/2 + x + (i*120) && msx < ((c.getWidth() - x) - (120*(col-1)) - 100)/2 + x + (i*120) + 100 && msy > y + (j)*120 && msy < y + (j)*120 + 100) {
							if(c.getInput().isMousePressed(0)) {
								if(usageMode == HighlightMode && !selectingDisabled) {
									selectedItem = (i + j*col);
								} else if (usageMode == HighlightMode && usingItem != null) {
									if(MultiplayerSettings.Inventory.size() > (invPage - 1)*(row*col) + i + j*col && !DisableList.contains(MultiplayerSettings.Inventory.get((invPage - 1)*(row*col) + i + j*col).getSource())) {
										destItem = MultiplayerSettings.Inventory.get((invPage - 1)*(row*col) + i + j*col);
										Window.optionPopup.setMessage("Are you sure you would like to use a " + usingItem.getName() + " on this " + destItem.getName() + "?");
										Window.optionPopup.setOption1Value("Cancel");
										Window.optionPopup.setOption2Value("Use");
										Window.optionPopup.setPanelMode(PromptPopup.UseItem);
										Window.optionPopup.setOpen(true);
									}
								} else if (usageMode == DragMode) {
									if(!DisableList.contains(MultiplayerSettings.Inventory.get((invPage - 1)*(row*col) + i + j*col).getSource())) {
										draggingObject = MultiplayerSettings.Inventory.get((invPage - 1)*(row*col) + i + j*col);
									}
								}
							}
							if(draggingObject == null && !DisableList.contains(MultiplayerSettings.Inventory.get((invPage - 1)*(row*col) + i + j*col).getSource())) {
								int dir = 1;
								if(msy > c.getHeight() - 250) {
									dir = -1;
								}
								g.setColor(new Color(170, 170, 170, 0.6f));
								g.fillRect(msx - 200, msy, 400, 250*dir);
								g.setColor(Color.black);
								g.setLineWidth(1.0f);
								g.drawRect(msx - 200, msy, 400, 250*dir);
								g.setFont(Fonts.ChatFont);
								try {
									String name = MultiplayerSettings.Inventory.get( i + j*col + (invPage - 1)*(row*col)).getName();
									int offset = (dir == -1) ? -250 : 0;
									g.drawString(name, msx - Fonts.ChatFont.getWidth(name)/2 , msy + 10 + offset);
									g.setFont(Fonts.TitleFont);
									ArrayList<String> lines = GraphicsUtil.splitBetweenLines(MultiplayerSettings.Inventory.get( i + j*col + (invPage - 1)*(row*col)).getDescription(), Fonts.TitleFont, 380);
									int dy = msy + 50 + offset;
									for(String s : lines) {
										g.drawString(s, msx - Fonts.TitleFont.getWidth(s)/2, dy);
										dy += 20;
									}
								} catch (IndexOutOfBoundsException e) {	
								}
							}
						} else {
							if(!c.getInput().isMouseButtonDown(0)) {
								if(usageMode == DragMode) {
									if(eventManager != null && draggingObject != null) {
										eventManager.onDraggableItemRelease(draggingObject, msx, msy, c);
									}
									draggingObject = null;
								}
							}
						}

					}
				}
			}
		}
		if(draggingObject != null && usageMode == DragMode) {
			g.drawImage(draggingObject.getImage(), msx - 32, msy - 32);
		}
		
	}
	
	public void useItem() {
		if(usingItem != null) {
			Window.optionPopup.setOpen(false);
			Window.alertPopup.setOpen(true);
			Window.alertPopup.setMessage("Working...");
			new Thread() {
				public void run() {
					try {
						int destId;
						
						if(destItem == null) {
							destId = -1;
						} else {
							destId = destItem.getId();
						}
						JSONObject item = API.getObject("http://api.studio878software.com/ccc/network/user/" + Settings.Username + "/inventory/use/" + usingItem.getId() + "/" + destId + "/" + Settings.Password);
						String result = (String) item.get("message");
						MultiplayerSettings.updateInventory();
						MultiplayerSettings.updateAbilities();
						Window.alertPopup.setOpen(false);
						Window.optionPopup.setOpen(false);
						Window.eventPopup.setOpen(true);
						if(result.equalsIgnoreCase("success")) {
							Window.eventPopup.setMessage("Item used.");
						} else if (result.equalsIgnoreCase("missing item")) {
							Window.eventPopup.setMessage("One or more items used could not be found in your inventory.");
						} else if (result.equalsIgnoreCase("invalid combination")) {
							Window.eventPopup.setMessage("These items cannot be used together.");
						} else if (result.equalsIgnoreCase("invalid item")) {
							Window.eventPopup.setMessage("This item cannot be used on its own.");
						} else if (result.equalsIgnoreCase("duplicate ability")) {
							Window.eventPopup.setMessage("You already have this temporary ability.");
						} else if (result.equalsIgnoreCase("invalid login")) {
							Window.eventPopup.setMessage("Unable to authenticate account credentials. Please restart Collect, Construct, Conquer!");
						} else {
							Window.eventPopup.setMessage("An error occured deleting this character. Please try again later.");
						}
					} catch (Exception e) {
						Window.optionPopup.setOpen(false);
						Window.eventPopup.setOpen(true);
						Window.eventPopup.setMessage("Unable to connect to the Studio878 API. Please try again later.");
						Window.alertPopup.setOpen(false);

					}
					usingItem = null;
					destItem = null;
					
					selectingDisabled = false;

				}
			}.start();	
		}
	}
}
