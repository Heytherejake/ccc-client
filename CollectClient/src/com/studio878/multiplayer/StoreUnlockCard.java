package com.studio878.multiplayer;

import java.awt.Point;
import java.util.ArrayList;

import org.lwjgl.opengl.Display;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

import com.studio878.multiplayer.LoadoutInventory.LoadoutItem;
import com.studio878.ui.Fonts;
import com.studio878.util.GraphicsUtil;
import com.studio878.util.Sheets;

public class StoreUnlockCard {

	double x = 0, y = 0;
	double gx, gy;
	float rotation, startRotation;
	Point position;

	Image is;

	LoadoutItem item;
	String name;
	String desc;
	Image bg;
	ArrayList<String> lines = new ArrayList<String>();
	public StoreUnlockCard(LoadoutItem item, String name, String desc, int color, Point position) {
		this.item = item;
		this.name = name;
		this.desc = desc;
		bg = Sheets.UNLOCK_CARDS.getSubImage(color%2, (int) Math.floor(color/2)).copy();
		lines = GraphicsUtil.splitBetweenLines(desc, Fonts.SmallFont, 160);
		this.position = position;
		gx = ((Display.getDisplayMode().getWidth() - 400) - (240*2) - 200)/2 + 400 + (position.x*240);
		gy = 100 + position.y*330;
		x = (int) Math.round(Math.random()*1 - 1)*100 + gx;
		y = Math.random()*-50 - 50; 
		startRotation = (float) Math.random()*40 - 20;
		rotation = startRotation;
	}

	public void draw(Graphics g) {
		try {
			Graphics gs = bg.getGraphics();
			gs.setColor(Color.black);
			gs.drawImage(Sheets.UNLOCK_CARDS.getSubImage(0, 0), 0, 0);
			gs.drawImage(item.getImage(), 68, 27);
			gs.setFont(Fonts.TitleFont);
			gs.drawString(name, 100 - Fonts.TitleFont.getWidth(name)/2, 103);
			int dy = 150;
			gs.setFont(Fonts.SmallFont);
			for(String s : lines) {
				gs.drawString(s,  100 - Fonts.SmallFont.getWidth(s)/2,  dy);
				dy += 15;
			}
		} catch (SlickException e) {
			e.printStackTrace();
		}


		bg.setRotation(rotation);
		g.drawImage(bg, (float) x, (float) y);

		x += (gx - x)/15;
		y += (gy - y)/15;
		if(Math.sqrt(Math.pow(gx - x, 2) + Math.pow(gy - y, 2)) > 1) {
			if(startRotation <= 0) {
				rotation += Math.sqrt(Math.pow(gx - x, 2) + Math.pow(gy - y, 2))/350;
			} else {
				rotation -= Math.sqrt(Math.pow(gx - x, 2) + Math.pow(gy - y, 2))/350;
			}
		}

	}
}
