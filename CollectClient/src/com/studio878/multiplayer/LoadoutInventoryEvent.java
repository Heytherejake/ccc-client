package com.studio878.multiplayer;

import org.newdawn.slick.GameContainer;

public interface LoadoutInventoryEvent {

	public void onDraggableItemRelease(MultiplayerInventoryObject object, int msx, int msy, GameContainer c);
}
