package com.studio878.multiplayer;

import java.awt.Point;
import java.util.HashMap;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;

import com.studio878.crafting.BonusItem.BonusItemType;
import com.studio878.ui.Fonts;
import com.studio878.ui.TableList;
import com.studio878.ui.TableListItem;
import com.studio878.util.GraphicsUtil;
import com.studio878.util.Sheets;

public class MultiplayerAbilityListItem extends TableListItem {
	
	public enum AbilityType {
		RewardsCard(13, "Rewards Card", "All income is doubled for 30 minutes.", new Point(0, 4), null, BonusItemType.CurrencyOutput),
		HollowPointAmmo(17, "Hollow Point Ammunition", "Ranged damage is increased for 30 minutes.", new Point(1, 4), null, BonusItemType.RangedDamage),
		;

		int id;
		String name, desc;
		Image image;
		int[] usableItems;
		BonusItemType type;
		private static HashMap<Integer, AbilityType> idLookup = new HashMap<Integer, AbilityType>();

		private AbilityType(int id, String name, String desc, Point position, int[] usableItems, BonusItemType type) {
			this.id = id;
			this.name = name;
			this.desc = desc;
			this.usableItems = usableItems;
			this.image = Sheets.LOADOUT_ITEMS.getSubImage(position.x, position.y);
			this.type = type;
		}

		public int getId() {
			return id;
		}
		
		public String getName() {
			return name;
		}
		
		public String getDescription() {
			return desc;
		}

		public int[] getUsableItems() {
			return usableItems;
		}

		public Image getImage() {
			return image;
		}

		public BonusItemType getType()  {
			return type;
		}
		
		public static AbilityType lookupId(int i) {
			return idLookup.get(i);
		}

		static {
			for(AbilityType t: values()) {
				idLookup.put(t.getId(), t);
			}
		}

	}
	AbilityType type;
	long endTime, currentTime;
	double multiplier;
	public MultiplayerAbilityListItem(AbilityType type, long endTime, long currentTime, double multiplier, TableList parent) {
		super(type.name(), 150, parent);
		this.type = type;
		this.endTime = endTime;
		this.multiplier = multiplier;
		System.out.println(System.currentTimeMillis()/1000 + "|" + currentTime);
		this.currentTime = System.currentTimeMillis()/1000 - currentTime;
	}
	
	public double getMultiplier() {
		return multiplier;
	}
	
	public AbilityType getType() {
		return type;
	}


	public void draw(Graphics g, int i) {
		long tr = endTime + currentTime - System.currentTimeMillis()/1000;
		Color cr = new Color(0, 90, 0, 0.4f);
		if(tr/60 < 5) {
			cr = new Color(90, 0, 0, 0.4f);
		} else if (tr/60 < 15) {
			cr = new Color(90, 90, 0, 0.4f);
		}
		g.setColor(cr);
		g.fillRect(0, i, parent.width, 150);
		g.drawImage(type.getImage(), 10, i + (150-64)/2);
		g.setColor(Color.black);
		g.setLineWidth(1.0f);
		g.setColor(new Color(0, 0, 0, 0.7f));
		g.drawLine(0, i + 150, parent.width, i + 150);
		g.setFont(Fonts.ChapterFont);
		GraphicsUtil.drawWithShadow(g, type.getName(), 100, i + 20, Color.white, Color.black);
		g.setFont(Fonts.TitleFont);
		GraphicsUtil.drawWithShadow(g, type.getDescription(), 100, i + 60, Color.white, Color.black);
		
		int hr = (int) Math.floor(tr/60/60);
		int min = (int) Math.floor(tr/60);
		int sec = (int) (tr % 60);
		String hour = (hr >= 10) ? "" + hr : "0" + hr;
		String minute = (min >= 10) ? "" + min : "0" + min;
		String second = (sec >= 10) ? "" + sec : "0" + sec;

		hour = (hr != 1) ? hour + " seconds" : hour + " second";
		minute = (min != 1) ? minute + " minutes" : minute + " minute";
		second = (sec != 1) ? second + " seconds" : second + " second";

		String time = "Time Remaining: ";
		if(hr > 0) {
			time += hour + ", " + minute + ", and " + second;
		} else if (min > 0) {
			time += minute + ", and " + second;
		} else if (sec > 0) {
			time += second;
		} else {
			parent.items.remove(this);
		}
		
		g.setFont(Fonts.ChapterFont);
		GraphicsUtil.drawWithShadow(g, time, parent.width - 20 - Fonts.ChapterFont.getWidth(time), i + 150 - Fonts.ChapterFont.getHeight(time) - 10, Color.white, Color.black);

	}
}
