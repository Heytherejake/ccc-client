package com.studio878.multiplayer;

import java.util.ArrayList;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.studio878.characters.PlayerCharacter;
import com.studio878.multiplayer.LoadoutInventory.LoadoutItem;
import com.studio878.multiplayer.MultiplayerAbilityListItem.AbilityType;
import com.studio878.util.API;
import com.studio878.util.Settings;

public class MultiplayerSettings {

	public static int Money = 0;
	public static ArrayList<MultiplayerInventoryObject> Inventory = new ArrayList<MultiplayerInventoryObject>();

	public static ArrayList<MultiplayerShopItem> StoreItems = new ArrayList<MultiplayerShopItem>();
	public static ArrayList<StoreItemButton> StoreButtons = new ArrayList<StoreItemButton>();
	public static ArrayList<StoreUnlockCard> PreviousUnlocks = new ArrayList<StoreUnlockCard>();

	public static void updateMultiplayerVariables() {
		Inventory.clear();
		try {
			updateCredits();
			updateInventory();
			updateStore();
			updateCharacters();
			updateAbilities();
		} catch (Exception e) {
			System.out.println("Failed to connect to API");
			Settings.MainWindow.alertPopup.setOpen(false);
			Settings.MainWindow.eventPopup.setMessage("Failed to connect to API");
			Settings.MainWindow.eventPopup.setOpen(true);
			e.printStackTrace();
		}
	}

	public static void updateCredits() throws Exception{
		JSONArray user = API.getArray("http://api.studio878software.com/ccc/network/user/" + Settings.Username + "/get");
		Money = Integer.parseInt((String) ((JSONObject) user.get(0)).get("credits"));
	}

	public static void updateInventory() throws Exception {
		ArrayList<MultiplayerInventoryObject> temp = new ArrayList<MultiplayerInventoryObject>();
		JSONArray inv = API.getArray("http://api.studio878software.com/ccc/network/user/" + Settings.Username + "/inventory/get");
		for(Object o : inv) {
			JSONObject jo = (JSONObject) o;
			LoadoutItem li = LoadoutItem.lookupId(Integer.parseInt((String) jo.get("item")));
			temp.add(new MultiplayerInventoryObject(Integer.parseInt((String) jo.get("id")), (String) jo.get("name"), (String) jo.get("description"), false, Integer.parseInt((String) jo.get("sell_value")), li));
		}
		Inventory.clear();
		Inventory.addAll(temp);
	}

	public static void updateAbilities() throws Exception {
		JSONArray inv = API.getArray("http://api.studio878software.com/ccc/network/user/" + Settings.Username + "/abilities/list/" + Settings.Session);
		Settings.MainWindow.multiplayerAbilityList.clearItems();
		for(Object o : inv) {
			JSONObject jo = (JSONObject) o;
			MultiplayerAbilityListItem li = new MultiplayerAbilityListItem(AbilityType.lookupId(Integer.parseInt((String) jo.get("ability"))), (Long) jo.get("end_time"), (Long) jo.get("curr_time"), Double.parseDouble(((String) jo.get("data")).split(",")[1]),Settings.MainWindow.multiplayerAbilityList);
			Settings.MainWindow.multiplayerAbilityList.addItem(li);
		}
	}
	
	public static void updateStore() throws Exception {
		JSONArray inv = API.getArray("http://api.studio878software.com/ccc/network/user/" + Settings.Username + "/store/list/" + Settings.Password);
		StoreButtons.clear();
		StoreItems.clear();
		for(Object o : inv) {
			JSONObject jo = (JSONObject) o;
			MultiplayerShopItem item = new MultiplayerShopItem(Integer.parseInt((String) jo.get("id")), (String) jo.get("name"), Integer.parseInt((String) jo.get("price")), Integer.parseInt((String) jo.get("img_offset")), Integer.parseInt((String) jo.get("color_id")), (String) jo.get("description"));
			StoreItems.add(item);
			StoreButtons.add(new StoreItemButton(0, 0, item));
		}
	}

	public static void updateCharacters() throws Exception {
		JSONArray inv = API.getArray("http://api.studio878software.com/ccc/network/user/" + Settings.Username + "/characters/list/" + Settings.Password);
		Settings.MainWindow.multiplayerCharacterList.clearItems();
		for(Object o : inv) {
			JSONObject jo = (JSONObject) o;
			MultiplayerCharacterListItem li = new MultiplayerCharacterListItem((String) jo.get("name"),  Integer.parseInt((String) jo.get("active")) == 1, PlayerCharacter.lookupId(Integer.parseInt((String) jo.get("type"))), Settings.MainWindow.multiplayerCharacterList);
			li.setLoadout((String) jo.get("loadout"));
			Settings.MainWindow.multiplayerCharacterList.addItem(li);
		}

	}
}


