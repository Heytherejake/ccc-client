package com.studio878.multiplayer;

import org.newdawn.slick.Image;

import com.studio878.multiplayer.LoadoutInventory.LoadoutItem;

public class MultiplayerInventoryObject {
	int id;
	String name, description;
	boolean usable;
	Image image;
	LoadoutItem source;
	int sellValue;

	public MultiplayerInventoryObject(int id, String name, String description, boolean usable, int sellValue, LoadoutItem source) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.usable = usable;
		this.source = source;
		this.image = source.getImage();
		this.sellValue = sellValue;
	}

	public int getId() {
		return id;
	}

	public int getSellValue() {
		return sellValue;
	}
	
	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public boolean isUsable() {
		return usable;
	}
	
	public LoadoutItem getSource() {
		return source;
	}

	public Image getImage() {
		return image;
	}

}
