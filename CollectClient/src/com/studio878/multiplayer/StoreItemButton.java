package com.studio878.multiplayer;

import java.util.ArrayList;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;

import com.studio878.ui.Fonts;
import com.studio878.ui.SizableButton;
import com.studio878.util.GraphicsUtil;

public class StoreItemButton extends SizableButton {

	MultiplayerShopItem item;
	public StoreItemButton(int x, int y, MultiplayerShopItem item) {
		super(x, y, 200, 300, "", false);
		this.item = item;
		setColorProfile(item.getColorProfile());

	}
	
	public MultiplayerShopItem getItem() {
		return item;
	}
	
	public void draw(GameContainer c, Graphics g) {
		super.draw(c, g);
		float alpha = 1.0f;
		if(isDisabled()) {
			alpha = 0.4f;
		}
		Image tsd = item.getImage().copy();
		tsd.setAlpha(alpha);
		g.drawImage(tsd, x + 25, y + 5);
		g.setFont(Fonts.ChatFont);

		Color fg = new Color(0, 0, 0, alpha);
		Color bg = new Color(255, 255, 255, alpha);
		switch(item.getColorProfile()) {
		case 1:
			fg = new Color(255, 255, 255, alpha);
			bg = new Color(0, 0, 0, alpha);
			break;
		}
		String str = item.getName();

		GraphicsUtil.drawWithShadow(g, str , x + 100 - Fonts.ChatFont.getWidth(str)/2, y + 150, fg, bg);
		str =  "$" + item.getCost();
		GraphicsUtil.drawWithShadow(g, str , x + 100 - Fonts.ChatFont.getWidth(str)/2, y + 170, fg, bg);

		g.setFont(Fonts.TitleFont);
		ArrayList<String> lines = GraphicsUtil.splitBetweenLines(item.getDescription(), Fonts.TitleFont, 180);
		int sy = y + 195;
		for(String s : lines) {
			GraphicsUtil.drawWithShadow(g, s, x + 100 - Fonts.TitleFont.getWidth(s)/2, sy, fg, bg);
			sy += 20;
		}
	}

}
