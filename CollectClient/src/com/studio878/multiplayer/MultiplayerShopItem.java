package com.studio878.multiplayer;

import java.awt.Point;

import org.newdawn.slick.Image;

import com.studio878.util.Sheets;

public class MultiplayerShopItem {

	String name, description;
	int id, cost, colorProfile;
	Image image;
	
	public MultiplayerShopItem(int id, String name, int cost, int img_offset, int colorProfile, String description) {
		this.id = id;
		this.name = name;
		this.cost = cost;
		this.description = description;
		this.colorProfile = colorProfile;
		this.image = Sheets.STORE_BOXES.getSubImage(img_offset%3, (int) Math.floor(img_offset/3));
	}
	
	public String getName() {
		return name;
	}
	
	public int getId() {
		return id;
	}
	
	public int getCost() {
		return cost;
	}
	
	public Image getImage() {
		return image;
	}
	
	public int getColorProfile() {
		return colorProfile;
	}
	
	public String getDescription() {
		return description;
	}
	
}
