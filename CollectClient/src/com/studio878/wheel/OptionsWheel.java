package com.studio878.wheel;

import java.util.ArrayList;

import org.newdawn.slick.Graphics;

import com.studio878.displays.MainGame;
import com.studio878.inventory.Inventory;
import com.studio878.inventory.ItemID;

public class OptionsWheel {
	ArrayList<WheelItem> items = new ArrayList<WheelItem>();

	MainGame game;

	public OptionsWheel(MainGame g) {
		game = g;
	}
	public void setGoals(int startx, int starty) {
		if(items.size() != 0) {
			double turn = 360/items.size();
			int seperation = 50;
			double angle = 0;
			for(WheelItem i : items) {
				i.goalX = (int) (startx + seperation*Math.cos(Math.toRadians(angle)));
				i.goalY = (int) (starty + seperation*Math.sin(Math.toRadians(angle)));
				i.x = startx;
				i.y = starty;
				i.initx = startx;
				i.inity = starty;
				angle += turn;

			}
		}
	}

	public WheelItem getClosestTarget(int x, int y) {
		WheelItem closest = null;
		double distance = 9999;
		for(WheelItem i : items) {
			double tmp = Math.sqrt((Math.abs((Math.pow(x - i.x, 2))) + Math.abs((Math.pow(y - i.y, 2)))));
			if(tmp < distance) {
				distance = tmp;
				closest = i;
			}
		}
		return closest;
	}
	public void animate() {
		if(items.size() != 0) {
			double turn = 360/items.size();
			double angle = 0;
			for(WheelItem i : items) {

				if(Math.abs(Math.abs((Math.pow(i.goalX - i.x, 2))) + Math.abs((Math.pow(i.goalY - i.y, 2)))) < Math.pow(WheelItem.MovementSpeed,2)) {
					i.x = i.goalX;
					i.y = i.goalY;
				} else {
					i.x += WheelItem.MovementSpeed*Math.cos(Math.toRadians(angle));
					i.y += WheelItem.MovementSpeed*Math.sin(Math.toRadians(angle));
				}
				angle += turn;
			}
		}

	}

	public void addItem(WheelItem i) {
		items.add(i);
	}

	public void draw(Graphics g) {
		for(WheelItem i : items) {
			i.draw(g);
		}
	}

	public void refreshItems() {
		items.clear();

		if(Inventory.contains(ItemID.lookupName("Grenade"))) {
			items.add(new ThrowGrenadeItem());
		}
		
		if(Inventory.contains(ItemID.lookupName("Landmine"))) {
			items.add(new ThrowLandmineItem());
		}
		
		if(game.cursorMode == 1) {
			items.add(new CancelSelectionItem());
		}
		
		if(MainGame.isPlacing) {
			items.add(new WeaponsModeItem());
			if(Inventory.countBlocks() > 1) {
				items.add((int) Math.ceil(items.size()/2), new PreviousBlockItem());
				items.add(0, new NextBlockItem());
			}
		} else { 
			items.add(new BlockModeItem());

			if(Inventory.countWeapons() > 1) {
				items.add((int) Math.ceil(items.size()/2), new PreviousWeaponItem());
				items.add(0, new NextWeaponItem());
			}

		}

	}
}
