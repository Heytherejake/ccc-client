package com.studio878.wheel;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;

import com.studio878.ui.Fonts;
import com.studio878.util.Settings;

public abstract class WheelItem {

	double initx, inity;
	double x, y;
	int goalX, goalY;
	Image item;
	String label = "Menu Item";
	boolean closest;
	
	final static double MovementSpeed = 1;
	
	public abstract void onRelease();
	
	public boolean isClosest() {
		return closest;
	}
	
	public void setClosest(boolean b) {
		closest = b;
	}
	public void draw(Graphics g) {
		if(!closest) {
			item.draw((float) x, (float) y, item.getWidth() * 0.8f, item.getHeight() * 0.8f);

		} else {
			item.draw((float) x, (float) y, item.getWidth(), item.getHeight());
			g.setFont(Fonts.ChatFont);
			g.setColor(new Color(100, 100, 100, 0.4f));

			g.fillRect((float) Settings.WindowWidth / 2 - (Fonts.ChatFont.getWidth(label)/2) + 10, (float) Settings.WindowHeight / 2 - 85, Fonts.ChatFont.getWidth(label) + 10, Fonts.ChatFont.getHeight(label) + 10);
			g.setColor(Color.black);
			g.drawString(label, (float) Settings.WindowWidth / 2 - (Fonts.ChatFont.getWidth(label)/2) + 15, (float) Settings.WindowHeight / 2 - 80);
			
		}
	}
}
