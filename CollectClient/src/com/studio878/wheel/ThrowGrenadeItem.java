package com.studio878.wheel;

import com.studio878.displays.MainGame;
import com.studio878.inventory.ItemID;
import com.studio878.util.Sheets;

public class ThrowGrenadeItem extends WheelItem {

	public ThrowGrenadeItem() {
		item = Sheets.ICON_TEXTURES.getSubImage(2, 0);
		label = "Throw Grenade";
	}
	
	public void onRelease() {
		MainGame.setProjectile = ItemID.lookupName("Grenade");
		MainGame.cursorMode = 1;
	}
}
