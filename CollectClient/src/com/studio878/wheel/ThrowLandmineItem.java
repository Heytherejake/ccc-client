package com.studio878.wheel;

import com.studio878.displays.MainGame;
import com.studio878.inventory.ItemID;
import com.studio878.util.Sheets;

public class ThrowLandmineItem extends WheelItem{
	public ThrowLandmineItem() {
		item = Sheets.ICON_TEXTURES.getSubImage(3, 0);
		label = "Throw Landmine";
	}
	
	public void onRelease() {
		MainGame.setProjectile = ItemID.lookupName("Landmine");
		MainGame.cursorMode = 1;
	}
}
