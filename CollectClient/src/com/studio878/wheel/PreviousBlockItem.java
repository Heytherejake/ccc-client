package com.studio878.wheel;

import com.studio878.inventory.Inventory;
import com.studio878.inventory.Item;
import com.studio878.io.NetObjects;
import com.studio878.packets.Packet26SetWeapon;
import com.studio878.util.Sheets;

public class PreviousBlockItem extends WheelItem {

	public PreviousBlockItem() {
		item = Sheets.ICON_TEXTURES.getSubImage(0, 0);
		label = "Previous Block";
	}
	public void onRelease() {
		Inventory.setPreviousInventoryBlock();
	}

}
