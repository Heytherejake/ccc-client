package com.studio878.wheel;

import com.studio878.inventory.Inventory;
import com.studio878.inventory.Item;
import com.studio878.io.NetObjects;
import com.studio878.packets.Packet26SetWeapon;
import com.studio878.util.Sheets;

public class NextBlockItem extends WheelItem {

	public NextBlockItem() {
		item = Sheets.ICON_TEXTURES.getSubImage(1, 0);
		label = "Next Block";
	}
	public void onRelease() {
		Inventory.setNextInventoryBlock();
	}

}
