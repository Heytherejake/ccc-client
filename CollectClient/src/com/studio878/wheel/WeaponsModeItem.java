package com.studio878.wheel;

import com.studio878.displays.MainGame;
import com.studio878.inventory.Inventory;
import com.studio878.io.NetObjects;
import com.studio878.packets.Packet53SetPlayerMode;
import com.studio878.util.Sheets;

public class WeaponsModeItem extends WheelItem {
	
	public WeaponsModeItem() {
		item = Sheets.ICON_TEXTURES.getSubImage(7, 0);
		label = "Use Weapons";
	}
	public void onRelease() {
		MainGame.isPlacing = false;
		Inventory.setNextInventoryWeapon();
		NetObjects.Sender.sendPacket(new Packet53SetPlayerMode(false));
	}
}
