package com.studio878.wheel;

import com.studio878.displays.MainGame;
import com.studio878.util.Sheets;

public class CancelSelectionItem extends WheelItem {
	
	public CancelSelectionItem() {
		item = Sheets.ICON_TEXTURES.getSubImage(4, 0);
		label = "Cancel Action";
	}
	public void onRelease() {
	MainGame.cursorMode = 0;
	}
}
