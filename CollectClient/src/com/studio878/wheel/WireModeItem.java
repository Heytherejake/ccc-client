package com.studio878.wheel;

import com.studio878.displays.MainGame;
import com.studio878.util.Sheets;

public class WireModeItem extends WheelItem {
	
	public WireModeItem() {
		item = Sheets.ICON_TEXTURES.getSubImage(8, 0);
		label = "Edit Wiring";
	}
	public void onRelease() {
		MainGame.wireOverlayOpen = true;
		MainGame.cursorMode = MainGame.WirePlacementMode;
		MainGame.wireMap.clear();
	}
}
