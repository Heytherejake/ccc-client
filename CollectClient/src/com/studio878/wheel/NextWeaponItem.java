package com.studio878.wheel;

import com.studio878.inventory.Inventory;
import com.studio878.inventory.Item;
import com.studio878.io.NetObjects;
import com.studio878.packets.Packet26SetWeapon;
import com.studio878.util.Sheets;

public class NextWeaponItem extends WheelItem {

	public NextWeaponItem() {
		item = Sheets.ICON_TEXTURES.getSubImage(1, 0);
		label = "Next Weapon";
	}
	public void onRelease() {
		Inventory.setNextInventoryWeapon();
	}

}
