package com.studio878.wheel;

import com.studio878.displays.MainGame;
import com.studio878.inventory.Inventory;
import com.studio878.io.NetObjects;
import com.studio878.packets.Packet53SetPlayerMode;
import com.studio878.util.Sheets;

public class BlockModeItem extends WheelItem {
	
	public BlockModeItem() {
		item = Sheets.ICON_TEXTURES.getSubImage(6, 0);
		label = "Place Blocks";
	}
	public void onRelease() {
		MainGame.isPlacing = true;
		Inventory.setNextInventoryBlock();
		NetObjects.Sender.sendPacket(new Packet53SetPlayerMode(true));
	}
}
