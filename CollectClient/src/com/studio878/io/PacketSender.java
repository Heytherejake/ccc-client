package com.studio878.io;

import java.io.PrintWriter;
import java.util.ArrayList;
import com.studio878.packets.Packet;

public class PacketSender extends Thread {
	private ArrayList<String> queue = new ArrayList<String>();
	private PrintWriter out;
	public PacketSender(PrintWriter p) {
		out = p;
	}
	public synchronized void sendPacket(Packet p) {
		queue.add(p.getDataString());
		notify();
	}
	
	public synchronized String getNextMessage() throws InterruptedException {
		while(queue.size() == 0) {
			wait();
		}
		String m = queue.get(0);
		queue.remove(0);
		return m;
	}
	
	public void sendMessageToServer(String m) {
		out.println(m);
		out.flush();
	}
	
	public void run() {
		try {
			while(!isInterrupted()) {
				String m = getNextMessage();
				sendMessageToServer(m);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}


}
