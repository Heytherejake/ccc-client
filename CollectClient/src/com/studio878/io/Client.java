package com.studio878.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;

import com.studio878.displays.Menu;
import com.studio878.objects.EntityManager;
import com.studio878.objects.Player;
import com.studio878.packets.Packet;
import com.studio878.packets.PacketConverter;
import com.studio878.util.Console;
import com.studio878.util.Settings;

public class Client extends Thread {
	private String host;
	public int port;
	BufferedReader in = null;
	PrintWriter out = null;
	PacketSender sender = null;
	public boolean softDisconnect = false;
	public Socket socket;
	static ArrayList<Player> players = new ArrayList<Player>();
	public Client(String h, int p) throws IOException{
		host = h;
		port = p;
		socket = new Socket(host, port);
		in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		out = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));
		Console.write("Connected to server " + host + ":" + port);
		sender = new PacketSender(out);
		sender.setDaemon(true);
		sender.start();
		NetObjects.Sender = sender;
		this.start();

	}
	public Player findPlayer(String n) {
		for(int i = 0; i < players.size(); i++) {
			if(players.get(i).getName().equalsIgnoreCase(n)) {
				return players.get(i);
			}
		}
		return null;
	}

	public Player addPlayer(String p, int id) {
		//Temporary
		Player ps = new Player(3200, 1300, p, id);
		players.add(ps);
		EntityManager.registerEntity(ps, id);
		return ps;
	}

	public void removePlayer(String p) {
		for(int i = 0; i < players.size(); i++) {
			if(players.get(i).getName().equalsIgnoreCase(p)) {
				players.remove(p);
			}
		}
	}
	public void run() {
		try {
			String m;
			while((m = in.readLine()) != null) {
				try {
					Packet ps = PacketConverter.createPacket(m);
					if (ps != null) {
						ps.process();
					}
				} catch (NullPointerException e) {
					if(m.substring(0, 1).equals("04")) {
						System.out.println("Invalid world.");
					} else {
						System.out.println("Invalid packet: " + m);
					}
					e.printStackTrace();
				}

			}
		} catch (IOException e) {
			System.out.println("Lost connection");
			if(!softDisconnect) {
				Console.error("Disconnected from " + host);
				Settings.MainWindow.setDisplay(new Menu(2));
				Menu.setDisplayMessage("Lost connection to server");
			}
			softDisconnect = false;

		} catch (NullPointerException e) {
		}
	}

	public void disconnect() {
		try {
			in.close();
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
