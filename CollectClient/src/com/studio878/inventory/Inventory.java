package com.studio878.inventory;

import java.util.ArrayList;
import java.util.concurrent.CopyOnWriteArrayList;

import com.studio878.crafting.CraftingRecipe;
import com.studio878.io.NetObjects;
import com.studio878.packets.Packet26SetWeapon;
import com.studio878.util.Settings;

public class Inventory {

	static CopyOnWriteArrayList<Item> items = new CopyOnWriteArrayList<Item>();
	static CopyOnWriteArrayList<CraftingRecipe> bonuses = new CopyOnWriteArrayList<CraftingRecipe>();
	public static int Slots = 11;
	static int selectedWeapon = 0;
	static int selectedBlock = 0;
	static ItemID selectedItemID;

	static int balance;

	public static CopyOnWriteArrayList<Item> getItems() {
		return items;
	}

	public static CopyOnWriteArrayList<CraftingRecipe> getBonuses() {
		return bonuses;
	}

	public static void addBonus(CraftingRecipe r) {
		bonuses.add(r);
	}

	public static boolean hasBonus(CraftingRecipe r) {
		for(CraftingRecipe rs : bonuses) {
			if(rs.getId() == r.getId()) { 
				return true;
			}
		}
		return false;
	}

	public static int getBalance() {
		return balance;
	}

	public static void setBalance(int amount) {
		balance = amount;
	}
	public static Item getSelectedBlock() {
		try {
			return getItemFromSlot(selectedBlock);
		} catch (Exception e) {
			return null;
		}
	}

	public static void clearBonuses() {
		bonuses.clear();
	}
	public static void setNextInventoryWeapon() {
		int nw = Inventory.getNextWeapon();
		if(nw != -1) {
			selectedItemID = getItemFromSlot(nw).getType();
			NetObjects.Sender.sendPacket(new Packet26SetWeapon(nw));
		}
	}

	public static void setSelectedWeapon(int id) {
		selectedWeapon = id;
	}
	
	public static void setNextInventoryBlock() {
		int nw = Inventory.getNextBlock();
		if(nw != -1) {
			selectedItemID = getItemFromSlot(nw).getType();
			NetObjects.Sender.sendPacket(new Packet26SetWeapon(nw));
		}
	}

	public static void setPreviousInventoryBlock() {
		int nw = Inventory.getPreviousBlock();
		if(nw != -1) {
			selectedItemID = getItemFromSlot(nw).getType();

			NetObjects.Sender.sendPacket(new Packet26SetWeapon(nw));
		}
	}

	public static int countWeapons() {
		int count = 0;
		for(Item i : items) {
			if(i.getType().isWeapon()) {
				count++;
			}
		}
		return count;
	}

	public static void setPreviousInventoryWeapon() {
		int nw = Inventory.getPreviousWeapon();
		if(nw != -1) {
			selectedItemID = getItemFromSlot(nw).getType();

			NetObjects.Sender.sendPacket(new Packet26SetWeapon(nw));
		}
	}

	public static int getNextBlock() {
		for(int i = selectedBlock + 1; i < Slots; i++) {
			if(getItemFromSlot(i) != null && getItemFromSlot(i).getType().isBlock()) {
				selectedBlock = i;
				selectedItemID = getItemFromSlot(i).getType();

				return i;
			}
		}
		for(int i = 0; i <= selectedBlock; i++) {
			if(getItemFromSlot(i) != null && getItemFromSlot(i).getType().isBlock()) {
				selectedBlock = i;
				selectedItemID = getItemFromSlot(i).getType();
				return i;
			}
		}
		return -1;

	}

	public static int getAmount(ItemID i) {
		if(i == ItemID.lookupName("Coins")) {
			return balance;
		}
		int amount = 0;
		for(Item is : items) {
			if(is.getType() == i) {
				amount += is.getAmount();
			}
		}
		return amount;
	}
	public static int getPreviousBlock() {
		for(int i = selectedBlock - 1; i > 0; i--) {
			if(getItemFromSlot(i) != null && getItemFromSlot(i).getType().isBlock()) {
				selectedBlock = i;
				selectedItemID = getItemFromSlot(i).getType();
				return i;
			}
		}
		for(int i = Slots - 1; i >= selectedBlock; i--) {
			if(getItemFromSlot(i) != null && getItemFromSlot(i).getType().isBlock()) {
				selectedBlock = i;
				selectedItemID = getItemFromSlot(i).getType();
				return i;
			}
		}
		return -1;
	}

	public static int getNextWeapon() {
		for(int i = selectedWeapon + 1; i < Slots; i++) {
			if(getItemFromSlot(i) != null && getItemFromSlot(i).getType().isWeapon()) {
				selectedWeapon = i;
				selectedItemID = getItemFromSlot(i).getType();
				return i;
			}
		}
		for(int i = 0; i <= selectedWeapon; i++) {
			if(getItemFromSlot(i) != null && getItemFromSlot(i).getType().isWeapon()) {
				selectedWeapon = i;
				selectedItemID = getItemFromSlot(i).getType();
				return i;
			}
		}
		return -1;
	}

	public static boolean isReloading() {
		return System.currentTimeMillis() - Settings.ActivePlayer.getLastFireTime() >= Settings.ActivePlayer.getWeapon().getTimeOffset();
	}

	public static int getPreviousWeapon() {
		for(int i = selectedWeapon - 1; i > 0; i--) {
			if(getItemFromSlot(i) != null && getItemFromSlot(i).getType().isWeapon()) {
				selectedWeapon = i;
				selectedItemID = getItemFromSlot(i).getType();
				return i;
			}
		}
		for(int i = Slots - 1; i >= selectedWeapon; i--) {
			if(getItemFromSlot(i) != null && getItemFromSlot(i).getType().isWeapon()) {
				selectedWeapon = i;
				selectedItemID = getItemFromSlot(i).getType();
				return i;
			}
		}
		return -1;
	}

	public static int countBlocks() {
		int count = 0;
		for(Item it: items) {
			if(it.getType().isBlock()) {
				count++;
			}
		}
		return count;
	}

	public static ArrayList<Item> getItems(ItemID i) {
		ArrayList<Item> result = new ArrayList<Item>();
		for(Item it : items) {
			if(it.getType() == i) {
				result.add(it);
			}
		}
		return result;
	}

	public static Item getItem(ItemID i) {
		for(Item it : items) {
			if(it.getType() == i) {
				return it;
			}
		}
		return null;
	}

	public static Item getSelectedItem()  {
		try {
			return getItemFromSlot(selectedWeapon);
		} catch (Exception e) {
			return null;
		}
	}

	public static Item getItemFromSlot(int slot) {
		for(Item it : items) {
			if(it.getSlot() == slot) {
				return it;
			}
		}
		return null;	
	}

	public static void clearInventory() {
		items.clear();
	}

	public static void removeItemFromSlot(int slot) {
		for(Item it : items) {
			if(it.getSlot() == slot) {
				items.remove(it);
			}
		}
	}


	public static Item getItem(ItemID i, int amount) {
		for(Item it : items) {
			if(it.getType() == i && it.getAmount() >= amount) {
				return it;
			}
		}
		return null;	
	}

	public static void addItem(ItemID i, int amount, int level, int slot) {
		if(i == ItemID.lookupName("Coins")){
			balance += amount;
			return;
		}
		Item s = new Item(i, amount, level);
		s.setSlot(slot);
		items.add(s);		
	}

	public static void addItem(ItemID i, int amount, int level, int slot, String data) {
		if(i == ItemID.lookupName("Coins")){
			balance += amount;
			return;
		}
		Item s = new Item(i, amount, level);
		s.setSlot(slot);
		s.setData(data);
		items.add(s);		
	}
	public static boolean contains(ItemID i) {
		if(i == ItemID.lookupName("Coins")){
			return (balance > 0);
		}
		for(Item it: items) {
			if(it.getType() == i) {
				return true;
			}
		}
		return false;
	}

	public static void setSelectedItem(ItemID i) {
		selectedItemID = i;
	}
	public static boolean contains(ItemID i, int amount) {
		if(i == ItemID.lookupName("Coins")){
			return (balance >= amount);
		}
		int total = 0;
		for(Item it: items) {
			if(it.getType() == i) {
				total += it.getAmount();
			}
		}
		return total >= amount;
	}

}
