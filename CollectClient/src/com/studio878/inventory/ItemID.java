package com.studio878.inventory;

import java.awt.Point;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import org.newdawn.slick.Image;

import com.studio878.block.BlockType;
import com.studio878.util.Console;
import com.studio878.util.Sheets;

public class ItemID {
	int id;
	Image image;
	Image antialiased;
	String referenceName, description;
	String[] names;
	int weaponType;
	int cost, buyCost;
	private static ConcurrentHashMap<Integer, ItemID> lookupId = new ConcurrentHashMap<Integer, ItemID>();
	private static ConcurrentHashMap<String, ItemID> lookupName = new ConcurrentHashMap<String, ItemID>();
	String source;
	
	private ItemID(int id, Point p, String refName, String[] names, int weaponType, String source, int cost, String description, int buyCost) {
		this.id = id;
		image = Sheets.ITEMS.getSubImage(p.x, p.y);
		antialiased = Sheets.ITEMS_ANTIALIASED.getSubImage(p.x, p.y);
		this.referenceName = refName;
		this.names = names;
		this.weaponType = weaponType;
		this.source = source;
		this.cost = cost;
		this.description = description;
		this.buyCost = buyCost;
	}
	
	public int getId() {
		return id;
	}
	
	public String getDescription() {
		return description;
	}
	
	public Image getImage() {
		return image;
	}
	
	public int getSellCost() {
		return cost;
	}
	
	public Image getAntiAliasedImage() {
		return antialiased;
	}
	
	public String getReferenceName() {
		return referenceName;
	}
	
	public int getWeaponType() {
		return weaponType;
	}
	
	public int getBuyCost() {
		return buyCost;
	}
	
	public String[] getNames() {
		return names;
	}
	
	public BlockType getSource() {
		return BlockType.lookupName(source);
	}
	
	
	public boolean isBlock() {
		return source != null && (!source.equals("Air") && !source.equals("null"));
	}
	
	public boolean isWeapon() {
		return weaponType == 1 ||weaponType == 2;
	}
	
	public static void clearItems() {
		lookupId.clear();
		lookupName.clear();
	}
	
	public static ItemID lookupId(int id) {
		return lookupId.get(id);
	}

	public static ItemID lookupName(String s) {
		for(Entry<String, ItemID> sc: lookupName.entrySet()) {
			if(s.equalsIgnoreCase(sc.getKey())) {
				return sc.getValue();
			}
		}
		return null;
	}
	
	public static void addItem(int id, String ref, String[] names, int weaponType, String source, Point img, int cost, String description, int buyCost) {
		ItemID item = new ItemID(id, img, ref, names, weaponType, source, cost, description, buyCost);
		lookupId.put(id, item);
		lookupName.put(ref, item);
		Console.write("Item registered: " + ref + " (" + id + ")");
	}
	
}
