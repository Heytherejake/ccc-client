package com.studio878.inventory;


public class Item implements Comparable<Item> {

	
	ItemID type;
	int amount;
	int slot = 0;
	String data = "";
	long lastReload = 0;
	int level;
	
	public Item(ItemID i, int a, int level) {
		type = i;
		amount = a;
		this.level = level;
	}
	
	public Item(ItemID i, int a) {
		type = i;
		amount = a;
		this.level = 0;
	}
	
	public String getData() {
		return data;
	}
	
	public int getLevel() {
		return level;
	}
	
	public void setData(String s) {
		data = s;
	}
	
	public ItemID getType() {
		return type;
	}
	
	public int getAmount() {
		return amount;
	}
	
	public void setAmount(int a) {
		amount = a;
	}
	
	public int getSlot() {
		return slot;
	}
	
	public void setSlot(int i) {
		slot = i;
	}
	
	public boolean isWeapon() {
		return data.split("/").length ==  5 && !data.split("/")[1].equals("0");
	}
	public int getClip() {
		String[] parts = data.split("/");
		if(parts.length == 5) {
			int c = Integer.parseInt(parts[0]);
			return c;
		}
		return 0;
	}
	
	public int getTotalSize() {
		String[] parts = data.split("/");
		if(parts.length == 5) {
			int c = Integer.parseInt(parts[1]);
			return c;
		}
		return 0;
	}
	
	public long getLastReloadTime() {
		return lastReload;
	}
	
	public void setLastReloadTime(long time) {
		this.lastReload = time;
	}
	
	public int getReloadTime() {
		String[] parts = data.split("/");
		if(parts.length == 5) {
			int c = Integer.parseInt(parts[3]);
			return c;
		}
		return 0;
	}
	public int getTotalAmmo() {
		String[] parts = data.split("/");
		if(parts.length == 5) {
			int c = Integer.parseInt(parts[2]);
			return c;
		}
		return 0;
	}
	
	public double getRateOfFireModifier() {
		String[] parts = data.split("/");
		if(parts.length == 5) {
			double c = Double.parseDouble(parts[4]);
			return c;
		}
		return 0;
	}
	
	public int compareTo(Item ts) {
		return ((Integer) slot).compareTo(ts.slot);
	}
}
