package com.studio878.world;



import org.lwjgl.input.Mouse;
import org.newdawn.slick.GameContainer;

import com.studio878.util.Settings;

public class Camera {
	public static float X_OFFSET = -3200;
	public static float Y_OFFSET = -1200;

	public static int X_GOAL = -3200;
	public static int Y_GOAL = -1200;
	public static void setOffset(int x, int y) {
		X_OFFSET = x;
		Y_OFFSET = y;
	}
	public static float convertXToCamera(int x) {
		return x - Camera.X_OFFSET;
	}
	public static float convertYToCamera(GameContainer c, int y) {
		return c.getHeight() - (Mouse.getY() + Camera.Y_OFFSET);
	}

	public static void reposition() {
		if(Settings.CameraPlayer != null) {
			Camera.X_OFFSET = (float) Math.round(-1 * Settings.CameraPlayer.getX() + (Settings.WindowWidth / 2));
			Camera.Y_OFFSET = (float) Math.round(-1 * Settings.CameraPlayer.getY() + (Settings.WindowHeight / 2));
		}

	}
}
