package com.studio878.packets;

import com.studio878.inventory.Inventory;
import com.studio878.inventory.Item;

public class Packet60ReceiveSlotUpdate implements Packet {

	String input;
	
	public Packet60ReceiveSlotUpdate(String input) {
		this.input = input;
	}
	
	public String getDataString() {
		return "60:" + input;
	}

	public void process() {
		String[] split = input.split(",");
		Item i = Inventory.getItemFromSlot(Integer.parseInt(split[2]));
		i.setData(split[3]);
		i.setAmount(Integer.parseInt(split[1]));
		System.out.println(split[3]);
	}

}
