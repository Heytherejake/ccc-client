package com.studio878.packets;

import org.newdawn.slick.Image;

import com.studio878.characters.PlayerCharacter;
import com.studio878.crafting.BonusItem;
import com.studio878.crafting.BonusItem.BonusItemType;
import com.studio878.crafting.CraftingRecipeList;
import com.studio878.crafting.CraftingRecipeList.RecipeType;
import com.studio878.displays.Menu;
import com.studio878.inventory.Item;
import com.studio878.inventory.ItemID;
import com.studio878.util.Sheets;

public class Packet71ReceiveRecipeList implements Packet {

	String input;

	public Packet71ReceiveRecipeList(String input) {
		this.input = input;
	}

	public String getDataString() {
		return "71:" + input;
	}

	public void process() {
		CraftingRecipeList.Recipes.clear();
		Menu.setDisplayMessage("Loading shop items...");

		for(String p : input.split("~")) {
			String[] split = p.split("_!_");

			int id = Integer.parseInt(split[0]);
			String name = split[1];
			String description = split[2];
			Item result = null;
			if(!split[3].equals(-1) && !split[4].equals(-1)) {
				result = new Item(ItemID.lookupId(Integer.parseInt(split[3])), Integer.parseInt(split[4]));
			}
			RecipeType type = RecipeType.lookupId(Integer.parseInt(split[5]));
			Image img = null;
			if(!split[6].equals("-1") && !split[7].equals("-1")) {
				img = Sheets.RESEARCH_IMAGES.getSubImage(Integer.parseInt(split[6]), Integer.parseInt(split[7]));
			}
			Item[] costs = null;
			if(!split[8].equals("null")) {
				String[] sp = split[8].split("_%_");
				costs = new Item[sp.length];
				int i = 0;
				for(String part : sp) {
					String[] spl = part.split("_&_");
					costs[i] = new Item(ItemID.lookupId(Integer.parseInt(spl[0])), Integer.parseInt(spl[1]));
					i++;
				}
			}
			
			PlayerCharacter[] specs = null;
			if(!split[9].equals("null")) {
				String[] sp = split[9].split("_%_");
				specs = new PlayerCharacter[sp.length];
				int i = 0;
				for(String s : sp) {
					specs[i] = PlayerCharacter.lookupId(Integer.parseInt(s));
					i++;
				}
			}

			String[] upsides = split[10].split("_%_");
			String[] downsides = null;
			if(!split[11].equals("null")) {
				downsides = split[11].split("_%_");
			}

			BonusItem[] bonuses = null;
			if(!split[12].equals("null")) {
				String[] sp = split[12].split("_%_");
				bonuses = new BonusItem[sp.length];
				int i = 0;
				for(String s : sp) {
					String[] spl = s.split("_&_");
					bonuses[i] = new BonusItem(BonusItemType.lookupId(Integer.parseInt(spl[0])), Double.parseDouble(spl[1]));
				}
			}
			
			int buycost = Integer.parseInt(split[13]);


			CraftingRecipeList.addRecipe(id, name, description, result, img, upsides, downsides, costs, bonuses, type, specs, buycost);
		}


	}

}
