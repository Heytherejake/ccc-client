package com.studio878.packets;

public class Packet23FireWeapon implements Packet{

	double angle;
	
	public Packet23FireWeapon(double angle) {
		this.angle = angle;
	}
	public String getDataString() {
		return "23:" + angle;
	}

	public void process() {
		
	}

}
