package com.studio878.packets;

import com.studio878.displays.Menu;
import com.studio878.util.Settings;

public class Packet01Connect implements Packet{
	private String username;
	private String session;
	public Packet01Connect(String username, String session) {
		this.username = username;
		this.session = session;
	}
	
	public String getDataString() {
		Menu.setDisplayMessage("Sending client information...");
		return "01:"+username + ":" + session + ":" + Settings.GameVersion;
	}

	public void process() {
		
	}
}
