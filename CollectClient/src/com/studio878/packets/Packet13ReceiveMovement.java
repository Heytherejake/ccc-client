package com.studio878.packets;

import com.studio878.block.Block;
import com.studio878.objects.Entity;
import com.studio878.objects.EntityManager;
import com.studio878.objects.HealthEntity;
import com.studio878.objects.PhysicsAffectedEntity;
import com.studio878.objects.Player;
import com.studio878.util.Settings;
import com.studio878.util.SoundEffects;
import com.studio878.world.Grid;

public class Packet13ReceiveMovement implements Packet {

	double x, y;
	int id;
	int facing;
	double rotation;

	public Packet13ReceiveMovement(double x, double y, int id, int t, double rotation) {
		this.x = x;
		this.y = y;
		this.id = id;
		facing = t;
		this.rotation = rotation;
	}

	public String getDataString() {
		return "13:" + x + ":" + y + ":" + id + ":" + facing + ":" + rotation;
	}

	public void process() {
		try {
			Entity e =  EntityManager.getEntity(id);
			if(e != null) {
				double px = e.getX();
				if(!(Settings.CalculateMovement && e.getId() == Settings.ActivePlayer.getId())) {
					if(!(e instanceof PhysicsAffectedEntity)) {
						e.setX(x);
						e.setY(y);
					} else {
						PhysicsAffectedEntity p = (PhysicsAffectedEntity) e;
						p.setGoal(x, y);
					}

					e.setFacingDirection(facing);
					e.setRotation(rotation);
				}
				Block b =  Grid.getClosestBlock((int)e.getX(),(int) e.getY() + e.getHeight());

				if(b != null && b.getType().getStepSound() != null && px != x &&  e instanceof PhysicsAffectedEntity) {
					PhysicsAffectedEntity p = (PhysicsAffectedEntity) e;

					if(p.stepSound) {
						if(System.currentTimeMillis() - p.timeSinceLastStep >= 350) {
							SoundEffects.playSound(b.getType().getStepSound(), (int) e.getX(), (int) e.getY());
							p.timeSinceLastStep = System.currentTimeMillis();
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
