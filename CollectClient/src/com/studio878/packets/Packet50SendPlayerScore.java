package com.studio878.packets;

import com.studio878.objects.EntityManager;
import com.studio878.objects.Player;

public class Packet50SendPlayerScore implements Packet {

	int id, kills, deaths;
	
	public Packet50SendPlayerScore(int id, int kills, int deaths) {
		this.id = id;
		this.kills = kills;
		this.deaths = deaths;
	}
	
	public String getDataString() {
		return "50:" + id + ":" + kills + ":" + deaths;
	}

	public void process() {
		Player p = (Player) EntityManager.getEntity(id);
		p.setKills(kills);
		p.setDeaths(deaths);
		System.out.println(kills + ":" + deaths);
	}

}
