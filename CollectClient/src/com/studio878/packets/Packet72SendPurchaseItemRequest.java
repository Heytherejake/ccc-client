package com.studio878.packets;

import com.studio878.inventory.ItemID;

public class Packet72SendPurchaseItemRequest implements Packet {

	ItemID item;
	
	public Packet72SendPurchaseItemRequest(ItemID item) {
		this.item = item;
	}
	
	public String getDataString() {
		return "72:" + item.getId();
	}

	public void process() {
		
	}

}
