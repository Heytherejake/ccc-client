package com.studio878.packets;

import com.studio878.displays.MainGame;
import com.studio878.inventory.Inventory;
import com.studio878.objects.Player;
import com.studio878.util.Settings;


public class Packet02Approve implements Packet{
	
	boolean story, calculateMovement;
	
	public Packet02Approve(boolean story, boolean calculateMovement) {
		this.story = story;
		this.calculateMovement = calculateMovement;
	}
	public String getDataString() {
		return "02";
	}

	public void process() {

		Settings.StoryMode = story;
		Settings.CalculateMovement = calculateMovement;
		Settings.MainWindow.setDisplay(new MainGame());
		Inventory.setNextInventoryWeapon();

	}

}
