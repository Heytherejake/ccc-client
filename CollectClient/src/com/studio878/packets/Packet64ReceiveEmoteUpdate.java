package com.studio878.packets;

import com.studio878.objects.Entity;
import com.studio878.objects.EntityManager;
import com.studio878.objects.Player;
import com.studio878.util.Sheets;
import com.studio878.util.Emote.EmoteType;

public class Packet64ReceiveEmoteUpdate implements Packet {

	EmoteType emote;
	int id;
	
	public Packet64ReceiveEmoteUpdate(EmoteType type, int id) {
		this.emote = type;
		this.id = id;
	}
	
	public String getDataString() {
		return "64:" + emote.getId();
	}

	public void process() {
		Entity e = EntityManager.getEntity(id);
		if(e instanceof Player) {
			Player p = (Player) e;
			p.setEmote(Sheets.EMOTES.getSubImage(emote.getPosition(), 0));
			p.setLastEmoteTime(System.currentTimeMillis());
		}
		
	}

}
