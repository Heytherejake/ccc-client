package com.studio878.packets;

import com.studio878.block.Block;

public class Packet06SendBlockDamage implements Packet{
	Block block;
	public Packet06SendBlockDamage(Block b) {
		block = b;
	}
	public String getDataString() {
		return "06:" + block.getLocationOnGrid().getX() + ":" + block.getLocationOnGrid().getY();
	}

	public void process() {

	}
}
