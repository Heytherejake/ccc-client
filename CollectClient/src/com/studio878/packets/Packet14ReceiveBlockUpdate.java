package com.studio878.packets;

import com.studio878.block.Block;
import com.studio878.block.BlockType;
import com.studio878.world.Grid;

public class Packet14ReceiveBlockUpdate implements Packet {

	int x, y, data, wireData;
	byte condition, offset;
	int type;
	
	BlockType wire;
	
	public Packet14ReceiveBlockUpdate(int x, int y, int type, int data, byte cond, byte offset, int wire, int wireData) {
		this.x = x;
		this.y = y;
		this.type = type;
		this.data = data;
		this.condition = cond;
		this.offset = offset;
		if(wire != -1) {
			this.wire = BlockType.lookupId(wire);
		}
		this.wireData = wireData;
	}
	
	public String getDataString() {
		return "14:" + x + ":" + y + ":" + type + ":" + data + ":" + condition + ":" + offset + ":" + wire.getId() + ":" + wireData;
	}

	public void process() {
		Block b = Grid.getBlockAt(x, y);
		if(b == null) return;
		b.setType(BlockType.lookupId(type));
		b.setCondition(condition);
		b.setData(data);
		b.setSquareLocation(offset);
		if(wire != null) {
			b.setType(wire);
		}
		b.setWireData(wireData);
	}

}
