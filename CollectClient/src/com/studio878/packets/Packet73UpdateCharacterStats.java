package com.studio878.packets;

import com.studio878.util.Settings;

public class Packet73UpdateCharacterStats implements Packet {
	int rangedDamage, meleeDamage, strength, dexterity, intelligence, health, mana, healthPerLevel, manaPerLevel, armor, magicDefence, healthRegen, manaRegen, lifeSteal;
	double moveSpeed;
	public Packet73UpdateCharacterStats(int rangedDamage, int meleeDamage, double moveSpeed, int strength, int dexterity, int intelligence, int health, int mana, int healthPerLevel, int manaPerLevel, int armor, int magicDefence, int healthRegen, int manaRegen, int lifeSteal) {
		this.rangedDamage = rangedDamage;
		this.meleeDamage = meleeDamage;
		this.moveSpeed = moveSpeed;
		this.strength = strength;
		this.dexterity = dexterity;
		this.intelligence = intelligence;
		this.health = health;
		this.mana = mana;
		this.healthPerLevel = healthPerLevel;
		this.manaPerLevel = manaPerLevel;
		this.armor = armor;
		this.magicDefence = magicDefence;
		this.healthRegen = healthRegen;
		this.manaRegen = manaRegen;
		this.lifeSteal = lifeSteal;
	}
	public String getDataString() {
		return "73:" + rangedDamage + ":" + meleeDamage + ":" + moveSpeed + ":" + strength + ":" + dexterity + ":" + intelligence + ":" + health + ":" + mana + ":" + healthPerLevel + ":" + manaPerLevel + ":" + armor + ":" + magicDefence + ":" + healthRegen + ":" + manaRegen + ":" + lifeSteal;
	}

	public void process() {
		Settings.ActivePlayer.getCharacter().updateCharacter(rangedDamage, meleeDamage, moveSpeed, strength, dexterity, intelligence, health, mana, healthPerLevel, manaPerLevel, armor, magicDefence, healthRegen, manaRegen, lifeSteal);
	}

}
