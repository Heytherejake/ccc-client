package com.studio878.packets;

public class Packet53SetPlayerMode implements Packet {
	
	boolean mode;
	
	public Packet53SetPlayerMode(boolean mode) {
		this.mode = mode;
	}
	
	public String getDataString() {		
		return "53:" + mode;
	}

	public void process() {
		
	}

}
