package com.studio878.packets;

import com.studio878.inventory.Inventory;
import com.studio878.util.Settings;

public class Packet32PlayerDeath implements Packet {

	public String getDataString() {
		return "32";
	}

	public void process() {
		Inventory.setNextInventoryWeapon();
		Settings.ActivePlayer.setHealth(0);
	}

}
