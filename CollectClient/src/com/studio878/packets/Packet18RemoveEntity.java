package com.studio878.packets;

import com.studio878.objects.Entity;
import com.studio878.objects.EntityManager;
import com.studio878.objects.EntityManager.EntityName;
import com.studio878.objects.Explosion;
import com.studio878.objects.Landmine;
import com.studio878.util.SoundEffects;

public class Packet18RemoveEntity implements Packet{
	int id;
	public Packet18RemoveEntity( int id) {
		this.id = id;
	}
	public String getDataString() {
		return "18:" + id;
	}

	public void process() {
		Entity es = EntityManager.getEntity(id);
		if(es != null) {
			EntityName t = es.getType();
			if(t != null && t == EntityName.Rocket || t == EntityName.Grenade || t == EntityName.Landmine) {
				if(!(es instanceof Landmine)) { 
					SoundEffects.playSound(SoundEffects.Grenade, (int) es.getX(), (int) es.getY());
						Explosion exp = new Explosion(es.getX() - 32, es.getY() - 32);
						EntityManager.registerEntity(exp, exp.getId());
				} else {
					Landmine ev = (Landmine) es;
					System.out.println(ev.hasHealthChanged + "|" + ev.getHealth());
					if(!(ev.hasHealthChanged && ev.getHealth() <= 0)) {
						SoundEffects.playSound(SoundEffects.Grenade, (int) es.getX(), (int) es.getY());
						Explosion exp = new Explosion(es.getX() - 32, es.getY() - 32);
						EntityManager.registerEntity(exp, exp.getId());
					}
				}
			}
			EntityManager.removeEntity(id);
		}

	}

}
