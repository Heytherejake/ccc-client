package com.studio878.packets;

import com.studio878.objects.Entity;

public class Packet65UpdateEntityData implements Packet {
	
	Entity entity;
	String data;
	
	public Packet65UpdateEntityData(Entity entity, String data) {
		this.entity = entity;
		this.data = data;
	}

	public String getDataString() {
		return "65:" + entity.getId() + ":" + data;
	}

	public void process() {
		entity.setData(data);
		
	}

}
