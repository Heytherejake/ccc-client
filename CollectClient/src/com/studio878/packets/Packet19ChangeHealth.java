package com.studio878.packets;

import com.studio878.objects.Entity;
import com.studio878.objects.EntityManager;
import com.studio878.objects.HealthEntity;
import com.studio878.objects.Player;
import com.studio878.util.API;
import com.studio878.util.Damage;
import com.studio878.util.Damage.DamageMethod;
import com.studio878.util.Settings;

public class Packet19ChangeHealth implements Packet{
	int id, health;
	DamageMethod source;
	String killer;
	Player p;
	public Packet19ChangeHealth(int id, int health, DamageMethod source, String killer) {
		this.id = id;
		this.health = health;
		this.source = source;
		this.killer = killer;
	}
	public String getDataString() {
		return "19:" + id + ":" + health + ":" + source + ":" + killer;
	}

	public void process() {
		Entity e = EntityManager.getEntity(id);
		if(e instanceof HealthEntity) {
			HealthEntity h = (HealthEntity) e;
			h.setHealth(health);
			EntityManager.emitBlood(h, e.getX(), e.getY(),((int) (Math.random()*10)+10));
			if(health <= 0) {
				EntityManager.emitBlood(h, e.getX(), e.getY(),((int) (Math.random()*50)+30));
				if(e instanceof Player) {
					 p = (Player) h;

					if(killer != null && killer.equals(p.getName())) {
						Damage.addMessage(source.getSuicideMessage().replace("[Player]", p.getName()));
						if(killer.equals(Settings.ActivePlayer.getName())) {
							Settings.CameraPlayer = null;
							Settings.ActivePlayer.setLastKiller(killer);
						}
					} else {
						Damage.addMessage(source.getMessage().replace("[Killer]", killer).replace("[Player]", p.getName()));
						if(!killer.equals(Settings.Username)) {
							Settings.CameraPlayer = EntityManager.matchPlayer(killer);
							Settings.ActivePlayer.setLastKiller(killer);
							try {
								new Thread() {
									public void run() {
										API.getObject("http://api.studio878software.com/ccc/network/user/" + Settings.Username + "/kill/log/" + killer + "/false/" + Settings.Password);
									}
								}.start();
							} catch (Exception es) {
								es.printStackTrace();
							}
						} else {
							try {
								new Thread() {
									public void run() {
										API.getObject("http://api.studio878software.com/ccc/network/user/" + Settings.Username + "/kill/log/" + Packet19ChangeHealth.this.p.getName() + "/true/" + Settings.Password);			
									}
								}.start();
							} catch (Exception es) {
								es.printStackTrace();
							}	
						}
					}
					h.setHealth(100);
				}
			}
		}

	}

}
