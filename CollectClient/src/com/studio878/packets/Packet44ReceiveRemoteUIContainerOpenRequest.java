package com.studio878.packets;

import com.studio878.remoteui.RemoteUIManager;

public class Packet44ReceiveRemoteUIContainerOpenRequest implements Packet{

	int id;

	public Packet44ReceiveRemoteUIContainerOpenRequest (int id) {
		this.id = id;
	}

	public String getDataString() {
		return "44:" + id;
	}

	public void process() {
		if(RemoteUIManager.getSelectedContainer() == -1) {
			RemoteUIManager.setSelectedContainer(id);
		}

	}
}
