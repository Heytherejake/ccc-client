package com.studio878.packets;

import com.studio878.characters.PlayerCharacter;

public class Packet74SelectCharacter implements Packet {

	PlayerCharacter character;
	
	public Packet74SelectCharacter(PlayerCharacter character) {
		this.character = character;
	}
	
	public String getDataString() {
		return "74:" + character.getId();
	}

	public void process() {
		
	}

}
