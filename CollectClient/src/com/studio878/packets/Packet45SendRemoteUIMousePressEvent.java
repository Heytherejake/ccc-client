package com.studio878.packets;

public class Packet45SendRemoteUIMousePressEvent implements Packet {

	int item, button, x, y, container;
	
	public Packet45SendRemoteUIMousePressEvent(int container, int item, int x, int y, int button) {
		this.item = item;
		this.button = button;
		this.x = x;
		this.y = y;
		this.container = container;
	}
	public String getDataString() {
		return "45:" + container + ":" + item + ":" + x + ":" + y + ":" + button;
	}

	public void process() {
		
	}

}
