package com.studio878.packets;

import com.studio878.inventory.ItemID;
import com.studio878.objects.EntityManager;
import com.studio878.story.MapEventManager.MapEventType;
import com.studio878.util.Console;
import com.studio878.util.Damage.DamageMethod;
import com.studio878.util.Emote.EmoteType;
import com.studio878.util.TeamManager.Team;


public class PacketConverter {
	public static Packet createPacket(String m) {
		try {
			String[] split = m.split(":");
			int id = Integer.parseInt(split[0]);

			switch (id) {
			case 2:
				return new Packet02Approve(Boolean.parseBoolean(split[1]), Boolean.parseBoolean(split[2]));
			case 3:
				return new Packet03WorldSize(Integer.parseInt(split[1]), Integer.parseInt(split[2]));
			case 4:
				return new Packet04ReceiveGrid(m.substring(3));
			case 8:
				String message = "";
				for(int i = 1; i < split.length; i++) {
					message += split[i] + ":";
				}
				message = message.substring(0, message.length() - 1);
				return new Packet08ReceiveMessage(message);
			case 9:
				return new Packet09ReceiveBlockDamage(split[1], Integer.parseInt(split[2]), Integer.parseInt(split[3]), Byte.parseByte(split[4]));
			case 10:
				return new Packet10ReceiveEntityList(m.substring(3));
			case 11:
				return new Packet11RemovePlayer(split[1], Integer.parseInt(split[2]));
			case 13:
				return new Packet13ReceiveMovement(Double.parseDouble(split[1]), Double.parseDouble(split[2]), Integer.parseInt(split[3]), Integer.parseInt(split[4]), Double.parseDouble(split[5]));
			case 14:
				return new Packet14ReceiveBlockUpdate(Integer.parseInt(split[1]), Integer.parseInt(split[2]), Integer.parseInt(split[3]), Integer.parseInt(split[4]), Byte.parseByte(split[5]), Byte.parseByte(split[6]), Integer.parseInt(split[7]), Integer.parseInt(split[8]));
			case 15:
				return new Packet15AddEntity(Integer.parseInt(split[1]), Integer.parseInt(split[2]), Double.parseDouble(split[3]), Double.parseDouble(split[4]), split[5]);
			case 16:
				return new Packet16KickPlayer(split[1]);
			case 18:
				return new Packet18RemoveEntity(Integer.parseInt(split[1]));
			case 19:
				return new Packet19ChangeHealth(Integer.parseInt(split[1]), Integer.parseInt(split[2]), DamageMethod.lookupId(Integer.parseInt(split[3])), split[4]);
			case 21:
				if(m.length() > 3) {
					return new Packet21ReceiveInventory(m.substring(3));
				}
			case 25:
				return new Packet25GetArmRotation(Integer.parseInt(split[1]), Double.parseDouble(split[2]));
			case 28:
				return new Packet28ReceiveWeaponChange(Integer.parseInt(split[1]), Integer.parseInt(split[2]), Integer.parseInt(split[3]));
			case 29:
				return new Packet29ReceiveBullet(Double.parseDouble(split[1]), Double.parseDouble(split[2]), Double.parseDouble(split[3]), ItemID.lookupId(Integer.parseInt(split[4])));
			case 30:
				return new Packet30WeaponFired(Double.parseDouble(split[1]), Double.parseDouble(split[2]), ItemID.lookupId(Integer.parseInt(split[3])));
			case 32:
				return new Packet32PlayerDeath();
			case 34:
				return new Packet34ReceiveServerCommandResponse(m.substring(3));
			case 36:
				return new Packet36ReceiveCraftingConfirmation(Integer.parseInt(split[1]), split[2], Boolean.parseBoolean(split[3]));
			case 37:
				return new Packet37ReceivePlayerInfo(m.substring(3));
			case 39:
				return new Packet39ReceiveHandshake(Long.parseLong(split[1]));
			case 41:
				return new Packet41ReceiveRemoteUIContainer(m.substring(3));
			case 42:
				return new Packet42ReceiveRemoteUIItemUpdate(m.substring(3));
			case 43:
				return new Packet43ReceiveRemoteUIContainerCloseRequest(Integer.parseInt(split[1]));
			case 44:
				return new Packet44ReceiveRemoteUIContainerOpenRequest(Integer.parseInt(split[1]));
			case 47:
				return new Packet47RequestRemoteUIItemValue(Integer.parseInt(split[1]), Integer.parseInt(split[2]));
			case 49:
				return new Packet49ReceiveRespawnTime(Integer.parseInt(split[1]));
			case 50:
				return new Packet50SendPlayerScore(Integer.parseInt(split[1]), Integer.parseInt(split[2]), Integer.parseInt(split[3]));
			case 51:
				return new Packet51RoundOver(Team.lookupId(Integer.parseInt(split[1])), Integer.parseInt(split[2]));
			case 54:
				return new Packet54PlayBlockSound(Integer.parseInt(split[1]), Integer.parseInt(split[2]));
			case 55:
				return new Packet55SetEntityState(Integer.parseInt(split[1]), Integer.parseInt(split[2]));
			case 56:
				return new Packet56ReceiveCriticalMessage(split[1]);
			case 58:
				return new Packet58ReceivePlayerBalance(Integer.parseInt(split[1]));
			case 60:
				return new Packet60ReceiveSlotUpdate(m.substring(3));
			case 61:
				return new Packet61ReceiveMapEvent(MapEventType.lookupId(Integer.parseInt(split[1])), split[2]);
			case 64:
				return new Packet64ReceiveEmoteUpdate(EmoteType.lookupId(Integer.parseInt(split[1])), Integer.parseInt(split[2]));
			case 65:
				return new Packet65UpdateEntityData(EntityManager.getEntity(Integer.parseInt(split[1])), split[2]);
			case 66:
				return new Packet66ForceLocationUpdate(Integer.parseInt(split[1]), Double.parseDouble(split[2]), Double.parseDouble(split[3]));
			case 67:
				return new Packet67ReceiveItemList(m.substring(3));
			case 68:
				return new Packet68ReceiveBlockList(m.substring(3));
			case 69:
				return new Packet69ReceiveVelocity(Integer.parseInt(split[1]), Double.parseDouble(split[2]), Double.parseDouble(split[3]));
			case 71:
				return new Packet71ReceiveRecipeList(m.substring(3));
			case 73:
				return new Packet73UpdateCharacterStats(Integer.parseInt(split[1]), Integer.parseInt(split[2]), Double.parseDouble(split[3]), Integer.parseInt(split[4]), Integer.parseInt(split[5]), Integer.parseInt(split[6]), Integer.parseInt(split[7]), Integer.parseInt(split[8]), Integer.parseInt(split[9]), Integer.parseInt(split[10]), Integer.parseInt(split[11]), Integer.parseInt(split[12]), Integer.parseInt(split[13]), Integer.parseInt(split[14]), Integer.parseInt(split[15]));
			}
		} catch (Exception e) {
			Console.error("Bad packet received from server: " + m);
			e.printStackTrace();
		}
		return null;
	}
}
