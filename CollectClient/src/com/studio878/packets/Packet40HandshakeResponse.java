package com.studio878.packets;

public class Packet40HandshakeResponse implements Packet{

	long time;
	
	public Packet40HandshakeResponse(long time) {
		this.time = time;
	}
	public String getDataString() {
		return "40:" + time;
	}

	public void process() {
		
	}

}
