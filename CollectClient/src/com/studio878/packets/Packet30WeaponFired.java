package com.studio878.packets;

import com.studio878.inventory.ItemID;
import com.studio878.objects.EntityManager;
import com.studio878.objects.Shell;
import com.studio878.util.Settings;

public class Packet30WeaponFired implements Packet{
	double x, y;
	ItemID id;
	public Packet30WeaponFired(double x, double y, ItemID id) {
		this.x = x;
		this.y = y;
		this.id = id;
	}
	public String getDataString() {
		return "29:" + x + ":" + y + ":" + id.getId();
	}

	public void process() {
		if(Settings.ActivePlayer.getWeapon().doesDropShell()) {
		Shell e = new Shell(x, y);
		double xoff = (Math.sin(Settings.ActivePlayer.gunRotation)*Settings.ActivePlayer.getWeapon().getXOffset());
		double yoff = (Math.cos(Settings.ActivePlayer.gunRotation)*Settings.ActivePlayer.getWeapon().getYOffset());
		e.setX(e.getX() + xoff);
		e.setY(e.getY() - yoff*3);
		e.setRotation(Math.toDegrees(Settings.ActivePlayer.gunRotation) - 90);
		e.setYSpeed(-1);
		int dir = -1;
		if(Settings.ActivePlayer.getFacingDirection() == 2 || Settings.ActivePlayer.getFacingDirection() == -1) {
			dir *= -1;
		}
		e.setXSpeed(Math.random() * dir);
		EntityManager.registerEntity(e, e.getId());
		}
	}

}
