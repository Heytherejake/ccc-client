package com.studio878.packets;

import java.util.StringTokenizer;

import com.studio878.displays.MainGame;
import com.studio878.inventory.Inventory;
import com.studio878.inventory.ItemID;


public class Packet21ReceiveInventory implements Packet {
	
	String intake;
	
	public Packet21ReceiveInventory(String intake) {
		this.intake = intake;
	}
	
	public String getDataString() {
		return "21";
	}

	public void process() {

		StringTokenizer t = new StringTokenizer(intake, ":");
		Inventory.clearInventory();
		while(t.hasMoreTokens()) {
			String[] parts = t.nextToken().split(",");
			Inventory.addItem(ItemID.lookupId(Integer.parseInt(parts[0])), Integer.parseInt(parts[1]), Integer.parseInt(parts[4]), Integer.parseInt(parts[2]), parts[3]);
		}
		
		if(MainGame.openCraftingMenu == 1) {
			if(MainGame.isSelling) {
				MainGame.populateSellingList();
			} else {
				MainGame.populateCraftingList(MainGame.openCraftingType);
			}
		}
	}

}
