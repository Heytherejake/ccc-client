package com.studio878.packets;

public interface Packet {
	public String getDataString();
	public void process();
}
 