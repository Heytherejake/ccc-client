package com.studio878.packets;

import java.awt.Point;

import com.studio878.displays.Menu;
import com.studio878.inventory.ItemID;

public class Packet67ReceiveItemList implements Packet {

	String input;

	public Packet67ReceiveItemList(String input) {
		this.input = input;
	}

	public String getDataString() {
		return "67:" + input;
	}

	public void process() {
		ItemID.clearItems();
		Menu.setDisplayMessage("Loading items...");

		for(String p : input.split("~")) {
			String[] split = p.split("_!_");
			ItemID.addItem(Integer.parseInt(split[0]), split[1], split[2].split("_%_"), Integer.parseInt(split[3]), split[4], new Point(Integer.parseInt(split[5]), Integer.parseInt(split[6])), Integer.parseInt(split[7]), split[8], Integer.parseInt(split[9]));
		}


	}

}
