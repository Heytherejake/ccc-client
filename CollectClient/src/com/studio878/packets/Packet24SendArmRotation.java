package com.studio878.packets;

public class Packet24SendArmRotation implements Packet {

	double angle;
	
	public Packet24SendArmRotation(double angle) {
		this.angle = angle;
	}
	
	public String getDataString() {
		return "24:"+angle;
	}

	public void process() {
		
	}

}
