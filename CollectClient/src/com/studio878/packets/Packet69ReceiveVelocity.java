package com.studio878.packets;

import com.studio878.objects.Entity;
import com.studio878.objects.EntityManager;
import com.studio878.objects.PhysicsAffectedEntity;

public class Packet69ReceiveVelocity implements Packet {

	int id;
	double dx, dy;
	
	public Packet69ReceiveVelocity(int id, double dx, double dy) {
		this.id = id;
		this.dx = dx;
		this.dy = dy;
	}
	
	public String getDataString() {
		return "69:" + id + ":" + dx + ":" + dy;
	}

	public void process() {
		Entity e = EntityManager.getEntity(id);
		if(e != null && (e instanceof PhysicsAffectedEntity)) {
			PhysicsAffectedEntity p = (PhysicsAffectedEntity) e;
			p.setXSpeed(dx);
			p.setYSpeed(dy);
		}
		
	}

}
