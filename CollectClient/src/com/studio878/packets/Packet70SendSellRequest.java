package com.studio878.packets;

import com.studio878.ui.SellListItem;

public class Packet70SendSellRequest implements Packet {

	SellListItem item;
	
	public Packet70SendSellRequest(SellListItem item) {
		this.item = item;
	}
	
	public String getDataString() {
		return "70:" + item.getItem().getType().getId() + ":" + item.getItem().getLevel();
	}

	public void process() {
		
	}

}
