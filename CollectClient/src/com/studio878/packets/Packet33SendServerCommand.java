package com.studio878.packets;

public class Packet33SendServerCommand implements Packet{

	String command;
	
	public Packet33SendServerCommand(String s) {
		command = s;
	}
	public String getDataString() {
		return "33:" + command;
	}

	public void process() {
		
	}

}
