package com.studio878.packets;

import com.studio878.crafting.CraftingRecipe;
import com.studio878.crafting.CraftingRecipeList;
import com.studio878.crafting.CraftingRecipeList.RecipeType;
import com.studio878.displays.MainGame;
import com.studio878.inventory.Inventory;

public class Packet36ReceiveCraftingConfirmation implements Packet {

	String message;
	int id;
	boolean didSucceed;
	
	public Packet36ReceiveCraftingConfirmation(int id, String message, boolean didSucceed) {
		this.id = id;
		this.message = message;
		this.didSucceed = didSucceed;
	}
	public String getDataString() {
		return "36:" + id + ":" + message + ":" + didSucceed;
	}

	public void process() {
		CraftingRecipe r = CraftingRecipeList.getRecipe(id);
		if(didSucceed && r != null && r.getType() == RecipeType.Research) {
			Inventory.addBonus(r);
		}
		
		MainGame.craftingMsgVisible = true;
		MainGame.craftingMsg.setMessage(message);
	}

}
