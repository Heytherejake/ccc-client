package com.studio878.packets;

import com.studio878.inventory.Inventory;

public class Packet58ReceivePlayerBalance implements Packet{

	int balance;
	
	public Packet58ReceivePlayerBalance(int amount) {
		balance = amount;
	}

	public String getDataString() {
		return "58:" + balance;
	}

	public void process() {
		Inventory.setBalance(balance);
	}
	
	
}
