package com.studio878.packets;

import java.util.TimerTask;

import com.studio878.app.Main;
import com.studio878.util.Settings;

public class Packet49ReceiveRespawnTime implements Packet {

	int seconds;
	public Packet49ReceiveRespawnTime(int seconds) {
		this.seconds = seconds;
	}
	
	public String getDataString() {
		return "49:" + seconds;
	}

	public void process() {
		Settings.ActivePlayer.setRespawnTime(seconds);
		Settings.ActivePlayer.setLastKilledTime(System.currentTimeMillis());
	}

}
