package com.studio878.packets;

import com.studio878.util.SoundEffects;

public class Packet54PlayBlockSound implements Packet{

	int x, y;
	
	public Packet54PlayBlockSound(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public String getDataString() {
		return "54:" + x + ":" + y;
	}

	public void process() {
		SoundEffects.playSound(SoundEffects.BlockPlaced, x*16, y*16);
	}

}
