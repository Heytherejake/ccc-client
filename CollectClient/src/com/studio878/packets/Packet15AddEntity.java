package com.studio878.packets;

import com.studio878.objects.Entity;
import com.studio878.objects.EntityManager;
import com.studio878.objects.EntityManager.EntityName;

public class Packet15AddEntity implements Packet{

	int id, lookup;
	double x, y;
	String data;

	public Packet15AddEntity(int id, int lookup, double x, double y, String data) {
		this.id = id;
		this.lookup = lookup;
		this.x = x;
		this.y = y;
		this.data = data;
	}
	public String getDataString() {
		return "15:" + id + ":" + lookup + ":" + x + ":" + y + ":" + data;
	}

	public void process() {
		Entity e = EntityManager.cloneEntity(EntityName.lookupId(lookup), x, y, id, data);
		if(e != null) {
			EntityManager.registerEntity(e, id);
		}

	}

}
