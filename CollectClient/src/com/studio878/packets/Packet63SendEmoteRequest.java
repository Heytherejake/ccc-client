package com.studio878.packets;

import com.studio878.util.Emote.EmoteType;

public class Packet63SendEmoteRequest implements Packet {

	EmoteType emote;
	
	public Packet63SendEmoteRequest(EmoteType emote) {
		this.emote = emote;
	}
	
	public String getDataString() {
		return "63:" + emote.getId();
	}

	public void process() {
		
	}

}
