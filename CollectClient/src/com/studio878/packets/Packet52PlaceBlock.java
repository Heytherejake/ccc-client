package com.studio878.packets;

import com.studio878.inventory.Item;

public class Packet52PlaceBlock implements Packet {

	int x, y;
	Item type;
	public Packet52PlaceBlock(int x, int y, Item item) {
		this.x = x;
		this.y = y;
		this.type = item;
	}
	public String getDataString() {
		return "52:" + x + ":" + y + ":" + type.getType().getId();
	}

	public void process() {
		
	}

}
