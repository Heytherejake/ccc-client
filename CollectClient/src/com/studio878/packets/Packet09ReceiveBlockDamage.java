package com.studio878.packets;

import com.studio878.world.Grid;

public class Packet09ReceiveBlockDamage implements Packet{

	int x, y;
	byte damage;
	String player;
	public Packet09ReceiveBlockDamage(String player, int x, int y, byte damage) {
		this.x = x;
		this.y = y;
		this.damage = damage;
		this.player = player;
	}
	public String getDataString() {
		return "09:" + player + ":" + x + ":" + y + ":" + damage;
	}
	
	public void process() {
		if(Grid.getBlockAt(x, y) != null) {
			Grid.getBlockAt(x, y).setCondition(damage);
		}

	}

}
