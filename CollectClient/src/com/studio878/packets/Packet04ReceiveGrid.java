package com.studio878.packets;

import java.util.StringTokenizer;

import com.studio878.app.Main;
import com.studio878.block.Block;
import com.studio878.block.BlockType;
import com.studio878.displays.Menu;
import com.studio878.util.Settings;
import com.studio878.world.Grid;


public class Packet04ReceiveGrid implements Packet{
	String data;
	public Packet04ReceiveGrid(String s) {
		this.data = s;
	}
	public String getDataString() {
		return data;
	}

	public void process() {
		Block[][] blocks = new Block[Settings.WorldHeight][Settings.WorldWidth];
		int size = Settings.WorldHeight * Settings.WorldWidth;
		Menu.setDisplayMessage("Loading world: 0%");
		StringTokenizer t = new StringTokenizer(getDataString(), ":");
		int count = 1;
		for (int i = 0; i < blocks.length; i++) {
			for (int j = 0; j < blocks[i].length; j++) {
				if(Main.isDisconnecting) return;
				String ps = t.nextToken();
				String[] parts = ps.split(",");
				blocks[i][j] = new Block(BlockType.lookupId(Integer.parseInt(parts[0])), Integer.parseInt(parts[2]), i, j, Byte.parseByte(parts[4]));
				blocks[i][j].setCondition(Byte.parseByte(parts[1]));
				blocks[i][j].setBackgroundType(BlockType.lookupId(Integer.parseInt(parts[4])));
				try {
					blocks[i][j].setForegroundType(BlockType.lookupId(Integer.parseInt(parts[5])));
				} catch (Exception e) {		
				}
				
				try {
					blocks[i][j].setDecorationType(BlockType.lookupId(Integer.parseInt(parts[6])));
				} catch (Exception e) {		
				}
				

				count++;
				Menu.setDisplayMessage("Loading world: " + ((int) (100*((double)count / size))) + "%");
			}
		}
		
		Menu.setDisplayMessage("Compiling world...");

		Grid.loadGrid(blocks);

		Menu.setDisplayMessage("Lighting world...");

		//FIX DERPY LIGHTING BUG
		for (int i = 0; i < blocks.length; i++) {
			for (int j = 0; j < blocks[i].length; j++) {
				Grid.blocks[i][j].setType(Grid.blocks[i][j].getType());
			}
		}
		
		Menu.setDisplayMessage("World loaded");
	}
}
