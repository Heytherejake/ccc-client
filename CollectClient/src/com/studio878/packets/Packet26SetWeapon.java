package com.studio878.packets;



public class Packet26SetWeapon implements Packet{
	int id;
	public Packet26SetWeapon(int id) {
		this.id = id;
	}
	
	public String getDataString() {
		return "26:" + id;
	}
	
	public void process() {
	}
}
