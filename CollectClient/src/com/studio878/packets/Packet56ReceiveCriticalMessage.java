package com.studio878.packets;

import com.studio878.ui.CriticalMessage;

public class Packet56ReceiveCriticalMessage implements Packet {

	String message;
	
	public Packet56ReceiveCriticalMessage(String message) {
		this.message = message;
	}
	
	public String getDataString() {
		return "56:" + message;
	}

	public void process() {
		CriticalMessage.addMessage(new CriticalMessage(message, 10, 100));
	}

}
