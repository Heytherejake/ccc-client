package com.studio878.packets;

import com.studio878.objects.Entity;
import com.studio878.objects.EntityManager;
import com.studio878.objects.Player;
import com.studio878.util.Settings;

public class Packet25GetArmRotation implements Packet{
	double angle;
	int id;
	public Packet25GetArmRotation(int id, double angle) {
		this.angle = angle;
		this.id = id;
	}

	public String getDataString() {
		return "25:" + id + ":" + angle;
	}

	public void process() {
		Entity e = EntityManager.getEntity(id);
		if(e instanceof Player) {
			Player p = (Player) e;
			if(!p.getName().equals(Settings.Username)){ 
				p.gunRotation = angle;
			}
		}
	}
}
