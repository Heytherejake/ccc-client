package com.studio878.packets;

import com.studio878.remoteui.RemoteUIContainer;
import com.studio878.remoteui.RemoteUIItem;
import com.studio878.remoteui.RemoteUIManager;

public class Packet42ReceiveRemoteUIItemUpdate implements Packet {

	String substring;
	
	public Packet42ReceiveRemoteUIItemUpdate(String substring) {
		this.substring = substring;
	}
	public String getDataString() {
		return "42:" + substring;
	}

	
	public void process() {
		String[] parts = substring.split(":");
		RemoteUIContainer c = RemoteUIManager.getContainer(Integer.parseInt(parts[0]));
		if(c != null) {
			String[] split = parts[1].split(",");
			RemoteUIItem item = c.getItem(Integer.parseInt(split[1]));

			if(item != null) {
				item.setX(Integer.parseInt(split[2]));
				item.setY(Integer.parseInt(split[3]));
				item.setZ(Integer.parseInt(split[4]));
				item.setWidth(Integer.parseInt(split[5]));
				item.setHeight(Integer.parseInt(split[6]));
				item.setState(Integer.parseInt(split[7]));
				item.setValue(split[8]);
				item.setCentered(Boolean.parseBoolean(split[9]));
			}
		}
	}

}
