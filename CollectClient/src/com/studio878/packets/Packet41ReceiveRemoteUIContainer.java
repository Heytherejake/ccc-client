package com.studio878.packets;

import java.util.StringTokenizer;

import com.studio878.remoteui.RemoteUIButton;
import com.studio878.remoteui.RemoteUIContainer;
import com.studio878.remoteui.RemoteUIHttpImage;
import com.studio878.remoteui.RemoteUIItem;
import com.studio878.remoteui.RemoteUILabel;
import com.studio878.remoteui.RemoteUIManager;
import com.studio878.remoteui.RemoteUISlider;
import com.studio878.remoteui.RemoteUISolidColorBox;
import com.studio878.remoteui.RemoteUIManager.RemoteUIItemType;
import com.studio878.remoteui.RemoteUITextField;

public class Packet41ReceiveRemoteUIContainer implements Packet{

	String substring;
	
	public Packet41ReceiveRemoteUIContainer(String s) {
		substring = s;
	}
	
	public String getDataString() {
		return "41:" + substring;
	}

	
	public void process() {
		StringTokenizer t = new StringTokenizer(substring, ":");
		RemoteUIContainer container = new RemoteUIContainer();
		container.setId(Integer.parseInt(t.nextToken()));
		while(t.hasMoreTokens()) {
			String sub = t.nextToken();
			String[] parts = sub.split(",");
			RemoteUIItem item = null;
			RemoteUIItemType type = RemoteUIItemType.lookupId(Integer.parseInt(parts[0]));
			switch(type) {
			case Button:
				item = new RemoteUIButton();
				break;
			case HttpImage:
				item = new RemoteUIHttpImage();
				break;
			case SolidColorBox:
				item = new RemoteUISolidColorBox();
				break;
			case TextField:
				item = new RemoteUITextField();
				break;
			case Slider:
				item = new RemoteUISlider();
				break;
			case Label:
				item = new RemoteUILabel();
			}
			item.setId(Integer.parseInt(parts[1]));
			item.setX(Integer.parseInt(parts[2]));
			item.setY(Integer.parseInt(parts[3]));
			item.setZ(Integer.parseInt(parts[4]));
			item.setWidth(Integer.parseInt(parts[5]));
			item.setHeight(Integer.parseInt(parts[6]));
			item.setState(Integer.parseInt(parts[7]));
			item.setValue(parts[8]);
			item.setCentered(Boolean.parseBoolean(parts[9]));
			container.addItem(item);
			
		}
		RemoteUIManager.registerContainer(container);
	}

}
