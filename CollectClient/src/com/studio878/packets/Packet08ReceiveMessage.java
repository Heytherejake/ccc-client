package com.studio878.packets;

import com.studio878.ui.Chat;

public class Packet08ReceiveMessage implements Packet{
	String message;
	public Packet08ReceiveMessage(String m) {
		message = m;
	}
	
	public String getDataString() {
		return "13:" + message;
	}
	
	public void process() {
		Chat.addMessage(message);
	}

}
