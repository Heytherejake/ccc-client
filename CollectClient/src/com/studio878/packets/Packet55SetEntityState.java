package com.studio878.packets;

import com.studio878.objects.Entity;
import com.studio878.objects.EntityManager;

public class Packet55SetEntityState implements Packet {

	int id, value;
	
	public Packet55SetEntityState(int id, int value) {
		this.id = id;
		this.value = value;
	}

	public String getDataString() {
		return "55:" + id + ":" + value;
	}

	public void process() {
		Entity e = EntityManager.getEntity(id);
		if(e != null) {
			e.setState(value);
		}
	}
}
