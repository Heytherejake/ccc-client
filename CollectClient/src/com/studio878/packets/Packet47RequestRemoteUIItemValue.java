package com.studio878.packets;

import com.studio878.io.NetObjects;
import com.studio878.remoteui.RemoteUIContainer;
import com.studio878.remoteui.RemoteUIItem;
import com.studio878.remoteui.RemoteUIManager;

public class Packet47RequestRemoteUIItemValue implements Packet {

	int container, id;
	
	public Packet47RequestRemoteUIItemValue(int container, int id) {
		this.container = container;
		this.id = id;
	}
	
	public String getDataString() {
		return "47:" + container + ":" + id;
	}

	public void process() {
		RemoteUIContainer c = RemoteUIManager.getContainer(container);
		if(c != null) {
			RemoteUIItem i = c.getItem(id);
			if(i != null) {
				NetObjects.Sender.sendPacket(new Packet48SendRemoteUIItemValue(i));
			}
		}
	}

}
