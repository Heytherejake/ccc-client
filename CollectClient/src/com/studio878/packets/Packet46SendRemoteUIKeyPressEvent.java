package com.studio878.packets;

public class Packet46SendRemoteUIKeyPressEvent implements Packet {

	int button, x, y, container;
	
	public Packet46SendRemoteUIKeyPressEvent(int container, int x, int y, int button) {
		this.button = button;
		this.x = x;
		this.y = y;
		this.container = container;
	}
	public String getDataString() {
		return "46:" + container + ":" + x + ":" + y + ":" + button;
	}

	public void process() {
		
	}

}
