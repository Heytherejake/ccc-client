package com.studio878.packets;

import com.studio878.objects.Entity;
import com.studio878.objects.EntityManager;

public class Packet66ForceLocationUpdate implements Packet {

	double x, y;
	int id;
	
	public Packet66ForceLocationUpdate(int id, double x, double y) {
		this.id = id;
		this.x = x;
		this.y = y;
	}

	public String getDataString() {
		return "66:" + id + ":" + x + ":" + y;
	}

	public void process() {
		Entity e = EntityManager.getEntity(id);
		if(e != null) {
			e.setX(x);
			e.setY(y);
		}
	}
}
