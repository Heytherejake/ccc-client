package com.studio878.packets;

public class Packet07SendChat implements Packet{
	String username;
	String message;
	public Packet07SendChat(String username, String message) {
		this.username = username;
		this.message = message;
	}
	public String getDataString() {
		return "07:" + username + ":" + message;
	}
	
	public void process() {
		
	}

}
