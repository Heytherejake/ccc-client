package com.studio878.packets;

import com.studio878.util.Movement.MovementType;



public class Packet12SendMovement implements Packet{

	
	MovementType mt;
	
	public Packet12SendMovement(MovementType m) {
		this.mt = m;
	}
	
	public String getDataString() {
		return "12:" + mt;
	}

	public void process() {
		
	}

}
