package com.studio878.packets;

import com.studio878.inventory.ItemID;


public class Packet17ThrowProjectile implements Packet{

	private double angle;
	ItemID item;
	
	public Packet17ThrowProjectile(double angle, ItemID id) {
		this.angle = angle;
		this.item = id;
	}

	public String getDataString() {
		return "17:" + angle + ":" + item.getId();
	}

	public void process() {
	}

}
