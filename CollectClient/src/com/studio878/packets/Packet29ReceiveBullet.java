package com.studio878.packets;

import org.newdawn.slick.Sound;

import com.studio878.inventory.ItemID;
import com.studio878.objects.Bullet;
import com.studio878.objects.EntityManager;
import com.studio878.util.SoundEffects;

public class Packet29ReceiveBullet implements Packet{

	double x, y, angle;
	ItemID id;
	public Packet29ReceiveBullet(double x, double y, double angle, ItemID id) {
		this.x = x;
		this.y = y;
		this.angle = angle;
		this.id = id;
	}
	public String getDataString() {
		return "29:" + x + ":" + y + ":" + angle + ":" + id.getId();
	}

	public void process() {
		Bullet e = new Bullet(x, y);
		angle -= Math.PI/2;
		e.setXSpeed(-30*Math.sin(angle));
		e.setYSpeed(30*Math.cos(angle));
		angle += Math.PI/2;
		e.setX(e.getX() + e.getXSpeed());
		e.setY(e.getY() + e.getYSpeed());
		e.setRotation(Math.toDegrees(angle));
		EntityManager.registerEntity(e, e.getId());

		Sound sfx = null;
		String name = id.getReferenceName();
		if(name.equals("Pistol")) {
			sfx = SoundEffects.Pistol;
		} else if(name.equals("Submachine Gun")) {
			sfx = SoundEffects.SMG;
		} else if(name.equals("Shotgun")) {
			sfx = SoundEffects.Shotgun;
		} else if(name.equals("Shovel")) {
			sfx = SoundEffects.SMG;
			//Fix this
		} else if(name.equals("Sniper Rifle")) {
			sfx = SoundEffects.Sniper;
		} else if(name.equals("Rocket Launcher")) {
			sfx = SoundEffects.RocketLauncher;
		} else if(name.equals("Flintlock Pistol")) {
			sfx = SoundEffects.Flintlock;
		}
		SoundEffects.playSound(sfx, (int) x, (int) y);
	}

}
