package com.studio878.packets;

import com.studio878.story.MapEventManager;
import com.studio878.story.MapEventManager.MapEventType;

public class Packet61ReceiveMapEvent implements Packet {

	String data;
	MapEventType type;
	
	public Packet61ReceiveMapEvent(MapEventType type, String data) {
		this.type = type;
		this.data = data;
	}

	public String getDataString() {
		return "61:" + type + ":" + data ;
	}

	public void process() {
		MapEventManager.addEvent(MapEventManager.matchEvent(type, data));
	}
	
	
}
