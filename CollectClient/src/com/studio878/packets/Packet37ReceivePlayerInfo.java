package com.studio878.packets;

import com.studio878.characters.PlayerCharacter;
import com.studio878.objects.EntityManager;
import com.studio878.objects.Player;
import com.studio878.ui.PlayerListItem;
import com.studio878.util.Settings;
import com.studio878.util.TeamManager;

public class Packet37ReceivePlayerInfo implements Packet{

	String data;

	public Packet37ReceivePlayerInfo(String data) {
		this.data = data;
	}

	public String getDataString() {
		return "37:" + data;
	}

	public void process() {
		try {
			String[] parts = data.split(":");
			Settings.MainWindow.folder.team.clearItems();
			Settings.MainWindow.folder.enemy.clearItems();

			for(String s : parts) {
				String[] sub = s.split(",");
				Player p = (Player) EntityManager.getEntity(Integer.parseInt(sub[0]));

				if(p != null) {
					PlayerCharacter c = PlayerCharacter.lookupId(Integer.parseInt(sub[4]));
					if(c != null) {
						p.setTeam(TeamManager.matchTeam(sub[2]));
						p.setCharacter(c);
						if(p.getTeam() == Settings.ActivePlayer.getTeam()) {
							Settings.MainWindow.folder.team.addItem(new PlayerListItem(p, Integer.parseInt(sub[3]), false));
						} else {
							Settings.MainWindow.folder.enemy.addItem(new PlayerListItem(p, Integer.parseInt(sub[3]), true));
						}
					}
		
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
