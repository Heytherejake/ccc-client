package com.studio878.packets;

import com.studio878.remoteui.RemoteUIManager;
import com.studio878.util.Console;

public class Packet43ReceiveRemoteUIContainerCloseRequest implements Packet{

	int id;

	public Packet43ReceiveRemoteUIContainerCloseRequest(int id) {
		this.id = id;
	}

	public String getDataString() {
		return "43:" + id;
	}

	public void process() {
		if(RemoteUIManager.getSelectedContainer() == id) {
			RemoteUIManager.setSelectedContainer(-1);
		} else {
			Console.error("[RemoteUI] Server tried to close a container that isn't open (ID: " + id + ")");
		}
	}

}
