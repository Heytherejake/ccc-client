package com.studio878.packets;

import com.studio878.remoteui.RemoteUIItem;

public class Packet48SendRemoteUIItemValue implements Packet {

	RemoteUIItem item;
	
	public Packet48SendRemoteUIItemValue(RemoteUIItem item) {
		this.item = item;
	}
	public String getDataString() {
		System.out.println(item.getValue());
		return "48:" + item.getParent().getId() + ":" + item.getId() + ":" + item.getValue();
	}

	public void process() {
		
	}

}
