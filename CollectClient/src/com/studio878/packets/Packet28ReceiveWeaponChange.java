package com.studio878.packets;

import com.studio878.inventory.Inventory;
import com.studio878.inventory.ItemID;
import com.studio878.objects.EntityManager;
import com.studio878.objects.Player;
import com.studio878.util.Settings;
import com.studio878.weapons.BlockWeapon;
import com.studio878.weapons.EmptyHand;
import com.studio878.weapons.Weapon;


public class Packet28ReceiveWeaponChange implements Packet {

	int iid;
	int id;
	int slot;

	public Packet28ReceiveWeaponChange(int id, int iid, int slot) {
		this.id = id;
		this.iid = iid;
		this.slot = slot;
	}

	public String getDataString() {
		return "28:" + id + ":" + iid + ":" + slot;
	}

	public void process() {
		Player p = (Player) (EntityManager.getEntity(id));
		if(p != null && iid != -1) {
			System.out.println(iid);
			Weapon w = Weapon.matchWeapon(iid);
			if(w != null) {
				p.setWeapon(w);
				if(p.getName().equals(Settings.ActivePlayer.getName())){
					Inventory.setSelectedWeapon(slot);
				}
				if(w instanceof BlockWeapon) {
					BlockWeapon bw = (BlockWeapon) w;
					bw.setType(ItemID.lookupId(iid).getSource());
				}
			}
		} else {
			if(p != null) {
				p.setWeapon(new EmptyHand());
				if(p.getName().equals(Settings.ActivePlayer.getName())){
					Inventory.setSelectedItem(null);
				}
			}
		}
	}
}
