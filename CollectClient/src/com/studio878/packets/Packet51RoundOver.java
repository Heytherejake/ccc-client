package com.studio878.packets;

import java.util.ArrayList;

import com.studio878.objects.EntityManager;
import com.studio878.objects.Player;
import com.studio878.ui.SuccessListItem;
import com.studio878.util.Settings;
import com.studio878.util.TeamManager.Team;

public class Packet51RoundOver implements Packet {

	Team team;
	int time;

	public Packet51RoundOver(Team team, int time) {
		this.team = team;
		this.time = time;
	}
	public String getDataString() {
		return "51:" + team.getId() + ":" + time;
	}

	public void process() {
		Settings.MainWindow.successView.setTeam(team);
		Settings.MainWindow.successView.setEndTime(System.currentTimeMillis());
		Settings.MainWindow.successView.setNextRoundTime(time);
		Settings.MainWindow.successView.table.clearItems();
		ArrayList<Player> players = EntityManager.getPlayers();
		for(int i = 0; i < 3; i++) {
			Player ps = null;
			int num = -1;
			for(Player p : players) {
				if(p.getKills() > num && p.getTeam() == team) {
					ps = p;
					num = p.getKills();
				}
			}
			players.remove(ps);
			if(ps != null) {
				Settings.MainWindow.successView.table.addItem(new SuccessListItem(ps.getName(), num));
			}
		}

	}

}
