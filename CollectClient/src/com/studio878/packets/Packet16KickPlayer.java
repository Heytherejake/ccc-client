package com.studio878.packets;

import com.studio878.displays.Menu;
import com.studio878.io.NetObjects;
import com.studio878.util.Settings;

public class Packet16KickPlayer implements Packet{

	private String message;
	
	public Packet16KickPlayer(String message) {
		this.message = message;
	}

	public String getDataString() {
		return "16:" + message;
	}

	public void process() {
		NetObjects.ActiveClient.softDisconnect = true;
		Menu.setDisplayMessage(message);
		Settings.MainWindow.setDisplay(new Menu(2));
	}

}
