package com.studio878.packets;

import com.studio878.util.Settings;

public class Packet59SendAbsolutePosition implements Packet {

	public String getDataString() {
		return "59:" + Settings.ActivePlayer.getX() + ":" + Settings.ActivePlayer.getY() + ":" + Settings.ActivePlayer.getFacingDirection() + ":" + Settings.ActivePlayer.getXSpeed() + ":" + Settings.ActivePlayer.getYSpeed();
	}

	public void process() {
		
	}

}
