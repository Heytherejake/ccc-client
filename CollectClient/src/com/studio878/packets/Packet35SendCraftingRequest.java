package com.studio878.packets;

public class Packet35SendCraftingRequest implements Packet {

	int id;
	
	public Packet35SendCraftingRequest(int id) {
		this.id = id;
	}
	
	public String getDataString() {
		return "35:" + id;
	}

	public void process() {
		
	}

}
