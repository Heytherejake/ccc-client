package com.studio878.packets;

import com.studio878.io.NetObjects;
import com.studio878.objects.EntityManager;

public class Packet11RemovePlayer implements Packet{
	String name;
	int id;
	public Packet11RemovePlayer(String n, int id) {
		this.name = n;
		this.id = id;
	}
	public String getDataString() {
		return "11:" + name + ":" + id;
	}
	
	public void process() {
		NetObjects.ActiveClient.removePlayer(name);
		EntityManager.removeEntity(id);

	}

}
