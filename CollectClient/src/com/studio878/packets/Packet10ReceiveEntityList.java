package com.studio878.packets;

import java.util.StringTokenizer;

import com.studio878.objects.Entity;
import com.studio878.objects.EntityManager;
import com.studio878.objects.EntityManager.EntityName;

public class Packet10ReceiveEntityList implements Packet  {

	String input;

	public Packet10ReceiveEntityList(String input) {
		this.input = input;
	}
	public String getDataString() {
		return "10:" + input;
	}

	public void process() {
		EntityManager.clearEntities();
		StringTokenizer t = new StringTokenizer(getDataString(), ":");
		System.out.println(input);
		t.nextToken();
		while(t.hasMoreElements()) {
			String next = t.nextToken();
			String[] parts = next.split(",");

			int id = Integer.parseInt(parts[0]);
			double x = Double.parseDouble(parts[2]);
			double y = Double.parseDouble(parts[3]);
			EntityName type = EntityName.lookupId(Integer.parseInt(parts[1]));
			String data = "";
			if(parts.length > 4) {
				data = parts[4];
			}
			Entity e = EntityManager.cloneEntity(type, x, y, id, data);
			if(EntityManager.getEntity(id) == null) {
				EntityManager.registerEntity(e, id);
			}

		}
	}

}
