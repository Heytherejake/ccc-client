package com.studio878.packets;


import com.studio878.inventory.ItemID;

public class Packet22RelocateItem implements Packet{

	ItemID type;
	int slot, initialSlot, amount;
	public Packet22RelocateItem(ItemID type, int slot, int islot, int amount) {
		this.type = type;
		this.slot = slot;
		initialSlot = islot;
		this.amount = amount;
	}
	
	public String getDataString() {
		return "22:" + type.getId() + ":" + slot + ":" + initialSlot + ":" + amount;
	}

	public void process() {
	
	}

}
