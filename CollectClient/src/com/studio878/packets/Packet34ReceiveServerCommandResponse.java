package com.studio878.packets;

import com.studio878.ui.ChatColors;
import com.studio878.util.Console;

public class Packet34ReceiveServerCommandResponse implements Packet{

	String response;
	
	public Packet34ReceiveServerCommandResponse(String s) {
		response = s;
	}
	public String getDataString() {
		return "34:" + response;
	}

	public void process() {
		Console.write(ChatColors.Yellow + "[Server] " + response);
	}

}
