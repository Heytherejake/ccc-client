package com.studio878.packets;

import com.studio878.displays.Menu;
import com.studio878.util.Settings;


public class Packet03WorldSize implements Packet{
	int width = 0;
	int height = 0;
	
	public Packet03WorldSize(int width, int height) {
		this.width = width;
		this.height = height;
	}
	public String getDataString() {
		
		return "03:" + width + ":" + height;
	}
	
	public void process() {
		Menu.setDisplayMessage("Receiving world information...");

		Settings.WorldHeight = width;
		Settings.WorldWidth = height;

	}

}
