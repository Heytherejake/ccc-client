package com.studio878.packets;

import com.studio878.io.NetObjects;

public class Packet39ReceiveHandshake implements Packet{

	long time;
	
	public Packet39ReceiveHandshake(long t) {
		time = t;
	}
	public String getDataString() {
		return "39:" + time;
	}

	public void process() {
		NetObjects.Sender.sendPacket(new Packet40HandshakeResponse(time));
	}
	

}
