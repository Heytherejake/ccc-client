package com.studio878.packets;

import com.studio878.block.BlockType;
import com.studio878.displays.Menu;

public class Packet68ReceiveBlockList implements Packet {

	String input;

	public Packet68ReceiveBlockList(String input) {
		this.input = input;
	}

	public String getDataString() {
		return "67:" + input;
	}

	public void process() {
		BlockType.clearBlocks();
		Menu.setDisplayMessage("Loading block types...");

		for(String p : input.split("~")) {
			String[] split = p.split("_!_");
			BlockType.addBlock(Integer.parseInt(split[0]), split[1], Boolean.parseBoolean(split[2]), Byte.parseByte(split[3]), Boolean.parseBoolean(split[4]), Integer.parseInt(split[5]), Integer.parseInt(split[6]), Boolean.parseBoolean(split[7]), Boolean.parseBoolean(split[8]), Boolean.parseBoolean(split[9]), Boolean.parseBoolean(split[10]), Integer.parseInt(split[11]), split[12]);
		}


	}

}
