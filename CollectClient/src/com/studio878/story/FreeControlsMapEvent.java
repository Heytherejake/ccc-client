package com.studio878.story;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

import com.studio878.util.Settings;

public class FreeControlsMapEvent extends MapEvent {

	public FreeControlsMapEvent(String data) {
		super(data);
	}

	public void process(Graphics g, GameContainer c) {
		Settings.PlayerControls = true;
		this.dispose();
	}

}
