package com.studio878.story;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

import com.studio878.story.Chapter.ChapterInfo;
import com.studio878.ui.Fonts;
import com.studio878.ui.TableList;
import com.studio878.ui.TableListItem;
import com.studio878.util.GraphicsUtil;

public class ChapterListItem extends TableListItem {

	ChapterInfo chapter;

	public ChapterListItem(ChapterInfo chapter, TableList parent) {
		super(chapter.getName(), 100, parent);
		this.chapter = chapter;
	}
	
	public ChapterInfo getChapter() {
		return chapter;
	}

	public void draw(Graphics g, int i) {
		if(this.isSelected()) {
			g.setColor(new Color(190, 190, 190));
		} else {
			g.setColor(new Color(170, 170, 170));
		}
		g.fillRect(0, i, this.parent.width, 100);
		g.setFont(Fonts.LargeItalicFont);
		g.setColor(new Color(0, 0, 0, 1.0f));
		GraphicsUtil.drawWithShadow(g, chapter.getTitle(), 120, i + 4, Color.black, Color.white);

		g.setFont(Fonts.ChapterFont);
		GraphicsUtil.drawWithShadow(g, chapter.getName(), 120, i + 25, Color.black, Color.white);
		g.drawImage(chapter.getImage().getScaledCopy(2.0f), 0, i);

	}

}
