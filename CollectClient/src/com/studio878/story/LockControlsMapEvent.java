package com.studio878.story;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

import com.studio878.util.Settings;

public class LockControlsMapEvent extends MapEvent {

	public LockControlsMapEvent(String data) {
		super(data);
	}

	public void process(Graphics g, GameContainer c) {
		Settings.PlayerControls = false;
		this.dispose();
	}

}
