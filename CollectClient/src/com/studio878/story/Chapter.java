package com.studio878.story;

import org.newdawn.slick.Image;

import com.studio878.util.Sheets;

public class Chapter {

	public enum ChapterInfo {
		Tutorial("Tutorial", "Initiation", Sheets.CHAPTER_PANELS.getSubImage(0, 0), "~tutorial.map"),
		//Chapter1("The First Chapter", "Arrival", Sheets.CHAPTER_PANELS.getSubImage(1, 0), "~ch1.map"),
		//Chapter2("The Second Chapter", "", Sheets.CHAPTER_PANELS.getSubImage(0, 0), "~ch2.map"),

		;
		
		private String title, name;
		private Image panel;
		private String map;
		
		private ChapterInfo(String title, String name, Image panel, String map) {
			this.title = title;
			this.name = name;
			this.panel = panel;
			this.map = map;
		}
		
		public String getName() {
			return name;
		}
		
		public Image getImage() {
			return panel;
		}
		
		public String getTitle() {
			return title;
		}
		
		public String getMap() {
			return map;
		}
	}
}
