package com.studio878.story;

import java.util.HashMap;

import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.Display;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;

import com.studio878.input.KeyBindings;
import com.studio878.inventory.ItemID;
import com.studio878.ui.Chat;
import com.studio878.ui.Fonts;
import com.studio878.util.GraphicsUtil;
import com.studio878.util.Sheets;

public class TextMapEvent extends MapEvent {
	public enum StoryTextSource {
		Unknown(0, "???", Sheets.STORY_PANELS.getSubImage(0, 0)),
		Speaker(1, "Intercom", Sheets.STORY_PANELS.getSubImage(1, 0));
		;

		int id;
		String name;
		Image img;

		static HashMap<Integer, StoryTextSource> lookupId = new HashMap<Integer, StoryTextSource>();

		private StoryTextSource(int id, String name, Image img) {
			this.id = id;
			this.name = name;
			this.img = img;
		}

		public int getId() {
			return id;
		}

		public String getName() {
			return name;
		}

		public Image getImage() {
			return img;
		}

		public static StoryTextSource lookupId(int id) {
			return lookupId.get(id);
		}

		static {
			for(StoryTextSource s : values()) {
				lookupId.put(s.getId(), s);
			}
		}

	}
	
	StoryTextSource source;
	String value;
	public TextMapEvent(String data) {
		super(data);
		this.source = StoryTextSource.lookupId(Integer.parseInt(data.split("~")[0]));
		this.value = data.split("~")[1];
	}
	
	public void process(Graphics g, GameContainer c) {
		GraphicsUtil.drawShadowBox(g, 0, 0, Display.getParent().getWidth(), 150, 0);
		g.drawImage(source.getImage().getScaledCopy(2.0f), 10, 10);
		g.setColor(Color.black);
		g.setFont(Fonts.ChatFont);
		g.drawString(source.getName(), 60 - Fonts.ChatFont.getWidth(source.getName())/2, 115);
		if(value.indexOf("|") == -1) {
			int sy = 10;
			for(String s : Chat.splitBetweenLines(value, Fonts.ChatFont, Display.getParent().getWidth() - 200)) {
				Fonts.drawWithColor(s, Fonts.ChatFont, g, 150, sy, 500, 1.0f);
				sy += 20;
			}
		} else {
			String[] split = value.split("\\|");
			int fx = 0;
			for(int i = 0; i < split.length; i++) {
				if(i % 2 == 0) {
					Fonts.drawWithColor(split[i], Fonts.ChatFont, g, fx, 0, 500, 1.0f);
					fx += Fonts.ChatFont.getWidth(split[i]);

				} else {
					String[] parts = split[i].split("=");
					if(parts.length > 0 && parts[0].equalsIgnoreCase("item")) {
						ItemID id = ItemID.lookupName(parts[1]);
						if(id != null) {
							Image s = id.getImage().copy();
							s.setAlpha(1.0f);
							g.drawImage(s, (float) fx, (float) 0 - 4);
							fx += id.getImage().getWidth();
						}
					}
				}
			}
		}
		g.setColor(new Color(0, 130, 130));
		g.fillRect((float) Display.getParent().getWidth() - 40, (float)  110, 32, 32);
		g.setColor(new Color(230, 230, 230));
		g.fillRect((float) Display.getParent().getWidth() - 38, (float) 112, 28, 28);
		g.setFont(Fonts.ChatFont);
		g.setColor(Color.black);
		g.drawString(KeyBindings.getKeyName(KeyBindings.Use), (float) Display.getParent().getWidth() -  29, (float) 115f);

	}

}
