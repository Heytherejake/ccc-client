package com.studio878.story;

import java.util.ArrayList;

import org.lwjgl.input.Keyboard;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

import com.studio878.input.KeyBindings;
import com.studio878.io.NetObjects;
import com.studio878.packets.Packet62SendNextEventRequest;
import com.studio878.ui.Fonts;
import com.studio878.util.GraphicsUtil;

public class InstructionsMapEvent extends MapEvent {

	String value;
	long created;
	public InstructionsMapEvent(String data) {
		super(data);
		this.value = data;
		created = System.currentTimeMillis();
		String osp = "";
		if(data.indexOf('|') != -1) {
			String[] split = value.split("\\|");
			for(int i = 0; i < split.length; i++) {
				if(i % 2 == 0) {
					osp += split[i];
				} else {
					String[] parts = split[i].split("=");
					if(parts.length == 2) {
						String act = parts[0];
						String val = parts[1];
						if(act.equalsIgnoreCase("key")) {
							int[] key = KeyBindings.getKey(val);
							osp += KeyBindings.getKeyName(key);
						}
					}
				}
			}
		}
		value = osp;
	}

	public void process(Graphics g, GameContainer c) {
		float transparency = ((float) (5000 - (System.currentTimeMillis() - created)))/1000f;
		if(transparency > 1.0) {
			transparency = 1.0f;
		}
		g.setColor(new Color(255, 255, 255, 0.6f*transparency));
		ArrayList<String> lines = GraphicsUtil.splitBetweenLines(value, Fonts.LargeFont, 800);
		g.fillRect(c.getWidth()/2 - 400, c.getHeight()/2 - 200, 800, lines.size()*50);
		g.setFont(Fonts.LargeFont);
		int sy = c.getHeight()/2 - 190;
		for(String s : lines) {
			GraphicsUtil.drawWithShadow(g, s, c.getWidth()/2 - Fonts.LargeFont.getWidth(s)/2, sy, new Color(0, 0, 0, transparency), new Color(255, 255, 255, transparency));
			sy += 40;
		}

		if(System.currentTimeMillis() - created >= 5000) {
			NetObjects.Sender.sendPacket(new Packet62SendNextEventRequest());
			this.dispose();

		}


	}

}
