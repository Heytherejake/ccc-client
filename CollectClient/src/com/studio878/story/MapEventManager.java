package com.studio878.story;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

public class MapEventManager {

	public enum MapEventType {
		Warp (0, false),
		SetVelocity(1, false),
		Damage(2, false),
		Heal(3, false),
		Text(4, false),
		Kill(5, false), 
		SetBlock(6, false),
		SetBackground(7, false),
		CompleteEvent(8, false), 
		LockControls(9, false),
		FreeControls(10, false),
		HasItem(12, true),
		Else(13, true),
		HasCoins(14, true),
		TakeItem(15, false),
		TakeCoins(16, false),
		GiveItem(17, false),
		CriticalMessage(18, false),
		ChatMessage(19, false),
		GetVariable(20, true),
		SetVariable(21, false),
		SpawnEntity(22, false),
		ShowInstructions(23, false),
		SetSpawn(24, false),
		;


		private int id;
		private static final Map<Integer, MapEventType> lookupId = new HashMap<Integer, MapEventType>();

		private MapEventType(int id, boolean conditional) {
			this.id = id;
		}

		public int getId() {
			return id;
		}

		public static MapEventType lookupId(int i) {
			return lookupId.get(i);
		}

		static {
			for(MapEventType t: values()) {
				lookupId.put(t.getId(), t);
			}
		}
	}

	public static CopyOnWriteArrayList<MapEvent> eventQueue = new CopyOnWriteArrayList<MapEvent>();

	static MapEvent currentEvent = null;
	public static MapEvent getNextHighestMapEvent() {
		if(currentEvent == null) {
			if(eventQueue.size() != 0) {
				currentEvent = eventQueue.get(0);
			} else {
				return null;
			}
		}
		return currentEvent;
	}
	
	public static void addEvent(MapEvent e) {
		eventQueue.add(e);
	}

	public static void completeEvent(MapEvent e) {
		if(currentEvent == e) {
			currentEvent = null;
		}
		eventQueue.remove(e);
	}

	public static MapEvent matchEvent(MapEventType t, String data) {
		switch(t) {
		case Text:
			return new TextMapEvent(data);
		case LockControls:
			return new LockControlsMapEvent(data);
		case FreeControls:
			return new FreeControlsMapEvent(data);
		case ShowInstructions:
			return new InstructionsMapEvent(data);
		}
		return null;
	}
}
