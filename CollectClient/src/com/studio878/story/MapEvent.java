package com.studio878.story;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public abstract class MapEvent{
	
	String data;

	public MapEvent(String data) {
		this.data = data;
	}
	
	public abstract void process(Graphics g, GameContainer c);
	
	public void dispose() {
		MapEventManager.completeEvent(this);
	}
}
