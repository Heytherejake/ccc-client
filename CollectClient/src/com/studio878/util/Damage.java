package com.studio878.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import com.studio878.ui.MessageObject;


public class Damage {
	public static enum DamageMethod {
		Grenade (0, "[Killer] blew up [Player]", "[Player] blew himself up."),
		FallDamage (1, "[Killer] knocked [Player] to their death", "[Player] leapt before he looked."),
		Pistol (2, "[Killer] shot [Player] with a pistol", "[Player] shot himself. That's embarrasing."),
		Shovel (3, "[Killer] dented [Player]'s head with a shovel", "[Player] killed himself in an impossible way."),
		Suicide (4, "[Player] had so little to live for.", "[Player] had so little to live for."),
		SMG (5, "[Killer] filled [Player] with lead", "[Player] shot himself. That's embarrasing"),
		Shotgun (6, "[Killer] riddled [Player] with holes", "[Player] shot himself. That's embarrasing"),
		SniperRifle (7, "[Killer] sniped [Player]", "[Player] shot himself. That's embarrasing."),
		Rocket (8, "[Killer] rocketed [Player]", "[Player] blew himself up."),
		Block (9, "[Killer] buried [Player]", "[Player] was buried to death."),
		Generic (10, "[Killer] killed [Player]", "[Player] died."),
		Landmine (11, "[Killer]'s landmine blew up [Player]", "[Player] forgot where he left his landmines."),
		Wrench (12, "[Killer] beat the life out of [Player]", "[Player] hit himself in the head with a wrench."),
		Sword(13, "[Killer] split [Player] in two", "[Player] cut off his head with his own sword"),
		Hatchet(14, "[Killer] chopped [Player] into lumber", "[Player] killed himself impossibly."),
		Lava(15, "", "[Player] turned into ashes."),
		OutOfBounds(16, "", "[Player] abandoned the battle.");
	
		;
		private static HashMap<Integer, DamageMethod> idLookup = new HashMap<Integer, DamageMethod>();
		private int id;
		private String message, suicide;
		private DamageMethod(int id, String msg, String suicide) {
			this.id = id;
			this.message = msg;
			this.suicide = suicide;
		}
		
		public int getId() {
			return id;
		}
		
		public String getMessage() {
			return message;
		} 
		
		public String getSuicideMessage() {
			return suicide;
		}
		
		public static DamageMethod lookupId(int i) {
			return idLookup.get(i);
		}
		static {
			for(DamageMethod t: values()) {
				idLookup.put(t.getId(), t);
			}
		}
	}
	
	static int deaths;
	static int kills;
	
	static ArrayList<MessageObject> messages = new ArrayList<MessageObject>();
	
	public static ArrayList<MessageObject> getMessages() {
		return messages;
	}
	
	public static void addMessage(String message) {
		messages.add(new MessageObject(message, new Date()));
	}
	public static ArrayList<MessageObject> getMessagesWithinTime(int s) {
		ArrayList<MessageObject> results = new ArrayList<MessageObject>();
		Date now = new Date();
		for(int i = messages.size() - 1; i >= 0; i--) {
			if((now.getTime() - messages.get(i).getTime().getTime()) / 1000 <= s && results.size() < 3) {
				results.add(messages.get(i));
			} else {
				return results;
			}
		}
		return results;
		
	}
	
	public static void addDeath() {
		deaths++;
	}
	
	public static void addKill() {
		kills++;
	}
	
	public static int getDeaths() {
		return deaths;
	}
	
	public static int getKills() {
		return kills;
	}
}
