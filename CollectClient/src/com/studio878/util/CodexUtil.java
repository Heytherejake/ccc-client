package com.studio878.util;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.StringTokenizer;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.UnicodeFont;

import com.studio878.app.Main;
import com.studio878.ui.CodexEntryItem;
import com.studio878.ui.DraggableTableList;
import com.studio878.ui.Fonts;

public class CodexUtil {

/*	public static ConcurrentHashMap<String, String> codexEntries = new ConcurrentHashMap<String, String>();


	public static ConcurrentHashMap<String, String> getCodexEntries() {
		return codexEntries;
	}

	public static String getCodexEntry(String s) {
		return codexEntries.get(s);
	}

	public static void loadCodexEntries() {
		if(codexEntries.size() != 0) {
			//fillCodexList();
			//return;
		}
		try {
			Class.forName("org.sqlite.JDBC");
			String file = new File(Main.downloadFolder, "codex.db").getAbsolutePath();
			Connection conn = DriverManager.getConnection("jdbc:sqlite:"+file);
			PreparedStatement ps = conn.prepareStatement("SELECT * FROM entries;");
			ps.setQueryTimeout(30);
			ResultSet rs = ps.executeQuery();
			int val = 0;
			codexEntries.clear();
			while(rs.next()) {
				String text = rs.getString("text");
				text = replaceWithTag(text, "'''", "<b>", "</b>");
				text = replaceWithTag(text, "''", "<i>", "</i>");
				text = replaceWithTag(text, "===", "<font size=4><b>", "</b></font></div>");
				text = text.replace("\n", "<br>");
				text = text.replace('*', '�');
				text = replaceWithTag(text, "==", "<br><font size=6>", "</font><br><hr><font face='Trebuchet MS'>");
				text = replaceLinks(text);

				codexEntries.putIfAbsent(rs.getString("title"), text);
				val++;
			}
			/*for(Entry<String, String> s : codexEntries.entrySet()) {
				if(s.getValue().contains("REDIRECT")) {
					System.out.println(s.getValue().split("'>")[0]);
					String trd = s.getValue().split("'>")[1];
					trd = trd.substring(0, trd.length() - 3);
					for(Entry<String, String> sv : codexEntries.entrySet()) {
						if(sv.getKey().equalsIgnoreCase(trd)) {
							codexEntries.remove(s.getKey());
							codexEntries.put(s.getKey(), trd);
						}
					}
				
				}
			}*/
		/*	Console.write("[Codex] " + val + " entries loaded.");
			fillCodexList();
		} catch (Exception e) {
			Console.error("Failed to load codex");
			e.printStackTrace();
		}

	}

	public static String replaceLinks(String source) {
		String text = "";
		String[] sp = source.split("\\[\\[");
		int i = 0;
		for(String s : sp) {
			String[] sb = s.split("]]");
			for(String sl : sb) {
				if(i % 2 == 1) {
					text += "<a href='" + sl + "'>" + sl+ "</a>";
				} else {
					text += sl;
				}
				i++;

			}
		}
			return text;
		}
		public static String replaceAllTags(String source, String delim, String lead, String post) {
			String text = "";
			if(!source.contains(delim)) {
				return source;
			}
			String[] sp = source.split(delim);
			for(String s : sp) {
				text += lead + s + post;
			}
			return text;
		}
		public static String replaceWithTag(String source, String delim, String lead, String post) {
			String text = "";
			String[] sp = source.split(delim);
			int i = 0;
			for(String s : sp) {
				if(i % 2 == 1) {
					text += lead + s + post;

				} else {
					text += s;
				}
				i++;
			}
			return text;
		}
		public static void fillCodexList() {
			DraggableTableList cl = Settings.MainWindow.codexList;
			cl.clearItems();
			for(Entry<String, String> ent : (new TreeMap<String, String>(codexEntries)).entrySet()) {
				if(!ent.getValue().substring(0, 1).equals("#") && !ent.getKey().equals("Main Page")) {
					cl.addItem(new CodexEntryItem(ent.getKey(), ent.getValue(), cl));
				}
			}

		}
*/
	}
