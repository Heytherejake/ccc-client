package com.studio878.util;

import java.io.File;

import org.lwjgl.LWJGLUtil;

import com.studio878.app.Window;
import com.studio878.objects.Player;
import com.studio878.threads.ServerThread;
import com.studio878.threads.WorldEditorThread;

public class Settings {

	public static String GameVersion = "1.32";
	public static String CompileDate = "4/1/2012";

	public static File SystemFolder;
	public static PropertiesReader PropertiesFile;

	public static boolean DebugMode = false;
	public static boolean ShowFPS = true;
	public static boolean ShowHUD = true;
	public static boolean ShowVersion = true;

	public static int ScrollAmount = 1;

	public static boolean LimitFPS;
	public static boolean RoundBlocks;

	public static int MusicVolume;
	public static int SoundVolume;
	public static boolean Fullscreen = false;
		
	public static boolean ShowLight = false;

	public static double Gravity = 0.18;
	public static double Friction = 0.90;

	public static Window MainWindow;

	public static int WorldHeight = 400;
	public static int WorldWidth = 300;
	public static boolean StoryMode = false;
	
	public static boolean PlayerControls = true;

	public static Player ActivePlayer;
	public static Player CameraPlayer;

	public static String Username = "";
	public static String Password = "";
	public static String Session = "";
	
	public static boolean CalculateMovement = false;
	

	public static ServerThread serverThread;
	public static WorldEditorThread worldEditorThread;


	public static void toggleDebug() {
		DebugMode = !DebugMode;
	}

	public static int WindowWidth;
	public static int WindowHeight;


	public static void setupSettings() {
		try {
		switch(LWJGLUtil.getPlatform()) {
		case LWJGLUtil.PLATFORM_LINUX:
			SystemFolder = new File(System.getProperty("user.home") + File.separator + ".Studio878" + File.separator + "CollectConstructConquer" + File.separator);
			break;
		case LWJGLUtil.PLATFORM_MACOSX:
			SystemFolder = new File(System.getProperty("user.home") + File.separator + "Library" + File.separator + "Application Support" + File.separator + "Studio878" + File.separator + "CollectConstructConquer" + File.separator);
			break;
		case LWJGLUtil.PLATFORM_WINDOWS:
			SystemFolder = new File(System.getenv("APPDATA") + "/Studio878/CollectConstructConquer/");
			break;
		}
		System.out.println("System Folder: " +SystemFolder);
		PropertiesFile =  new PropertiesReader(SystemFolder.getAbsolutePath() + File.separator + "settings.properties");
		System.out.println("Properties File: " + SystemFolder.getAbsolutePath() + File.separator + "settings.properties");
		} catch (Exception e) {
			System.out.println("Error in initialization of Settings");
			e.printStackTrace();
		}
		LimitFPS = PropertiesFile.readBoolean("vsync", true);
		MusicVolume = PropertiesFile.readInt("musicvol", 100);
		SoundVolume = PropertiesFile.readInt("soundvol", 100);
	//	Fullscreen = PropertiesFile.readBoolean("fullscreen", false);
		RoundBlocks = PropertiesFile.readBoolean("roundblocks", true);
		System.setProperty("system_folder", Settings.SystemFolder.getAbsolutePath());

	}
}