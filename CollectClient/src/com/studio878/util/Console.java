package com.studio878.util;

import java.util.ArrayList;
import java.util.concurrent.CopyOnWriteArrayList;

import com.studio878.io.NetObjects;
import com.studio878.packets.Packet07SendChat;
import com.studio878.packets.Packet27RequestSuicide;
import com.studio878.packets.Packet33SendServerCommand;
import com.studio878.ui.Chat;
import com.studio878.ui.ChatColors;

public class Console {
	public static boolean ShowConsole = false;
	static CopyOnWriteArrayList<String> messages = new CopyOnWriteArrayList<String>();

	public static void write(String m) {
		messages.add(m);
		System.out.println(m);
	}

	public static CopyOnWriteArrayList<String> read() {
		return messages;
	}

	public static void error(String e) {
		messages.add("[Error] " + e);
		System.out.println("[Error] " + e);
	}

	public static void toggleConsole() {
		ShowConsole = !ShowConsole;
	}
	public static Boolean processLogic(String a) {
		if(a.equalsIgnoreCase("true") || a.equals("1")) {
			return true;
		} else if(a.equalsIgnoreCase("false") || a.equals("0")) {
			return false;
		}
		return null;

	}
	public static void parseCommand(String s) {
		boolean add = false;
		String[] split = s.split(" ");
		if(s.length() > 3 && s.substring(0, 3).equalsIgnoreCase("sv_")) {
			String sd = s.substring(3);
			NetObjects.Sender.sendPacket(new Packet33SendServerCommand(sd));
			return;
		}
		if(split.length >= 2) {
			if(split[0].equalsIgnoreCase("showfps")) {
				Boolean b = processLogic(split[1]);
				if(b != null) {
					Settings.ShowFPS = b;
					add = true;
				} else {
					error("Improper argument for command 'showfps' (Usage: showfps [true/false])");
					return;
				}
			} else if(split[0].equalsIgnoreCase("showdebug")) {
				Boolean b = processLogic(split[1]);
				if(b != null) {
					Settings.DebugMode = b;
					add = true;
				} else {
					error("Improper argument for command 'showdebug' (Usage: showdebug [true/false])");
					return;
				}

			} else if (split[0].equalsIgnoreCase("showversion")) {
				Boolean b = processLogic(split[1]);
				if(b != null) {
					Settings.ShowVersion = b;
					add = true;
				} else {
					error("Improper argument for command 'showversion' (Usage: showversion [true/false])");
					return;
				}
			} else if (split[0].equalsIgnoreCase("say")) {
				String tsd = "";
				for(int i = 1; i < split.length; i++) {
					tsd += split[i] + " ";
				}
				NetObjects.Sender.sendPacket(new Packet07SendChat(Settings.Username, tsd.trim()));
				Console.write("[" + Settings.Username + "]: " + tsd.trim());
				add = true;
			}
		} else if(split[0].equalsIgnoreCase("quit")) {
			Console.write("Quitting...");
			System.exit(0);
		}else if(split[0].equalsIgnoreCase("suicide") || split[0].equalsIgnoreCase("kill")) {
			Console.write("You've killed yourself.");
			NetObjects.Sender.sendPacket(new Packet27RequestSuicide());
			add = true;
		} else if (split[0].equalsIgnoreCase("testchat")) {
			Chat.addMessage("Colors Test: " + ChatColors.Black + "Black" + ChatColors.Blue + "Blue" + ChatColors.Cyan + "Cyan" + ChatColors.LightBlue + "LightBlue" + ChatColors.Purple + "Purple" + ChatColors.Red + "Red" + ChatColors.LightRed + "LightRed" + ChatColors.Green + "Green" + ChatColors.LightGreen + "LightGreen" + ChatColors.Yellow + "Yellow" + ChatColors.Gray + "Gray" + ChatColors.White + "White");
			add = true;

		} else if (split[0].equalsIgnoreCase("help")) {
			Console.write(ChatColors.Yellow + "Client Commands | [Required] (Optional)");
			Console.write("    showfps [true/false] - Show FPS display");
			Console.write("    showdebug [true/false] - Show debug display");
			Console.write("    showversion [true/false] - Show version in main game");
			Console.write("    quit - Exit the game");
			Console.write("    suicide - Kill self");
			Console.write("    say - Send a message");
			add = true;

		}
		if(add) {
			messages.add(">" + s);
			if(messages.size() > 19) {
				messages.remove(0);
			}
			System.out.println("Command run: " + s);
		} else {
			error("Invalid command: " + s);
		}
	}
}
