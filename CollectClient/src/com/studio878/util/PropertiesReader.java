package com.studio878.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map.Entry;

public class PropertiesReader {

	HashMap<String, String> p = new HashMap<String, String>();
	File file;
	public PropertiesReader(String f) {

		try {
			file = new File(f);
			if(!file.exists()) {
				createNewFile(f);
			}
			BufferedReader in = new BufferedReader(new FileReader(f));
			String str;
			while ((str = in.readLine()) != null) {
				if(str.trim().length() > 0 && str.charAt(0) != '#') {
					String[] parts = str.split("=", 2);
					p.put(parts[0], parts[1]);
				}
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}


	}
	public void createNewFile(String f) {
		File dataFile = new File(f);

		try {
			if(f.lastIndexOf("/") != -1) {
				String folder = f.substring(0,f.lastIndexOf("/"));
				(new File(folder)).mkdirs();
			}
			dataFile.createNewFile();
		} catch (IOException e) {
			System.out.println("Error in generating file");
		}

	}
	public int readInt(String key, int def) {
		Integer i;
		String s = p.get(key);
		try {
			if(s == null) {
				p.put(key, ""+def);
				save();
				s = ""+def;
			}
			i =  Integer.parseInt(s);
		} catch (NumberFormatException e) {
			System.out.println("NumberFormatException reading " + key + " from properties. (Result: " + s +")");
			return def;
		}
		return i;
	}
	public int readInt(String key) {
		Integer i;
		try {
			i =  Integer.parseInt(p.get(key));
		} catch (NumberFormatException e) {
			System.out.println("NumberFormatException reading " + key + " from properties.");
			return 0;
		}
		return i;
	}

	public String readString(String key, String def) {
		String t = p.get(key);  
		if(t == null) {
			p.put(key, def);
			save();
			t = def;
		}
		return t;
	}

	public String readString(String key) {
		String t = p.get(key);
		if(t == null) {
		}
		return t;
	}

	public boolean readBoolean(String key, boolean def) {
		Boolean i;
		try {
			String b = p.get(key);
			if(b == null) {
				p.put(key, ""+def);
				save();
				b = ""+def;
			}
			i =  Boolean.parseBoolean(b);
		} catch (Exception e) {
			System.out.println("FormatException reading " + key + " from properties.");
			return def;
		}
		return i;

	}
	public boolean readBoolean(String key) {
		Boolean i;
		try {
			i =  Boolean.parseBoolean(p.get(key));
		} catch (Exception e) {
			System.out.println("NumberFormatException reading " + key + " from properties.");
			return false;
		}
		return i;

	}

	public long readLong(String key, long def) {
		Long i;
		try {
			String s = p.get(key);
			if(s == null) {
				p.put(key, ""+def);
				save();
				s = ""+def;
			}
			i =  Long.parseLong(s);
		} catch (Exception e) {
			System.out.println("FormatException reading " + key + " from properties.");
			p.put(key, ""+def);
			return def;
		}
		return i;
	}

	public long readLong(String key) {
		Long i;
		try {
			i =  Long.parseLong(p.get(key));
		} catch (Exception e) {
			System.out.println("NumberFormatException reading " + key + " from properties.");
			return -1;
		}
		return i;

	}

	public double readDouble(String key, double def) {
		double i;
		try {
			String s = p.get(key);
			if(s == null) {
				p.put(key, ""+def);
				save();
				s = ""+def;
			}
			i =  Double.parseDouble(s);
		} catch (Exception e) {
			System.out.println("FormatException reading " + key + " from properties.");
			return def;
		}
		return i;
	}

	public double readDouble(String key) {
		double i;
		try {
			i =  Double.parseDouble(p.get(key));
		} catch (Exception e) {
			System.out.println("NumberFormatException reading " + key + " from properties.");
			return -1;
		}
		return i;
	}

	public void setInt(String key, int def) {
		String s = p.get(key);
		if(s == null) {
			p.put(key, ""+def);
			save();
			s = ""+def;
		} else {
			p.remove(key);
			p.put(key,  ""+def);
		}
		save();
	}

	public void setString(String key, String def) {
		String s = p.get(key);
		if(s == null) {
			p.put(key, ""+def);
			save();
			s = ""+def;
		} else {
			p.remove(key);
			p.put(key,  ""+def);
		}
		save();
	}

	public void setBoolean(String key, boolean def) {
		String s = p.get(key);
		if(s == null) {
			p.put(key, ""+def);
			save();
			s = ""+def;
		} else {
			p.remove(key);
			p.put(key,  ""+def);
		}
		save();
	}

	public void setLong(String key, long def) {
		String s = p.get(key);
		if(s == null) {
			p.put(key, ""+def);
			save();
			s = ""+def;
		} else {
			p.remove(key);
			p.put(key,  ""+def);
		}
		save();
	}

	public void setDouble(String key, double def) {
		String s = p.get(key);
		if(s == null) {
			p.put(key, ""+def);
			save();
			s = ""+def;
		} else {
			p.remove(key);
			p.put(key,  ""+def);
		}
		save();
	}

	public void save() {
		try {
			BufferedWriter out = new BufferedWriter(new FileWriter(file));

			for(Entry<String, String> entry : p.entrySet()) {
				out.write(entry.getKey() + "=" + entry.getValue());
				out.newLine();
			}
			out.close();
		} catch (Exception e) {

		}
	}

}



