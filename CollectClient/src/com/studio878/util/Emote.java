package com.studio878.util;

import java.util.HashMap;

import com.studio878.util.Damage.DamageMethod;

public class Emote {

	public enum EmoteType {
		Happy (1, 0),
		Angry (2, 1),
		Disapproval (3, 2),
		Left (4, 3),
		Right (5, 4),
		Stop (6, 5),
		Thumbs_Up (7, 6),
		Thumbs_Down(8, 7),
		;
		
		int id, position;
		
		private static HashMap<Integer, EmoteType> idLookup = new HashMap<Integer, EmoteType>();

		private EmoteType(int id, int position) {
			this.id = id;
			this.position = position;
		}
		
		public int getId() {
			return id;
		}
		
		public int getPosition() {
			return position;
		}

		public static EmoteType lookupId(int i) {
			return idLookup.get(i);
		}
		
		static {
			for(EmoteType t: values()) {
				idLookup.put(t.getId(), t);
			}
		}
	}
}
