package com.studio878.util;

import java.util.ArrayList;
import java.util.HashMap;

import org.newdawn.slick.SlickException;
import org.newdawn.slick.Sound;
import org.newdawn.slick.util.ResourceLoader;

import com.studio878.app.Window;

public class SoundEffects {
	public static Sound Grenade;
	public static Sound Pistol;
	public static Sound SMG;
	public static Sound Shotgun;
	public static Sound Sniper;
	public static Sound RocketLauncher;
	public static Sound DryFire;
	public static Sound Landmine;
	public static Sound Flintlock;
	
	public static Sound Click;
	public static Sound BlockPlaced;
	public static Sound Coins;
	
	public static Sound Stone1;
	public static Sound Dirt1;
	public static Sound Dirt2;
	public static ArrayList<Sound> DirtSounds = new ArrayList<Sound>();
	public static ArrayList<Sound> StoneSounds = new ArrayList<Sound>();

	public enum SoundEffectSets {
		Dirt (1, DirtSounds),
		Stone (2, StoneSounds),
		;
		
		private static HashMap<Integer, SoundEffectSets> lookupId = new HashMap<Integer, SoundEffectSets>();
		
		private int id;
		private ArrayList<Sound> sounds;
		private SoundEffectSets(int id, ArrayList<Sound> sounds) {
			this.id = id;
			this.sounds = sounds;
		}
		
		public int getId() {
			return id;
		}
		
		public ArrayList<Sound> getSounds() {
			return sounds;
		}
		
		public static ArrayList<Sound> lookupId(int i) {
			return i == 0 ? null : lookupId.get(i).getSounds();
		}
		static {
			for(SoundEffectSets s : SoundEffectSets.values()) {
				lookupId.put(s.id, s);
			}
		}
	}
	public static void setupSounds() {
		try {
			Console.write("Setting up sound effects...");
			Grenade = new Sound(ResourceLoader.getResource("sfx/weapons/grenade.ogg"));
			Pistol = new Sound(ResourceLoader.getResource("sfx/weapons/pistol.ogg"));
			SMG = new Sound(ResourceLoader.getResource("sfx/weapons/smg.ogg"));
			Shotgun = new Sound(ResourceLoader.getResource("sfx/weapons/shotgun.ogg"));
			Sniper = new Sound(ResourceLoader.getResource("sfx/weapons/sniper.ogg"));
			DryFire = new Sound(ResourceLoader.getResource("sfx/weapons/dry.ogg"));
			RocketLauncher = new Sound(ResourceLoader.getResource("sfx/weapons/rocket.ogg"));
			Landmine = new Sound(ResourceLoader.getResource("sfx/weapons/mine.ogg"));
			Flintlock = new Sound(ResourceLoader.getResource("sfx/weapons/flintlock.ogg"));
			
			Click = new Sound(ResourceLoader.getResource("sfx/system/click.ogg"));
			BlockPlaced = new Sound(ResourceLoader.getResource("sfx/system/place.ogg"));
			Coins = new Sound(ResourceLoader.getResource("sfx/system/coins.ogg"));
			
			Stone1 = new Sound(ResourceLoader.getResource("sfx/steps/stone1.ogg"));
			StoneSounds.add(Stone1);
			Dirt1 = new Sound(ResourceLoader.getResource("sfx/steps/dirt1.ogg"));
			DirtSounds.add(Dirt1);
			Dirt2 = new Sound(ResourceLoader.getResource("sfx/steps/dirt2.ogg"));
			DirtSounds.add(Dirt2);
			Console.write("...Sound effects are ready.");
		} catch (SlickException e) {
			Console.error("Error creating sound files");
			e.printStackTrace();
		}
	}
	
	public static void playSound(Sound s) {

		if(Settings.SoundVolume != 0) {
			s.play(1.0f, (float)Settings.SoundVolume/(float)100);
		}
	}
	public static void playSound(Sound s, int x, int y) {
		double td = Math.sqrt(Math.pow(Settings.ActivePlayer.getX() - x, 2) + Math.pow(Settings.ActivePlayer.getY() - y, 2));
		if(td != 0 && Settings.SoundVolume != 0) {
			s.play(1.0f,((float) ((float) Settings.SoundVolume/100) * ((float)Math.abs(((1000-td)/1000)))));
		}
	}  
	public static void playSound(Sound s, int x, int y, float mod) {
		double td = Math.sqrt(Math.pow(Settings.ActivePlayer.getX() - x, 2) + Math.pow(Settings.ActivePlayer.getY() - y, 2));
		if(td != 0 && Settings.SoundVolume != 0) {
			s.play(1.0f,((float) ((float) Settings.SoundVolume/100) * ((float)Math.abs(((1000-td)/1000)))) * mod);
		}
	}  
}
