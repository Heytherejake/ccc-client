package com.studio878.util;

import java.util.ArrayList;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.fills.GradientFill;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.geom.Transform;

public class GraphicsUtil {

	public static void drawWithShadow(Graphics g, String s, int x, int y, Color f, Color b) {
		g.setColor(b);
		g.drawString(s, x, y + 1);
		g.setColor(f);
		g.drawString(s, x, y);
	}

	public static void drawRightJustifiedString(Graphics g, GameContainer c, String s, int y) {
		g.drawString(s, c.getWidth() - g.getFont().getWidth(s) - 10, y);
	}

	public static void drawShadowBox(Graphics g, int x, int y, int width, int height, double rot) {
		Shape r = new Rectangle(x + 3, y + 3,  width, height);
		r = r.transform(Transform.createRotateTransform((float) Math.toRadians(rot), r.getCenterY(), r.getCenterY()));

		GradientFill f = new GradientFill(-100, -100, new Color(0, 0, 0, 0.0f), r.getWidth(), r.getHeight(), new Color(0, 0, 0, 0.4f));
		g.fill(r, f);
		g.setColor(Color.white);
		r.setX(r.getX() - 3);
		r.setY(r.getY() - 3);
		g.fill(r);
	}

	public static ArrayList<String> splitBetweenLines(String s, UnicodeFont f, int w) {
		ArrayList<String> results = new ArrayList<String>();
		if(f.getWidth(s) < w) {
			results.add(s);
			return results;
		} else {
			String left = s;
			for(int i = 0; i < left.length(); i++) {

				if(f.getWidth(left.substring(0, i)) > w) {
					int lm = left.substring(0, i).indexOf("<br>") + 4;
					if(lm == 3) {
						lm = left.substring(0, i).lastIndexOf(' ');
					}
					results.add(left.substring(0, lm).replace("<br>", "").trim());
					left = left.substring(lm);
					i = 0;
				}
			}
			results.add(left.trim());
		}
		return results;

	}
}
