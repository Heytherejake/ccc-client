package com.studio878.util;

import java.util.HashMap;

import com.studio878.util.Damage.DamageMethod;


public class TeamManager {

	public enum Team {
		Red (0),
		Blue (1),
		;
		
		int id;
		private static HashMap<Integer, Team> idLookup = new HashMap<Integer, Team>();

		private Team(int id) {
			this.id = id;
		}
		
		public int getId() {
			return id;
		}
		
		public static Team lookupId(int i) {
			return idLookup.get(i);
		}
		
		static {
			for(Team t: values()) {
				idLookup.put(t.getId(), t);
			}
		}
	}


	public static Team matchTeam(String s) {
		if(s.equals("Red")) {
			return Team.Red;
		} else if(s.equals("Blue")) {
			return Team.Blue;
		}
		return null;
	}
}
