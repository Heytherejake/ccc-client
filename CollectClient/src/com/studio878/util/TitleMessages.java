package com.studio878.util;

import java.util.ArrayList;

public class TitleMessages {
	static String[] messages = {
		"Warning: Not complient with Article 57 of the Shadow Proclimation",
		"Studio878 celebrates \"We Love The Programmer\" Day.",
		"The idea for these messages was totally not stolen from Minecraft",
		"If you have to ask, then you don't know",
		"GUI Interface created in Visual Basic to track IP addresses",
		"Code goes in, game comes out. You can't explain that.",
		"Collect, Construct, Conquer. This kills the free time.",
		"Explode ALL the things!",
		"Would like to ask you out for texting and scones",
		"Specifically designed to require every keyboard key",
		"The official flagship game of Studio878 Software Laboratories",
		"Is sworn to carry your burdens..."
	};
	
	public static String getRandomMessage() {
		return messages[(int) (Math.random()*messages.length)];
	}
	
	static String[] tips = {
		"Kill players or mine resources to get money to buy useful items to augment your character.",
		"Grenades can be used to quickly clear large chunks of terrain.",
		"Koen's abilities enable him to quickly earn coins through mining.",
		"Yalwn's passive encourages him to spend more money researching rather than on items for himself, making him a great support.",
		"Koen starts with a larger dig radius than other characters.",
		"If you run out of ammunition, retreat back to your base to refill.",
		"Avoid death.",
		"Fluids slow you down. Avoid them if possible.",
		"Place torches to increase visibility underground.",
		"Sell anything you don't need to conserve inventory space.",
		"You can mine resources or kill player to earn more coins.",
		"You can increase your stats using temporary items. Buy stat boosting items in the store!",
		"At level 5, 10, and 15, you can put points into your Ultimate ability.",
		"Talk to your team! Failing to communicate is just as bad as feeding!",
		"Knowing each character's abilities gives you a huge advantage over a less-informed player.",
		"Some characters benefit more from stat increasing items instead of weaponry.",
		"Mining resources greatly reduces the costs of items.",
		"Some classes are much more efficient with melee weapons over ranged weapons.",
		"Some items have active abilities. Don't forget to use them!",
		"Weapons you've added to your loadout are buffed when you obtain them ingame.",

	};
	
	public static String getRandomTip() {
		return tips[(int) (Math.random()*tips.length)];
	}


}

