package com.studio878.util;

import java.io.IOException;

import javax.annotation.Resource;

import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.ResourceLoader;

public class Sheets {

	public static SpriteSheet BLOCK_TEXTURES;
	public static SpriteSheet BLOCK_TEXTURES_NOTALIASED;
	public static SpriteSheet BLOCK_BREAK_TEXTURES;
	public static SpriteSheet ICON_TEXTURES;
	public static SpriteSheet BUTTONS;
	public static SpriteSheet CHECKBOXES;
	public static SpriteSheet PROJECTILES;
	public static SpriteSheet WEAPONS;
	public static SpriteSheet WEAPONS_FIRING;
	public static SpriteSheet ITEMS;
	public static SpriteSheet ITEMS_ANTIALIASED;
	public static SpriteSheet PARTICLES;
	public static SpriteSheet ROCKET;
	public static SpriteSheet HUD_ICONS;
	public static SpriteSheet CRAFTING_IMAGES;
	public static SpriteSheet RESEARCH_IMAGES;
	public static SpriteSheet STAT_ICONS;
	public static SpriteSheet GENERATOR;
	public static SpriteSheet EXPLOSION;
	public static SpriteSheet HANDS;
	public static SpriteSheet CLASS_ICONS;
	public static SpriteSheet STORY_PANELS;
	public static SpriteSheet CHAPTER_PANELS;
	public static SpriteSheet PIPE_CORNERS;
	public static SpriteSheet EMOTES;
	public static SpriteSheet WIRE_OVERLAY_ICONS;
	public static SpriteSheet WIRE_CORNERS;
	public static SpriteSheet LIGHT_WIRE_CORNERS;
	public static SpriteSheet LOADOUT_ITEMS;
	public static SpriteSheet FLIP_COUNTER;
	public static SpriteSheet STORE_BOXES;
	public static SpriteSheet UNLOCK_CARDS;
	public static SpriteSheet ITEM_INSET_BOXES;
	public static SpriteSheet CLOSE_BUTTONS;
	public static SpriteSheet ITEM_FLAIR;
	public static Image UI_BARS;
	
	public static SpriteSheet CHARACTER_SPLASH;
	public static SpriteSheet ABILITY_ICONS;

	
	public static SpriteSheet EXCAVATOR_RED;
	public static SpriteSheet EXCAVATOR_BLUE;
	
	public static SpriteSheet RESEARCHER_RED;
	public static SpriteSheet RESEARCHER_BLUE;
	
	public static SpriteSheet SOLDIER_RED;
	public static SpriteSheet SOLDIER_BLUE;
	
	public static void createSheets() {
		try {
			
			Image is = new Image(ResourceLoader.getResourceAsStream("textures/excavator.png"), "exp_inp", false, Image.FILTER_NEAREST);
			EXCAVATOR_BLUE = new SpriteSheet(is.getSubImage(0, 0, 196, 64), 32, 64);
			EXCAVATOR_RED = new SpriteSheet(is.getSubImage(0, 64, 196, 64), 32, 64);
			
			is = new Image(ResourceLoader.getResourceAsStream("textures/researcher.png"), "rec_inp", false, Image.FILTER_NEAREST);
			RESEARCHER_BLUE = new SpriteSheet(is.getSubImage(0, 0, 196, 64), 32, 64);
			RESEARCHER_RED = new SpriteSheet(is.getSubImage(0, 64, 196, 64), 32, 64);
			
			is = new Image(ResourceLoader.getResourceAsStream("textures/soldier.png"), "sol_inp", false, Image.FILTER_NEAREST);
			SOLDIER_BLUE = new SpriteSheet(is.getSubImage(0, 0, 196, 64), 32, 64);
			SOLDIER_RED = new SpriteSheet(is.getSubImage(0, 64, 196, 64), 32, 64);
			
			BLOCK_BREAK_TEXTURES = new SpriteSheet("breaks", ResourceLoader.getResourceAsStream("textures/breaks.png"), 16, 16);
			
			is = new Image(ResourceLoader.getResourceAsStream("textures/textures.png"), "block_inp", false, Image.FILTER_NEAREST);
			BLOCK_TEXTURES_NOTALIASED = new SpriteSheet(is, 16, 16);
			
			BLOCK_TEXTURES = new SpriteSheet("blocks", ResourceLoader.getResourceAsStream("textures/textures.png"), 16, 16);
			ICON_TEXTURES = new SpriteSheet("icons", ResourceLoader.getResourceAsStream("textures/icons.png"), 33, 33);
			BUTTONS = new SpriteSheet("buttons", ResourceLoader.getResourceAsStream("textures/buttons.png"), 200, 50);
			CHECKBOXES = new SpriteSheet("checkboxes", ResourceLoader.getResourceAsStream("textures/checkbox.png"), 13, 13);
			PROJECTILES = new SpriteSheet("projectiles", ResourceLoader.getResourceAsStream("textures/projectiles.png"), 32, 32);
			
			WEAPONS = new SpriteSheet("weapons", ResourceLoader.getResourceAsStream("textures/weapons.png"), 64, 32);

			WEAPONS_FIRING = new SpriteSheet("weaponsfiring", ResourceLoader.getResourceAsStream("textures/weapons_firing.png"), 64, 32);

			
			is = new Image(ResourceLoader.getResourceAsStream("textures/items.png"), "image_inp", false, Image.FILTER_NEAREST);
			ITEMS = new SpriteSheet(is, 32, 32);
			
			is = new Image(ResourceLoader.getResourceAsStream("textures/ui_bars.png"), "bars_inp", false, Image.FILTER_NEAREST);
			UI_BARS = new SpriteSheet(is, 32, 32);
			
			is = new Image(ResourceLoader.getResourceAsStream("textures/story_panels.png"), "snp_inp", false, Image.FILTER_NEAREST);
			STORY_PANELS = new SpriteSheet(is, 50, 50);
			
			is = new Image(ResourceLoader.getResourceAsStream("textures/chapter_panels.png"), "chp_inp", false, Image.FILTER_NEAREST);
			CHAPTER_PANELS = new SpriteSheet(is, 50, 50);
			
			ITEMS_ANTIALIASED = new SpriteSheet("items_aa", ResourceLoader.getResourceAsStream("textures/items.png"), 32, 32);
			PARTICLES = new SpriteSheet("particles", ResourceLoader.getResourceAsStream("textures/particles.png"), 4, 4);
			ROCKET = new SpriteSheet("rocket", ResourceLoader.getResourceAsStream("textures/rocket.png"), 32, 32);
			HUD_ICONS = new SpriteSheet("hudicons", ResourceLoader.getResourceAsStream("textures/iconset.png"), 16, 16);
			CRAFTING_IMAGES = new SpriteSheet("crafting", ResourceLoader.getResourceAsStream("textures/craft_images.png"), 128, 128);
			RESEARCH_IMAGES = new SpriteSheet("research", ResourceLoader.getResourceAsStream("textures/research_images.png"), 128, 128);
			STAT_ICONS = new SpriteSheet("staticons", ResourceLoader.getResourceAsStream("textures/stat_icons.png"), 64, 64);
			GENERATOR = new SpriteSheet("generator", ResourceLoader.getResourceAsStream("textures/generator.png"), 227, 101);
			EXPLOSION = new SpriteSheet("explosion", ResourceLoader.getResourceAsStream("textures/explosion.png"), 128, 128);
			PIPE_CORNERS = new SpriteSheet("pipecorners", ResourceLoader.getResourceAsStream("textures/pipecorners.png"), 16, 16);
			EMOTES = new SpriteSheet("emotes", ResourceLoader.getResourceAsStream("textures/emotes.png"), 32, 32);
			WIRE_OVERLAY_ICONS = new SpriteSheet("wireoverlayicons", ResourceLoader.getResourceAsStream("textures/wire_overlay_icons.png"), 32, 32);
			WIRE_CORNERS = new SpriteSheet("wirecorners", ResourceLoader.getResourceAsStream("textures/wirecorners.png"), 16, 16);
			LIGHT_WIRE_CORNERS = new SpriteSheet("lightwirecorners", ResourceLoader.getResourceAsStream("textures/lightwires.png"), 16, 16);
			LOADOUT_ITEMS = new SpriteSheet("loadout_items", ResourceLoader.getResourceAsStream("textures/loadout_items.png"), 64, 64);
			FLIP_COUNTER = new SpriteSheet("flip_counter", ResourceLoader.getResourceAsStream("textures/flipcounter.png"), 30, 40);
			STORE_BOXES = new SpriteSheet("store_boxes", ResourceLoader.getResourceAsStream("textures/store_boxes.png"), 150, 150);
			UNLOCK_CARDS = new SpriteSheet("unlock_cards", ResourceLoader.getResourceAsStream("textures/unlock_cards.png"), 200, 300);
			ITEM_INSET_BOXES = new SpriteSheet("inset_box", ResourceLoader.getResourceAsStream("images/inset_box.png"), 100, 100);
			CLOSE_BUTTONS = new SpriteSheet("close_button", ResourceLoader.getResourceAsStream("textures/close_button.png"), 13, 13);
			ITEM_FLAIR = new SpriteSheet("item_flair", ResourceLoader.getResourceAsStream("textures/item_flair.png"), 16, 16);

			CHARACTER_SPLASH = new SpriteSheet("character_splash", ResourceLoader.getResourceAsStream("textures/character_splash.png"), 100, 100);
			ABILITY_ICONS = new SpriteSheet("ability_icons", ResourceLoader.getResourceAsStream("textures/skill_icons.png"), 50, 50);
			
			is = new Image(ResourceLoader.getResourceAsStream("textures/hand.png"), "hands_inp", false, Image.FILTER_NEAREST);

			HANDS = new SpriteSheet(is, 20, 5);
			
			is = new Image(ResourceLoader.getResourceAsStream("textures/class_icons.png"), "ci_inp", false, Image.FILTER_LINEAR);
			CLASS_ICONS = new SpriteSheet(is, 32, 32);
			
		} catch (SlickException e) {
			Console.error("Failed to create texture sheet. Game is going to fail.");
			e.printStackTrace();
		}
	}
}
