package com.studio878.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import com.studio878.displays.Menu;
import com.studio878.threads.ServerThread;
import com.studio878.threads.WorldEditorThread;

public class WorldEditorLoader {
	static String version;

	public static boolean downloadMapEditor() {
		try {
			URL url = new URL("http://dl.studio878software.com/mapeditor.version");

			BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
			version = in.readLine();
			Menu.setDisplayMessage("Checking map editor version...");
			File cvs = new File(Settings.SystemFolder, "mapeditor.version");
			if(!cvs.exists()) {
				cvs.createNewFile();
				FileWriter f = new FileWriter(cvs);
				f.write("0.0");
				f.flush();
				f.close();
			}
			File fd2 = new File(Settings.SystemFolder, "CCCWorldEditor.jar");
			BufferedReader iv2 = new BufferedReader(new FileReader(cvs));
			String vs = iv2.readLine();
			if(!version.equals(vs) || !fd2.exists()) {
				Menu.setDisplayMessage(("Downloading CCCWorldEditor.jar (Version " + version + ") (0%)"));
				downloadFile(new URL("http://dl.studio878software.com/CCCWorldEditor.jar"), "");
				Menu.setDisplayMessage("Server downloaded, preparing for startup...");

			} else {
				Menu.setDisplayMessage("Server is up to date, preparing for startup...");
			}
			FileWriter fs = new FileWriter(cvs);
			fs.write(version);
			fs.flush();
			Settings.worldEditorThread = new WorldEditorThread();
			Settings.worldEditorThread.start();

			Menu.setDisplayMessage("Launching world editor, please wait...");

		} catch (IOException e) {
			e.printStackTrace();
			Menu.setDisplayMessage("Failed to download current ccc_server.jar");
			return false;
		}
		return true;
	}


	public static void downloadFile(URL url, String fld) throws IOException, MalformedURLException{
		String end = url.getFile().split("/")[url.getFile().split("/").length - 1];
		BufferedInputStream in = new BufferedInputStream(url.openStream());
		FileOutputStream fos = new FileOutputStream(Settings.SystemFolder.getAbsolutePath() + File.separator + fld + File.separator + end);
		BufferedOutputStream bout = new BufferedOutputStream(fos,1024);
		byte data[] = new byte[64];
		int count;
		double total = 0;
		double size = url.openConnection().getContentLength();
		while((count = in.read(data,0,64)) != -1){
			total += count;
			bout.write(data,0,count);
			Menu.setDisplayMessage("Downloading " + end + "  (Version " + version + ") (" + ((int) ((total/size)*100)) + "%)");


		}
		bout.close();
		fos.close();
		in.close();

	}

	public static void wait (int n){
		long t0,t1;
		t0=System.currentTimeMillis();
		do{
			t1=System.currentTimeMillis();
		}
		while (t1-t0<n);
	}

}
