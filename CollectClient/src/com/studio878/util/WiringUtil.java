package com.studio878.util;

import java.awt.Point;
import java.util.Map.Entry;

import org.newdawn.slick.Image;

import com.studio878.block.BlockType;
import com.studio878.displays.MainGame;
import com.studio878.inventory.ItemID;

public class WiringUtil {

	public enum WiringElement {
		AndGate("AND Gate", 2, Sheets.BLOCK_TEXTURES.getSubImage(0,62), null, BlockType.lookupName("And Gate")),
		OrGate("OR Gate", 2, Sheets.BLOCK_TEXTURES.getSubImage(0,64), null, BlockType.lookupName("Or Gate")),
		NotGate("NOT Gate", 2, Sheets.BLOCK_TEXTURES.getSubImage(0,66), null, BlockType.lookupName("Not Gate")),
		NorGate("NOR Gate", 2, Sheets.BLOCK_TEXTURES.getSubImage(0,68), null, BlockType.lookupName("Nor Gate")),
		XorGate("XOR Gate", 2, Sheets.BLOCK_TEXTURES.getSubImage(0,70),null, BlockType.lookupName("Xor Gate")),
		Battery("Battery", 2, Sheets.BLOCK_TEXTURES.getSubImage(0,72), null, BlockType.lookupName("Battery")),
		Turret("Turret", 3, Sheets.BLOCK_TEXTURES.getSubImage(32, 54*16, 32, 32), ItemID.lookupName("Turret"), null),
		;

		String name;
		int type;
		ItemID source;
		BlockType blockSource;
		Image image;

		private WiringElement(String name, int type, Image image, ItemID source, BlockType blockSource) {
			this.name = name;
			this.type = type;
			this.image = image;
			this.source = source;
			this.blockSource = blockSource;
		}

		public String getName() {
			return name;
		}

		public Image getImage() {
			return image;
		}
		
		public int getType() {
			return type;
		}

		public BlockType getBlockSource() {
			return blockSource;
		}
		

		public ItemID getItemSource() {
			return source;
		}


	}

	public static void mapLocation(int x, int y, int value) {
		for(Point p : MainGame.wireMap.keySet()) {
			if(p.getX() == x && p.getY() == y) {
				MainGame.wireMap.remove(p);
				MainGame.wireMap.put(new Point(x, y), value);
				return;
			}
		}
		MainGame.wireMap.put(new Point(x, y), value);
	}

	public static void unmapLocation(int x, int y) {
		for(Point p : MainGame.wireMap.keySet()) {
			if(p.getX() == x && p.getY() == y) {
				MainGame.wireMap.remove(p);
				return;
			}
		}
	}

	public static int countWires() {
		int count = 0;
		for(Integer p : MainGame.wireMap.values()) {
			if(p == 30 || p == 31) {
				count++;
			}
		}
		return count;
	}

	public static int countGates() {
		int count = 0;
		for(Integer p : MainGame.wireMap.values()) {
			if(p >= 32 && p <= 37) {
				count++;
			}
		}
		return count;
	}
	
	public static int countElement(WiringElement e) {
		int count = 0;
		for(Integer p : MainGame.wireMap.values()) {
			if(e.getItemSource() != null) {
				if(p == e.getItemSource().getId() + 100000) {
				count++;
				}
			} else {
				if(p == e.getBlockSource().getPosition()) {
					count++;
				}
			}
		}
		return count;
	}
	
	
	public static int getValueAt(int x, int y) {
		for(Entry<Point, Integer> p : MainGame.wireMap.entrySet()) {
			if(p.getKey().getX() == x && p.getKey().getY() == y) {
				return p.getValue();
			}
		}
		return -1;
	}

	public static boolean isVerticalSection(int x, int y) {
		int pos = 0;
		for(Entry<Point, Integer> p : MainGame.wireMap.entrySet()) {
			if(p.getKey().getX() == x && (p.getKey().getY() == y - 1 || p.getKey().getY() == y + 1) && p.getValue() == 30) {
				pos++;
			}
		}
		return pos >= 1;
	}
	
	public static boolean isHorizontalSection(int x, int y) {
		int pos = 0;
		for(Entry<Point, Integer> p : MainGame.wireMap.entrySet()) {
			if(p.getKey().getY() == y && (p.getKey().getX() == x - 1 || p.getKey().getX() == x + 1) && p.getValue() == 31) {
				pos++;
			}
		}
		return pos == 2;
	}

}
