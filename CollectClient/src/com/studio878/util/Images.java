package com.studio878.util;

import javax.annotation.Resource;

import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.util.ResourceLoader;

public class Images {
	public static Image backgroundImage;
	public static Image Cursor;
	public static Image menuBackground;
	public static Image cloud;
	public static Image logo;
	public static Image shell;
	public static Image bullet;
	public static Image slider;
	public static Image woodPanel;
	public static Image scavengerIcon;
	public static Image button;
	public static Image BlockTray;
	public static Image BlockTrayBackground;
	
	public static Image Workbench;
	public static Image ResearchBench;
	
	public static Image FolderBackground;
	public static Image FolderTab;
	public static Image FolderClipFront;
	public static Image FolderClipBack;
	public static Image FolderTape;
	
	public static Image GridItem;
	public static Image ShopBackground;
	
	public static Image StripeBackground;
	public static Image BlueprintBackground;
	
	
	public static void init() {
		try {
			cloud = new Image(ResourceLoader.getResourceAsStream("backgrounds/cloud.png"), "cloud", false);
			logo = new Image(ResourceLoader.getResourceAsStream("backgrounds/logo.png"), "logo", false);
			backgroundImage = new Image(ResourceLoader.getResourceAsStream("backgrounds/clouds.png"), "clouds", false);
			menuBackground = new Image(ResourceLoader.getResourceAsStream("backgrounds/menu.png"), "menubg", false);
			Cursor = new Image(ResourceLoader.getResourceAsStream("textures/cursor.png"), "cursor", false);
			shell = new Image(ResourceLoader.getResourceAsStream("textures/shell.png"), "shell", false);
			bullet = new Image(ResourceLoader.getResourceAsStream("textures/bullet.png"), "bullet", false);
			slider = new Image(ResourceLoader.getResourceAsStream("textures/slider.png"), "slider", false);
			woodPanel = new Image(ResourceLoader.getResourceAsStream("images/woodpanel.png"), "woodbg", false);
			button = new Image(ResourceLoader.getResourceAsStream("textures/button_segments.png"), "button_segment", false);
			FolderBackground = new Image(ResourceLoader.getResourceAsStream("images/folder_main.png"), "folderbg", false);
			FolderTab = new Image(ResourceLoader.getResourceAsStream("images/folder_tab.png"), "foldertab", false);
			FolderClipFront = new Image(ResourceLoader.getResourceAsStream("images/folder_paperclip_front.png"), "clipfront", false);
			FolderClipBack = new Image(ResourceLoader.getResourceAsStream("images/folder_paperclip_back.png"), "clipback", false);
			FolderTape = new Image(ResourceLoader.getResourceAsStream("images/folder_tape.png"), "tape", false);
			scavengerIcon = new Image(ResourceLoader.getResourceAsStream("images/icon.png"), "scavicon", false);
			Workbench = new Image(ResourceLoader.getResourceAsStream("textures/workbench.png"), "workbench", false);
			ResearchBench = new Image(ResourceLoader.getResourceAsStream("textures/research_bench.png"), "research_bench", false);
			BlockTray = new Image(ResourceLoader.getResourceAsStream("textures/block_tray.png"), "block_tray", false);
			BlockTrayBackground = new Image(ResourceLoader.getResourceAsStream("textures/block_tray_background.png"), "block_tray_background", false);
			StripeBackground = new Image(ResourceLoader.getResourceAsStream("images/stripe_bg.png"), "stripe_bg", false);
			GridItem = new Image(ResourceLoader.getResourceAsStream("textures/grid_item.png"), "grid_item", false);
			ShopBackground = new Image(ResourceLoader.getResourceAsStream("textures/shop_background.png"), "shop_background", false);
			BlueprintBackground = new Image(ResourceLoader.getResourceAsStream("textures/blueprint_bg.png"), "blueprint_bg", false);

		} catch (SlickException e) {
			Console.error("Failed to load background image.");
		}
	}
}
