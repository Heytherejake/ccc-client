package com.studio878.util;

import java.util.HashMap;


public class Movement {
	public static enum MovementType {
		Left (0),
		Right(1),
		Jump (2),
		None (3),
		FlippedNone (4)
		;
		private static HashMap<Integer, MovementType> idLookup = new HashMap<Integer, MovementType>();
		private int id;
		private MovementType(int id) {
			this.id = id;
		}
		
		public int getId() {
			return id;
		}
		
		public static MovementType lookupId(int i) {
			return idLookup.get(i);
		}
		static {
			for(MovementType t: values()) {
				idLookup.put(t.getId(), t);
			}
		}
	}
}
