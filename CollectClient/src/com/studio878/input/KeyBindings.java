package com.studio878.input;


import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map.Entry;

import org.lwjgl.input.Keyboard;
import org.newdawn.slick.GameContainer;

import com.studio878.app.Window;
import com.studio878.ui.KeyMapListItem;
import com.studio878.util.Console;
import com.studio878.util.Settings;

public class KeyBindings extends Object {

	public static HashMap<String, int[]> KeyBindMap = new HashMap<String, int[]>();

	public static int[] Move_Left;
	public static int[] Move_Right;
	public static int[] Open_Console;
	public static int[] Show_Debug_Information;
	public static int[] Open_Menu;
	public static int[] Take_Screenshot;
	public static int[] Send_Chat;
	public static int[] Open_All_Chat;
	public static int[] Open_Chat;
	public static int[] Close_Window;
	public static int[] Scroll_Chat_Up;
	public static int[] Scroll_Chat_Down;
	public static int[] Jump;
	public static int[] Open_Inventory;
	public static int[] Open_Player_Statistics;
	public static int[] Open_Team_Information;
	public static int[] Open_Enemy_Information;
	public static int[] Reload;
	public static int[] Toggle_Display;
	public static int[] Open_Wheel;
	public static int[] Attack;
	public static int[] Use;
	public static int[] Voice_Menu;


	private static boolean tabDown = false;
	private static boolean backspaceDown = false;
	private static boolean mouse1Down = false;
	private static boolean mouse2Down = false;

	private static HashMap<Integer, Boolean> pressMap = new HashMap<Integer, Boolean>();

	public static void loadInitialKeyBindings() {
		Move_Left = loadKeyBinding("Move_Left",  new int[]{Keyboard.KEY_A});
		Move_Right = loadKeyBinding("Move_Right", new int[]{Keyboard.KEY_D});
		Open_Console = loadKeyBinding("Open_Console", new int[]{Keyboard.KEY_GRAVE});
		Show_Debug_Information = loadKeyBinding("Show_Debug_Information", new int[]{Keyboard.KEY_F3});
		Open_Menu = new int[]{Keyboard.KEY_ESCAPE};
		Take_Screenshot = loadKeyBinding("Take_Screenshot", new int[]{Keyboard.KEY_F2});
		Send_Chat = new int[]{Keyboard.KEY_RETURN};
		Open_All_Chat = loadKeyBinding("Open_All_Chat", new int[]{Keyboard.KEY_LSHIFT, Keyboard.KEY_RETURN});
		Open_Chat = loadKeyBinding("Open_Chat", new int[]{Keyboard.KEY_RETURN});
		Close_Window = loadKeyBinding("Close_Window", new int[]{Keyboard.KEY_TAB});
		Scroll_Chat_Up = loadKeyBinding("Scroll_Chat_Up", new int[]{Keyboard.KEY_UP});
		Scroll_Chat_Down = loadKeyBinding("Scroll_Chat_Down", new int[]{Keyboard.KEY_DOWN});
		Jump = loadKeyBinding("Jump", new int[]{Keyboard.KEY_SPACE});
		Open_Inventory = loadKeyBinding("Open_Inventory", new int[]{Keyboard.KEY_I});
		Open_Player_Statistics = loadKeyBinding("Open_Player_Statistics", new int[]{Keyboard.KEY_Y});
		Open_Team_Information = loadKeyBinding("Open_Team_Information", new int[]{Keyboard.KEY_TAB});
		Open_Enemy_Information = loadKeyBinding("Open_Enemy_Information", new int[]{Keyboard.KEY_P});
		Reload = loadKeyBinding("Reload", new int[]{Keyboard.KEY_R});
		Toggle_Display = loadKeyBinding("Toggle_Display", new int[]{Keyboard.KEY_F4});
		Open_Wheel = loadKeyBinding("Open_Wheel", new int[]{1001});
		Attack = loadKeyBinding("Attack", new int[]{1000});
		Use = loadKeyBinding("Use", new int[]{Keyboard.KEY_E});
		Voice_Menu = loadKeyBinding("Voice_Menu", new int[]{Keyboard.KEY_V});



	}

	public static int[] loadKeyBinding(String name, int[] def) {
		String pars = Settings.PropertiesFile.readString(name);
		if(pars != null) {
			String[] parts = pars.split("\\+");
			int[] keys = new int[parts.length];

			int i = 0;
			for(String p : parts) {
				try {
					keys[i] = Integer.parseInt(parts[i]);
				} catch (NumberFormatException e) {
					e.printStackTrace();
				}
				i++;
			}
			KeyBindMap.put(name, keys);
			return keys;
		} else {
			KeyBindMap.put(name, def);
			return def;
		}
	}

	public static boolean isKeyPressed(GameContainer c, int[] key) {
		if(key[0] == -1){
			return false;
		}
		if(Settings.PlayerControls || key == Use) {
			ArrayList<Integer> revert = new ArrayList<Integer>();
			boolean success = true;
			for(int i : key) {
				if(i == 15) {
					if(c.getInput().isKeyDown(15)) {
						if(!tabDown) {	
							tabDown = true;
						}
					} else {	
						success = false;
						tabDown = false;
					}
				}
				if(i == 14) {
					if(c.getInput().isKeyDown(14)) {
						if(!backspaceDown) {	
							backspaceDown = true;
						}
					} else {	
						success = false;
						backspaceDown = false;
					}
				}
				if(i == 1000) {
					if(!c.getInput().isMousePressed(0)) success = false;
				} else if (i == 1001) {
					if(!c.getInput().isMousePressed(1)) success = false;
				} else {
					if(c.getInput().isKeyDown(i)) {
						Boolean src = pressMap.get(i);
						if(src != null) {
							if(!src) {
								pressMap.put(i, true);
								revert.add(i);
							} else {
								success = false;
							}
						} else {
							pressMap.put(i, true);

						}
					} else {
						success = false;
						pressMap.put(i, false);
					}
				}
			}
			if(!success) {
				for(Integer i : revert) {
					pressMap.put(i, false);
				}
			}
			return success;
		}
		return false;
	}

	public static String getKeyName(int[] key) {
		if(key[0] == -1) {
			return "[Not Set]";
		}
		String s = "";
		for(int i : key) {
			if(i == 1000) {
				s += "Mouse 1";
			} else if (i == 1001) {
				s += "Mouse 2";
			} else {
				String name = Keyboard.getKeyName(i);
				name = name.replace("LSHIFT", "Shift");
				name = name.replace("LCONTROL", "Ctrl");
				name = name.replace("RETURN", "Enter");

				if(name.length() > 1) {
					s += capitalizeFirst(name);
				} else {
					s += name;
				}
			}
			s += " + ";
		}
		return s.substring(0, s.length() - 3);
	}

	public static boolean isKeyDown(GameContainer c, int[] key) {
		if(key[0] == -1){
			return false;
		}
		if(Settings.PlayerControls || key == Use) {
			boolean success = true;
			for(int i : key) {
				if(i == 1000) {
					if(!c.getInput().isMouseButtonDown(0)) success = false;
				} else if (i == 1001) {
					if(!c.getInput().isMouseButtonDown(1)) success = false;
				} else {
					if(!c.getInput().isKeyDown(i)) success = false;
				}
			}
			return success;
		}
		return false;
	}
	public static void setKey(String action, int[] key) {
		try {
			Field[] fields = KeyBindings.class.getFields();
			for(Field f : fields) {
				if(f.getName().equalsIgnoreCase(action)) {
					f.setAccessible(true);
					f.set(null, key);
					if(key[0] != -1) {
						KeyBindMap.put(action, key);
						for(Entry<String, int[]> e : KeyBindMap.entrySet()) {
							if(Arrays.toString(key).equals(Arrays.toString(e.getValue())) && !e.getKey().equals(action)) {
								Settings.PropertiesFile.setString(Window.KeyMapTable.getSelected().getMessage().split(":")[0].replace(' ', '_'), "-1");
								Window.populateKeyBindingList();
								setKey(e.getKey(), new int[]{-1});
							}
						}
					}
				}
			}
		} catch (IllegalArgumentException e) {
			Console.error("Failed to set key: Key is not valid");
		} catch (IllegalAccessException e) {
			Console.error("Failed to set key: Cannot access KeyBindings");
		} catch (SecurityException e) {
			Console.error("Failed to set key: Insufficient access to KeyBindings");
		}
	}

	public static int[] getKey(String name) {
		try {
			Field[] fields = KeyBindings.class.getFields();
			for(Field f : fields) {
				if(f.getName().equalsIgnoreCase(name)) {
					return (int[]) f.get(null);
				}
			}
		} catch (IllegalArgumentException e) {
			Console.error("Failed to set key: Key is not valid");
		} catch (IllegalAccessException e) {
			Console.error("Failed to set key: Cannot access KeyBindings");
		} catch (SecurityException e) {
			Console.error("Failed to set key: Insufficient access to KeyBindings");
		}
		return new int[]{0};
	}

	protected static String capitalizeFirst(String s) {
		char[] chars = s.toLowerCase().toCharArray();
		String ret = "";
		for (int i = 0; i < chars.length; i++) {
			if(i == 0) {
				ret += Character.toUpperCase(chars[i]);
			} else {
				ret += Character.toLowerCase(chars[i]);
			}
		}
		return ret;
	}
}


