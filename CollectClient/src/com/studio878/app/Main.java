/*Collect, Construct, Conquer! Source
 *(c)2011 Studio878 Software Laboratories
 *
 *Do not distribute. Redistribution of this code without consent from
 *Studio878 Software Laboratories is unlawful under the United States
 *Digital Millennium Copywrite Act. All rights reserved.
 *
 *Modification of this code is not permitted. All modifications should
 *be done with the official API provided by Studio878. Adding any API calls
 *or hooks is against the Studio878 Terms of Service and will result in the
 *termination of your account, as well as access to Collect, Construct, Conquer! with
 *no refund issued. 
 */

package com.studio878.app;

import java.applet.Applet;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.DisplayMode;
import java.awt.Frame;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Toolkit;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JFrame;
import javax.swing.JLayeredPane;
import javax.swing.SwingUtilities;

import org.lwjgl.opengl.Display;
import org.newdawn.slick.CanvasGameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.util.ResourceLoader;

import com.studio878.io.NetObjects;
import com.studio878.objects.EntityManager;
import com.studio878.packets.Packet05Disconnect;
import com.studio878.ui.SplashPanel;
import com.studio878.util.Console;
import com.studio878.util.Settings;


public class Main{
	static File folder;
	static File musicFolder = new File(Settings.SystemFolder, "assets" + File.separator + "music");
	public static File downloadFolder;

	public static JFrame frame;
	public static CanvasGameContainer canvas;
	public static boolean fullyLoaded = false;
	public static SplashPanel splash;
	public static JLayeredPane panel = new JLayeredPane();
	
	public static boolean isDisconnecting = false;
	
	static Timer timer = new Timer("main");

	public static void main(String[] args) {
		System.out.println("Starting up...");

		for(int i = 0; i < args.length; i++) {
			System.out.println("(" + i + "): " + args[i]);
		}

		System.out.println("Determining system settings...");
		Settings.setupSettings();
		folder = new File(Settings.SystemFolder, "assets" + File.separator + "fonts");
		musicFolder = new File(Settings.SystemFolder, "assets" + File.separator + "music");
		downloadFolder = new File(Settings.SystemFolder, "assets" + File.separator + "data");

		extractFiles();

		try {
			System.setProperty("sun.java2d.noddraw", "true");

			frame = new JFrame("Collect, Construct, Conquer!"); 
			frame.setLayout(new BorderLayout()); 
			Window w = new Window();
			canvas = new CanvasGameContainer(w);
			//canvas.setName("Collect, Construct, Conquer!");
			canvas.setSize(1024, 768);

			//c.setDisplayMode(p.readInt("windowWidth", 1080), p.readInt("windowHeight", 840), false);
			canvas.getContainer().setAlwaysRender(true);
			splash = new SplashPanel();
			frame.setSize(1024, 768);
			splash.setSize(frame.getSize());
			panel.add(splash, 1);
			panel.add(canvas, 0);
			//splash.setLocation(0, 0);
			canvas.setLocation(10000, 10000);
			canvas.getContainer().setMultiSample(2);
			frame.add(panel);
			frame.setResizable(false);
			frame.setUndecorated(false);
			new Thread() {
				public void run() {
					while(!Main.fullyLoaded) {
						try {
							this.sleep(50);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} 
					}
					Console.write("...Game fully loaded.");
					Main.canvas.setLocation(0, 0);
					Main.panel.remove(Main.splash);
				}
			}.start();
			frame.addComponentListener(new ComponentListener() {

				public void componentShown(ComponentEvent e) {

				}

				public void componentResized(ComponentEvent e) {
					//canvas.setSize(frame.getWidth() - frame.getInsets().left - frame.getInsets().right, frame.getHeight() - frame.getInsets().top - frame.getInsets().bottom);
				}

				public void componentMoved(ComponentEvent e) {

				}

				public void componentHidden(ComponentEvent arg0) {

				}
			});
			
			frame.setLocationRelativeTo(null);

			if(Settings.Fullscreen) {
				frame.setUndecorated(true);
				Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
				frame.setBounds(0,0,screenSize.width, screenSize.height);
			}
			setWindowSize(new DisplayMode(Settings.PropertiesFile.readInt("windowWidth", 1024), Settings.PropertiesFile.readInt("windowHeight", 728), 32, 60), Settings.Fullscreen);
			frame.setVisible(true);
			frame.addWindowListener(windowListener); 
			java.awt.Image i = Toolkit.getDefaultToolkit().getImage(ResourceLoader.getResource("images/ccc_icon.png"));
			frame.setIconImage(i);

			canvas.start();
		} catch (SlickException e) {
			Console.error("Failed to start game.");
			e.printStackTrace();
		}

	}

	public static void dispose() {
		frame.dispose();
	}
	public static void toggleFullscreen() {
		if(!Settings.Fullscreen) {
			frame.dispose();
			frame.setUndecorated(true);
			Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
			frame.setBounds(0,0,screenSize.width, screenSize.height);
			frame.setVisible(true);
			frame.requestFocus();
		} else {
			frame.dispose();
			frame.setUndecorated(false);
			frame.setBounds(0, 0, 1024, 768);
			frame.setVisible(true);
			frame.requestFocus();

		}
	}

	public static void setWindowSize(DisplayMode dsp, boolean fullscreen) {
		try {
			if(fullscreen) {
				System.setProperty("sun.java2d.noddraw", "true");

				frame.removeNotify();
				frame.setUndecorated(true);
				frame.addNotify();
				GraphicsDevice dev = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
				dev.setFullScreenWindow(Main.frame);
				dev.setDisplayMode(dsp);
				org.lwjgl.opengl.Display.setDisplayMode(new org.lwjgl.opengl.DisplayMode(dev.getDisplayMode().getWidth(), dev.getDisplayMode().getHeight()));

				Main.canvas.setSize(new Dimension(dsp.getWidth() - frame.getInsets().left - frame.getInsets().right, dsp.getHeight() - frame.getInsets().top - frame.getInsets().bottom));
				Main.frame.setSize(new Dimension(dsp.getWidth(), dsp.getHeight()));
				Main.panel.setSize(new Dimension(dsp.getWidth() - frame.getInsets().left - frame.getInsets().right, dsp.getHeight() - frame.getInsets().top - frame.getInsets().bottom));
				Main.splash.setSize(new Dimension(dsp.getWidth() - frame.getInsets().left - frame.getInsets().right, dsp.getHeight() - frame.getInsets().top - frame.getInsets().bottom));
				frame.requestFocus();
			} else {
				GraphicsDevice dev = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
				dev.setFullScreenWindow(null);
				frame.removeNotify();
				frame.setUndecorated(false);
				frame.addNotify();
				Main.canvas.setSize(new Dimension(dsp.getWidth(), dsp.getHeight() - frame.getInsets().top - frame.getInsets().bottom));
				Main.frame.setSize(new Dimension(dsp.getWidth(), dsp.getHeight()));
				Main.panel.setSize(new Dimension(dsp.getWidth() - frame.getInsets().left - frame.getInsets().right, dsp.getHeight() - frame.getInsets().top - frame.getInsets().bottom));
				Main.splash.setSize(new Dimension(dsp.getWidth() - frame.getInsets().left - frame.getInsets().right, dsp.getHeight() - frame.getInsets().top - frame.getInsets().bottom));
				frame.requestFocus();
			}
		} catch (Exception e) {
			Console.error("Failed to set resolution");
			e.printStackTrace();
		}
	}

	public static void startGame() {
		try {
			canvas.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}
	public static void stopGame() {
		isDisconnecting = true;
		new Thread() {

			public void run() {
				try {
					NetObjects.ActiveClient.softDisconnect = true;

					NetObjects.Sender.sendPacket(new Packet05Disconnect());

					NetObjects.ActiveClient.disconnect();

					NetObjects.ActiveClient.interrupt();

					if(Settings.serverThread != null) {
						Console.write("Stopping server...");

						Settings.serverThread.shutdown();
						Settings.serverThread.interrupt();
						Settings.serverThread = null;

					}
				} catch (Exception e) {
				}
				EntityManager.clearEntities();
				Console.write("Disconnected.");
				isDisconnecting = false;
			}
		}.start();
	}

	public static Applet loadFromOtherApplication() {
		Display.destroy();
		try {
			canvas = new CanvasGameContainer(new Window());
			canvas.setName("Collect, Construct, Conquer!");
			canvas.setSize(1024, 480);
			canvas.getContainer().setAlwaysRender(true);
			canvas.start();
			//canvas.setVisible(true);
			Applet j = new Applet();
			j.setLayout(new BorderLayout());
			j.add(canvas, "Center");
			j.setVisible(true);
			return j;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;

	}
	private static WindowListener windowListener = new WindowListener() { 
		public void windowOpened(WindowEvent e) { } 
		public void windowIconified(WindowEvent e) { } 
		public void windowDeiconified(WindowEvent e) { } 
		public void windowDeactivated(WindowEvent e) { } 
		public void windowClosed(WindowEvent e) { } 
		public void windowActivated(WindowEvent e) { } 
		public void windowClosing(WindowEvent e) { 
			Main.stopGame(); 
			SwingUtilities.invokeLater(new Runnable() { 
				@Override 
				public void run() { 
					org.lwjgl.opengl.Display.destroy();
					System.exit(0);
				} 
			}); 
		} 
	}; 

	public static void extractFiles() {
		try {
			folder.mkdirs();
			musicFolder.mkdirs();
			downloadFolder.mkdirs();

			URL u = ResourceLoader.getResource("fonts/lucon.ttf");
			File f = new File(folder, "lucon.ttf");
			if(!f.exists()) {
				downloadFont(u);
			}
			
			u = ResourceLoader.getResource("music/title_1.ogg");
			f = new File(musicFolder, "title_1.ogg");
			if(!f.exists()) {
				downloadSong(u);
			}
			u = ResourceLoader.getResource("fonts/KIN668.TTF");
			f = new File(folder, "KIN668.TTF");
			if(!f.exists()) {
				downloadFont(u);
			}

			u = ResourceLoader.getResource("fonts/Telex.ttf");
			f = new File(folder, "Telex.TTF");
			if(!f.exists()) {
				downloadFont(u);
			}

		/*	URL uf = ResourceLoader.getResource("data/codex.db");
			File ff = new File(downloadFolder, "codex.db");
			downloadDatabase(uf);*/

		} catch (Exception e) {
			Console.error("Failed to extract font files");
			e.printStackTrace();
		}


	}

	public static void downloadSong(URL url) throws IOException, MalformedURLException{
		String end = url.getFile().split("/")[url.getFile().split("/").length - 1];
		Console.write("Extracting to " + musicFolder.getAbsolutePath() + File.separator + end);

		BufferedInputStream in = new BufferedInputStream(url.openStream());
		FileOutputStream fos = new FileOutputStream(musicFolder.getAbsolutePath() + File.separator + end);
		BufferedOutputStream bout = new BufferedOutputStream(fos,1024);
		byte data[] = new byte[1024];
		int count;
		while((count = in.read(data,0,1024)) != -1){
			bout.write(data,0,count);
		}
		bout.close();
		fos.close();
		in.close();

	}
	public static void downloadFont(URL url) throws IOException, MalformedURLException{
		String end = url.getFile().split("/")[url.getFile().split("/").length - 1];
		Console.write("Extracting to " + folder.getAbsolutePath() + File.separator + end);

		BufferedInputStream in = new BufferedInputStream(url.openStream());
		FileOutputStream fos = new FileOutputStream(folder.getAbsolutePath() + File.separator + end);
		BufferedOutputStream bout = new BufferedOutputStream(fos,1024);
		byte data[] = new byte[1024];
		int count;
		while((count = in.read(data,0,1024)) != -1){
			bout.write(data,0,count);
		}
		bout.close();
		fos.close();
		in.close();

	}

	public static void downloadDatabase(URL url) throws IOException, MalformedURLException{
		String end = url.getFile().split("/")[url.getFile().split("/").length - 1];
		Console.write("Extracting to " + downloadFolder.getAbsolutePath() + File.separator + end);

		BufferedInputStream in = new BufferedInputStream(url.openStream());
		FileOutputStream fos = new FileOutputStream(downloadFolder.getAbsolutePath() + File.separator + end);
		BufferedOutputStream bout = new BufferedOutputStream(fos,1024);
		byte data[] = new byte[1024];
		int count;
		while((count = in.read(data,0,1024)) != -1){
			bout.write(data,0,count);
		}
		bout.close();
		fos.close();
		in.close();

	}


	public static void registerTimerTask(TimerTask t, int delay) {
		timer.schedule(t, delay);
	}
}

