package com.studio878.app;


import java.awt.Font;
import java.io.File;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.TreeMap;

import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JLayeredPane;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.StyleSheet;

import org.lwjgl.LWJGLUtil;
import org.lwjgl.Sys;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.DisplayMode;
import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.Color;
import org.newdawn.slick.Game;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Music;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.imageout.ImageOut;
import org.newdawn.slick.util.ResourceLoader;

import com.studio878.displays.Display;
import com.studio878.displays.Loader;
import com.studio878.displays.MainGame;
import com.studio878.displays.Menu;
import com.studio878.input.KeyBindings;
import com.studio878.multiplayer.LoadoutPanel;
import com.studio878.multiplayer.MultiplayerSettings;
import com.studio878.ui.Button;
import com.studio878.ui.Chat;
import com.studio878.ui.ChatColors;
import com.studio878.ui.ClosablePopup;
import com.studio878.ui.CodexEntryItem;
import com.studio878.ui.DraggableTableList;
import com.studio878.ui.Folder;
import com.studio878.ui.Fonts;
import com.studio878.ui.KeyMapListItem;
import com.studio878.ui.Popup;
import com.studio878.ui.PromptPopup;
import com.studio878.ui.RoundEndView;
import com.studio878.ui.SizableButton;
import com.studio878.ui.Slider;
import com.studio878.ui.TableList;
import com.studio878.ui.TableListItem;
import com.studio878.util.Console;
import com.studio878.util.Images;
import com.studio878.util.Settings;

public class Window implements Game{

	public Display d = new Loader();
	GameContainer c;

	//Menu Objects
	public static boolean MenuOpen = false;
	public static int MenuId = 0;
	Button b; //Disconnect
	Button b2; //Options
	Button b3; //Quit
	Button b4; //Loadout;

	Slider s1; //Music Volume
	Slider s2; //SFX Volume

	int prevMusicVolume;
	int prevSoundVolume;

	Button o1; //Graphics Settings
	Button o2; //Controls
	Button o3; //Back

	Button v1; //Vertical Sync
	Button v2; //Block Effects
	Button v4; //Resolution...
	Button v3; //Back

	SizableButton c1; //Back

	public static TableList KeyMapTable; //Key Mapping List
	Button k1; //Key Mapping Back
	Button k2; //Key Mapping Remap
	Popup kp; //Key Mapping Popup

	TableList rs; //Resolution list
	Button r1; //Resolution Accept
	Button r2; //Resolution Back
	Button r3; //Fullscreen Toggle


	public Folder folder;

	public LoadoutPanel loadoutPanel;

	public RoundEndView successView;

	public TableList characterSelect;

	public TableList crafting;
	public TableList research;

	public TableList multiplayerCharacterList;
	public TableList multiplayerAbilityList;

	public Image craftlogo;

	public DraggableTableList codexList;

	boolean bindKey = false;

	float fillColor = 0.0f;

	public static Music Music;

	public static boolean isMainGame = false;

	JEditorPane codexViewer;
	JScrollPane codexScroll;

	CodexEntryItem prevCodexItem;

	public static ClosablePopup eventPopup;
	public static Popup alertPopup;
	public static PromptPopup optionPopup;

	boolean fullscreen = Settings.Fullscreen;
	String prevSelectedResolution = "";

	public void init(GameContainer c) throws SlickException {
		c.setVerbose(false);

		Settings.Username = System.getProperty("username");
		Settings.Password = System.getProperty("password");
		Settings.Session = System.getProperty("session");
		System.out.println(Settings.Username + "/" + Settings.Password);
		c.setAlwaysRender(true);
		c.setShowFPS(false);
		//Main.splash.message = "Determining system information...";

		Console.write("Collect, Construct, Conquer!");
		Console.write("(C)2011-2012 Studio878 Software Laboratories");
		Console.write("Game version: " + Settings.GameVersion +" (Compiled " + Settings.CompileDate + ")");
		Console.write("Dimensions: " + c.getScreenHeight() + "," +  c.getScreenWidth());

		Console.write("LWJGL version: " + Sys.getVersion());
		Console.write("Slick version: " + AppGameContainer.getBuildVersion());
		String platform = "";
		switch(LWJGLUtil.getPlatform()) {
		case LWJGLUtil.PLATFORM_LINUX:
			platform = "Linux";
			break;
		case LWJGLUtil.PLATFORM_MACOSX:
			platform = "Mac OS X";
			break;
		case LWJGLUtil.PLATFORM_WINDOWS:
			platform = "Windows";
			break;
		default:
			platform = "Unregistered";
		}
		if(Settings.LimitFPS) {
			c.setVSync(true);
		}
		Console.write("VSync: " + Settings.LimitFPS);
		Console.write("Platform: " + platform);
		Console.write("Launching game...");
		Console.write("Creating spritesheets.");

		Console.write("Colors Test: " + ChatColors.Black + "Black" + ChatColors.Blue + "Blue" + ChatColors.Cyan + "Cyan" + ChatColors.LightBlue + "LightBlue" + ChatColors.Purple + "Purple" + ChatColors.Red + "Red" + ChatColors.LightRed + "LightRed" + ChatColors.Green + "Green" + ChatColors.LightGreen + "LightGreen" + ChatColors.Yellow + "Yellow" + ChatColors.Gray + "Gray" + ChatColors.White + "White");


		c.setMaximumLogicUpdateInterval(10);
		c.setMinimumLogicUpdateInterval(10);
		Settings.MainWindow = this;
		this.c = c;
		Images.init();
		d.init(c);
		loadoutPanel = new LoadoutPanel(c);
		b =  new Button(- 100, c.getHeight() / 2 - 200, "Disconnect", true);
		b4 = new Button(-100, c.getHeight()/2 - 100, "Loadout", true);
		b2 =  new Button(- 100, c.getHeight() / 2 - 0, "Options", true);
		b3 =  new Button(- 100, c.getHeight() / 2 + 100, "Quit", true);
		v1 =  new Button( - 300, 250, "Limit FPS: Off", true);
		if(Settings.LimitFPS){
			v1.setValue("Vertical Sync: On");
		} else {
			v1.setValue("Vertical Sync: Off");
		}

		v2 = new Button(-300, 400, "Block Effects: On", true);
		v4 = new Button(100, 250, "Window Size...", true);
		v3 = new Button(-100, 500, "Back", true);
		o1 = new Button(-300, 250, "Graphics Settings", true);
		s1 = new Slider(-200, 200, 200, Fonts.ChatFont, true, true);
		s2 = new Slider(200, 200, 200, Fonts.ChatFont, true, true);

		c1 = new SizableButton(c.getWidth() - 300, c.getHeight() - 100, 100, 50, "Back", false);

		s1.setPosition(Settings.MusicVolume);
		s2.setPosition(Settings.SoundVolume);
		prevMusicVolume = s1.getPosition();
		prevSoundVolume = s2.getPosition();
		o2 =  new Button(100, 250, "Key Mapping", true);
		o3 =  new Button(- 100, c.getHeight() / 2 + 100, "Back", true);

		r1 = new Button(-400, 500, "Back", true);
		r2 = new Button(200, 500, "Accept", true);
		r3 = new Button(-100, 500, "Fullscreen: Off", true);
		rs = new TableList(-400, 150, 800, 300, Fonts.SmallFont, true, 0);
		HashMap<Integer, Integer> used = new HashMap<Integer, Integer>();
		try {
			for(DisplayMode d : org.lwjgl.opengl.Display.getAvailableDisplayModes()) {
				if(d.getFrequency() == 60 && d.getWidth() > 1024 && (used.get(d.getWidth()) == null || used.get(d.getWidth()) != d.getHeight())) {
					used.put(d.getWidth(), d.getHeight());
				}
			}
			TreeMap<Integer, Integer> sorted = new TreeMap<Integer, Integer>();
			sorted.putAll(used);
			for(java.util.Map.Entry<Integer, Integer> d : sorted.entrySet()) {
				rs.addItem(new TableListItem(d.getKey() + "x" + d.getValue(), 20, rs));
			}
		} catch (Exception e) {
			Console.error("Failed to list all possible display modes");
		}

		if(Settings.Fullscreen){
			r3.setValue("Fullscreen: On");
		} else {
			r3.setValue("Fullscreen: Off");
		}

		System.out.println("Loading key bindings...");
		KeyBindings.loadInitialKeyBindings();
		
		KeyMapTable = new TableList(-400, 150, 800, 300, Fonts.SmallFont, true, 0);
		populateKeyBindingList();

		k1 = new Button(100, c.getHeight() / 2 + 100, "Remap Key", true);
		k2 = new Button(-300, c.getHeight() / 2 + 100, "Back", true);
		kp = new Popup("Select a key to bind", c.getWidth() / 2 - 300, c.getHeight()/2 - 100, 600, 200, Fonts.ChatFont, true);




		try {
			int song = ((int) (Math.random()*2));
			String loc = "music/title_1.ogg";
			switch(song) {
			case 1:
				loc = "music/title_2.ogg";
				break;
			}
			System.out.println(song + "|" + loc);
			Music = new Music(ResourceLoader.getResource(loc));
			Music.loop();
			Music.setVolume((float) ((float) Settings.MusicVolume/100));
		} catch (SlickException e) {
			e.printStackTrace();
		}

		try {
			c.setMouseCursor(Images.Cursor, 8, 8);
		} catch (SlickException e) {
			e.printStackTrace();
		}

		folder = new Folder();
		successView = new RoundEndView();

		codexViewer = new JEditorPane();
		codexViewer.requestFocus();
		codexViewer.setEditable(false);
		codexViewer.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
		codexViewer.setSelectionColor(java.awt.Color.white);
		codexViewer.setContentType("text/html");
		codexViewer.addHyperlinkListener(new HyperlinkListener() {

			public void hyperlinkUpdate(HyperlinkEvent e) {

				if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
					for(int i = 0; i < codexList.items.size(); i++) {
						CodexEntryItem ic = (CodexEntryItem) codexList.items.get(i);
						if(ic.getTitle().equalsIgnoreCase(e.getDescription())) {
							codexList.setSelected(i);
							codexViewer.setText("<html><body><font face='Trebuchet MS'>" + ic.getValue() + "</font></body></html>");

						}
					}
				}
			}
		});


		HTMLEditorKit kit = new HTMLEditorKit();
		codexViewer.setEditorKit(kit);
		StyleSheet styleSheet = kit.getStyleSheet();
		//styleSheet.addRule("body {color:#000; font-family:times; margin: 4px; }");
		styleSheet.addRule("a {color: #9ACD28; text-decoration: none;}");
		//	styleSheet.addRule("h2 {color: #ff0000;}");
		//	styleSheet.addRule("pre {font : 10px monaco; color : black; background-color : #fafafa; }");

		codexScroll = new JScrollPane();
		codexScroll.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED); 
		codexScroll.setViewportView(codexViewer);
		//codexPanel.add(codexViewer, BorderLayout.CENTER);

		Main.panel.add(codexScroll, JLayeredPane.MODAL_LAYER);

		try {
			crafting = new TableList(10, 0, 750, 450, Fonts.LargeFont, false, 0);

			research = new TableList(10, 0, 750, 450, Fonts.LargeFont, false, 0);

			characterSelect = new TableList(200, 0, 400, 450, Fonts.LargeFont, false, 0);

			codexList = new DraggableTableList(20, 0, 200, c.getHeight(), Fonts.LargeFont, 100, false, 0);

			multiplayerCharacterList = new TableList(420, 0, 300, c.getHeight(), Fonts.ChatFont, false, 0);

			multiplayerAbilityList = new TableList(420, 100, c.getWidth() - 440, c.getHeight() - 200, Fonts.ChatFont, false, 0);

			craftlogo = new Image(300, 300);

		} catch (Exception e) {
			e.printStackTrace();
		}

		alertPopup = new Popup("Alert", 0, 0, 600, 200, Fonts.ChatFont, false);
		eventPopup = new ClosablePopup("Event", "Accept", 0, 0, 600, 200, Fonts.ChatFont, false);
		optionPopup = new PromptPopup("Option", "Accept", "Decline", 0, 0, 600, 200, Fonts.ChatFont, false);

		com.studio878.characters.PlayerCharacter.populateCharacterList();
		MainGame.populateCharacterSelectList();




	}

	public void render(GameContainer c, Graphics g) throws SlickException {
		d.draw(c, g);
		if(MenuOpen) {
			if(fillColor < 0.6f) {
				fillColor += 0.01f;
			}
			g.setColor(new Color(0, 0, 0, fillColor));
			g.fillRect(0, 0, c.getWidth(), c.getHeight());
			if(MenuId == 0) {
				b.draw(g, c);
				b2.draw(g, c);
				b3.draw(g, c);
				b4.draw(g, c);
			} else if (MenuId == 1) {
				o1.draw(g, c);
				o2.draw(g, c);
				o3.draw(g, c);

				s1.draw(c, g);
				s2.draw(c, g);
				g.setFont(Fonts.ChatFont);
				g.setColor(Color.black);
				g.drawString("Music Volume", c.getWidth()/2 - 200 - Fonts.ChatFont.getWidth("Music Volume")/2, 170);
				g.drawString("Sound Volume", c.getWidth()/2 + 200 - Fonts.ChatFont.getWidth("Sound Volume")/2, 170);

			} else if (MenuId == 2) {

				KeyMapTable.draw(g);
				k1.draw(g, c);
				k2.draw(g, c);
			} else if (MenuId == 3) {
				v1.draw(g, c);
				v2.draw(g, c);
				v3.draw(g, c);
				v4.draw(g, c);
			} else if (MenuId == 4) {
				g.setColor(new Color(235, 235, 235));
				g.fillRect(20, 0, 200, c.getHeight());
				codexList.setDrawBackground(false);
				codexList.draw(g);
				c1.draw(c, g);

			} else if (MenuId == 5) {
				r1.draw(g, c);
				r2.draw(g, c);
				//	r3.draw(g, c);
				rs.draw(g);
			} else if (MenuId == 6) {
				loadoutPanel.draw(g, c);
			}
		} else {
			fillColor = 0.0f;
		}
		if(bindKey) {
			kp.draw(g, c);
		} 
		if(alertPopup.isOpen()) {
			alertPopup.setX(c.getWidth()/2 - 300);
			alertPopup.setY(c.getHeight()/2 - 100);
			alertPopup.draw(g, c);
		}
		if(eventPopup.isOpen()) {
			eventPopup.setX(c.getWidth()/2 - 300);
			eventPopup.setY(c.getHeight()/2 - 100);
			eventPopup.draw(g, c);
		}

		if(optionPopup.isOpen()) {
			optionPopup.setX(c.getWidth()/2 - 300);
			optionPopup.setY(c.getHeight()/2 - 100);
			optionPopup.draw(g, c);
		}


	}

	public void update(GameContainer c, int g) throws SlickException {

		if(bindKey) {	
			String out_str = "";
			
			ArrayList<Integer> keyArray = new ArrayList<Integer>();
			if(Keyboard.isKeyDown(Keyboard.KEY_LSHIFT)) {
				keyArray.add(Keyboard.KEY_LSHIFT);
				out_str += Keyboard.KEY_LSHIFT + "+";
			}
			if (Keyboard.isKeyDown(Keyboard.KEY_LCONTROL)) {
				keyArray.add(Keyboard.KEY_LCONTROL);
				out_str += Keyboard.KEY_LCONTROL + "+";
			}
			if (Keyboard.isKeyDown(Keyboard.KEY_TAB)) {
				keyArray.add(Keyboard.KEY_TAB);
				out_str += Keyboard.KEY_TAB + "+";
			}


			int key = Keyboard.getEventKey();
			if((c.getInput().isMouseButtonDown(0) || c.getInput().isMouseButtonDown(1))) {
				if (c.getInput().isMouseButtonDown(0)) {
					keyArray.add(1000);
					String output = out_str + "1000";
					int[] converted = convertIntegers(keyArray);

					Settings.PropertiesFile.setString(KeyMapTable.getSelected().getMessage().split(":")[0].replace(' ', '_'), output);
					KeyBindings.setKey(KeyMapTable.getSelected().getMessage().split(":")[0].replace(' ', '_'), converted);
					populateKeyBindingList();

					bindKey = false;
					return;
				} else if (c.getInput().isMouseButtonDown(1)) {
					keyArray.add(1001);
					String output = out_str + "1001";
					int[] converted = convertIntegers(keyArray);

					Settings.PropertiesFile.setString(KeyMapTable.getSelected().getMessage().split(":")[0].replace(' ', '_'), output);
					KeyBindings.setKey(KeyMapTable.getSelected().getMessage().split(":")[0].replace(' ', '_'), converted);
					populateKeyBindingList();

					bindKey = false;
					return;
				}
			} else {
				if((key != Keyboard.KEY_LSHIFT && key != Keyboard.KEY_RSHIFT && key != Keyboard.KEY_LCONTROL && key != Keyboard.KEY_RCONTROL && key != Keyboard.KEY_TAB)) {
					if(Keyboard.getEventKey() != Keyboard.CHAR_NONE && Keyboard.getEventKeyState()) {

						keyArray.add(key);
						String output = out_str + key;
						int[] converted = convertIntegers(keyArray);

						Settings.PropertiesFile.setString(KeyMapTable.getSelected().getMessage().split(":")[0].replace(' ', '_'), output);
						KeyBindings.setKey(KeyMapTable.getSelected().getMessage().split(":")[0].replace(' ', '_'), converted);
						populateKeyBindingList();

						bindKey = false;
						return;
					}  
				}
			}
		}

		//All the always-do keys. Like screenshots for menus!
		if(KeyBindings.isKeyPressed(c, KeyBindings.Close_Window)) {
			if (Console.ShowConsole) {
				Console.ShowConsole = false;
			} 
		}

		if(KeyBindings.isKeyPressed(c, KeyBindings.Show_Debug_Information)) {
			Settings.toggleDebug();
		}

		if (KeyBindings.isKeyPressed(c, KeyBindings.Take_Screenshot)) {

			Image target = new Image(c.getWidth(), c.getHeight());
			c.getGraphics().copyArea(target, 0, 0);
			Date d = new Date();
			Format f = new SimpleDateFormat("y-M-d_h.m.s");
			File fs = new File(Settings.SystemFolder.getAbsolutePath(), "screenshots");
			if(!fs.exists()) {
				fs.mkdir();
			}
			ImageOut.write(target, Settings.SystemFolder.getAbsolutePath() + "/screenshots/"+f.format(d)+".png", false);
			target.destroy();
			Console.write("Screenshot saved to screenshots/"+f.format(d)+".png");
			Chat.addMessage(ChatColors.Green + "Screenshot saved to screenshots/"+f.format(d)+".png");
		}

		d.update(c, g);
		if(KeyBindings.isKeyPressed(c, KeyBindings.Open_Menu)) {
			/*if((d instanceof MainGame)) {
				Main.stopGame();
				System.exit(0);
			}*/
			if(MenuOpen) {
				if(!eventPopup.isOpen() && !alertPopup.isOpen() && !optionPopup.isOpen()) {
					MenuOpen = false;
				}
			}
			if(bindKey) {
				bindKey = false;
			}
		}


		if(MenuId != 4 || !MenuOpen) {
			codexScroll.setLocation(-10000, -10000);
		}
		if(MenuOpen) {
			if(MenuId == 0) {
				b.check(c);
				b2.check(c);
				b3.check(c);
				b4.check(c);
				b.setY(c.getHeight()/2 - 200);
				b4.setY(c.getHeight()/2 - 100);
				b2.setY(c.getHeight() / 2 - 0); 
				b3.setY(c.getHeight() / 2 + 100);
			} else if (MenuId == 1) {
				o1.check(c);
				o2.check(c);
				o3.check(c);
				s1.update(c);
				s2.update(c);
				Settings.SoundVolume = s2.getPosition();
				if(s1.isDragging()) {
					Settings.MusicVolume = s1.getPosition();
					Music.setVolume((float) ((float) Settings.MusicVolume/100));
					Settings.PropertiesFile.setInt("musicvol", s1.getPosition());
				}
				if(s2.isDragging()) {
					Settings.SoundVolume = s2.getPosition();
					Settings.PropertiesFile.setInt("soundvol", s2.getPosition());

				}

			} else if (MenuId == 2) {
				if(!bindKey) {
					k1.check(c);
					k2.check(c);
					KeyMapTable.update(c);
					int msw = Mouse.getEventDWheel();
					if(msw > 0) {
						KeyMapTable.updateScrollPosition(-5*Settings.ScrollAmount);
					} else if(msw < 0) {
						KeyMapTable.updateScrollPosition(5*Settings.ScrollAmount);
					}
				}
				if(o1.isSelected()) {
					MenuId = 3;
				}
			} else if (MenuId == 3) {
				v1.check(c);
				v2.check(c);
				v3.check(c);
				v4.check(c);
				if(v4.isSelected()) {
					MenuId = 5;
				}
				if(v3.isSelected()) {
					MenuId = 1;
				}
			} else if (MenuId == 4) {
				codexList.update(c);
				codexList.setHeight(c.getHeight());
				CodexEntryItem entry = (CodexEntryItem) codexList.getSelected();
				if(entry != null && codexViewer.getText().contains("<p") || entry != prevCodexItem) {
					codexViewer.setText("<html><body><font face='Trebuchet MS'>" + entry.getValue() + "</font></body></html>");
					prevCodexItem = entry;

				}
				codexScroll.setSize(c.getWidth() - 270, c.getHeight() - 200);
				codexScroll.setLocation(250, 100);
				c1.setX(c.getWidth() - 150);
				c1.setY(c.getHeight() - 90);
				c1.check(c);
				if(c1.isSelected()) {
					if(d instanceof Menu) {
						MenuOpen = false;
					}
					MenuId = 0;
				}

			} else if (MenuId == 5) {
				r1.check(c);
				r2.check(c);
				//r3.check(c);
				rs.update(c);
				if(r1.isSelected()) {
					MenuId = 3;
				}
				if(rs.getSelected() == null || prevSelectedResolution == rs.getSelected().getMessage()) {
					r2.isDisabled(true);
				} else {
					r2.isDisabled(false);
				}
				if(r2.isSelected()) {
					String[] sel = rs.getSelected().getMessage().split("x");
					Main.setWindowSize(new java.awt.DisplayMode(Integer.parseInt(sel[0]), Integer.parseInt(sel[1]), 32, 60), fullscreen);
					Main.frame.toFront();
					Main.frame.setState(JFrame.NORMAL);
					Settings.PropertiesFile.setBoolean("fullscreen", fullscreen);
					Settings.PropertiesFile.setInt("windowWidth", Integer.parseInt(sel[0]));
					Settings.PropertiesFile.setInt("windowHeight", Integer.parseInt(sel[1]));
					prevSelectedResolution = rs.getSelected().getMessage();
				}
			} else if (MenuId == 6) {
				loadoutPanel.update(c);
			}



			if(b.isSelected()) {
				this.setDisplay(new Menu(3));
				Main.stopGame();

			} else if (b2.isSelected()) {
				MenuId = 1;
			} else if(b3.isSelected()){
				Main.stopGame();
				Main.dispose();

				System.exit(0);
			} else if (v1.isSelected()) {
				if(!Settings.LimitFPS) {
					c.setVSync(true);
					Settings.LimitFPS = true;
					v1.setValue("Vertical Sync: On");
					Settings.PropertiesFile.setBoolean("vsync", true);
				} else {
					c.setVSync(false);
					Settings.LimitFPS = false;
					v1.setValue("Vertical Sync: Off");
					Settings.PropertiesFile.setBoolean("vsync", false);

				}
			} else if (v2.isSelected()) {
				if(!Settings.RoundBlocks) {
					Settings.RoundBlocks = true;
					v2.setValue("Block Effects: On");
					Settings.PropertiesFile.setBoolean("roundblocks", true);
				} else {
					Settings.RoundBlocks = false;
					v2.setValue("Block Effects: Off");
					Settings.PropertiesFile.setBoolean("roundblocks", false);

				}
			} else if (r3.isSelected()) {
				if(!fullscreen) {
					fullscreen = true;
					r3.setValue("Fullscreen: On");
				} else {
					fullscreen = false;
					r3.setValue("Fullscreen: Off");

				}
			} else if (o1.isSelected()) {
				MenuId = 3;
			} else if(o2.isSelected()) {
				MenuId = 2;
			} else if (o3.isSelected()) {
				if(d instanceof Menu) {
					MenuOpen = false;
				}
				MenuId = 0;
			} else if (k2.isSelected()) {
				MenuId = 1;
			} else if (k1.isSelected()) {
				if(KeyMapTable.getSelected() != null) {
					kp.setMessage("Select a key to bind to " + KeyMapTable.getSelected().getMessage().split(":")[0]);
					bindKey = true;
				}
			}

			if(b4.isSelected()) {
				Settings.MainWindow.alertPopup.setMessage("Connecting to server...");
				Settings.MainWindow.alertPopup.setOpen(true);
				new Thread() {
					public void run() {
						MultiplayerSettings.updateMultiplayerVariables();
						Settings.MainWindow.MenuId = 6;
						Settings.MainWindow.MenuOpen = true;
						Settings.MainWindow.alertPopup.setOpen(false);
					}
				}.start();
			}
		}

	}
	public boolean closeRequested() {
		Main.stopGame();
		c.exit();
		return true;
	}


	public String getTitle() {
		return null;
	}



	public void setDisplay(Display ds) {
		ds.init(c);
		this.d = ds;
		Window.MenuOpen = false;
	}

	public void setDisplayMessage(String s) {
		d.setMessage(s);
	}

	public static void populateKeyBindingList() {
		KeyMapTable.clearItems();
		
		addKeyMapValue("Attack", KeyBindings.Attack);
		addKeyMapValue("Open Wheel", KeyBindings.Open_Wheel);
		addKeyMapValue("Move Left", KeyBindings.Move_Left);
		addKeyMapValue("Move Right", KeyBindings.Move_Right);
		addKeyMapValue("Jump", KeyBindings.Jump);
		addKeyMapValue("Use", KeyBindings.Use);
		addKeyMapValue("Reload", KeyBindings.Reload);
		addKeyMapValue("Open Inventory", KeyBindings.Open_Inventory);
		addKeyMapValue("Open Player Statistics", KeyBindings.Open_Player_Statistics);
		addKeyMapValue("Open Team Information", KeyBindings.Open_Team_Information);
		addKeyMapValue("Open Enemy Information", KeyBindings.Open_Enemy_Information);
		addKeyMapValue("Open Chat", KeyBindings.Open_Chat);
		addKeyMapValue("Open All Chat", KeyBindings.Open_All_Chat);
		addKeyMapValue("Scroll Chat Up", KeyBindings.Scroll_Chat_Up);
		addKeyMapValue("Scroll Chat Down", KeyBindings.Scroll_Chat_Down);
		addKeyMapValue("Take Screenshot", KeyBindings.Take_Screenshot);
		addKeyMapValue("Open Console", KeyBindings.Open_Console);
		addKeyMapValue("Show Debug Information", KeyBindings.Show_Debug_Information);
		addKeyMapValue("Toggle Display", KeyBindings.Toggle_Display);
	}
	protected static void addKeyMapValue(String key, int[] def) {

		KeyMapTable.addItem(new KeyMapListItem(key, KeyBindings.getKeyName(def), KeyMapTable));
	}
	protected String capitalizeFirst(String s) {
		char[] chars = s.toLowerCase().toCharArray();
		String ret = "";
		for (int i = 0; i < chars.length; i++) {
			if(i == 0) {
				ret += Character.toUpperCase(chars[i]);
			} else {
				ret += Character.toLowerCase(chars[i]);
			}
		}
		return ret;
	}

	public static int[] convertIntegers(List<Integer> integers){
		int[] ret = new int[integers.size()];
		Iterator<Integer> iterator = integers.iterator();
		for (int i = 0; i < ret.length; i++) {
			ret[i] = iterator.next().intValue();
		}
		return ret;
	}




}
