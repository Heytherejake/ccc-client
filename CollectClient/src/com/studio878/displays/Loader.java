package com.studio878.displays;

import org.newdawn.slick.Color;
import org.newdawn.slick.Font;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.gui.TextField;

import com.studio878.app.Main;
import com.studio878.block.BlockType;
import com.studio878.ui.Fonts;
import com.studio878.util.Settings;
import com.studio878.util.Sheets;
import com.studio878.util.SoundEffects;
import com.studio878.util.TitleMessages;

public class Loader extends Display{
	TextField t;
	TextField ct;
	int messageOffset = 0;
	Thread updateThread;
	Font f;
	Thread td;
	boolean isDone = false;
	public void init(GameContainer c) {
		try {
			f = new UnicodeFont("fonts/Telex.ttf",32, false, false);
			Sheets.createSheets();

			Fonts.loadFonts();

			SoundEffects.setupSounds();
			
			BlockType.populateDefaultBlocks();

			isDone = true;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void draw(GameContainer c, Graphics g) {
		//Backgrounds
		g.setColor(Color.gray);
		g.fillRect(0, 0, c.getWidth(), c.getHeight());
		g.setFont(f);
		g.setColor(Color.black);
		g.drawString("Loading game", (c.getWidth()-f.getWidth("Loading game"))/2, (c.getHeight() - f.getHeight("Loading game")) / 2);


	}

	public void update(GameContainer c, int arg1) {
		if(isDone) {
			Settings.MainWindow.setDisplay(new Menu(0));

		}

	}

}