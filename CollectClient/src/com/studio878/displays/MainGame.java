package com.studio878.displays;

import java.awt.Point;
import java.awt.Robot;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.xml.ws.handler.MessageContext.Scope;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.ShapeFill;
import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.fills.GradientFill;
import org.newdawn.slick.geom.Rectangle;

import com.studio878.abilities.Ability;
import com.studio878.app.Window;
import com.studio878.block.Block;
import com.studio878.block.BlockType;
import com.studio878.characters.PlayerCharacter;
import com.studio878.crafting.BonusItem;
import com.studio878.crafting.BonusItem.BonusItemType;
import com.studio878.crafting.CraftingRecipe;
import com.studio878.crafting.CraftingRecipeList;
import com.studio878.crafting.CraftingRecipeList.RecipeType;
import com.studio878.input.KeyBindings;
import com.studio878.inventory.Inventory;
import com.studio878.inventory.Item;
import com.studio878.inventory.ItemID;
import com.studio878.io.NetObjects;
import com.studio878.objects.ClientSideEntity;
import com.studio878.objects.Entity;
import com.studio878.objects.EntityManager;
import com.studio878.objects.Generator;
import com.studio878.objects.PhysicsAffectedEntity;
import com.studio878.objects.ResearchBench;
import com.studio878.objects.UsableEntity;
import com.studio878.objects.Workbench;
import com.studio878.packets.Packet07SendChat;
import com.studio878.packets.Packet12SendMovement;
import com.studio878.packets.Packet17ThrowProjectile;
import com.studio878.packets.Packet20RequestInventory;
import com.studio878.packets.Packet22RelocateItem;
import com.studio878.packets.Packet24SendArmRotation;
import com.studio878.packets.Packet31RequestReload;
import com.studio878.packets.Packet35SendCraftingRequest;
import com.studio878.packets.Packet52PlaceBlock;
import com.studio878.packets.Packet57RequestPlayerBalance;
import com.studio878.packets.Packet62SendNextEventRequest;
import com.studio878.packets.Packet63SendEmoteRequest;
import com.studio878.packets.Packet70SendSellRequest;
import com.studio878.packets.Packet72SendPurchaseItemRequest;
import com.studio878.remoteui.RemoteUIManager;
import com.studio878.story.MapEventManager;
import com.studio878.story.TextMapEvent;
import com.studio878.ui.Button;
import com.studio878.ui.CharacterSelectListItem;
import com.studio878.ui.Chat;
import com.studio878.ui.ChatColors;
import com.studio878.ui.CriticalMessage;
import com.studio878.ui.Fonts;
import com.studio878.ui.InventoryUI;
import com.studio878.ui.MessageObject;
import com.studio878.ui.ModifiedTextField;
import com.studio878.ui.Popup;
import com.studio878.ui.ResearchListItem;
import com.studio878.ui.SellListItem;
import com.studio878.ui.ShopListItem;
import com.studio878.ui.SizableButton;
import com.studio878.ui.WireModeButton;
import com.studio878.util.Console;
import com.studio878.util.Damage;
import com.studio878.util.Emote.EmoteType;
import com.studio878.util.GraphicsUtil;
import com.studio878.util.Images;
import com.studio878.util.Movement.MovementType;
import com.studio878.util.Settings;
import com.studio878.util.Sheets;
import com.studio878.util.SoundEffects;
import com.studio878.util.WiringUtil;
import com.studio878.util.WiringUtil.WiringElement;
import com.studio878.weapons.EmptyHand;
import com.studio878.wheel.CancelSelectionItem;
import com.studio878.wheel.NextWeaponItem;
import com.studio878.wheel.OptionsWheel;
import com.studio878.wheel.PreviousWeaponItem;
import com.studio878.wheel.ThrowGrenadeItem;
import com.studio878.wheel.WheelItem;
import com.studio878.world.Camera;
import com.studio878.world.Grid;

public class MainGame extends Display{

	String msg = "";
	ModifiedTextField t;
	ModifiedTextField ct;
	int messageOffset = 0;
	Thread updateThread;
	int overflow;
	OptionsWheel wheel;

	boolean wheelOpen = false;
	boolean isFiring = false;

	boolean canHitInventory = false;
	boolean isFolderOpen = false;
	boolean isGameOver = false;

	public static int openCraftingMenu = 0;
	public static boolean classMenuOpen = false;
	public boolean firstClasMenuOpen = false;

	Item draggingItem;

	WheelItem closest;
	public static int cursorMode = 0;
	public static ItemID setProjectile;
	DecimalFormat fmt = new DecimalFormat("0.00");
	Robot bot;

	public static Popup craftingMsg;
	Button closeCraftingMsg;
	Button craftButton;
	Button closeCrafting;
	SizableButton showMaterials;
	SizableButton showWeapons;
	SizableButton showAll;
	SizableButton showSell;

	Button cancelClass;
	Button selectClass;

	public static boolean isSelling = false;

	WireModeButton wirePlaceButton = new WireModeButton(200, 35, 1);
	WireModeButton latchPlaceButton = new WireModeButton(200 + 16, 35, 2);
	WireModeButton inputPlaceButton = new WireModeButton(200 + 32, 35, 4);
	WireModeButton turretPlaceButton = new WireModeButton(200 + 48, 35, 3);
	WireModeButton closeWireButton = new WireModeButton(200 + 48, 35, 5);

	public static int WirePlacementType = 0;


	public static RecipeType openCraftingType;

	long timeSinceChatClosed = 0;

	long timeSinceTrayOpened = 0;

	public static boolean isPlacing = false;

	public static boolean craftingMsgVisible = false;

	public static boolean VoiceMenuOpen = false;

	public static boolean wireOverlayOpen = false;

	public static ConcurrentHashMap<Point, Integer> wireMap = new ConcurrentHashMap<Point, Integer>();

	public static WiringElement selectedWire = null;

	public static Point previousWire;

	long timeSinceLastMovementCalculation = 0;

	int cx = 0, cy = 0;
	double backgroundY = 0;

	public static CraftingRecipe activeRecipe;
	public static ItemID activeRecipeItem;
	public static ItemID activeSellItem;

	public static PlayerCharacter activeCharacterSelect;
	int characterSelectScrollPosition = 0, characterSelectTotalHeight = 0;
	public static ArrayList<String> loreLines;

	public static final int DefaultCursorMode = 0;
	public static final int ProjectileCursorMode = 1;
	public static final int InventoryCursorMode = 2;
	public static final int DraggingInventoryCursorMode = 3;
	public static final int WirePlacementMode = 4;
	
	long loadTime = System.currentTimeMillis();

	public void init(GameContainer c) {
		canOpenMenu = true;
		Settings.ShowLight = true;
		isPlacing = false;
		cursorMode = 0;
		openCraftingMenu = 0;
		openCraftingType = null;
		Console.write("Generating console objects.");
		t = new ModifiedTextField(c, Fonts.DefaultFont, 0, 285, c.getWidth(), 15, 0, "", false, false);
		t.setForegroundColor(Color.white);
		t.setBackgroundColor(new Color(0, 0, 0, 0.5f));
		t.setLimit(1024);
		Console.write("Generating chat objects.");
		ct = new ModifiedTextField(c, Fonts.ChatFont, 10, c.getHeight() - 60, 500, 22, 0, "", false, false);
		ct.setForegroundColor(Color.white);
		ct.setBackgroundColor(new Color(0, 0, 0, 0.5f));
		ct.setLimit(256);
		wheel = new OptionsWheel(this);
		wheel.addItem(new PreviousWeaponItem());
		wheel.addItem(new NextWeaponItem());
		wheel.addItem(new ThrowGrenadeItem());
		wheel.addItem(new CancelSelectionItem());
		NetObjects.Sender.sendPacket(new Packet20RequestInventory());

		craftingMsg = new Popup("Crafting Message", - 200, c.getHeight()/2 - 125, 400, 250, Fonts.LargeFont, true);
		closeCraftingMsg = new Button(-100, c.getHeight()/2 + 50, "Close", true);
		closeCrafting = new Button((int) (c.getWidth()*.75), c.getHeight()/2 + 100, "Close", false);
		craftButton = new Button((int) (c.getWidth()*.75), c.getHeight()/2, "Craft", false);
		showMaterials = new SizableButton((int) (c.getWidth()*.75), c.getHeight()/2 + 150, 100, 50, "Materials", false);
		showWeapons = new SizableButton((int) (c.getWidth()*.75) + 110, c.getHeight()/2 + 150, 100, 50, "Weapons", false);
		showAll = new SizableButton((int) (c.getWidth()*.75) + 110, c.getHeight()/2 + 150, 100, 50, "All", false);
		showSell = new SizableButton((int) (c.getWidth()*.75) + 110, c.getHeight()/2 + 150, 100, 50, "Sell...", false);
		cancelClass = new Button(100, 100, "Close", false);
		selectClass = new Button(100, 100, "Select", false);

		populateCraftingList();


		MapEventManager.eventQueue.clear();





	}


	public void draw(GameContainer c, Graphics g) {
		//Backgrounds
		g.setColor(new Color(0, 204, 255));
		g.fillRect(0, 0, c.getWidth(), c.getHeight());


		backgroundY = Camera.Y_OFFSET > 0 ? 0 : 1*Camera.Y_OFFSET/50;

		g.drawImage(Images.backgroundImage,0, (float) backgroundY);
		Camera.reposition();
		g.setColor(Color.black);
		//All translatable things (world essentially) go here
		g.translate(Math.round(Camera.X_OFFSET), Math.round(Camera.Y_OFFSET));
		Grid.drawGrid(c, g);
		if(Math.abs(Camera.X_OFFSET) + c.getWidth() >= Settings.WorldWidth*16) {
			for(int i = (int) Math.abs((Camera.X_OFFSET - Camera.X_OFFSET%16)) + c.getWidth(); i >= Settings.WorldWidth*16; i -= 16) {
				for(int j = (int) Math.abs(Camera.Y_OFFSET - Camera.Y_OFFSET%16); j <= (int) Math.abs(Camera.Y_OFFSET - (16 - Camera.Y_OFFSET%16)) + c.getHeight(); j+= 16) {
					Block bs = Grid.getClosestEndRowBlock(16, j);
					if(bs != null && bs.getBackgroundType() != BlockType.lookupName("Air")) {
						int sl = bs.getLightLevel();
						int ll = 10 - sl;
						if(ll < 0) {
							ll = 0;
						}

						if(ll != 10) {
							g.drawImage(bs.getBackgroundImage(), i, j);
						}
						g.setColor(new Color(0, 0, 0, ll/10.0f));
						g.fillRect(i, j, 16, 16);
					}
				}
			}
		} else if (Camera.X_OFFSET > 0) {

			for(int i = -1* (int) Math.abs((Camera.X_OFFSET + (16 - Camera.X_OFFSET%16))); i < 0; i += 16) {
				for(int j = (int) Math.abs(Camera.Y_OFFSET - Camera.Y_OFFSET%16); j <= (int) Math.abs(Camera.Y_OFFSET - (16 - Camera.Y_OFFSET%16)) + c.getHeight(); j+= 16) {
					Block bs = Grid.getClosestEndRowBlock(0, j);
					if(bs != null && bs.getBackgroundType() != BlockType.lookupName("Air")) {
						int sl = bs.getLightLevel();
						int ll = 10 - sl;
						if(ll < 0) {
							ll = 0;
						}

						if(ll != 10) {
							g.drawImage(bs.getBackgroundImage(), i, j);
						}
						g.setColor(new Color(0, 0, 0, ll/10.0f));
						g.fillRect(i, j, 16, 16);
					}
				}
			}
		}
		if(Math.abs(Camera.Y_OFFSET) + c.getHeight() >= Settings.WorldHeight*16) {
			g.setColor(Color.black);
			g.fillRect(-1*Camera.X_OFFSET, Settings.WorldHeight*16, c.getWidth(), Math.abs(Camera.Y_OFFSET) + c.getHeight() - Settings.WorldHeight*16);
		}

		Camera.reposition();


		ArrayList<Entity> e = EntityManager.getEntities();
		for(Entity es : e) {
			if(es instanceof Generator) {
				es.draw(g);
			}
		}
		for(int i = 0; i < e.size(); i++) {
			if(e.get(i) != Settings.ActivePlayer && !(e.get(i) instanceof Generator)) {
				try {
					e.get(i).draw(g);
				} catch (Exception ex) {

				}
			}

		}

		Settings.ActivePlayer.draw(g);
		if(Settings.ShowLight) {
			Grid.drawLightMap(c, g);
		}



		float mx = (float) Camera.convertXToCamera(Mouse.getX());
		float my = (float) Camera.convertYToCamera(c, Mouse.getY());
		Block bd = Grid.getClosestBlock((int) (mx - (mx%16)),(int)  (my - (my%16)));
		if(bd != null && (bd.getType() != BlockType.lookupName("Air") || isPlacing)) {
			g.setLineWidth(1.0f);
			if(bd.getLightLevel() <= 5) {
				g.setColor(new Color(255, 255, 255, 0.6f));
			} else {
				g.setColor(new Color(0, 0, 0, 0.6f));

			}
			g.drawRect(mx - (mx%16), my - (my%16), 16, 16);

		}


		//UI
		g.resetTransform();

		if(wheelOpen){
			wheel.animate();
			wheel.draw(g);
		}

		boolean charSelect = (Settings.ActivePlayer.getCharacter() == null);

		if(!charSelect) {
			if(Settings.ActivePlayer != null && (Settings.ActivePlayer.getX() < 0 || Settings.ActivePlayer.getX() > Settings.WorldWidth*16) && Settings.ActivePlayer.getHealth() > 0) {
				double amt = 1.0;
				if(Settings.ActivePlayer.getX() < 0) {
					amt = 0.95 * (Settings.ActivePlayer.getX() > -500 ? Settings.ActivePlayer.getX()/-500 : 1);
				} else {
					amt = 0.95 * (Settings.ActivePlayer.getX() < Settings.WorldWidth*16 + 500 ? (Settings.ActivePlayer.getX()-(double)(Settings.WorldWidth*16))/500 : 1);

				}
				g.setColor(new Color(255, 255, 255, (float) amt));
				g.fillRect(0, 0, c.getWidth(), c.getHeight());
				g.setColor(new Color(90, 0, 0));
				String str = "You are leaving the battlefield.";
				g.setFont(Fonts.ChapterFont);
				g.drawString(str, c.getWidth()/2 - Fonts.ChapterFont.getWidth(str)/2, c.getHeight()/2 - 100);
				str = "Return to the battle to avoid being killed.";
				g.setFont(Fonts.TitleFont);
				g.drawString(str, c.getWidth()/2 - Fonts.TitleFont.getWidth(str)/2, c.getHeight()/2 - 60);
			}
		}

		if(Settings.ActivePlayer.getWeapon().getType() == ItemID.lookupName("Sniper Rifle")) {
			g.setColor(new Color(255, 0, 0, 1.0f));
			int msx = c.getInput().getMouseX();
			int msy = c.getInput().getMouseY();
			int diameter = 10;
			g.setLineWidth(2.0f);
			g.setColor(new Color(90, 0, 0));
			g.drawOval((float) msx - diameter/2,(float)  msy - diameter/2,(float)  diameter, (float) diameter);
		}

		if(Settings.ShowHUD && isFolderOpen) {
			Settings.MainWindow.folder.draw(c, g);
		}


		if(cursorMode == 1) {
			double msx = c.getInput().getMouseX();
			double msy = c.getInput().getMouseY();
			g.drawImage(this.setProjectile.getAntiAliasedImage(),(int) (msx - 16), (int) (msy - 16));
		} else if (cursorMode == 3) {
			double msx = c.getInput().getMouseX();
			double msy = c.getInput().getMouseY();
			g.drawImage(draggingItem.getType().getAntiAliasedImage(),(int) (msx - 16), (int) (msy - 16));
			g.setColor(Color.black);
			g.setFont(Fonts.SmallFont);
			g.drawString(""+draggingItem.getAmount(), (float) msx - Fonts.SmallFont.getWidth("" + draggingItem.getAmount()) + 18, (float) msy + 6);
		}

		if(Keyboard.isKeyDown(Keyboard.KEY_0)) {
			Settings.ActivePlayer.setYSpeed(-20);
		}


		if(charSelect) {
			int shopWidth = c.getWidth() - c.getWidth()%100 - 200;
			int shopHeight = 600;

			Image tl = Images.ShopBackground.getSubImage(0, 0, 11, 11);
			Image top = Images.ShopBackground.getSubImage(11, 0, 100, 11);
			Image tr = Images.ShopBackground.getSubImage(111, 0, 11, 11);
			Image right = Images.ShopBackground.getSubImage(111, 11, 11, 100);
			Image br = Images.ShopBackground.getSubImage(111, 111, 11, 11);
			Image bot = Images.ShopBackground.getSubImage(11, 111, 100, 11);
			Image bl = Images.ShopBackground.getSubImage(0, 111, 11, 11);
			Image left = Images.ShopBackground.getSubImage(0, 11, 11, 100);
			Image mid = Images.ShopBackground.getSubImage(11, 11, 100, 100);

			int srt = (int)((c.getWidth() - shopWidth)/2) - 11;
			int srl = (int)((c.getHeight() - shopHeight)/2) - 11;

			for(int i = 0; i < 400/100; i++) {
				for(int j = 0; j < shopHeight/100; j++) {
					g.drawImage(mid, srt + i*100 + 11, srl + j*100 + 11);
				}
			}

			for(float i = 4.21f; i < shopWidth/100; i++) {
				for(int j = 0; j < shopHeight/100; j++) {
					if(shopWidth/100 - i < 1) {
						g.drawImage(mid.getSubImage(0, 0, (int) (100*(shopWidth/100 - i)), 100), srt + i*100 + 11, srl + j*100 + 11);

					} else {
						g.drawImage(mid, srt + i*100 + 11, srl + j*100 + 11);
					}
				}
			}

			g.drawImage(tl, srt, srl);
			g.drawImage(tl, srt + 421, srl);

			g.drawImage(tr, srt + 401, srl);
			g.drawImage(tr, srt + shopWidth + 11, srl);
			g.drawImage(br, srt + shopWidth + 11, srl + shopHeight + 11);
			g.drawImage(br, srt + 401, srl + shopHeight + 11);
			g.drawImage(bl, srt + 421, srl + shopHeight + 11);
			g.drawImage(bl, srt, srl + shopHeight + 11);

			g.drawImage(top, srt + 11, srl);
			g.drawImage(top, srt + 111, srl);
			g.drawImage(top, srt + 211, srl);
			g.drawImage(top.getSubImage(0, 0, 90, 11), srt + 311, srl);

			g.drawImage(bot, srt + 11, srl + shopHeight + 11);
			g.drawImage(bot, srt + 111, srl + shopHeight + 11);
			g.drawImage(bot, srt + 211, srl + shopHeight + 11);
			g.drawImage(bot.getSubImage(0, 0, 90, 11), srt + 311, srl + shopHeight + 11);

			for(float i = 4.21f; i < shopWidth/100; i++) {
				if(shopWidth/100 - i < 1) {
					g.drawImage(top.getSubImage(0, 0, (int) (100*(shopWidth/100 - i)), 11), srt + i*100 + 11, srl);
					g.drawImage(bot.getSubImage(0, 0, (int) (100*(shopWidth/100 - i)), 11), srt + i*100 + 11, srl + shopHeight + 11);

				} else {
					g.drawImage(top, srt + i*100 + 11, srl);
					g.drawImage(bot, srt + i*100 + 11, srl + shopHeight + 11);

				}
			}

			for(int i = 0; i < shopHeight/100; i++) {
				g.drawImage(right, srt + shopWidth + 11, srl + 100*i + 11);
				g.drawImage(left, srt, srl + 100*i + 11);
				g.drawImage(right, srt + 401, srl + 100*i + 11);
				g.drawImage(left, srt + 421, srl + 100*i + 11);
			} 

			g.setFont(Fonts.ChapterFont);
			g.setColor(Color.white);
			int i = 30;
			int lst = srt + 210;
			int mst = srt + ((shopWidth - 221 + 22)/2) + 321;

			GraphicsUtil.drawWithShadow(g, "Character Selection", lst - Fonts.ChapterFont.getWidth("Character Selection")/2, srl + i , Color.white, Color.black);

			Settings.MainWindow.characterSelect.draw(g);

			if(activeCharacterSelect != null) {
				Rectangle oldClip = g.getWorldClip();
				g.setClip(srt + 426, srl + 5, shopWidth-421, shopHeight-10);
				i = 30 - characterSelectScrollPosition;
				GraphicsUtil.drawWithShadow(g, activeCharacterSelect.getFullName(), mst - Fonts.ChapterFont.getWidth(activeCharacterSelect.getFullName())/2, srl + i, Color.white, Color.black);
				i += 40;
				
				g.setFont(Fonts.ChatFont);
				String tags = "";
				for(String sp : activeCharacterSelect.getTags()) {
					tags += sp + ", ";
				}
				tags = tags.substring(0, tags.length() - 2);
				
				GraphicsUtil.drawWithShadow(g, tags, mst - Fonts.ChatFont.getWidth(tags)/2, srl + i, Color.white, Color.black);
				i += 40;

				g.setFont(Fonts.LargeFont);
				String s = "Stats";
				GraphicsUtil.drawWithShadow(g, s, mst - Fonts.LargeFont.getWidth(s)/2, srl + i, Color.white, Color.black);
				i += 40;

				int si = i;

				g.setFont(Fonts.ChatFont);
				s = "Ranged Damage: " + activeCharacterSelect.getRangedDamage();
				GraphicsUtil.drawWithShadow(g, s, mst - Fonts.ChatFont.getWidth(s)/2 - 150, srl + i, Color.white, Color.black);

				i += 30;
				s = "Melee Damage: " + activeCharacterSelect.getMeleeDamage();
				GraphicsUtil.drawWithShadow(g, s, mst - Fonts.ChatFont.getWidth(s)/2 - 150, srl + i, Color.white, Color.black);

				i += 30;
				s = "Movement Speed: " + activeCharacterSelect.getMoveSpeed();
				GraphicsUtil.drawWithShadow(g, s, mst - Fonts.ChatFont.getWidth(s)/2 - 150, srl + i, Color.white, Color.black);

				i = si;
				s = "Strength: " + activeCharacterSelect.getStrength();
				GraphicsUtil.drawWithShadow(g, s, mst - Fonts.ChatFont.getWidth(s)/2 + 150, srl + i, Color.white, Color.black);

				i += 30;
				s = "Dexterity: " + activeCharacterSelect.getDexterity();
				GraphicsUtil.drawWithShadow(g, s, mst - Fonts.ChatFont.getWidth(s)/2 + 150, srl + i, Color.white, Color.black);

				i += 30;
				s = "Intelligence: " + activeCharacterSelect.getIntelligence();
				GraphicsUtil.drawWithShadow(g, s, mst - Fonts.ChatFont.getWidth(s)/2 + 150, srl + i, Color.white, Color.black);

				i += 60;
				g.setFont(Fonts.LargeFont);
				s = "Abilities";
				GraphicsUtil.drawWithShadow(g, s, mst - Fonts.LargeFont.getWidth(s)/2, srl + i, Color.white, Color.black);

				i += 40;
				int pi = i;
				g.drawImage(activeCharacterSelect.getPassiveAbility().getImage(), mst - (75*5)/2 + 10 , srl + i);

				for(int j = 0; j < 4; j++) {
					Ability a = activeCharacterSelect.getAbility(j);
					g.drawImage(a.getImage(), mst - (75*5)/2 + (j + 1)*75 + 10, srl + i);
				}

				i += 100;
				g.setFont(Fonts.LargeFont);
				s = "Lore";
				GraphicsUtil.drawWithShadow(g, s, mst - Fonts.LargeFont.getWidth(s)/2, srl + i, Color.white, Color.black);

				i += 30;
				g.setFont(Fonts.SmallFont);
				for(String ss : loreLines) {
					g.drawString(ss, mst - Fonts.SmallFont.getWidth(ss)/2, srl + i);
					i += 20;
				}
				
				i += 30;

				characterSelectTotalHeight = i - shopHeight + characterSelectScrollPosition;

				g.setClip(oldClip);

				int msx = c.getInput().getMouseX();
				int msy = c.getInput().getMouseY();
				Ability a = null;
				if(msx >= mst - (75*5)/2 + 10 && msx <= mst - (75*5)/2 + 10 + 50 && msy >= srl + pi && msy <= srl + pi + 50) {
					a = activeCharacterSelect.getPassiveAbility();
				}

				for(int j = 1; j < 5; j++) {
					if(msx >= mst - (75*5)/2 + 10 + j*75 && msx <= mst - (75*5)/2 + 10 + 50 + j*75 && msy >= srl + pi && msy <= srl + pi + 50) {
						a = activeCharacterSelect.getAbility(j - 1);
					}
				}
				if(a != null) {
					ArrayList<String> lines = GraphicsUtil.splitBetweenLines(a.getDescription().replace("|", ""), Fonts.TitleFont, 360);
					int height = lines.size()*20 + 90;
					for(String sp : lines) {
						if(sp.indexOf("<br>") != -1) {
							height += 20;
						}
					}
					g.setColor(new Color(0, 0, 0, 0.8f));
					if(msx + 400 >= c.getWidth()) {
						msx -= 400;
					}
					g.fillRoundRect(msx, msy, 400, height, 6);

					g.setColor(new Color(255, 179, 0));
					g.setLineWidth(2.0f);
					g.drawRoundRect(msx, msy, 400, height, 6);
					g.setFont(Fonts.LargeFont);
					g.setColor(Color.white);
					i = 10;
					g.drawString(a.getName(), msx + 20, msy + i);
					i += 30;
					s = "";
					if(a.getManaCost(0) != -2) {
						for(int j = 0; j < 4; j++) {
							s += a.getManaCost(j) + "/";
						}
						s = s.substring(0, s.length() - 1);
						s += " Mana";
					} else {
						s = "(Passive)";
					}
					g.setFont(Fonts.BoldFont);
					g.setColor(new Color(255, 255, 222));
					g.drawString(s, msx + 20, msy + i);

					i += 30;
					g.setFont(Fonts.TitleFont);
					g.setColor(Color.white);
					for(String sps : lines) {
						if(sps.indexOf("<br>") != -1) {
							i += 20;
							sps = sps.replace("<br>", "");
						}
						g.drawString(sps, msx + 20, msy + i);
						i += 20;
					}
				}
			}

		}

		if(Settings.ShowHUD && !charSelect) {
			CopyOnWriteArrayList<CriticalMessage> messages = CriticalMessage.getMessages();
			for(CriticalMessage m : messages) {
				m.draw(g);
				m.move();
			}

			ArrayList<MessageObject> kills = Damage.getMessagesWithinTime(5);
			for(int i = 0; i < kills.size(); i++) {
				UnicodeFont f = Fonts.SmallFont;
				g.setFont(f);
				g.setColor(new Color(190, 190, 190));
				g.fillRoundRect(c.getWidth() - 20 - f.getWidth(kills.get(i).getMessage()), (i+1)*30, f.getWidth(kills.get(i).getMessage()) + 10, 20, 5);
				g.setColor(Color.black);
				g.drawString(kills.get(i).getMessage(), c.getWidth() - 15 - f.getWidth(kills.get(i).getMessage()), (i+1)*30 + 3);
			}



			long cst = System.currentTimeMillis() - Settings.ActivePlayer.getLastKilledTime();
			if(Settings.ShowHUD &&  cst <= Settings.ActivePlayer.getRespawnTime()*1000) {
				g.setColor(new Color(255, 255, 255, 0.6f));
				g.fillRoundRect(c.getWidth()/2 - 244, c.getHeight() - 67, 560, 50, 4);
				String rst = "Respawning in " + (int) Math.ceil(Settings.ActivePlayer.getRespawnTime() - (double)cst/(double)1000);
				g.setFont(Fonts.ChatFont);

				GraphicsUtil.drawWithShadow(g, rst, c.getWidth()/2 - Fonts.ChatFont.getWidth(rst)/2 + 30, c.getHeight() - 43, Color.black, Color.white);
				if(Settings.ActivePlayer.getLastKiller().equals(Settings.ActivePlayer.getName())) {
					rst = "You killed yourself";
				} else {
					rst = "You were killed by " + Settings.ActivePlayer.getLastKiller();
				}
				GraphicsUtil.drawWithShadow(g, rst, c.getWidth()/2 - Fonts.ChatFont.getWidth(rst)/2 + 30, c.getHeight() - 63, Color.black, Color.white);

			} else {
				if(Settings.ActivePlayer.getCharacter() != null) { 
					if(Settings.CameraPlayer != Settings.ActivePlayer) {
						Settings.CameraPlayer = Settings.ActivePlayer;
						Settings.ActivePlayer.setXSpeed(0);
						Settings.ActivePlayer.setYSpeed(0);
					}
				} else {
					Camera.X_OFFSET = Settings.WorldWidth*-8;
					Camera.Y_OFFSET = Settings.WorldHeight * -8 + 400;
				}
				int maxHealth = 100;
				if(Settings.ActivePlayer.getCharacter() != null) {
					maxHealth = Settings.ActivePlayer.getCharacter().getHealth();
				}
				g.setColor(new Color(255, 255, 255, 0.3f));
				g.fillRoundRect(c.getWidth()/2 - 250, c.getHeight() - 85, 500, 75, 4);


				double hp = (double) Settings.ActivePlayer.getHealth();
				if(hp == 0) {
					g.drawImage(Sheets.UI_BARS.getSubImage(0, 30, 2, 30), c.getWidth()/2 - 202, c.getHeight() - 50);
				} else {
					g.drawImage(Sheets.UI_BARS.getSubImage(0, 0, 2, 30), c.getWidth()/2 - 202, c.getHeight() - 50);
				}
				double pct = (double)Settings.ActivePlayer.getHealth()/(double) maxHealth;
				g.drawImage(Sheets.UI_BARS.getSubImage(2, 0, 1, 30).getScaledCopy((int) (pct*400), 30), c.getWidth()/2 - 200, c.getHeight() - 50);
				g.drawImage(Sheets.UI_BARS.getSubImage(2, 30, 2, 30).getScaledCopy((int) ((1-pct)*400), 30), c.getWidth()/2 - 200 + (int) (pct*400), c.getHeight() - 50);
				if(hp == maxHealth) {
					g.drawImage(Sheets.UI_BARS.getSubImage(4, 0, 2, 30), c.getWidth()/2 + 199, c.getHeight() - 50);
				} else {
					g.drawImage(Sheets.UI_BARS.getSubImage(4, 30, 2, 30), c.getWidth()/2 + 199, c.getHeight() - 50);

				}

				g.setColor(new Color(255, 255, 255, 0.7f));
				g.setFont(Fonts.ChatFont);
				int wdt = Fonts.ChatFont.getWidth(Settings.ActivePlayer.getHealth() + " / " +  maxHealth);
				g.drawString(Settings.ActivePlayer.getHealth() + " / " +  maxHealth, (c.getWidth() / 2) - wdt/2 , c.getHeight() - 45);
				g.setColor(Color.black);
				g.drawString(Settings.ActivePlayer.getHealth() + " / " +  maxHealth, (c.getWidth() / 2) - wdt/2 , c.getHeight() - 46);


				String str = "--";
				Item it = Inventory.getSelectedItem();
				if(it != null && it.isWeapon()) {

					double part = (double)it.getClip()/(double)it.getTotalSize();
					str = it.getClip() + "/" + it.getTotalAmmo();


					str = it.getClip() + "/" + it.getTotalAmmo();

					double oft = BonusItem.calculateBonus(BonusItemType.RateOfFire);
					if(Inventory.getSelectedItem() != null) {
						oft += Inventory.getSelectedItem().getRateOfFireModifier();
					}
					oft = 2 - oft;
					if(oft <= 0.1) {
						oft = 0.1;
					}

					int offset = 0;
					if(System.currentTimeMillis() - it.getLastReloadTime() <= it.getReloadTime()) {
						str = "Reloading...";
						offset = 20;
						part = ((double) (System.currentTimeMillis() - it.getLastReloadTime())/(double)it.getReloadTime());
					}

					if(it.getClip() == 0) {
						g.drawImage(Sheets.UI_BARS.getSubImage(6, 40, 2, 20), c.getWidth()/2 - 2, c.getHeight() - 72);
					} else {
						g.drawImage(Sheets.UI_BARS.getSubImage(6, 0 + offset, 2, 20), c.getWidth()/2 - 2, c.getHeight() - 72);
					}
					g.drawImage(Sheets.UI_BARS.getSubImage(8, 0 + offset, 1, 20).getScaledCopy((int) (part*200), 20), c.getWidth()/2, c.getHeight() - 72);
					g.drawImage(Sheets.UI_BARS.getSubImage(8, 40, 2, 20).getScaledCopy((int) ((1-part)*200), 20), c.getWidth()/2 + (int) (part*200), c.getHeight() - 72);
					if(it.getClip() == it.getTotalSize()) {
						g.drawImage(Sheets.UI_BARS.getSubImage(10, 0 + offset, 2, 20), c.getWidth()/2 + 199, c.getHeight() - 72);
					} else {
						g.drawImage(Sheets.UI_BARS.getSubImage(10, 40, 2, 20), c.getWidth()/2 + 199, c.getHeight() - 72);

					}

					if(System.currentTimeMillis() - Settings.ActivePlayer.getLastFireTime() < Settings.ActivePlayer.getWeapon().getTimeOffset()*oft && it.getClip() > 0) {
						g.setColor(new Color(0, 0, 0, 0.3f));
						Image is = Sheets.UI_BARS.getSubImage(10, 0, 2, 20);
						is.setColor(0, 255, 255, 255, 0.1f);
						g.drawImage(is, c.getWidth()/2 + 199, c.getHeight() - 72);
						g.setColor(new Color(0, 0, 0, 0.3f));
						g.fillRect(c.getWidth()/2 + 200, c.getHeight() - 72, -200 + (float) (200*(double)((System.currentTimeMillis() - Settings.ActivePlayer.getLastFireTime())/(double)Settings.ActivePlayer.getWeapon().getTimeOffset()*oft)), 20);
					}

				} else if( it != null && it.getType().isBlock()) {
					str = ""+it.getAmount();
					g.drawImage(Sheets.UI_BARS.getSubImage(6, 40, 2, 20), c.getWidth()/2 - 2, c.getHeight() - 72);
					g.drawImage(Sheets.UI_BARS.getSubImage(8, 40, 2, 20).getScaledCopy((int) (200), 20), c.getWidth()/2, c.getHeight() - 72);
					g.drawImage(Sheets.UI_BARS.getSubImage(10, 40, 2, 20), c.getWidth()/2 + 199, c.getHeight() - 72);
				} else {
					g.drawImage(Sheets.UI_BARS.getSubImage(6, 40, 2, 20), c.getWidth()/2 - 2, c.getHeight() - 72);
					g.drawImage(Sheets.UI_BARS.getSubImage(8, 40, 2, 20).getScaledCopy((int) (200), 20), c.getWidth()/2, c.getHeight() - 72);
					g.drawImage(Sheets.UI_BARS.getSubImage(10, 40, 2, 20), c.getWidth()/2 + 199, c.getHeight() - 72);
				}
				g.setFont(Fonts.TitleFont);
				g.setColor(new Color(255, 255, 255, 0.7f));
				g.drawString(str, c.getWidth() / 2 - Fonts.TitleFont.getWidth(str)/2 + 100, c.getHeight() - 72);
				g.setColor(Color.black);
				g.drawString(str, c.getWidth() / 2 - Fonts.TitleFont.getWidth(str)/2 + 100, c.getHeight() - 73);
			} 
		}
		g.resetLineWidth();
		if(Settings.StoryMode) {
			Settings.ShowVersion = false;
		}
		if(!Console.ShowConsole && Settings.ShowHUD) {
			if(Settings.DebugMode & Settings.ShowHUD) {
				Block b = Grid.getClosestBlock((int) Camera.convertXToCamera(Mouse.getX()), (int) Camera.convertYToCamera(c,Mouse.getY()));
				String bt = "None";
				String bgt = "None";
				String bx = "-";
				String by = "-";
				String bc = "-";
				if(b != null) {
					bt = b.getType().getName();
					bgt = b.getBackgroundType().getName();
					bx = ""+b.getX();
					by = ""+b.getY();
					bc = ""+ b.getCondition();
				}
				g.setColor(new Color(255, 255, 255, 0.5f));

				g.fillRect(0, 0, c.getWidth(), 90);
				Fonts.drawWithColor(ChatColors.Black + "Collect, Construct, Conquer! Development Build " + Settings.GameVersion, Fonts.TitleFont, g, 5, 3, c.getWidth());
				Fonts.drawWithColor(ChatColors.Black + "bX: " + fmt.format(Camera.X_OFFSET) + " | bY: " + fmt.format(Camera.Y_OFFSET), Fonts.TitleFont, g, 5, 19, c.getWidth());
				Fonts.drawWithColor(ChatColors.Black + "pX:  " + fmt.format(Settings.ActivePlayer.getX()) + " | pY: " + fmt.format(Settings.ActivePlayer.getY()), Fonts.TitleFont, g, 5, 35, c.getWidth());
				Fonts.drawWithColor(ChatColors.Black + "mX: " + fmt.format(Mouse.getX()) +  " (" + fmt.format(Camera.convertXToCamera(Mouse.getX())) + ") | mY: " + fmt.format(Mouse.getY())+ " (" + fmt.format(Camera.convertYToCamera(c, Mouse.getY())) + ")", Fonts.TitleFont, g, 5, 51, c.getWidth());
				Fonts.drawWithColor(ChatColors.Black +  "B:    " + bt + " (" + bx + "," + by + " | " + bc + ") (" + bgt + ")", Fonts.TitleFont, g, 5, 67, c.getWidth());
				double used = Runtime.getRuntime().totalMemory()/1024/1024;
				double total = Runtime.getRuntime().maxMemory()/1024/1024;
				int percent = (int) (100*(used/total));
				GraphicsUtil.drawRightJustifiedString(g, c, "Memory Usage: " + ((int) used) + " MB (" + percent + "%)", 19);
				GraphicsUtil.drawRightJustifiedString(g, c, "Free Memory: " + ((int) (total-used)) + " MB", 35);
			} else if(Settings.ShowVersion) {

				Fonts.drawWithColor(ChatColors.Black + "Collect, Construct, Conquer! Development Build " + Settings.GameVersion, Fonts.TitleFont,  g, 5, 3, c.getWidth());
			}
			if(Settings.ShowFPS & Settings.ShowHUD) {
				if(!Settings.DebugMode) {
					g.setColor(new Color(255, 255, 255, 0.5f));
					g.fillRect(c.getWidth() -80, 0, 80, 25);
				}
				Fonts.drawWithColor(ChatColors.Black + "FPS:" + c.getFPS(), Fonts.TitleFont,  g, c.getWidth() - 70, 3, c.getWidth());
			}
			if((!Settings.StoryMode || EntityManager.getPlayers().size() > 1)) {
				if(Chat.ShowChat& Settings.ShowHUD) {
					g.setColor(new Color(0,0,0, .5f));
					g.fillRect(10, c.getHeight() - 360, 500, 300);
					CopyOnWriteArrayList<MessageObject> messages = Chat.getMessages();
					int offset = 0;
					int maxMessages = 15;

					if(messages.size() > maxMessages) {
						offset += messages.size() - maxMessages;
					} else {
						maxMessages = messages.size();
					}
					if(offset - messageOffset < 0) {
						messageOffset = offset;
					}
					for(int i = offset - messageOffset; i < maxMessages + offset - messageOffset; i++) {

						Fonts.drawWithColor(messages.get(i).getMessage(), Fonts.ChatFont, g, 12, (c.getHeight() - 360) + (i-offset + messageOffset)*19, c.getWidth());

					}
					g.setColor(Color.black);
					ct.setFocus(true);
					ct.draw(c, g);
				} else if(Settings.ShowHUD) {
					g.setColor(new Color(0,0,0, .5f));

					ArrayList<MessageObject> messages = Chat.getMessagesWithinTime(10);
					Collections.reverse(messages);
					g.fillRect(10, c.getHeight() - 100, 500, 20*(messages.size()));

					int offset = 0;
					int maxMessages = 15;
					if(messages.size() > maxMessages) {
						offset += messages.size() - maxMessages;
					}
					if(offset - messageOffset < 0) {
						messageOffset = offset;
					}
					for(int i = messages.size() - 1 - messageOffset; i >= offset - messageOffset; i--) {
						if(Fonts.ChatFont.getWidth(messages.get(i).getMessage()) < c.getWidth() / 2) {
							Fonts.drawWithColor(messages.get(i).getMessage(), Fonts.ChatFont, g, 12, (c.getHeight() - 100) + (i-offset + messageOffset)*19, c.getWidth());
						} else {
							ArrayList<String> tmps = splitBetweenLines(messages.get(i).getMessage(), Fonts.ChatFont, 500);

							Fonts.drawWithColor(tmps.get(0) + "...", Fonts.ChatFont, g, 12, (c.getHeight() - 100) + (i-offset + messageOffset)*19, c.getWidth());

						}
					}
				}
			}
		}



		if(Settings.ShowHUD && craftingMsgVisible) {
			craftingMsg.draw(g, c);
			closeCraftingMsg.draw(g, c);
		}

		if(Settings.ShowHUD && openCraftingMenu == 1) {
			/*	g.drawImage(Images.FolderTab, c.getWidth()/2 - Images.FolderBackground.getWidth()/2 + 50, c.getHeight()/2 - Images.FolderBackground.getHeight()/2 - 30);
			g.setFont(Fonts.TypewriterFont);
			g.setColor(Color.white);
			g.drawString("Crafting",  c.getWidth()/2 - Images.FolderBackground.getWidth()/2 + 105, c.getHeight()/2 - Images.FolderBackground.getHeight()/2 - 20);
			g.drawImage(Images.FolderBackground, c.getWidth()/2 - Images.FolderBackground.getWidth()/2, c.getHeight()/2 - Images.FolderBackground.getHeight()/2); */

			int shopWidth = c.getWidth() - c.getWidth()%100 - 200;
			int shopHeight = 600;

			Image tl = Images.ShopBackground.getSubImage(0, 0, 11, 11);
			Image top = Images.ShopBackground.getSubImage(11, 0, 100, 11);
			Image tr = Images.ShopBackground.getSubImage(111, 0, 11, 11);
			Image right = Images.ShopBackground.getSubImage(111, 11, 11, 100);
			Image br = Images.ShopBackground.getSubImage(111, 111, 11, 11);
			Image bot = Images.ShopBackground.getSubImage(11, 111, 100, 11);
			Image bl = Images.ShopBackground.getSubImage(0, 111, 11, 11);
			Image left = Images.ShopBackground.getSubImage(0, 11, 11, 100);
			Image mid = Images.ShopBackground.getSubImage(11, 11, 100, 100);

			int srt = (int)((c.getWidth() - shopWidth)/2) - 11;
			int srl = (int)((c.getHeight() - shopHeight)/2) - 11;

			for(int i = 0; i < 300/100; i++) {
				for(int j = 0; j < shopHeight/100; j++) {
					g.drawImage(mid, srt + i*100 + 11, srl + j*100 + 11);
				}
			}

			for(float i = 3.21f; i < shopWidth/100; i++) {
				for(int j = 0; j < shopHeight/100; j++) {
					if(shopWidth/100 - i < 1) {
						g.drawImage(mid.getSubImage(0, 0, (int) (100*(shopWidth/100 - i)), 100), srt + i*100 + 11, srl + j*100 + 11);

					} else {
						g.drawImage(mid, srt + i*100 + 11, srl + j*100 + 11);
					}
				}
			}

			g.drawImage(tl, srt, srl);
			g.drawImage(tl, srt + 321, srl);

			g.drawImage(tr, srt + 301, srl);
			g.drawImage(tr, srt + shopWidth + 11, srl);
			g.drawImage(br, srt + shopWidth + 11, srl + shopHeight + 11);
			g.drawImage(br, srt + 301, srl + shopHeight + 11);
			g.drawImage(bl, srt + 321, srl + shopHeight + 11);
			g.drawImage(bl, srt, srl + shopHeight + 11);

			g.drawImage(top, srt + 11, srl);
			g.drawImage(top, srt + 111, srl);
			g.drawImage(top.getSubImage(0, 0, 90, 11), srt + 211, srl);
			g.drawImage(bot, srt + 11, srl + shopHeight + 11);
			g.drawImage(bot, srt + 111, srl + shopHeight + 11);
			g.drawImage(bot.getSubImage(0, 0, 90, 11), srt + 211, srl + shopHeight + 11);

			for(float i = 3.21f; i < shopWidth/100; i++) {
				if(shopWidth/100 - i < 1) {
					g.drawImage(top.getSubImage(0, 0, (int) (100*(shopWidth/100 - i)), 11), srt + i*100 + 11, srl);
					g.drawImage(bot.getSubImage(0, 0, (int) (100*(shopWidth/100 - i)), 11), srt + i*100 + 11, srl + shopHeight + 11);

				} else {
					g.drawImage(top, srt + i*100 + 11, srl);
					g.drawImage(bot, srt + i*100 + 11, srl + shopHeight + 11);

				}
			}

			for(int i = 0; i < shopHeight/100; i++) {
				g.drawImage(right, srt + shopWidth + 11, srl + 100*i + 11);
				g.drawImage(left, srt, srl + 100*i + 11);
				g.drawImage(right, srt + 301, srl + 100*i + 11);
				g.drawImage(left, srt + 321, srl + 100*i + 11);
			} 



			if(activeRecipe != null) {
				g.setFont(Fonts.ChapterFont);
				g.setColor(Color.white);
				int i = 30;
				int mst = srt + ((shopWidth - 321 + 22)/2) + 321;
				GraphicsUtil.drawWithShadow(g, activeRecipe.getName(), mst - Fonts.ChapterFont.getWidth(activeRecipe.getName())/2, srl + i , Color.white, Color.black);

				g.setFont(Fonts.TitleFont);
				ArrayList<String> lines = GraphicsUtil.splitBetweenLines(activeRecipe.getDescription(), Fonts.TitleFont, shopWidth - 450);
				i += 40;
				for(String s : lines) {
					GraphicsUtil.drawWithShadow(g, s, mst - Fonts.TitleFont.getWidth(s)/2, srl  + i, Color.white, Color.black);
					i+= 20;
				}

				i += 20;
				int si = i;

				for(String s : activeRecipe.getUpsides()) {
					lines = splitBetweenLines(s, Fonts.TitleFont, (shopWidth - 450 - 50)/2);
					for(String sl : lines) {
						GraphicsUtil.drawWithShadow(g, sl, mst - (shopWidth - 500)/4 - Fonts.TitleFont.getWidth(sl)/2, srl  + i, new Color(0, 255, 0), Color.black);
						i+= 20;
					}
					i += 10;
				}
				int ti = i;
				i = si;
				if(activeRecipe.getDownsides() != null) {
					for(String s : activeRecipe.getDownsides()) {
						lines = splitBetweenLines(s, Fonts.TitleFont, (shopWidth - 450 - 50)/2);
						for(String sl : lines) {
							GraphicsUtil.drawWithShadow(g, sl, (int) ((int) mst + 25 + (shopWidth - 500 - 25)/4) - Fonts.TitleFont.getWidth(sl)/2, srl  + i, new Color(255, 0, 0), Color.black);
							i+= 20;
						}
						i += 10;
					}
				}
				i = ti > i ? ti : i;

				i += 20;

				g.setFont(Fonts.ChapterFont);
				int t2 = Fonts.ChapterFont.getWidth("Cost: " + activeRecipe.getCalculatedBuyCost()) + 32;
				GraphicsUtil.drawWithShadow(g, "Cost: " + activeRecipe.getCalculatedBuyCost(),  mst - t2/2, srl + i, Color.white, Color.black);
				g.drawImage(ItemID.lookupName("Coins").getImage(), mst + t2/2 - 28, srl + i);
				if(activeRecipe.getResult().getAmount() > 1) {
					g.setFont(Fonts.ChatFont);
					String str = "(Gives " + activeRecipe.getResult().getAmount() + ")";
					i += 35;
					GraphicsUtil.drawWithShadow(g, str, mst - Fonts.ChatFont.getWidth(str)/2, srl + i, Color.white, Color.black);
				}
				i += 45;

				boolean hasMaterials = true;
				for(Item iv: activeRecipe.getRequirements()) {
					if(!Inventory.contains(iv.getType(), iv.getAmount())) {
						hasMaterials = false;
					}
				}
				if(Inventory.getAmount(ItemID.lookupName("Coins")) < activeRecipe.getCalculatedBuyCost()) {
					hasMaterials = false;
				}

				int xoffset = 0, yoffset = 0;
				xoffset = hasMaterials ? 1 : 0;
				yoffset = hasMaterials ? 0 : 1;

				g.drawImage(Images.GridItem.getSubImage(50*xoffset, 50*yoffset, 50, 50), mst - 25, srl + i);
				g.drawImage(activeRecipe.getResult().getType().getImage(), mst - 16, srl + i + 9);

				i += 100;

				int tw = (activeRecipe.getRequirements().length - 1) * 100;

				int pos = 0;

				g.setLineWidth(2.0f);
				for(Item iv : activeRecipe.getRequirements()) {
					ItemID type = iv.getType();
					ItemID active = null;
					CraftingRecipe subRecipe = null;
					for(CraftingRecipe r : CraftingRecipeList.Recipes) {
						if(r.getResult().getType() == type) {
							subRecipe = r;
						}
					}
					if(subRecipe == null) {
						active = iv.getType();
						hasMaterials = Inventory.getAmount(ItemID.lookupName("Coins")) >= active.getSellCost(); //Fix this!
					} else {
						for(Item il: subRecipe.getRequirements()) {
							if(!Inventory.contains(il.getType(), il.getAmount())) {
								hasMaterials = false;
							}
						}
						if(Inventory.getAmount(ItemID.lookupName("Coins")) < subRecipe.getCalculatedBuyCost()) {
							hasMaterials = false;
						}
					}

					xoffset = hasMaterials ? 1 : 0;
					yoffset = hasMaterials ? 0 : 1;
					if(Inventory.contains(iv.getType(), iv.getAmount())) {
						xoffset = 0;
						yoffset = 0;
					}

					g.drawImage(Images.GridItem.getSubImage(xoffset*50, yoffset*50, 50, 50), mst - tw/2 - 25 + pos*100, srl + i);
					g.drawImage(iv.getType().getImage(), mst - tw/2 - 25 + pos*100 + 9, srl + i + 9);
					g.setFont(Fonts.SmallFont);
					if(iv.getAmount() > 1) {
						GraphicsUtil.drawWithShadow(g, "x" + iv.getAmount(), mst - tw/2 - 25 + pos*100 + 45 - Fonts.SmallFont.getWidth("x" + iv.getAmount()), srl + i + 32, Color.black, Color.white);
					}
					g.setColor(Color.white);
					g.setLineWidth(2.0f);
					g.drawLine(mst - tw/2 + pos*100, srl + i - 3, mst - tw/2 + pos*100, srl + i - 25);
					g.drawLine(mst - tw/2 + pos*100, srl + i - 25, mst, srl + i - 25);

					pos++;
				}

				g.setColor(Color.white);
				g.setLineWidth(2.0f);
				g.drawLine(mst, srl + i - 25, mst, srl + i - 47);
				pos = 0;
				for(Item iv : activeRecipe.getRequirements()) {
					int msx = c.getInput().getMouseX();
					int msy = c.getInput().getMouseY();
					if(msx >= mst - tw/2 - 25 + pos*100 && msx <= mst - tw/2 + 25 + pos*100 && msy >= srl + i && msy <= srl + i + 50) {
						g.setColor(new Color(255, 255, 255, 0.6f));
						g.setColor(new Color(255, 222, 173));
						String title = iv.getType().getNames()[iv.getLevel()];
						g.setFont(Fonts.ChatFont);
						g.setColor(new Color(255, 255, 255, 0.6f));
						g.fillRect(c.getInput().getMouseX() - (Fonts.ChatFont.getWidth(title) + 10)/2, c.getInput().getMouseY() - 30, (Fonts.ChatFont.getWidth(title) + 10), 30);
						g.setColor(Color.black);
						g.drawString(title, c.getInput().getMouseX() - (Fonts.ChatFont.getWidth(title) + 10)/2 + 5, c.getInput().getMouseY() - 25);
						g.setLineWidth(1.0f);
						g.drawRect(c.getInput().getMouseX() - (Fonts.ChatFont.getWidth(title) + 10)/2, c.getInput().getMouseY() - 30, (Fonts.ChatFont.getWidth(title) + 10), 30);

						if(c.getInput().isMouseButtonDown(0)) {
							boolean found = false;
							for(CraftingRecipe r : CraftingRecipeList.Recipes) {
								if(r.getResult().getType() == iv.getType()) {
									activeRecipe = r;
									found = true;
								}
							}
							if(!found) {
								activeRecipe = null;
								activeRecipeItem = iv.getType();
							}
						}
					}
					pos++;
				}


			} else if (activeRecipeItem != null) {
				g.setFont(Fonts.ChapterFont);
				g.setColor(Color.white);
				int i = 30;
				int mst = srt + ((shopWidth - 321 + 22)/2) + 321;
				GraphicsUtil.drawWithShadow(g, activeRecipeItem.getReferenceName(), mst - Fonts.ChapterFont.getWidth(activeRecipeItem.getReferenceName())/2, srl + i , Color.white, Color.black);

				g.setFont(Fonts.TitleFont);
				ArrayList<String> lines = GraphicsUtil.splitBetweenLines(activeRecipeItem.getDescription(), Fonts.TitleFont, shopWidth - 450);
				i += 40;
				for(String s : lines) {
					GraphicsUtil.drawWithShadow(g, s, mst - Fonts.TitleFont.getWidth(s)/2, srl  + i, Color.white, Color.black);
					i+= 20;
				}


				i += 20;

				g.setFont(Fonts.ChapterFont);
				int t2 = Fonts.ChapterFont.getWidth("Cost: " + activeRecipeItem.getBuyCost()) + 32;
				GraphicsUtil.drawWithShadow(g, "Cost: " + activeRecipeItem.getBuyCost(),  mst - t2/2, srl + i, Color.white, Color.black);
				g.drawImage(ItemID.lookupName("Coins").getImage(), mst + t2/2 - 28, srl + i);

				i += 45;

				boolean hasMaterials = false;
				if(Inventory.getBalance() >= activeRecipeItem.getBuyCost()) {
					hasMaterials = true;
				}

				int xoffset = 0, yoffset = 0;
				xoffset = hasMaterials ? 1 : 0;
				yoffset = hasMaterials ? 0 : 1;

				g.drawImage(Images.GridItem.getSubImage(50*xoffset, 50*yoffset, 50, 50), mst - 25, srl + i);
				g.drawImage(activeRecipeItem.getImage(), mst - 16, srl + i + 9);

			} else if (activeSellItem != null) {
				g.setFont(Fonts.ChapterFont);
				g.setColor(Color.white);
				int i = 30;
				int mst = srt + ((shopWidth - 321 + 22)/2) + 321;
				GraphicsUtil.drawWithShadow(g, activeSellItem.getReferenceName(), mst - Fonts.ChapterFont.getWidth(activeSellItem.getReferenceName())/2, srl + i , Color.white, Color.black);

				g.setFont(Fonts.TitleFont);
				ArrayList<String> lines = GraphicsUtil.splitBetweenLines(activeSellItem.getDescription(), Fonts.TitleFont, shopWidth - 450);
				i += 40;
				for(String s : lines) {
					GraphicsUtil.drawWithShadow(g, s, mst - Fonts.TitleFont.getWidth(s)/2, srl  + i, Color.white, Color.black);
					i+= 20;
				}
				i += 10;

				int amount = Inventory.getAmount(activeSellItem);
				GraphicsUtil.drawWithShadow(g, "You have " + amount + ".", mst - Fonts.TitleFont.getWidth("You have " + amount + ".")/2, srl  + i, Color.white, Color.black);

				i += 40;

				g.setFont(Fonts.ChapterFont);
				int t2 = Fonts.ChapterFont.getWidth("Sell Value: " + activeSellItem.getSellCost()) + 32;
				GraphicsUtil.drawWithShadow(g, "Sell Value: " + activeSellItem.getSellCost(),  mst - t2/2, srl + i, Color.white, Color.black);
				g.drawImage(ItemID.lookupName("Coins").getImage(), mst + t2/2 - 28, srl + i);

				i += 45;


				int xoffset = 0, yoffset = 0;

				g.drawImage(Images.GridItem.getSubImage(50*xoffset, 50*yoffset, 50, 50), mst - 25, srl + i);
				g.drawImage(activeSellItem.getImage(), mst - 16, srl + i + 9);
			}

			craftButton.draw(g, c);
			closeCrafting.draw(g, c);
			showWeapons.draw(c, g);
			showMaterials.draw(c, g);
			showAll.draw(c, g);
			showSell.draw(c, g);


			Settings.MainWindow.crafting.setX(c.getWidth()/2 - 430);

			Settings.MainWindow.crafting.draw(g);
			boolean hasMaterials = true;
			if(activeSellItem != null && isSelling) {
				SellListItem sel = (SellListItem) Settings.MainWindow.crafting.getSelected();
				if(sel != null) {
					craftButton.isDisabled(false);
				} else {
					craftButton.isDisabled(true);
				}
			} else if(activeRecipeItem != null) {
				if(Inventory.getBalance() >= activeRecipeItem.getBuyCost()) {
					craftButton.isDisabled(false);
				} else {
					craftButton.isDisabled(true);
				}
			} else if (activeRecipe != null) {
				for(Item i: activeRecipe.getRequirements()) {
					if(!Inventory.contains(i.getType(), i.getAmount())) {
						hasMaterials = false;
					}
				}
				if(Inventory.getBalance() >= activeRecipe.getCalculatedBuyCost()) {
					hasMaterials = true;
				}

				if(hasMaterials) {
					craftButton.isDisabled(false);
				} else {
					craftButton.isDisabled(true);
				}
			}
		} else {
			craftButton.isDisabled(true);
		}


		if(Settings.ShowHUD && openCraftingMenu == 2) {
			g.drawImage(Images.FolderTab, c.getWidth()/2 - Images.FolderBackground.getWidth()/2 + 50, c.getHeight()/2 - Images.FolderBackground.getHeight()/2 - 30);
			g.setFont(Fonts.TypewriterFont);
			g.setColor(Color.white);
			g.drawString("Research",  c.getWidth()/2 - Images.FolderBackground.getWidth()/2 + 105, c.getHeight()/2 - Images.FolderBackground.getHeight()/2 - 20);
			g.drawImage(Images.FolderBackground, c.getWidth()/2 - Images.FolderBackground.getWidth()/2, c.getHeight()/2 - Images.FolderBackground.getHeight()/2);
			Settings.MainWindow.research.setX(c.getWidth()/2 - 430);

			Settings.MainWindow.research.draw(g);
			craftButton.draw(g, c);
			closeCrafting.draw(g, c);


			boolean hasMaterials = true;
			ResearchListItem sel = (ResearchListItem) Settings.MainWindow.research.getSelected();
			if(sel != null) {
				if(activeRecipe.getRequirements() != null) {
					for(Item i: activeRecipe.getRequirements()) {
						if(!Inventory.contains(i.getType(), i.getAmount())) {
							hasMaterials = false;
						}
					}
				}
				if(Inventory.getAmount(ItemID.lookupName("Coins")) > activeRecipe.getCalculatedBuyCost()) {
					hasMaterials = true;
				}
				if(hasMaterials && !Inventory.hasBonus(activeRecipe)) {
					craftButton.isDisabled(false);
				} else {
					craftButton.isDisabled(true);
				}
			}


		}



		if(Settings.ShowHUD && RemoteUIManager.getSelectedContainer() != -1) {
			try {
				RemoteUIManager.getContainer(RemoteUIManager.getSelectedContainer()).draw(c, g);
			} catch (Exception es) {

			}
		}
		if(System.currentTimeMillis() - Settings.MainWindow.successView.start <= Settings.MainWindow.successView.time*1000) {
			Settings.MainWindow.successView.draw(g, c);
		}

		if(wireOverlayOpen) {
			g.setColor(new Color(0, 0, 0, 0.4f));
			g.fillRect(0, 0, c.getWidth(), c.getHeight());


			g.setColor(new Color(255, 255, 255, 0.3f));
			g.setLineWidth(1.0f);

			float slx = (float) Camera.X_OFFSET%16;
			float sly = (float) Camera.Y_OFFSET%16;
			for(float i = slx; i < c.getWidth(); i += 16) {
				g.drawLine(i, 0, i, c.getHeight());
			}

			for(float i = sly; i < c.getHeight(); i += 16) {
				g.drawLine(0, i, c.getWidth(), i);
			}
			g.translate(Math.round(Camera.X_OFFSET), Math.round(Camera.Y_OFFSET));
			for(Entry<Point, Integer> wp : wireMap.entrySet()) {
				Image is;
				if(wp.getValue() < 100000) {
					BlockType bw = BlockType.lookupId(wp.getValue());

					is = Sheets.BLOCK_TEXTURES.getSubImage(0, bw.getPosition()*2);
				} else {
					is = ItemID.lookupId(wp.getValue() - 100000).getImage();
				}

				if(wp.getValue() == 30 || wp.getValue() == 31) {
					boolean[][] grid = new boolean[3][3];
					for(int i = -1; i <= 1; i++) {
						for(int j = -1; j <= 1; j++) {
							int svl = WiringUtil.getValueAt((int) wp.getKey().getX() + i, (int) wp.getKey().getY() + j);
							if(svl != -1) {							
								if(svl == 30 || svl == 31) {
									grid[i + 1][j + 1] = true;
								} else {
									grid[i + 1][j + 1] = false;
								}
							} else {
								grid[i + 1][j + 1] = false;
							}

						}
					}

					boolean t = grid[1][0];
					boolean b = grid[1][2];
					boolean l = grid[0][1];
					boolean r = grid[2][1];


					int sx = -1;
					int sy = -1;
					if(t && b && l && r) {
						sx = 1;
						sy = 1;
					} else if (t && b && l && !r) {
						sx = 2;
						sy = 1;
					} else if (t && b && !l && r) {
						sx = 0;
						sy = 1;
					} else if (t && !b && l && r) {
						sx = 1;
						sy = 2;
					} else if (!t && b && l && r) {
						sx = 1;
						sy = 0;
					} else if (!t && b && !l && r) {
						sx = 0;
						sy = 0;
					} else if (!t && b && l && !r) {
						sx = 2;
						sy = 0;
					} else if (t && !b && l && !r) {
						sx = 2;
						sy = 2;
					} else if (t && !b && !l && r) {
						sx = 0;
						sy = 2;
					}

					if(sx != -1 && sy != -1) {
						is = Sheets.WIRE_CORNERS.getSubImage(sx, sy);
					}
				}

				g.drawImage(is, (float) wp.getKey().getX()*16, (float) wp.getKey().getY()*16);
			}
			g.resetTransform();

			Rectangle r1 = new Rectangle(0, 20, c.getWidth(), 62);
			ShapeFill fill = new GradientFill(c.getWidth(), 62, new Color(100, 100, 100), 0, 20, new Color(160, 160, 160));
			g.fill(r1, fill);
			g.setColor(Color.black);
			g.draw(r1);


			if(WirePlacementType == 1) {
				g.drawImage(ItemID.lookupName("Copper Wire").getImage().getScaledCopy(2.0f), 20, 20);
				g.setFont(Fonts.LargeFont);
				GraphicsUtil.drawWithShadow(g, "x" + (Inventory.getAmount(ItemID.lookupName("Copper Wire")) - WiringUtil.countWires()), 85, 45, new Color(87, 87, 87), new Color(198, 198, 198));
			} else if(WirePlacementType == 2) {
				g.drawImage(ItemID.lookupName("Integrated Circuit").getImage().getScaledCopy(2.0f), 20, 15);
				g.setFont(Fonts.LargeFont);
				GraphicsUtil.drawWithShadow(g, "x" + (Inventory.getAmount(ItemID.lookupName("Integrated Circuit")) - WiringUtil.countGates()), 90, 40, new Color(87, 87, 87), new Color(198, 198, 198));
			}
			long time = System.currentTimeMillis() - timeSinceTrayOpened;
			Rectangle oldClip = g.getWorldClip();
			Rectangle r2 = new Rectangle(0, 80, c.getWidth(), (float) (Math.sqrt(time)/300.0)*1000);
			g.setWorldClip(r2);
			if(WirePlacementType != 1) {
				g.drawImage(Images.BlockTray.getScaledCopy(c.getWidth(), 70), 0, 80);
				int sx = 10;
				for(WiringElement ce : WiringElement.values()) {
					if(ce.getType() == WirePlacementType) {
						g.drawImage(Images.BlockTrayBackground, sx, 90);
						g.drawImage(ce.getImage(), sx + 9, 99);
						if(WirePlacementType != 2 && ce.getItemSource() != null) {
							String str = "x" + (Inventory.getAmount(ce.getItemSource()) - WiringUtil.countElement(ce));
							g.setFont(Fonts.SmallFont);
							g.drawString(str, sx + 45 - Fonts.SmallFont.getWidth(str), 123);
						}
						double msx = c.getInput().getMouseX();
						double msy = c.getInput().getMouseY();		
						if(msx >= sx && msx <= sx + 50 && msy >= 90 && msy <= 140 ){
							String tsp = ce.getName();
							g.setColor(new Color(90, 90, 90, 0.5f));
							g.setFont(Fonts.SmallFont);
							g.fillRect(sx + 25 - Fonts.SmallFont.getWidth(tsp)/2 - 10 , 145, Fonts.SmallFont.getWidth(tsp) + 20 , 20);
							g.setColor(Color.black);
							g.drawString(tsp, sx + 25 - Fonts.SmallFont.getWidth(tsp)/2, 147);
							g.drawRect(sx + 25 - Fonts.SmallFont.getWidth(tsp)/2 - 10 , 145, Fonts.SmallFont.getWidth(tsp) + 20 , 20);
							if(KeyBindings.isKeyDown(c, KeyBindings.Attack)) {
								if(ce != null) {
									selectedWire = ce; 
								}
							}
						}
						sx += 60;
					}
				}
			}
			g.setWorldClip(oldClip);

			wirePlaceButton.check(c);
			latchPlaceButton.check(c);
			inputPlaceButton.check(c);
			turretPlaceButton.check(c);
			closeWireButton.check(c);

			wirePlaceButton.draw(g, c);
			latchPlaceButton.draw(g, c);
			inputPlaceButton.draw(g, c);
			turretPlaceButton.draw(g, c);
			closeWireButton.draw(g, c);

			if(selectedWire != null) {
				mx = (float) Camera.convertXToCamera(Mouse.getX());
				my = (float) Camera.convertYToCamera(c, Mouse.getY());
				if(Grid.getClosestBlock((int) mx, (int) my) != null) {
					Image img = selectedWire.getImage().copy();
					if((Inventory.getAmount(ItemID.lookupName("Integrated Circuit")) - WiringUtil.countGates()) < 1) {
						img.setColor(0, 1.0f, 0, 0, 0.3f);
						img.setColor(1, 1.0f, 0, 0, 0.3f);
						img.setColor(2, 1.0f, 0, 0, 0.3f);
						img.setColor(3, 1.0f, 0, 0, 0.3f);
					} else {
						img.setAlpha(0.5f);
					}

					g.drawImage(img, (float) Math.round(mx - (mx%16) + Camera.X_OFFSET), (float) Math.round(my - (my%16) + Camera.Y_OFFSET));
				}
			} else if (WirePlacementType == 1) {
				mx = (float) Camera.convertXToCamera(Mouse.getX());
				my = (float) Camera.convertYToCamera(c, Mouse.getY());
				if(Grid.getClosestBlock((int) mx, (int) my) != null) {
					Image img = Sheets.BLOCK_TEXTURES.getSubImage(0, BlockType.lookupName("Wire").getPosition()*2).copy();
					if((Inventory.getAmount(ItemID.lookupName("Copper Wire")) - WiringUtil.countWires()) < 1) {
						img.setColor(0, 1.0f, 0, 0, 0.3f);
						img.setColor(1, 1.0f, 0, 0, 0.3f);
						img.setColor(2, 1.0f, 0, 0, 0.3f);
						img.setColor(3, 1.0f, 0, 0, 0.3f);
					} else {
						img.setAlpha(0.5f);
					}



					g.drawImage(img, (float) Math.round(mx - (mx%16) + Camera.X_OFFSET), (float) Math.round(my - (my%16) + Camera.Y_OFFSET));
				}
			}


		}


		if(VoiceMenuOpen) {
			g.setColor(new Color(0, 0, 0, 0.7f));
			g.fillRoundRect(100, c.getHeight() - 240, 200, 200, 4);
			int pos = 0;
			g.setColor(new Color(255, 140, 0));
			g.setFont(Fonts.TitleFont);
			for(EmoteType t : EmoteType.values()) {
				g.drawString((pos + 1) + ". " + t.name().replace('_', ' '), 110, c.getHeight() - 230 + pos*20);
				pos++;
			}
			g.drawString(KeyBindings.getKeyName(KeyBindings.Voice_Menu) + ". Close", 110, c.getHeight() - 230 + pos*20);
		}

		if(MapEventManager.getNextHighestMapEvent() != null) {
			MapEventManager.getNextHighestMapEvent().process(g, c);
		}

		//Console display
		if(Console.ShowConsole & Settings.ShowHUD){
			g.setColor(new Color(0,0,0, .5f));
			g.fillRect(0, 0, c.getWidth(), 285);
			CopyOnWriteArrayList<String> messages = Console.read();
			int offset = 0;
			if(messages.size() > 19) {
				offset += messages.size() - 19;
			}

			if(offset - messageOffset < 0) {
				messageOffset = offset;
			}
			for(int i = messages.size() - 1 - messageOffset; i >= offset - messageOffset; i--) {

				if(messages.get(i).length() > 7 && messages.get(i).substring(0, 7).equalsIgnoreCase("[Error]")) {
					Fonts.drawWithColor(ChatColors.LightRed + messages.get(i), Fonts.DefaultFont, g, 0, (i-offset + messageOffset)*15, c.getWidth());
				} else if (messages.get(i).substring(0,1).equals(">")) {
					Fonts.drawWithColor(ChatColors.Yellow + messages.get(i), Fonts.DefaultFont, g, 0, (i-offset + messageOffset)*15, c.getWidth());
				} else {
					Fonts.drawWithColor(ChatColors.White + messages.get(i), Fonts.DefaultFont, g, 0, (i-offset + messageOffset)*15, c.getWidth());

				}


			}
			g.setColor(Color.black);
			t.setFocus(true);
			t.draw(c, g);
		}

	}

	public void update(GameContainer c, int arg1) {
		if(Settings.CalculateMovement && Settings.ActivePlayer == Settings.CameraPlayer && System.currentTimeMillis() - timeSinceLastMovementCalculation >= 10) {
			Settings.ActivePlayer.step();
			timeSinceLastMovementCalculation = System.currentTimeMillis();
		}

		Settings.WindowHeight = c.getHeight();
		Settings.WindowWidth = c.getWidth();
		double angle = calculateAngle(c);
		ct.setY(c.getHeight() - 60);
		t.setWidth(c.getWidth() + 1);

		boolean charSelect = (Settings.ActivePlayer.getCharacter() == null);

		if(charSelect) {
			int shopWidth = c.getWidth() - c.getWidth()%100 - 200;
			int shopHeight = 600;
			int srt = (int)((c.getWidth() - shopWidth)/2) - 11;
			int srl = (int)((c.getHeight() - shopHeight)/2) - 11;
			Settings.MainWindow.characterSelect.setX(srt + (421 - 115*3)/2);
			Settings.MainWindow.characterSelect.setY(130);
			Settings.MainWindow.characterSelect.setDrawBackground(false);
			int msw = Mouse.getDWheel();
			if(msw > 0) {
				if(characterSelectScrollPosition - 20 <= 0) {
					characterSelectScrollPosition = 0;
				} else {
					characterSelectScrollPosition -= 20;
				}
			} else if (msw < 0) {
				if(characterSelectScrollPosition + 20 >= characterSelectTotalHeight) {
					characterSelectScrollPosition = characterSelectTotalHeight;
				} else {
					characterSelectScrollPosition += 20;
				}
			}

		}


		if(isPlacing && !(Settings.ActivePlayer.getWeapon() instanceof EmptyHand)) {
			if(Inventory.getSelectedItem() == null) {
				Inventory.setNextInventoryBlock();
			}
		}



		if(wireOverlayOpen) {
			wirePlaceButton.setX(c.getWidth() - 250);
			latchPlaceButton.setX(c.getWidth() - 250 + 32 + 6);
			inputPlaceButton.setX(c.getWidth() - 250 + 64 + 12);
			turretPlaceButton.setX(c.getWidth() - 250 + 96 + 18);
			closeWireButton.setX(c.getWidth() - 250 + 128 + 24);


			if(wirePlaceButton.isSelected() && WirePlacementType != 1) {
				WirePlacementType = 1;
				selectedWire = null; //fix
			} else if(latchPlaceButton.isSelected() && WirePlacementType != 2) {
				WirePlacementType = 2;
				timeSinceTrayOpened = System.currentTimeMillis();

			} else if(inputPlaceButton.isSelected() && WirePlacementType != 4) {
				WirePlacementType = 4;
				selectedWire = null;
				timeSinceTrayOpened = System.currentTimeMillis();
			} else if(turretPlaceButton.isSelected() && WirePlacementType != 3) {
				WirePlacementType = 3;
				selectedWire = null;
				timeSinceTrayOpened = System.currentTimeMillis();
			} else if (closeWireButton.isSelected()) {
				WirePlacementType = 0;
				selectedWire = null;
				wireOverlayOpen = false;
				cursorMode = 0;
			}

			if(KeyBindings.isKeyDown(c, KeyBindings.Open_Wheel) && KeyBindings.isKeyPressed(c, KeyBindings.Open_Wheel)) {
				if(c.getInput().getMouseY() > 150) {
					float mx = (float) Camera.convertXToCamera(Mouse.getX());
					float my = (float) Camera.convertYToCamera(c, Mouse.getY());
					Block bs = Grid.getClosestBlock((int) mx, (int) my);
					if(bs != null) {
						WiringUtil.unmapLocation(bs.getX(), bs.getY());
					}
				}
			}

		}


		if(KeyBindings.isKeyPressed(c, KeyBindings.Open_Menu) && KeyBindings.isKeyDown(c, KeyBindings.Open_Menu)) {
			if(isFolderOpen) {
				isFolderOpen = false;
			} else if(openCraftingMenu != 0) {
				openCraftingMenu = 0;
			} else if(RemoteUIManager.getSelectedContainer() != -1) {
				RemoteUIManager.setSelectedContainer(-1);
			} else if(Chat.ShowChat){
				Chat.ShowChat = false;
				ct.setFocus(false);
			} else if (Console.ShowConsole) {
				Console.ShowConsole = false;
				t.setFocus(false);
			} else {
				Window.MenuOpen = !Window.MenuOpen;
				Window.MenuId = 0;
			}
		}

		if(Settings.ShowHUD && RemoteUIManager.getSelectedContainer() != -1) {
			try {
				RemoteUIManager.getContainer(RemoteUIManager.getSelectedContainer()).update(c);
			} catch (Exception e) {

			}
		} else {
			int msw = Mouse.getDWheel();
			if(!isFolderOpen && !Settings.MainWindow.MenuOpen) {
				if(openCraftingMenu == 0) {
					if(msw > 0) {
						if(!Chat.ShowChat && !Console.ShowConsole) {
							if(isPlacing) {
								Inventory.setNextInventoryBlock();
							} else {
								Inventory.setNextInventoryWeapon();
							}
						} else {
							messageOffset += Settings.ScrollAmount;

						}
					}else if(msw < 0) {
						if(!Chat.ShowChat && !Console.ShowConsole) {
							if(isPlacing) {
								Inventory.setPreviousInventoryBlock();
							} else {
								Inventory.setPreviousInventoryWeapon();
							}
						} else {
							if(messageOffset > Settings.ScrollAmount - 1) {
								messageOffset -= Settings.ScrollAmount;
							}

						}
					}
				}
			} else {
				if(msw > 0) {
					Settings.MainWindow.folder.bonuses.updateScrollPosition(-3*Settings.ScrollAmount);
				} else if(msw < 0) {
					Settings.MainWindow.folder.bonuses.updateScrollPosition(3*Settings.ScrollAmount);
				}
			}
			if(Settings.ShowHUD && craftingMsgVisible) {
				craftingMsg.check(c);
				closeCraftingMsg.check(c);
				if(closeCraftingMsg.isSelected()) {
					craftingMsgVisible = false;
				}
			}
			if(!Settings.StoryMode || EntityManager.getPlayers().size() > 1) {
				if(!Window.MenuOpen && Chat.ShowChat == false && openCraftingMenu == 0 && System.currentTimeMillis() - timeSinceChatClosed >= 250) {
					if(KeyBindings.isKeyPressed(c, KeyBindings.Open_All_Chat) && KeyBindings.isKeyDown(c, KeyBindings.Open_All_Chat)){
						System.out.println("All");
						Chat.ShowChat = true;
						messageOffset = 0;
						ct.setText("/all ");
						ct.setCursorPos(5);
					} else if(KeyBindings.isKeyPressed(c, KeyBindings.Open_Chat) && KeyBindings.isKeyDown(c, KeyBindings.Open_Chat)) {
						System.out.println("Chat");
						Chat.ShowChat = true;
						messageOffset = 0;
						ct.setText("");
					}
				}
			}

			if(Settings.ShowHUD && openCraftingMenu == 1 & !Window.MenuOpen) {
				int shopWidth = c.getWidth() - c.getWidth()%100 - 200;
				int srt = (int)((c.getWidth() - shopWidth)/2) - 11;
				int srl = (int)((c.getHeight() - 600)/2) - 11;

				Settings.MainWindow.crafting.setY((int)((c.getHeight() - 600)/2) + 20);
				Settings.MainWindow.crafting.setX(c.getWidth()%100 + 200);
				Settings.MainWindow.crafting.height = 500;
				Settings.MainWindow.crafting.width = 200;
				Settings.MainWindow.crafting.setDrawBackground(false);
				Settings.MainWindow.crafting.update(c);
				craftButton.check(c);
				closeCrafting.check(c);

				int mid = srt + ((shopWidth - 321 + 22)/2) + 321;
				craftButton.setX(mid + 100);
				closeCrafting.setX(mid - 300);
				closeCrafting.setY(c.getHeight()/2 + 240);
				craftButton.setY(c.getHeight()/2 + 240);

				showWeapons.setX(srt + 20);
				showWeapons.setY(srl + 30);

				showMaterials.setX(srt + 20);
				showMaterials.setY(srl + 90);

				showAll.setX(srt + 20);
				showAll.setY(srl + 150);

				showSell.setX(srt + 20);
				showSell.setY(srl + 210);

				if(openCraftingType == null) {
					showAll.isDisabled(true);
				} else {
					showAll.isDisabled(false);
				}

				if(openCraftingType == RecipeType.Material) {
					showMaterials.isDisabled(true);
				} else {
					showMaterials.isDisabled(false);
				}

				if(openCraftingType == RecipeType.Weapon) {
					showWeapons.isDisabled(true);
				} else {
					showWeapons.isDisabled(false);
				}
				if(isSelling) {
					showWeapons.isDisabled(false);
					showAll.isDisabled(false);
					showMaterials.isDisabled(false);

					showSell.isDisabled(true);
				} else {
					showSell.isDisabled(false);
				}
				showAll.check(c);
				showMaterials.check(c);
				showWeapons.check(c);
				showSell.check(c);

				if(msw > 0) {
					Settings.MainWindow.crafting.updateScrollPosition(-20);

				}else if(msw < 0) {
					Settings.MainWindow.crafting.updateScrollPosition(20);

				}

				if(isSelling) {
					craftButton.setValue("Sell");
				} else {
					craftButton.setValue("Buy");
				}

				if(craftButton.isSelected()) {
					if(isSelling) {
						NetObjects.Sender.sendPacket(new Packet70SendSellRequest(((SellListItem) Settings.MainWindow.crafting.getSelected())));

					} else {
						if(activeRecipe != null) {
							NetObjects.Sender.sendPacket(new Packet35SendCraftingRequest(activeRecipe.getId()));
						}else if(activeRecipeItem != null) {
							NetObjects.Sender.sendPacket(new Packet72SendPurchaseItemRequest(activeRecipeItem));

						}

					}
					//openCraftingMenu = 0;
					SoundEffects.playSound(SoundEffects.Coins);

				}
				if(closeCrafting.isSelected()) {
					openCraftingMenu = 0;
				}

				if(showSell.isSelected()) {
					populateSellingList();
					isSelling = true;
				}

				if(showAll.isSelected()) {
					openCraftingType = null;
					populateCraftingList(openCraftingType);
					Settings.MainWindow.crafting.scrollPosition = 0.0;
					isSelling = false;
					activeSellItem = null;


				}
				if(showMaterials.isSelected()) {
					openCraftingType = RecipeType.Material;
					populateCraftingList(openCraftingType);
					Settings.MainWindow.crafting.scrollPosition = 0.0;
					isSelling = false;
					activeSellItem = null;

				}

				if(showWeapons.isSelected()) {
					openCraftingType = RecipeType.Weapon;
					populateCraftingList(openCraftingType);
					Settings.MainWindow.crafting.scrollPosition = 0.0;
					isSelling = false;
					activeSellItem = null;
				}

				boolean nearWorkbench = false;
				for(Entity e : EntityManager.getEntities()) {
					if((e instanceof Workbench)) {
						if(Math.sqrt(Math.pow(Settings.ActivePlayer.getX() - e.getX(), 2) + Math.pow(Settings.ActivePlayer.getY() - e.getY(), 2)) <= 150) {
							nearWorkbench = true;
						} 
					}
				}
				if(!nearWorkbench) {
					openCraftingMenu = 0;
				}
			}
			if(Settings.ShowHUD && openCraftingMenu == 2 & !Window.MenuOpen) {

				Settings.MainWindow.research.setY(c.getHeight()/2 - 450/2);

				Settings.MainWindow.research.update(c);
				if(Settings.MainWindow.research.getSelected() == null) {
					craftButton.isDisabled(true);
				}
				craftButton.check(c);
				closeCrafting.check(c);
				craftButton.setX(c.getWidth()/2 + 100);
				closeCrafting.setX(c.getWidth()/2  - 300);
				closeCrafting.setY(c.getHeight()/2 + 240);
				craftButton.setY(c.getHeight()/2 + 240);

				if(msw > 0) {
					Settings.MainWindow.research.updateScrollPosition(-20);

				}else if(msw < 0) {
					Settings.MainWindow.research.updateScrollPosition(20);

				}
				if(craftButton.isSelected()) {
					if(openCraftingMenu == 1) {
						if(Settings.MainWindow.research.getSelected() != null) {
							NetObjects.Sender.sendPacket(new Packet35SendCraftingRequest(activeRecipe.getId()));
						}
					} else if (openCraftingMenu == 2) {
						if(Settings.MainWindow.research.getSelected() != null) {
							NetObjects.Sender.sendPacket(new Packet35SendCraftingRequest(activeRecipe.getId()));
						}

					}
					openCraftingMenu = 0;
				}
				if(closeCrafting.isSelected()) {
					openCraftingMenu = 0;
				}

				boolean nearWorkbench = false;
				for(Entity e : EntityManager.getEntities()) {
					if((e instanceof ResearchBench)) {
						if(Math.sqrt(Math.pow(Settings.ActivePlayer.getX() - e.getX(), 2) + Math.pow(Settings.ActivePlayer.getY() - e.getY(), 2)) <= 150) {
							nearWorkbench = true;
						}
					}
				}
				if(!nearWorkbench) {
					openCraftingMenu = 0;
				}
			}


			if(!Window.MenuOpen) {
				if(wheelOpen) {
					WheelItem tmp = wheel.getClosestTarget(c.getInput().getMouseX(), c.getInput().getMouseY());
					if(tmp != null && tmp != closest) {
						if(closest != null) {
							closest.setClosest(false);
						}
						tmp.setClosest(true);
						closest = tmp;

					}
				}
				if(!Chat.ShowChat && openCraftingMenu == 0 && !Console.ShowConsole && !wireOverlayOpen) {
					if(KeyBindings.isKeyDown(c, KeyBindings.Open_Wheel) && (cursorMode == 0 || cursorMode == 1) && !Settings.ActivePlayer.isDead()) {
						if(!wheelOpen) {
							wheel.refreshItems();
							wheelOpen = true;
							wheel.setGoals(c.getWidth() / 2, c.getHeight() / 2);
						}
					} else {
						if(wheelOpen) {
							wheelOpen = false;
							if(closest != null) {
								closest.onRelease();
							}
						}
					}
				}


				if(!Chat.ShowChat && openCraftingMenu == 0 && KeyBindings.isKeyPressed(c, KeyBindings.Open_Console)  && KeyBindings.isKeyDown(c, KeyBindings.Open_Console)) {
					if(Console.ShowConsole){
						Console.ShowConsole = false;
					} else {
						Console.ShowConsole = true;
						Chat.ShowChat = false;
						t.setText("");
					}
				}
				if(KeyBindings.isKeyPressed(c, KeyBindings.Close_Window)) {
					if(Chat.ShowChat) {
						Chat.ShowChat = false;
						ct.setFocus(false);
					} else if (Console.ShowConsole) {
						Console.ShowConsole = false;
						t.setFocus(false);
					}

				}


				if(!Console.ShowConsole && !Chat.ShowChat && KeyBindings.isKeyDown(c, KeyBindings.Voice_Menu) && KeyBindings.isKeyPressed(c, KeyBindings.Voice_Menu)) {
					VoiceMenuOpen = !VoiceMenuOpen;
				}
				if(Console.ShowConsole || Chat.ShowChat) {
					VoiceMenuOpen = false;
				}

				if(VoiceMenuOpen && !Console.ShowConsole && !Chat.ShowChat) {
					if(c.getInput().isKeyDown(Keyboard.KEY_1) && c.getInput().isKeyPressed(Keyboard.KEY_1)) {
						NetObjects.Sender.sendPacket(new Packet63SendEmoteRequest(EmoteType.Happy));
						VoiceMenuOpen = false;
					}
					if(c.getInput().isKeyDown(Keyboard.KEY_2) && c.getInput().isKeyPressed(Keyboard.KEY_2)) {
						NetObjects.Sender.sendPacket(new Packet63SendEmoteRequest(EmoteType.Angry));
						VoiceMenuOpen = false;
					}
					if(c.getInput().isKeyDown(Keyboard.KEY_3) && c.getInput().isKeyPressed(Keyboard.KEY_3)) {
						NetObjects.Sender.sendPacket(new Packet63SendEmoteRequest(EmoteType.Disapproval));
						VoiceMenuOpen = false;
					}
					if(c.getInput().isKeyDown(Keyboard.KEY_4) && c.getInput().isKeyPressed(Keyboard.KEY_4)) {
						NetObjects.Sender.sendPacket(new Packet63SendEmoteRequest(EmoteType.Left));
						VoiceMenuOpen = false;
					}
					if(c.getInput().isKeyDown(Keyboard.KEY_5) && c.getInput().isKeyPressed(Keyboard.KEY_5)) {
						NetObjects.Sender.sendPacket(new Packet63SendEmoteRequest(EmoteType.Right));
						VoiceMenuOpen = false;
					}
					if(c.getInput().isKeyDown(Keyboard.KEY_6) && c.getInput().isKeyPressed(Keyboard.KEY_6)) {
						NetObjects.Sender.sendPacket(new Packet63SendEmoteRequest(EmoteType.Stop));
						VoiceMenuOpen = false;
					}
					if(c.getInput().isKeyDown(Keyboard.KEY_7) && c.getInput().isKeyPressed(Keyboard.KEY_7)) {
						NetObjects.Sender.sendPacket(new Packet63SendEmoteRequest(EmoteType.Thumbs_Up));
						VoiceMenuOpen = false;
					}
					if(c.getInput().isKeyDown(Keyboard.KEY_8) && c.getInput().isKeyPressed(Keyboard.KEY_8)) {
						NetObjects.Sender.sendPacket(new Packet63SendEmoteRequest(EmoteType.Thumbs_Down));
						VoiceMenuOpen = false;
					}
				}

				if(!Console.ShowConsole && !Chat.ShowChat && KeyBindings.isKeyDown(c, KeyBindings.Use) && KeyBindings.isKeyPressed(c, KeyBindings.Use) && !Settings.ActivePlayer.isDead()) {
					if(!(MapEventManager.getNextHighestMapEvent() instanceof TextMapEvent)) {
						if(openCraftingMenu != 0) {
							openCraftingMenu = 0;

						} else {
							for(Entity e : EntityManager.getEntities()) {
								if(e instanceof UsableEntity) {
									UsableEntity es = (UsableEntity) e;
									if(e.isRollover()) {
										es.onSelected();
									}
								}
							}
						} 
					} else {
						MapEventManager.getNextHighestMapEvent().dispose();
						NetObjects.Sender.sendPacket(new Packet62SendNextEventRequest());
					}

				}
				if(!Console.ShowConsole && !Chat.ShowChat) {
					if (KeyBindings.isKeyDown(c, KeyBindings.Move_Left)) {
						if(Settings.CalculateMovement) {
							Settings.ActivePlayer.setXSpeed(-2*BonusItem.calculateBonus(BonusItemType.Movement));
							Settings.ActivePlayer.setFacingDirection(-1);
						} else {
							NetObjects.Sender.sendPacket(new Packet12SendMovement(MovementType.Left));
						}
					} else if (KeyBindings.isKeyDown(c, KeyBindings.Move_Right)) {
						if(Settings.CalculateMovement) {
							Settings.ActivePlayer.setXSpeed(2*BonusItem.calculateBonus(BonusItemType.Movement));
							Settings.ActivePlayer.setFacingDirection(1);


						} else {
							NetObjects.Sender.sendPacket(new Packet12SendMovement(MovementType.Right));
						}
					} else {
						if(Settings.ActivePlayer.getFacingDirection() == 1) {
							NetObjects.Sender.sendPacket(new Packet12SendMovement(MovementType.None));
							Settings.ActivePlayer.setFacingDirection(0);

						} else if(Settings.ActivePlayer.getFacingDirection() == -1) {
							NetObjects.Sender.sendPacket(new Packet12SendMovement(MovementType.FlippedNone));
							Settings.ActivePlayer.setFacingDirection(2);

						}
					}

					if(KeyBindings.isKeyDown(c, KeyBindings.Jump)){
						if(Settings.CalculateMovement) {
							Settings.ActivePlayer.jump();
						} else {
							NetObjects.Sender.sendPacket(new Packet12SendMovement(MovementType.Jump));
						}
					}


					if(KeyBindings.isKeyPressed(c, KeyBindings.Attack)  && KeyBindings.isKeyDown(c, KeyBindings.Attack) && openCraftingMenu == 0) {
						if(cursorMode == 3 && Settings.MainWindow.folder.getSelected() == 1) {
							int mx = c.getInput().getMouseX();
							int my = c.getInput().getMouseY();
							if(mx >= InventoryUI.getX() - 400 && mx <= InventoryUI.getX()  + 400 && my >= InventoryUI.getY()  - 100 && my <= InventoryUI.getY()  + 200) {
								for(int i = 0; i < Inventory.Slots - 1; i++) {
									int tx = InventoryUI.getX() - 400 + (i % 3)*82;
									int ty = InventoryUI.getY() - 100 + ((int) Math.floor(i/3))*82;
									if(c.getInput().getMouseX() >= tx && c.getInput().getMouseX() <= tx + 62 && c.getInput().getMouseY() >= ty && c.getInput().getMouseY() <= ty + 62) {
										if(Inventory.getItemFromSlot(i + 1) == null || Inventory.getItemFromSlot(i + 1).getAmount() == 0) {
											if(draggingItem.getSlot() != i + 1) {
												NetObjects.Sender.sendPacket(new Packet22RelocateItem(draggingItem.getType(), i + 1, draggingItem.getSlot(), draggingItem.getAmount()));
											} else {
												Inventory.addItem(draggingItem.getType(), draggingItem.getAmount(), draggingItem.getLevel(), draggingItem.getSlot());
											}		
											cursorMode = 2;

										}
									}
								}
							} else {
								NetObjects.Sender.sendPacket(new Packet22RelocateItem(draggingItem.getType(), -1, draggingItem.getSlot(), draggingItem.getAmount()));
								cursorMode = 2;
							}
						} else if (cursorMode == 2 && Settings.MainWindow.folder.getSelected() == 1) {
							for(int i = 0; i < 9; i++) {

								int tx = InventoryUI.getX() - 400 + (i % 3)*82;
								int ty = InventoryUI.getY() - 100 + ((int) Math.floor(i/3))*82;
								if(c.getInput().getMouseX() >= tx && c.getInput().getMouseX() <= tx + 62 && c.getInput().getMouseY() >= ty && c.getInput().getMouseY() <= ty + 62) {
									if(Inventory.getItemFromSlot(i + 1) != null) {
										draggingItem = Inventory.getItemFromSlot(i + 1);
										Inventory.removeItemFromSlot(i + 1);
										cursorMode = 3;
									}
								}
							}
						} else 	if(cursorMode == 1) {
							if(setProjectile != null) {
								NetObjects.Sender.sendPacket(new Packet17ThrowProjectile(angle, setProjectile));
							}
							cursorMode = 0;
						} else if (cursorMode == 0) {
							if(isPlacing) {
								if(Inventory.getSelectedItem() != null) {

									double msx = c.getInput().getMouseX();
									double msy = c.getInput().getMouseY();			
									int mx = (int) Camera.convertXToCamera((int) msx);
									int my = (int) Camera.convertYToCamera(c, (int) msy);
									Block b = Grid.getClosestBlock((int) mx, (int) my);
									if(b != null && b.getType() == BlockType.lookupName("Air")) {
										if(Inventory.getSelectedBlock() != null) {
											NetObjects.Sender.sendPacket(new Packet52PlaceBlock(b.getX(), b.getY(), Inventory.getSelectedBlock()));
										}
									}
								}
							} else {							
								isFiring = true;
							}
						} else if(cursorMode == WirePlacementMode && c.getInput().getMouseY() > 150) {
							double msx = c.getInput().getMouseX();
							double msy = c.getInput().getMouseY();		
							int dx = (int) (msx - (msx%16));
							int dy = (int) (msy - (msy%16));
							switch(WirePlacementType) {
							case 2:

								if(selectedWire != null && selectedWire.getBlockSource().isElectric() && Inventory.getAmount(ItemID.lookupName("Integrated Circuit")) >= WiringUtil.countGates() + 1) {
									float mx = (float) Camera.convertXToCamera(Mouse.getX());
									float my = (float) Camera.convertYToCamera(c, Mouse.getY());
									Block bs = Grid.getClosestBlock((int) mx, (int) my);
									if(bs != null) {
										WiringUtil.mapLocation(bs.getX(), bs.getY(), selectedWire.getBlockSource().getId());
									}
								}
								break;
							case 3:
								if(selectedWire != null && selectedWire.getType() == 3 && Inventory.getAmount(selectedWire.getItemSource()) >= WiringUtil.countElement(selectedWire) + 1) {
									float mx = (float) Camera.convertXToCamera(Mouse.getX());
									float my = (float) Camera.convertYToCamera(c, Mouse.getY());
									Block bs = Grid.getClosestBlock((int) mx, (int) my);
									if(bs != null) {
										WiringUtil.mapLocation(bs.getX(), bs.getY(), 100000 + selectedWire.getItemSource().getId());
									}
								}
							}
						}
					}
					if(KeyBindings.isKeyDown(c, KeyBindings.Attack) && cursorMode == WirePlacementMode) {
						if(WirePlacementType == 1 && Inventory.getAmount(ItemID.lookupName("Copper Wire")) >= WiringUtil.countWires() + 1) {
							float mx = (float) Camera.convertXToCamera(Mouse.getX());
							float my = (float) Camera.convertYToCamera(c, Mouse.getY());
							Block bs = Grid.getClosestBlock((int) mx, (int) my);
							if(bs != null) {
								int bid = 31;
								if(previousWire != null && (Math.abs(previousWire.getY() - bs.getY()) == 1 || WiringUtil.isVerticalSection(bs.getX(), bs.getY())) || WiringUtil.getValueAt(bs.getX(), bs.getY()) == 30) {
									bid = 30;
								}
								if(WiringUtil.isHorizontalSection(bs.getY(), bs.getY())) {
									bid = 31;
								}
								WiringUtil.mapLocation(bs.getX(), bs.getY(), bid);
								previousWire = new Point(bs.getX(), bs.getY());


							}
						}
					}




					if (cursorMode == 0 && isFiring) {
						Item it = Inventory.getSelectedItem();

						double offset = BonusItem.calculateBonus(BonusItemType.RateOfFire);
						if(Inventory.getSelectedItem() != null) {
							offset += Inventory.getSelectedItem().getRateOfFireModifier();
						}
						offset = 2 - offset;
						if(offset <= 0.1) {
							offset = 0.1;
						}
						if(System.currentTimeMillis() - Settings.ActivePlayer.getLastFireTime() >= Settings.ActivePlayer.getWeapon().getTimeOffset()*offset) {
							Block b = Grid.getClosestBlock((int) Settings.ActivePlayer.getX(), (int) Settings.ActivePlayer.getY() + Settings.ActivePlayer.getHeight() - 16);
							if(b != null && (!(b.getType() == BlockType.lookupName("Blue Barrier") || b.getType() == BlockType.lookupName("Red Barrier")) || Settings.ActivePlayer.getWeapon().getType() == ItemID.lookupName("Shovel"))) {

								Settings.ActivePlayer.setLastFireTime(System.currentTimeMillis());

								if(it != null && it.isWeapon()  && !Inventory.isReloading()) {
									if(it.getClip() > 0) {
										double msx = c.getInput().getMouseX();
										double msy = c.getInput().getMouseY();
										Settings.ActivePlayer.getWeapon().onFire(angle, (int) msx, (int) msy, c);
										Settings.ActivePlayer.setLastBulletTime(System.currentTimeMillis());
									} else {
										SoundEffects.playSound(SoundEffects.DryFire, (int) Settings.ActivePlayer.getX(), (int) Settings.ActivePlayer.getY());
									}
								} else {
									if(it != null && it.getType() == ItemID.lookupName("Shovel")) {
										double msx = c.getInput().getMouseX();
										double msy = c.getInput().getMouseY();
										Settings.ActivePlayer.getWeapon().onFire(angle, (int) msx, (int) msy, c);
									}
								}

							}
						}

					}
					if(!Mouse.isButtonDown(0) || cursorMode != 0) {
						isFiring = false;

					}

					if(KeyBindings.isKeyPressed(c, KeyBindings.Reload) && KeyBindings.isKeyDown(c, KeyBindings.Reload)) {
						Item id = Inventory.getSelectedItem();
						if(id != null && id.isWeapon() && id.getClip() != id.getTotalSize() && id.getTotalAmmo() != 0) {
							if(System.currentTimeMillis() - id.getLastReloadTime() >= id.getReloadTime()) {
								NetObjects.Sender.sendPacket(new Packet31RequestReload());
								id.setLastReloadTime(System.currentTimeMillis());
							}
						}
					}
				}

				if(Settings.ActivePlayer.getWeapon() != null && Settings.ActivePlayer.getWeapon().isMelee() && KeyBindings.isKeyDown(c, KeyBindings.Attack)) {
					Settings.ActivePlayer.gunRotation += 0.2;
					if(System.currentTimeMillis()%20 == 0) {
						NetObjects.Sender.sendPacket(new Packet24SendArmRotation(Settings.ActivePlayer.gunRotation));
					}
				} else {
					Settings.ActivePlayer.gunRotation = angle;
					if(System.currentTimeMillis()%20 == 0) {
						NetObjects.Sender.sendPacket(new Packet24SendArmRotation(angle));
					}
				}
				if(Console.ShowConsole) {
					t.update(c);
				}
				if(Chat.ShowChat) {
					ct.update(c);

				}
				if(Console.ShowConsole || Chat.ShowChat){
					if(KeyBindings.isKeyDown(c, KeyBindings.Scroll_Chat_Up)) {
						messageOffset+= Settings.ScrollAmount;
					} else if (KeyBindings.isKeyDown(c, KeyBindings.Scroll_Chat_Down)) {
						if(messageOffset > Settings.ScrollAmount - 1) {
							messageOffset -= Settings.ScrollAmount;
						}
					}
				}
				if(KeyBindings.isKeyPressed(c, KeyBindings.Send_Chat)) {
					messageOffset = 0;
					if(Console.ShowConsole) {
						Console.parseCommand(t.getText().trim());
						t.setText("");
					} else if (Chat.ShowChat) {
						String cs = ct.getText().trim();
						if(cs.length() > 0) {
							NetObjects.Sender.sendPacket(new Packet07SendChat(Settings.ActivePlayer.getName(), cs));

						} 
						ct.setText("");
						Chat.ShowChat = false;
						timeSinceChatClosed = System.currentTimeMillis();

					}
				} 
			}
			if(KeyBindings.isKeyPressed(c, KeyBindings.Toggle_Display)  && KeyBindings.isKeyDown(c, KeyBindings.Toggle_Display)) {
				Settings.ShowHUD = !Settings.ShowHUD;
			}



			if(!(Window.MenuOpen || Chat.ShowChat || Console.ShowConsole || openCraftingMenu != 0)) {
				if (KeyBindings.isKeyPressed(c, KeyBindings.Open_Inventory) && KeyBindings.isKeyDown(c, KeyBindings.Open_Inventory)) {
					if(!isFolderOpen) {
						isFolderOpen = true;
						Settings.MainWindow.folder.setSelected(1);

						NetObjects.Sender.sendPacket(new Packet20RequestInventory());
						NetObjects.Sender.sendPacket(new Packet57RequestPlayerBalance());
						cursorMode = 2;
					} else {
						if(Settings.MainWindow.folder.getSelected() == 1) {
							isFolderOpen = false;
							cursorMode = 0;

						} else {
							Settings.MainWindow.folder.setSelected(1);
							NetObjects.Sender.sendPacket(new Packet20RequestInventory());
							cursorMode = 2;
						}
					}
				} else if (KeyBindings.isKeyPressed(c, KeyBindings.Open_Player_Statistics) && KeyBindings.isKeyDown(c, KeyBindings.Open_Player_Statistics)) {
					if(!isFolderOpen) {
						isFolderOpen = true;
						Settings.MainWindow.folder.setSelected(0);

						cursorMode = 2;
					} else {
						if(Settings.MainWindow.folder.getSelected() == 0) {
							isFolderOpen = false;
							cursorMode = 0;

						} else {
							Settings.MainWindow.folder.setSelected(0);
							cursorMode = 2;
						}
					}
				} else if (KeyBindings.isKeyPressed(c, KeyBindings.Open_Team_Information) && KeyBindings.isKeyDown(c, KeyBindings.Open_Team_Information)) {
					if(!isFolderOpen) {
						isFolderOpen = true;
						Settings.MainWindow.folder.setSelected(2);

						cursorMode = 2;
					} else {
						if(Settings.MainWindow.folder.getSelected() == 2) {
							isFolderOpen = false;
							cursorMode = 0;

						} else {
							Settings.MainWindow.folder.setSelected(2);
							cursorMode = 2;
						}
					}
				} else if (KeyBindings.isKeyPressed(c, KeyBindings.Open_Enemy_Information) && KeyBindings.isKeyDown(c, KeyBindings.Open_Enemy_Information)) {
					if(!isFolderOpen) {
						isFolderOpen = true;
						Settings.MainWindow.folder.setSelected(3);

						cursorMode = 2;
					} else {
						if(Settings.MainWindow.folder.getSelected() == 3) {
							isFolderOpen = false;
							cursorMode = 0;

						} else {
							Settings.MainWindow.folder.setSelected(3);
							cursorMode = 2;
						}
					}
				}


			}
			for(ClientSideEntity e : EntityManager.getClientSideEntities()) {
				e.step();
			}
			for(PhysicsAffectedEntity e : EntityManager.getPhysicsAffectedEntities()) {
				e.stepTowardsGoal();
			}
		}
	}


	private static void populateCraftingList() {
		Settings.MainWindow.crafting.clearItems();

		ArrayList<CraftingRecipe> vr = new ArrayList<CraftingRecipe>();
		for(CraftingRecipe c : CraftingRecipeList.Recipes) {
			if(c.getType() != RecipeType.Research) {
				if(c.isValidForClass(Settings.ActivePlayer.getCharacter())) {
					vr.add(c);
				}
			}
		}
		for(int i = 0; i < vr.size(); i+= 3) {
			if(vr.size() - i >= 3) {
				Settings.MainWindow.crafting.addItem(new ShopListItem(vr.get(i), vr.get(i+1), vr.get(i+2), Settings.MainWindow.crafting));
			} else {
				if(vr.size() - i == 2) {
					Settings.MainWindow.crafting.addItem(new ShopListItem(vr.get(i), vr.get(i+1), null, Settings.MainWindow.crafting));
				} else {
					Settings.MainWindow.crafting.addItem(new ShopListItem(vr.get(i), null, null, Settings.MainWindow.crafting));
				}
			}
		}
	}

	public static void populateSellingList() {
		Settings.MainWindow.crafting.clearItems();
		ArrayList<ItemID> types = new ArrayList<ItemID>();
		for(Item i : Inventory.getItems()) {
			if(i.getType().getSellCost() > 0) {
				if(!types.contains(i.getType())) {
					types.add(i.getType());
				}
			}
		}
		for(ItemID t : types) {
			Settings.MainWindow.crafting.addItem(new SellListItem(new Item(t, 1), Settings.MainWindow.crafting));

		}
	}

	public static void populateResearchList() {
		Settings.MainWindow.research.clearItems();
		for(CraftingRecipe c : CraftingRecipeList.Recipes) {
			if(c.getType() == RecipeType.Research) { 
				if(c.isValidForClass(Settings.ActivePlayer.getCharacter())) {
					Settings.MainWindow.research.addItem(new ResearchListItem(c, Settings.MainWindow.research));
				}
			}
		}
	}

	public static void populateCharacterSelectList() {
		ArrayList<Entry<Integer, PlayerCharacter>> vr = new ArrayList<Entry<Integer, PlayerCharacter>>(PlayerCharacter.characterList.entrySet());
		for(int i = 0; i < vr.size(); i+= 3) {
			if(vr.size() - i >= 3) {
				Settings.MainWindow.characterSelect.addItem(new CharacterSelectListItem(vr.get(i).getValue(), vr.get(i+1).getValue(), vr.get(i+2).getValue(), Settings.MainWindow.characterSelect));

			} else {
				if(vr.size() - i == 2) {
					Settings.MainWindow.characterSelect.addItem(new CharacterSelectListItem(vr.get(i).getValue(), vr.get(i+1).getValue(), null, Settings.MainWindow.characterSelect));
				} else {
					Settings.MainWindow.characterSelect.addItem(new CharacterSelectListItem(vr.get(i).getValue(), null, null, Settings.MainWindow.characterSelect));
				}
			}
		}
	}
	public static void populateCraftingList(RecipeType type) {

		if(type == null) {
			populateCraftingList();

			return;
		}
		Settings.MainWindow.crafting.clearItems();

		ArrayList<CraftingRecipe> vr = new ArrayList<CraftingRecipe>();
		for(CraftingRecipe c : CraftingRecipeList.Recipes) {
			if(c.getType() == type) { 
				if(c.isValidForClass(Settings.ActivePlayer.getCharacter())) {
					vr.add(c);
				}
			}
		}
		for(int i = 0; i < vr.size(); i+= 3) {
			if(vr.size() - i >= 3) {
				Settings.MainWindow.crafting.addItem(new ShopListItem(vr.get(i), vr.get(i+1), vr.get(i+2), Settings.MainWindow.crafting));

			} else {
				if(vr.size() - i == 2) {
					Settings.MainWindow.crafting.addItem(new ShopListItem(vr.get(i), vr.get(i+1), null, Settings.MainWindow.crafting));
				} else {
					Settings.MainWindow.crafting.addItem(new ShopListItem(vr.get(i), null, null, Settings.MainWindow.crafting));
				}
			}
		}
	}

	private double calculateAngle(GameContainer c) {
		double px = c.getWidth() / 2 + Settings.ActivePlayer.getWidth()/2;
		double py = c.getHeight() / 2 + Settings.ActivePlayer.getHeight()/2;
		double msx = c.getInput().getMouseX();
		double msy = c.getInput().getMouseY();
		return Math.atan2(px - msx, msy - py);
	}
	private ArrayList<String> splitBetweenLines(String s, UnicodeFont f, int w) {
		ArrayList<String> results = new ArrayList<String>();
		if(f.getWidth(s) < w) {
			results.add(s);
			return results;
		} else {
			String left = s;
			for(int i = 0; i < left.length(); i++) {
				if(f.getWidth(left.substring(0, i)) > w) {
					results.add(left.substring(0, i));
					int is = left.substring(0, i-1).lastIndexOf("�");
					left = left.substring(is, is + 2) + left.substring(i, left.length());
					i = 0;
				}
			}
			results.add(left);
		}
		return results;

	}



}