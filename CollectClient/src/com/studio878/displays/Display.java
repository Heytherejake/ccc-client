package com.studio878.displays;


import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public abstract class Display {
	public String msg;
	public boolean canOpenMenu = false;
	public abstract void draw(GameContainer c, Graphics g);
	public abstract void update(GameContainer arg0, int arg1);
	public abstract void init(GameContainer c);
	
	public void setMessage(String s) {
		this.msg = s;
	}

}
