package com.studio878.displays;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.ConnectException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.UnknownHostException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Random;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.newdawn.slick.Color;
import org.newdawn.slick.Font;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

import com.studio878.app.Main;
import com.studio878.app.Window;
import com.studio878.input.KeyBindings;
import com.studio878.inventory.Inventory;
import com.studio878.inventory.ItemID;
import com.studio878.io.Client;
import com.studio878.io.NetObjects;
import com.studio878.multiplayer.MultiplayerSettings;
import com.studio878.objects.EntityManager;
import com.studio878.objects.MenuCloud;
import com.studio878.objects.Player;
import com.studio878.packets.Packet01Connect;
import com.studio878.remoteui.RemoteUIManager;
import com.studio878.story.Chapter.ChapterInfo;
import com.studio878.story.ChapterListItem;
import com.studio878.ui.Button;
import com.studio878.ui.Checkbox;
import com.studio878.ui.CondensedServerListItem;
import com.studio878.ui.Fonts;
import com.studio878.ui.ModifiedTextField;
import com.studio878.ui.ServerListItem;
import com.studio878.ui.TableList;
import com.studio878.util.API;
import com.studio878.util.Base64;
import com.studio878.util.Console;
import com.studio878.util.GraphicsUtil;
import com.studio878.util.Images;
import com.studio878.util.PropertiesReader;
import com.studio878.util.ServerLoader;
import com.studio878.util.Settings;
import com.studio878.util.TitleMessages;
import com.studio878.util.WorldEditorLoader;
import com.studio878.world.Camera;
import com.studio878.world.Grid;


public class Menu extends Display{
	ModifiedTextField pass; //Password
	ModifiedTextField user; //Username
	ModifiedTextField server; //Connect to a Server

	ModifiedTextField server_port; //Host Port
	ModifiedTextField server_name; //Host Name
	ModifiedTextField server_seed; //Host Seed
	ModifiedTextField server_max_players; //Host Max Players

	static TableList servers = new TableList(-400, 185, 800, 330, Fonts.LargeFont, true, 0);
	static TableList chapters = new TableList(-400, 185, 800, 330, Fonts.LargeFont, true, 0);
	String server_message;

	static String message = "";
	static String tip = "";
	Checkbox save;
	ArrayList<MenuCloud> clouds = new ArrayList<MenuCloud>();
	static String quip = TitleMessages.getRandomMessage();
	public static int currentMenu = 0;
	static int previousMenu = 0;
	static int lastFrameMenu = currentMenu;
	boolean startedJoining = false;
	static boolean isConnecting = false;
	
	Button login; //Login

	Button options; //Menu3 - Options
	Button host; //Menu3 - Host
	Button connect; //Menu3 - Connect
	Button quit; //Menu3 - Quit
	Button adventure; //Menu3 - Adventure
	Button loadout; //Menu3 - loadout

	Button server_back; // Left-justified back
	Button back; // Centered back
	Button back_3; //Centered back (Menu3)

	Button join; //Join a server
	Button join_back; //Back (Menu1 --> Menu3)

	Button server_start; //Host

	Button refresh; //Refresh server list
	Button list_connect;
	Button list_back;
	Button direct_connect;
	Button switch_mode;
	Button search_speed;

	Button play_chapter;
	Button map_editor;
	Button chapter_back;

	boolean condensed = Settings.PropertiesFile.readBoolean("list-condensed", false);
	int speed = Settings.PropertiesFile.readInt("server-search-speed", 1);

	String ext_ip = "-IP not found-";
	String int_ip = "-IP not found-";

	static String attemptServer;
	static int attemptPort;

	Thread refreshThread;
	Thread loginThread;



	boolean refreshingServers = false;
	int search_mode = 0;

	public Menu(int defaultMenu) {
		currentMenu = defaultMenu;
	}
	public void init(GameContainer c) {
		EntityManager.clearEntities();
		Inventory.clearBonuses();
		RemoteUIManager.setSelectedContainer(-1);
		RemoteUIManager.clearContainers();
		
		MainGame.wireOverlayOpen = false;
		MainGame.wireMap.clear();
		
		Settings.ActivePlayer = new Player(0, 0, "Player", -999);


		Settings.ShowLight = false;
		isConnecting = false;

		Main.splash.message = "Setting up menus...";
		if(Grid.blocks == null) {
			Settings.WorldWidth = 400;
			Settings.WorldHeight = 150;
			Grid.generateGrid(400, 150);
		}

		Camera.setOffset(-100, 0);

		user = new ModifiedTextField(c, Fonts.LargeFont, 0, 250, 600, 50, 4, "Username", false, true);
		user.setFocus(true);
		pass = new ModifiedTextField(c, Fonts.LargeFont, 0, 350, 600, 50, 4, "Password", true, true);

		server = new ModifiedTextField(c, Fonts.LargeFont, 0, 300, 600, 50, 4, "Server", false, true);

		server_name = new ModifiedTextField(c, Fonts.LargeFont, 0, 230, 600, 50, 4, "Server Name", false, true);
		server_seed = new ModifiedTextField(c, Fonts.LargeFont, 0, 300, 600, 50, 4, "(Random Seed)", false, true);
		server_port = new ModifiedTextField(c, Fonts.LargeFont, -150, 370 , 280 , 50, 4, "Port", false, true);
		server_max_players = new ModifiedTextField(c, Fonts.LargeFont, 150, 370, 280 ,50 ,4, "Max Players", false, true);

		PropertiesReader p = new PropertiesReader(Settings.SystemFolder + "server_settings.properties");
		server_name.setText(p.readString("name", ""));
		server_port.setText(p.readString("port", "31337"));
		server_max_players.setText(p.readString("max-players", ""));

		server_seed.setMaxLength(6);
		server_max_players.setMaxLength(3);
		server_seed.setMaxLength(32);
		server_name.setMaxLength(64);

		server.setText(Settings.PropertiesFile.readString("lastServer", ""));
		if(currentMenu == 0) {
			server.setVisibility(false);
		} else if(currentMenu == 1) {
			user.setVisibility(false);
			pass.setVisibility(false);
		}
		save = new Checkbox(287, 425, true, true);

		String usr = Settings.PropertiesFile.readString("username");
		if(usr != null) {
			user.setText(usr);
		}
		String pswd = Settings.PropertiesFile.readString("password");
		if(pswd != null) {
			pass.setText(decrypt(pswd));
		}
		user.setCursorPos(user.getText().length());
		pass.setCursorPos(pass.getText().length());
		server.setCursorPos(server.getText().length());

		login = new Button(-100, 460, "Login", true);

		connect = new Button(100, 250, "Play Online", true);
		host = new Button(-300, 350, "Host a Server", true);
		options = new Button(-300, 450, "Options", true);
		quit = new Button(100, 450, "Quit", true);
		adventure = new Button(-300, 250, "Adventure", true);
		loadout = new Button(100, 350, "Loadout", true);

		back = new Button(-100, c.getHeight() / 2 + 100, "Back", true);
		back_3 = new Button(-100, c.getHeight() / 2 + 200, "Back", true);

		server_back = new Button(-300, 600, "Back", true);
		server_start = new Button(100, 600, "Host", true);

		join = new Button(100, 400, "Join", true);
		join_back = new Button(-300, 400, "Back", true);


		list_back = new Button(-400, 560, "Back", true);
		refresh = new Button(-100, 560, "Refresh", true);
		list_connect = new Button(200, 560, "Connect", true);
		direct_connect = new Button(-100,  620, "Direct Connect", true);

		String s = "Fast";
		if(speed == 1) {
			s = "Regular";
		} else if(speed == 2) {
			s = "Full";
		}

		search_speed = new Button(200, 620, "Search: " + s, true);

		s = "Full";
		if(condensed) {
			s = "Condensed";
		}
		switch_mode = new Button(-400, 620, "View: " + s, true);

		play_chapter = new Button(200, 620, "Start", true);
		chapter_back = new Button(-400, 620, "Back", true);
		map_editor = new Button(-100, 620, "World Editor", true);

		servers.setDrawBackground(false);

		new Thread() {
			public void run() {
				try {
					InetAddress thisIp =InetAddress.getLocalHost();
					int_ip = thisIp.getHostAddress();
					URL getip = new URL("http://api.studio878software.com/util/ip");
					BufferedReader in = new BufferedReader(new InputStreamReader(getip.openStream()));
					ext_ip = in.readLine();

				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}.start();

		if(currentMenu == 0 && Settings.Username != null && Settings.Password != null) {
			currentMenu = 3;
		}

		Main.fullyLoaded = true;
	}

	public void draw(GameContainer c, Graphics g) {
		g.setFont(Fonts.ChatFont);

		g.setColor(new Color(168, 237, 255));

		g.fillRect(0, 0, c.getWidth(), c.getHeight());
		for(MenuCloud cd: clouds){
			if(cd.getY() > -500) {
				g.drawImage(Images.cloud, cd.getX(), cd.getY());
			}
		}
		g.translate(Math.round(Camera.X_OFFSET), Math.round(Camera.Y_OFFSET));
		Grid.drawGrid(c, g);
		g.resetTransform();

		g.setColor(new Color(255, 255, 255, 0.6f));
		g.fillRect(0, 0, c.getWidth(), c.getHeight());
		g.drawImage(Images.logo, c.getWidth()/2 - Images.logo.getWidth() / 2, 0);
		g.setFont(Fonts.LargeFont);
		drawWithShadow(g, quip, c.getWidth() / 2 - Fonts.LargeFont.getWidth(quip)/2, 150, Color.black, Color.white);

		g.setFont(Fonts.ChatFont);
		GraphicsUtil.drawWithShadow(g, "Version " + Settings.GameVersion, 3, c.getHeight() - Fonts.ChatFont.getHeight("�2011-2012 Studio878 Software Laboratories. All rights reserved.") - 3, Color.black, Color.white);

		GraphicsUtil.drawWithShadow(g, "�2011-2012 Studio878 Software Laboratories. All rights reserved.", c.getWidth() - Fonts.ChatFont.getWidth("�2011-2012 Studio878 Software Laboratories. All rights reserved.") - 9, c.getHeight() - Fonts.ChatFont.getHeight("�2011 Studio878 Software Laboratories. All rights reserved.") - 3, Color.black, Color.white);
		g.setColor(Color.black);
		if(currentMenu == 0) {
			g.setFont(Fonts.ChatFont);
			g.drawString("Save login information: ", c.getWidth() / 2 + 90, 420);
			save.draw(g);

			user.draw(c,  g);
			pass.draw(c,  g);

			login.draw(g, c);

			g.drawString("Username", c.getWidth()/2 - Fonts.ChatFont.getWidth("Username")/2, 220);
			g.drawString("Password", c.getWidth()/2 - Fonts.ChatFont.getWidth("Password")/2, 320);
		} else if(currentMenu == 1) {
			g.drawString("Server", c.getWidth()/2 - Fonts.LargeFont.getWidth("Server")/2, 260);
			server.draw(c, g);
			join_back.draw(g, c);
			join.draw(g, c);
		} else if (currentMenu == 2) {
			g.setFont(Fonts.LargeFont);
			g.setColor(Color.black);
			g.drawString(message, c.getWidth() / 2 - Fonts.LargeFont.getWidth(message)/2, 300);
			if(isConnecting) {
				String st = "Tip: " + tip;
				g.setFont(Fonts.ChatFont);
				GraphicsUtil.drawWithShadow(g, st, c.getWidth()/2 - Fonts.ChatFont.getWidth(st)/2, c.getHeight() - 100, Color.black, new Color(255, 255, 255, 0.8f));
			}
			back.draw(g, c);

		} else if (currentMenu == 3) {
			connect.draw(g, c);
			host.draw(g, c);
			options.draw(g, c);
			quit.draw(g, c);
			adventure.draw(g, c);
			loadout.draw(g, c);
		} else if (currentMenu == 4) {
			server_name.draw(c, g);
			server_port.draw(c, g);
			server_seed.draw(c, g);
			server_max_players.draw(c, g);
			server_start.draw(g, c);
			server_back.draw(g, c);
			Font f = Fonts.LargeFont;
			g.setFont(f);
			Color cs = new Color(255, 255, 255);
			drawWithShadow(g, "Host a server", c.getWidth() / 2 - f.getWidth("Host a server")/2, 195,  Color.black, cs);
			Font ft = Fonts.ChatFont;
			g.setFont(ft);
			String s = "The Collect, Construct, Conquer! Dedicated Server will be downloaded and run through this client.";
			drawWithShadow(g, s, c.getWidth() / 2 - ft.getWidth(s)/2, 440, Color.black, cs);

			s = "Players can join you by connecting to " + ext_ip + " (" + int_ip + " over LAN)";
			drawWithShadow(g, s, c.getWidth() / 2 - ft.getWidth(s)/2, 470, Color.black, cs);

			s = "Hosting a server is resource-intensive. It is not recommended on slower computers.";
			drawWithShadow(g, s, c.getWidth() / 2 - ft.getWidth(s)/2, 500, Color.black, cs);

			s = "You can view the server output and enter commands by accessing the console with " + capitalizeFirst(KeyBindings.getKeyName(KeyBindings.Open_Console));
			drawWithShadow(g, s, c.getWidth() / 2 - ft.getWidth(s)/2, 530, Color.black, cs);
		} else if (currentMenu == 5) {
			servers.draw(g);
			refresh.draw(g, c);
			list_connect.draw(g, c);
			list_back.draw(g, c);
			direct_connect.draw(g, c);
			switch_mode.draw(g, c);
			search_speed.draw(g, c);
			if(server_message != null) {
				Fonts.drawWithColor(server_message, Fonts.ChatFont, g, c.getWidth()/ 2 - Fonts.ChatFont.getWidth(server_message)/2, 525, c.getWidth());
			}
		} else if (currentMenu == 6) {
			chapter_back.draw(g, c);
			play_chapter.draw(g, c);
			//map_editor.draw(g, c);
			chapters.draw(g);
		}


	}

	public void update(GameContainer c, int arg1) {

		if(lastFrameMenu != currentMenu) {
			if(currentMenu == 2) {
				tip = TitleMessages.getRandomTip();
			}
		}

		lastFrameMenu = currentMenu;

		if(currentMenu != 0) {

			if(currentMenu != 3) {

				if(Camera.Y_OFFSET <= -Settings.WorldHeight*16*.35) {
					Camera.Y_OFFSET += 20;
				}
			} else {
				if(Camera.Y_OFFSET >= -Settings.WorldHeight*16*.4) {
					Camera.Y_OFFSET -= 20;
				}
				if(!Window.MenuOpen) {
					if(c.getInput().getMouseX() >= c.getWidth() - 200 && Camera.X_OFFSET >= -(Settings.WorldWidth-100)*16 ) {
						Camera.X_OFFSET -= 5;
					} else if (c.getInput().getMouseX() <= 200 && Camera.X_OFFSET <= -100) {
						Camera.X_OFFSET += 5;
					}
				}
			}
		} else {
			if((int) (Math.random()*100) >= 99) {
				clouds.add(new MenuCloud((float) (Math.random()*(c.getWidth()+200) - 200), (float) 20));
			}
		}
		for(MenuCloud cd: clouds) {
			cd.move();
		}



		if(!Window.MenuOpen && !Window.alertPopup.isOpen() && !Window.eventPopup.isOpen() &&!Window.optionPopup.isOpen()) {
			if(currentMenu == 0) {
				user.update(c);
				pass.update(c);
				save.process(c);
				login.check(c);

			} else if(currentMenu == 1) {
				join_back.check(c);
				join.check(c);
				server.update(c);
			} else if (currentMenu == 2) {
				back.check(c);
			} else if (currentMenu == 3) {
				options.check(c);
				connect.check(c);
				host.check(c);
				quit.check(c);
				adventure.check(c);
				loadout.check(c);
			} else if (currentMenu == 4) {
				server_back.check(c);
				server_start.check(c);
				server_max_players.update(c);
				server_seed.update(c);
				server_name.update(c);
				server_port.update(c);
			} else if(currentMenu == 5) {
				servers.update(c);
				list_connect.check(c);
				refresh.check(c);
				list_back.check(c);
				direct_connect.check(c);
				switch_mode.check(c);
				search_speed.check(c);

				int msw = Mouse.getDWheel();
				if(msw > 0) {
					servers.updateScrollPosition(-5*Settings.ScrollAmount);
				} else if(msw < 0) {
					servers.updateScrollPosition(5*Settings.ScrollAmount);
				}

			} else if (currentMenu == 6) {
				chapter_back.check(c);
				play_chapter.check(c);
				chapters.update(c);
				//map_editor.check(c);
				if(chapters.getSelected() == null) {
					play_chapter.isDisabled(true);
				} else {
					play_chapter.isDisabled(false);

				}
			}
		}



		if(c.getInput().isKeyPressed(Keyboard.KEY_TAB)) {
			if(currentMenu == 0) {
				if(user.hasFocus()) {
					pass.setFocus(true);
				} else if (pass.hasFocus()) {
					user.setFocus(true);
				}
			} else if (currentMenu == 4) {
				if(server_name.hasFocus()) {
					server_port.setFocus(true);
				} else if (server_port.hasFocus()) {
					server_name.setFocus(true);
				}
			}
		}
		if(back_3.isSelected() || server_back.isSelected() || list_back.isSelected()) {
			currentMenu = 3;
			previousMenu = 3;
		}

		if(connect.isSelected()) {
			currentMenu = 5;
			previousMenu = 5;
			refreshServers();

		}

		if(adventure.isSelected()) {
			currentMenu = 6;
			processChapterMenu();
		}

		if(join_back.isSelected()) {
			currentMenu = 5;
		}
		if(host.isSelected()) {
			currentMenu = 4;
			previousMenu = 4;
		}


		if(loadout.isSelected()) {

			Settings.MainWindow.alertPopup.setMessage("Connecting to server...");
			Settings.MainWindow.alertPopup.setOpen(true);
			new Thread() {
				public void run() {
					MultiplayerSettings.updateMultiplayerVariables();
					Settings.MainWindow.MenuId = 6;
					Settings.MainWindow.MenuOpen = true;
					Settings.MainWindow.alertPopup.setOpen(false);
				}
			}.start();


		}

		if(switch_mode.isSelected()) {
			if(condensed) {
				condensed = false;
				switch_mode.setValue("View: Full");
				Settings.PropertiesFile.setBoolean("list-condensed", false);
				refreshServers();

			} else {
				condensed = true;
				switch_mode.setValue("View: Condensed");
				Settings.PropertiesFile.setBoolean("list-condensed", true);
				refreshServers();
			}
		}
		if(search_speed.isSelected()) {
			switch(search_mode){
			case 0:
				search_mode = 1;
				search_speed.setValue("Search: Regular");
				Settings.PropertiesFile.setInt("server-search-speed", 1);

				break;
			case 1:
				search_mode = 2;
				search_speed.setValue("Search: Full");
				Settings.PropertiesFile.setInt("server-search-speed", 2);

				break;
			case 2:
				search_mode = 0;
				search_speed.setValue("Search: Fast");
				Settings.PropertiesFile.setInt("server-search-speed", 3);

				break;
			}
		}
		if(quit.isSelected()) {
			Main.dispose();
			System.exit(0);
		}

		if(list_connect.isSelected()) {
			if(servers.getSelected() != null) {
				currentMenu = 2;

				ServerListItem i = (ServerListItem) servers.getSelected();
				String ip = i.getIp();
				int port = i.getPort();
				connectToServer(ip, port);
			}
		}
		if(refresh.isSelected()) {
			refreshServers();
		}

		if(back.isSelected()) {
			currentMenu = previousMenu;
			new Thread() {
				public void run() {
					Main.stopGame();
				}
			}.start();
			isConnecting = false;
			if(loginThread != null) {
				loginThread.stop();
			}
		}

		if(server_start.isSelected()) {
			startServer();
		}

		if(join.isSelected()) {
			startConnect();
		}

		if(chapter_back.isSelected()) {
			currentMenu = 3;
		}

		if(map_editor.isSelected()) {
			startWorldEditor();
		}

		if(play_chapter.isSelected()) {
			previousMenu = 6;
			ChapterListItem item = (ChapterListItem) chapters.getSelected();
			startChapter(item.getChapter());
		}

		if(login.isSelected()) {
			startLogin();
		}
		if(direct_connect.isSelected()) {
			currentMenu = 1;
		}

		if(options.isSelected()) {
			Settings.MainWindow.MenuId = 1;
			Settings.MainWindow.MenuOpen = true;
		}

		if(c.getInput().isKeyPressed(Keyboard.KEY_RETURN)) {
			if(currentMenu == 0) {
				startLogin();
			} else if(currentMenu == 1) {
				startConnect();
			} else if (currentMenu == 4) {
				startServer();
			}
		}

	}

	public String getHash(String password) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		MessageDigest digest = MessageDigest.getInstance("SHA-256");
		digest.reset();
		byte[] input = digest.digest(password.getBytes("UTF-8"));
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < input.length; i++) {
			sb.append(Integer.toString((input[i] & 0xff) + 0x100, 16).substring(1));
		}
		return sb.toString();
	}

	public void startLogin() {
		loginThread = new Thread() {
			public void run() {
				currentMenu = 2;
				String username = user.getText();
				String password = pass.getText();
				String owp = password;
				message = "Connecting to studio878software.com";
				try {
					JSONObject content = API.getObject("http://api.studio878software.com/user/" + username + "/auth/" + password);
					if(content.get("username").equals(username) && content.get("auth").equals(true)) {
						Settings.Username = (String) content.get("username");
						Settings.Password = password;
						Settings.Session = (String) content.get("session");
						System.out.println("Session ID: " + Settings.Session);
						if(save.isSelected()) {
							Settings.PropertiesFile.setString("username", username);
							Settings.PropertiesFile.setString("password", encrypt(owp));
						} else {
							Settings.PropertiesFile.setString("username", "");
							Settings.PropertiesFile.setString("password", "");
						}
						currentMenu = 3;
						previousMenu = 1;
						server.setFocus(true);
						user.setVisibility(false);
						pass.setVisibility(false);
						server.setVisibility(true);
					} else {
						message = "Invalid username or password";
						pass.setText("");
						return;
					}
				} catch (Exception e) {
					message = "Error receiving information from studio878software.com";
					e.printStackTrace();
					return;
				}
			}
		};
		loginThread.start();
	}
	public void startConnect() {
		if(!startedJoining) {

			currentMenu = 2;
			startedJoining = true;
			String[] split = server.getText().split(":");
			int port = 0;
			String server = "";
			if(split.length == 2) {
				server = split[0];
				try {
					port = Integer.parseInt(split[1]);
				} catch (NumberFormatException e) {
					startedJoining = false;
					setDisplayMessage("Invalid port.");
				}
			} else {
				server = split[0];
				port = 31337;
			}
			connectToServer(server, port);
			startedJoining = false;
		}
	}

	@SuppressWarnings("deprecation")
	public void refreshServers() {
		if(refreshingServers) {
			refreshThread.stop();
		}
		server_message = "Receiving server data...";
		refreshThread = new Thread() {
			public void run() {
				refreshingServers = true;

				servers.clearItems();
				try {
					JSONArray content = API.getArray("http://api.studio878software.com/ccc/network/server/list");
					int numServers = 0;
					int totalServers = content.size();
					int timeout = 100;
					if(search_mode == 1) {
						timeout = 250;
					} else if (search_mode == 2) {
						timeout = 500;
					}
					server_message = "Polling servers... (0 out of " + totalServers + ")";
					int pos = 0;
					for(Object o : content) {
						pos++;
						JSONObject cst = (JSONObject) o;
						try {
							String ip = (String) cst.get("ip");
							int port = Integer.parseInt((String) cst.get("port"));
							String name = (String) cst.get("name");
							Socket s = null;
							long totalTime = 99999;
							long start = System.currentTimeMillis();
							int players = 0, maxPlayers = 0;
							try {
								server_message = "�0Polling servers... �c(" + pos + " out of " + totalServers + ")";
								SocketAddress sockaddr = new InetSocketAddress(ip, port);
								s = new Socket();

								s.connect(sockaddr, timeout);

								BufferedReader in = new BufferedReader(new InputStreamReader(s.getInputStream()));
								PrintWriter out = new PrintWriter(new OutputStreamWriter(s.getOutputStream()));
								out.println("requestinfo");
								out.flush();
								String m = null;
								while((m = in.readLine()) == null) {
									if(System.currentTimeMillis() - start >= 2000) {
										System.out.println("Timed out");
										throw new SocketTimeoutException();
									}
								}
								String[] split = m.split(":");

								totalTime = (System.currentTimeMillis() - start);
								if(!Settings.GameVersion.equals(split[1])) {
									throw new SocketTimeoutException();
								}
								players = Integer.parseInt(split[2]);
								maxPlayers = Integer.parseInt(split[3]);
								numServers++;
							} catch(SocketTimeoutException e) {
								continue;
							} catch(UnknownHostException e) {
							} catch(IOException e) {
							}


							if(condensed) {
								servers.addItem(new CondensedServerListItem(ip, port, name, totalTime, players, maxPlayers, servers));
							} else {
								servers.addItem(new ServerListItem(ip, port, name, totalTime, players, maxPlayers, servers));
							}


						} catch (NumberFormatException e) {
							refreshingServers = false;
							e.printStackTrace();

						}

					}

					server_message = numServers + " servers found";
					if(numServers == 1) {
						server_message = "1 server found";
					} else if (numServers == 0) {
						server_message = "No servers found";
					}
					refreshingServers = false;
				} catch (Exception e) {
					e.printStackTrace();
					server_message = "Unable to get server list";
					refreshingServers = false;
				}

			}
		};
		refreshThread.start();

	}
	public static void connectToServer(String sv, int port) {
		attemptServer = sv;
		attemptPort = port;
		new Thread() {
			public void run() {
				isConnecting = true;
				boolean connected = false;
				message = "Connecting...";
				Console.write("Attempting to connect to " + attemptServer);
				try {
					String[] split = attemptServer.split(":");
					if(split.length != 2) {
						if(NetObjects.ActiveClient != null) {
							NetObjects.ActiveClient.interrupt();
							NetObjects.ActiveClient = null;
						}
						NetObjects.ActiveClient = new Client(attemptServer, attemptPort);
					} else {
						int m = 31337;
						try {
							m = Integer.parseInt(split[1]);
						} catch (NumberFormatException e) {
						}
						NetObjects.ActiveClient = new Client(split[0], m);
					}
					connected = true;
				} catch (ConnectException e) {
					message = "Server not found";
					isConnecting = false;

				} catch (UnknownHostException e) {
					Console.error("Host not found.");
					message = "Connection error: Server not found.";
					isConnecting = false;

				} catch (IOException e) {
					message = "Connection error: Already connected.";
					e.printStackTrace();
					isConnecting = false;

				} catch (IllegalArgumentException e) {
					Console.error("Connection error: Server not found");
					e.printStackTrace();
					message = "Server not found";
					isConnecting = false;

				}
				if(connected) {
					try {
						Settings.PropertiesFile.setString("lastServer", attemptServer);
						Console.write("Connected to " +attemptServer);
						message = "Connected to " + attemptServer;
						NetObjects.Sender.sendPacket(new Packet01Connect(Settings.Username, Settings.Session));
						Window.Music.stop();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}.run();

	}
	public String decrypt(String input) {
		try {
			String passkey = "sab107a234bjla34";
			SecretKeySpec key = new SecretKeySpec(passkey.getBytes(), "AES");
			Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding", "SunJCE");
			cipher.init(Cipher.DECRYPT_MODE, key);
			byte[] r =  Base64.decode(input);
			byte[] sb = cipher.doFinal(r);
			String c = new String(sb, "UTF8");
			return c;
		} catch (Exception e) {
			e.printStackTrace();
			message = "Error loading saved password";
			return  "";
		}
	}

	public static void drawWithShadow(Graphics g, String s, int x, int y, Color f, Color b) {
		g.setColor(b);
		g.drawString(s, x, y + 1);
		g.setColor(f);
		g.drawString(s, x, y);
	}
	public String encrypt(String input) {
		try {
			String passkey = "sab107a234bjla34";
			SecretKeySpec key = new SecretKeySpec(passkey.getBytes(), "AES");
			Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding", "SunJCE");
			cipher.init(Cipher.ENCRYPT_MODE, key);
			byte[] r = cipher.doFinal(input.getBytes());
			String br = Base64.encodeBytes(r);
			return br;
		} catch (Exception e) {
			message = "Encryption failure.";
			return "";
		}
	}

	public static void setDisplayMessage(String s) {
		message = s;
	}

	private void startWorldEditor() {
		currentMenu = 2;
		previousMenu = 6;
		new Thread () {
			public void run() {
				WorldEditorLoader.downloadMapEditor();
			}
		}.start();
	}
	private void startServer() {
		currentMenu = 2;
		if(server_port.getText().matches("[0-9]{4,6}")) {
			if(server_max_players.getText().matches("[0-9]{1,3}")) {
				if(server_name.getText().length() > 0 && server_name.getText().length() < 32) {
					PropertiesReader p = new PropertiesReader(Settings.SystemFolder + File.separator + "server_settings.properties");
					p.setString("name", server_name.getText());
					p.setString("port", server_port.getText());
					p.setString("seed", getSeed());
					p.setBoolean("storymode", false);

					p.setString("max-players", server_max_players.getText());
					ServerLoader.downloadServer();
				} else {
					setDisplayMessage("Invalid server name.");

				}
			} else {
				setDisplayMessage("Invalid max players value.");

			}
		} else {
			setDisplayMessage("Invalid port number. A port number must be 4-6 digits.");
		}
	}

	private void startChapter(ChapterInfo ch) {
		currentMenu = 2;
		PropertiesReader p = new PropertiesReader(Settings.SystemFolder + File.separator + "server_settings.properties");
		System.out.println(Settings.SystemFolder + "server_settings.properties");
		p.setString("name", Settings.Username + "'s Adventure (" + ch.getTitle() + ")");
		p.setString("port", "31337");
		p.setString("max-players", "4");
		p.setString("map", ch.getMap());
		p.setString("storymode", "true");
		p.save();
		ServerLoader.downloadServer();

	}
	private String getSeed() {
		String seed = server_seed.getText();
		if(seed == null || seed.length() == 0) {
			Random random = new Random();
			char[] digits = new char[12];
			digits[0] = (char) (random.nextInt(9) + '1');
			for (int i = 1; i < 12; i++) {
				digits[i] = (char) (random.nextInt(10) + '0');
			}
			return "" + Long.parseLong(new String(digits));
		} else {
			return seed;
		}
	}

	public void processChapterMenu() {
		chapters.clearItems();
		for(ChapterInfo i : ChapterInfo.values()) {
			chapters.addItem(new ChapterListItem(i, chapters));
		}
	}

	protected String capitalizeFirst(String s) {
		char[] chars = s.toLowerCase().toCharArray();
		String ret = "";
		for (int i = 0; i < chars.length; i++) {
			if(i == 0) {
				ret += Character.toUpperCase(chars[i]);
			} else {
				ret += Character.toLowerCase(chars[i]);
			}
		}
		return ret;
	}
}
