package com.studio878.remoteui;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

import com.studio878.ui.Fonts;

public class RemoteUILabel extends RemoteUIItem {


	public void update(GameContainer c) {
		if(centered) {
			x = c.getWidth()/2 + sx - Fonts.LargeFont.getWidth(value)/2;
		}
	}
	
	public void draw(GameContainer c, Graphics g) {
		Fonts.drawWithColor(value, Fonts.LargeFont, g, x, y, width);
	}

}
