package com.studio878.remoteui;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

import com.studio878.remoteui.RemoteUIManager.RemoteUIItemType;


public abstract class RemoteUIItem {

	int x, y, z, width, height, id;
	String value;
	boolean centered;
	int state;
	RemoteUIItemType type;
	RemoteUIContainer container;

	int sx, sy;

	public int getId() {
		return id;
	}
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	public int getZ() {
		return z;
	}
	
	public int getWidth() {
		return width;
	}
	
	public int getHeight() {
		return height;
	}
	
	public boolean isCentered() {
		return centered;
	}
	
	public RemoteUIItemType getType() {
		return type;
	}
	
	public int getState() {
		return state;
	}
	
	
	public String getValue() {
		return value;
	}
	
	public void setX(int x) {
		this.x = x;
		this.sx = x;
	}

	public void setY(int y) {
		this.y = y;
		this.sy = y;
	}
	
	public void setZ(int z) {
		this.z = z;
	}
	
	public void setState(int s) {
		state = s;
	}
	
	public void setWidth(int w) {
		width = w;
	}
	
	public void setHeight(int h) {
		height = h;
	}
	
	public void setValue(String s) {
		s = s.replace("/U+003A", ":");
		s = s.replace("/U+002C", ",");
		value = s;
	}
	
	public RemoteUIContainer getParent() {
		return container;
	}
	
	public void setId(int i) {
		id = i;
	}
	
	public void setCentered(boolean c) {
		centered = c;
	}

	protected void setContainer(RemoteUIContainer container) {
		this.container = container;
	}

	public void update(GameContainer c) {
		if(centered) {
			x = sx + c.getWidth()/2 - width/2;
		}
	}

	public abstract void draw(GameContainer c, Graphics g);

}
