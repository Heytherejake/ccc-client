package com.studio878.remoteui;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

public class RemoteUIManager {
	
	public enum RemoteUIItemType {
		Button (0),
		HttpImage (1),
		SolidColorBox (2),
		TextField (3),
		Slider (4),
		Label (5),
		;
		
		int id;
		private static final Map<Integer, RemoteUIItemType> lookupId = new HashMap<Integer, RemoteUIItemType>();

		private RemoteUIItemType(int id) {
			this.id = id;
		}
		
		public int getId() {
			return id;
		}
		
		public static RemoteUIItemType lookupId(int i) {
			return lookupId.get(i);
		}
		
		static {
			for(RemoteUIItemType t: values()) {
				lookupId.put(t.getId(), t);
			}
		}
		
	}
	

	
	private static CopyOnWriteArrayList<RemoteUIContainer> containers = new CopyOnWriteArrayList<RemoteUIContainer>();
	
	private static int selected = 0;
	
	public static void registerContainer(RemoteUIContainer container) {
		RemoteUIContainer c = getContainer(container.getId());
		if(c != null) {
			containers.remove(c);
		}
		containers.add(container);
	}
	
	public static void clearContainers() {
		containers.clear();
	}
	
	public static RemoteUIContainer getContainer(int id) {
		for(RemoteUIContainer c : containers) {
			if(c.getId() == id) {
				return c;
			}
		}
		return null;
	}
	
	public static int getSelectedContainer() {
		return selected;
	}
	
	public static void setSelectedContainer(int i) {
		selected = i;
	}
	

	
	
	
}
