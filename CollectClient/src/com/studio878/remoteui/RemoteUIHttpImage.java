package com.studio878.remoteui;

import java.net.URL;
import java.net.URLConnection;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;

import com.studio878.remoteui.RemoteUIManager.RemoteUIItemType;
import com.studio878.util.Console;

public class RemoteUIHttpImage extends RemoteUIItem{

	Image is;
	boolean shouldUpdateImage = false;
	public RemoteUIHttpImage() {
		this.type = RemoteUIItemType.HttpImage;
	}

	public void setValue(String s) {
		super.setValue(s);
		shouldUpdateImage = true;
	}
	
	public void update(GameContainer c) {
		if(shouldUpdateImage) {
			try {
				URLConnection conn = new URL(value).openConnection();
				is = new Image(conn.getInputStream(), "remote_ui_http_" + id, false);
			} catch (Exception e) {
				Console.error("RemoteUIException - Image not found");
				e.printStackTrace();
			}
			shouldUpdateImage = false;
		}
		
		if(centered && is != null) {
			x = sx + c.getWidth()/2 - is.getWidth()/2;
		}
	}

	public void draw(GameContainer c, Graphics g) {
		if(is != null) {
			g.drawImage(is, x, y);
		}
	}
}
