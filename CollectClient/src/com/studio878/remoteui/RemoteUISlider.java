package com.studio878.remoteui;

import org.lwjgl.input.Mouse;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

import com.studio878.io.NetObjects;
import com.studio878.packets.Packet48SendRemoteUIItemValue;
import com.studio878.ui.Fonts;
import com.studio878.ui.Slider;
import com.studio878.util.Console;

public class RemoteUISlider extends RemoteUIItem {

	public static final int HIDE_AMOUNT = 0;
	public static final int SHOW_AMOUNT = 1;

	Slider slider;
	int previousValue = 0;
	boolean wasMouseDown = false;

	public RemoteUISlider() {
		this.type = type.Slider;
	}


	public void setValue(String s) {
		try {
			int f = Integer.parseInt(s);
			if(slider != null) {
				slider.setPosition(f);
			}
			this.value = ""+f;
		} catch (NumberFormatException e) {
			Console.error("Invalid value sent by server (" + s + ")");
		}
	}
	public void update(GameContainer c) {
		if(slider == null) {
			boolean show = false;
			if(state == SHOW_AMOUNT) {
				show = true;
			}
			slider = new Slider(x, y, width, Fonts.ChatFont, centered, show);
			try {
			slider.setPosition(Integer.parseInt(this.value));
			} catch (NumberFormatException e) {
				Console.error("Unable to set slider position");
			}
		}

		slider.update(c);
		if((wasMouseDown && !Mouse.isButtonDown(0))  &&  slider.getPosition() != previousValue) {
			this.value = slider.getPosition() + "";
			NetObjects.Sender.sendPacket(new Packet48SendRemoteUIItemValue(this));
			previousValue = slider.getPosition();

		}

		if(Mouse.isButtonDown(0)) {
			wasMouseDown = true;
		} else {
			wasMouseDown = false;
		}
	}

	public void draw(GameContainer c, Graphics g) {
		if(slider != null) {
			slider.draw(c, g);
		}
	}

}
