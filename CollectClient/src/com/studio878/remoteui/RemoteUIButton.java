package com.studio878.remoteui;


import org.lwjgl.input.Mouse;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;

import com.studio878.remoteui.RemoteUIManager.RemoteUIItemType;
import com.studio878.ui.Fonts;
import com.studio878.util.Images;

public class RemoteUIButton extends RemoteUIItem{
	public final int STATE_ENABLED = 0;
	public final int STATE_DISABLED = 1;

	public RemoteUIButton() {
		this.type = RemoteUIItemType.Button;
	}

	public void draw(GameContainer c, Graphics g) {
		int offset = 0;
		int mx = c.getInput().getMouseX();
		int my = c.getInput().getMouseY();
		if(mx >= x && mx <= x + width && my >= y && my <= y + height) {
			offset = 50;
			if(Mouse.isButtonDown(0)) {
				offset = 100;
			}
		}
		Image is = Images.button;
		Color cs = is.getColor(20, 20 + offset);
		g.setColor(cs);
		g.fillRect(x + 6, y + 6, width - 12, height-12);
		g.drawImage(is.getSubImage(0, offset, 6, 6), x, y);

		g.drawImage(is.getSubImage(7, offset, 6, 6), x + width - 6, y);
		g.drawImage(is.getSubImage(0, 44 + offset, 6, 6), x, y + height - 6);
		g.drawImage(is.getSubImage(7, 44+ offset, 6, 6), x + width - 6, y + height - 6);
		Image isd = is.getSubImage(6, 0+ offset, 1, 6);
		Image ise = is.getSubImage(6, 44+ offset, 1, 6);
		for(int i = 0; i < width - 12; i++) {
			g.drawImage(isd, x + i + 6, y);
			g.drawImage(ise, x + i + 6, y + height - 6);
		}
		Image isf = is.getSubImage(0, 12+ offset, 6, 1);
		Image isg = is.getSubImage(7, 12+ offset, 6, 1);
		for(int i = 0; i < height - 12; i++) {
			g.drawImage(isf, x, y + i + 6);
			g.drawImage(isg, x + width - 6, y + i + 6);
		}
		g.setColor(Color.black);
		g.setFont(Fonts.ChatFont);
		g.drawString(value, x + width/2 -  Fonts.ChatFont.getWidth(value)/2, y + height/2 - Fonts.ChatFont.getHeight(value)/2 - 2);
	}
}
