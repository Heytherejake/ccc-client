package com.studio878.remoteui;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

import com.studio878.remoteui.RemoteUIManager.RemoteUIItemType;

public class RemoteUISolidColorBox extends RemoteUIItem {

	Color color;
	public RemoteUISolidColorBox() {
		this.type = RemoteUIItemType.SolidColorBox;
	}

	public void setValue(String s) {
		value = s;
		String[] split = s.split("-");
		color = new Color(Integer.parseInt(split[0]), Integer.parseInt(split[1]), Integer.parseInt(split[2]), ((float) ((float)Integer.parseInt(split[3])/(float)255)));
	}
	
	
	public void draw(GameContainer c, Graphics g) {
		if(color != null) {
			g.setColor(color);
			g.fillRect(x, y, width, height);
		}
	}
}
