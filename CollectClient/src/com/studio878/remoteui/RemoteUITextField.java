package com.studio878.remoteui;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

import com.studio878.io.NetObjects;
import com.studio878.packets.Packet48SendRemoteUIItemValue;
import com.studio878.remoteui.RemoteUIManager.RemoteUIItemType;
import com.studio878.ui.Fonts;
import com.studio878.ui.ModifiedTextField;

public class RemoteUITextField extends RemoteUIItem{
	ModifiedTextField field;
	String pv = "";
	
	public RemoteUITextField() {
		this.type = RemoteUIItemType.TextField;
	}
	
	public void setValue(String s) {
		if(s == null || s.length() == 0) {
			s = " ";
		}
		if(field != null) {
			field.setText(s);
		}
		this.value = s;

	}

	public void update(GameContainer c) {
		if(field != null) {
			setValue(field.getText());
			field.update(c);
			if(!pv.equals(field.getText())) {
				NetObjects.Sender.sendPacket(new Packet48SendRemoteUIItemValue(this));
			}
			pv = field.getText();
		}
	}

	public void draw(GameContainer c, Graphics g) {
		if(field == null) {
			field = new ModifiedTextField(c, Fonts.ChatFont, x, y, width, height, 0, "", false, centered);
			field.setText(""+value);
		}
		field.draw(c, g);

	}
}
