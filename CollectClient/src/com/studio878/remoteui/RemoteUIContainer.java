package com.studio878.remoteui;

import java.util.ArrayList;

import org.lwjgl.input.Keyboard;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

import com.studio878.io.NetObjects;
import com.studio878.packets.Packet45SendRemoteUIMousePressEvent;
import com.studio878.packets.Packet46SendRemoteUIKeyPressEvent;

public class RemoteUIContainer {

	ArrayList<RemoteUIItem> items = new ArrayList<RemoteUIItem>();
	Object lock = new Object();
	int id;


	public void addItem(RemoteUIItem s) {
		synchronized (lock) {
			s.setContainer(this);
			items.add(s);

		}
	}

	public void removeItem(RemoteUIItem s) {
		synchronized (lock) {
			s.setContainer(null);
			items.remove(s);

		}
	}

	public RemoteUIItem getItem(int id) {
		synchronized (lock) {
			for(RemoteUIItem i : items) {
				if(i.getId() == id) {
					return i;
				}
			}
		}
		return null;
	}
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void update(GameContainer c) {

		for(RemoteUIItem i : items) {
			i.update(c);
		}

		int mx = c.getInput().getMouseX();
		int my = c.getInput().getMouseY();
		int button = -1;
		if(c.getInput().isMousePressed(0)) {
			button = 0;
		} else if (c.getInput().isMousePressed(1)) {
			button = 1;
		}
		if(button != -1) {
			ArrayList<RemoteUIItem> m0 = new ArrayList<RemoteUIItem>();
			for(RemoteUIItem i : items) {
				if(mx >= i.getX() && mx <= i.getX() + i.getWidth() && my >= i.getY() && my <= i.getY() + i.getHeight()) {
					m0.add(i);
				}
			}
			RemoteUIItem trt = null;
			for(RemoteUIItem i : m0) {
				if(trt == null || i.getZ() > trt.getZ()) {
					trt = i;
				}
			}
			NetObjects.Sender.sendPacket(new Packet45SendRemoteUIMousePressEvent(this.getId(), trt.getId(), mx, my, button));
		}

		boolean isFocused = false;
		for(RemoteUIItem i : items) {
			if(i instanceof RemoteUITextField) {
				RemoteUITextField t = (RemoteUITextField) i;
				if(t.field.hasFocus()) {
					isFocused = true;
				}
			}
		}
		if(!isFocused) {
			for(int i = 1; i < Keyboard.getKeyCount(); i++) {
				if(c.getInput().isKeyPressed(i)) {
					NetObjects.Sender.sendPacket(new Packet46SendRemoteUIKeyPressEvent(this.getId(), mx, my, i));
				}
			}
		}


	}

	public void draw(GameContainer c, Graphics g) {
		for(RemoteUIItem i : items) {
			i.draw(c, g);
		}
	}


}
