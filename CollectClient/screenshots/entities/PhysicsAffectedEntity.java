package com.studio878.server.entities;

import com.studio878.server.app.Main;
import com.studio878.server.block.Block;
import com.studio878.server.entities.EntityManager.EntityName;
import com.studio878.server.packets.Packet13SendMovement;
import com.studio878.server.util.Settings;
import com.studio878.server.world.Grid;



public class PhysicsAffectedEntity extends Entity{
	double yspeed = 0;
	double xspeed = 0;
	Block b;
	int facingDirection;
	static final int TerminalVelocity = 10;
	int prevx = 0;
	int prevy = 0;
	public PhysicsAffectedEntity(int x, int y, int height, int width, EntityName type) {
		super(x, y, height, width, type);

	}

	public void setFacingDirection(int d) {
		facingDirection = d;
	}

	public int getFacingDirection() {
		return facingDirection;
	}

	public void setVelocity(double x, double y) {
		xspeed = x;
		yspeed = y;
	}

	public void step() {
		boolean doMove = true;
		int dx = x - (x%16);
		int dy = y - (y%16);
		b =  Grid.getClosestBlock(dx, dy + height);
		Block ba = b.getBlockAdjacent(1, 0);
		if(ba == null) {
			this.x += -2*xspeed;
			xspeed = 0;
			return;
		}
		if(b != null && ba != null &&  (!b.getType().getPermiability() || !ba.getType().getPermiability()) && yspeed > 0) {
			doMove = false;
		
		}

		if(doMove) {
			yspeed += Settings.Gravity;
		} else {
			yspeed = 0;
			if(this instanceof Player) {
				((Player) this).hasJumped = false;
				y -= (y - dy);
			}
		}
		b = Grid.getClosestBlock(dx, dy);
		if(yspeed < 0.0 && b != null && (!b.getType().getPermiability() || !b.getBlockAdjacent(1, 0).getType().getPermiability())) {
			y -= 2*yspeed;
			yspeed = 0;
		} else {
			y += yspeed;
			if(yspeed > TerminalVelocity) {
				yspeed = TerminalVelocity;
			}
		}

		if(prevx != x || prevy != y) {
			if(Main.getDispatch().getClients().size() > 0) {
					Main.getDispatch().broadcastPacket(new Packet13SendMovement(Main.getDispatch().getClients().get(0), x, y, id, facingDirection));
				}
		}
		if(xspeed != 0) {
			//Fix this
			if(isLocationValid((int) (x + xspeed), y, (int) (Math.abs(xspeed)%xspeed))) {
				x += xspeed;
				if(yspeed == 0) {
					if(xspeed + Settings.Friction < Settings.Friction) {
						xspeed += Settings.Friction;
					} else if (xspeed - Settings.Friction > Settings.Friction) {
						xspeed -= Settings.Friction;

					}
				} else {
				}
			} else {
				xspeed = 0;
			}
		}
		prevx = x;
		prevy = y;
	}


}
