package com.studio878.server.entities;

import java.util.ArrayList;
import java.util.HashMap;



public class EntityManager {
	
	public enum EntityName {
		Player (0),
		Grenade (1)
		;
		
		private static HashMap<Integer, EntityName> idLookup = new HashMap<Integer, EntityName>();

		private int id;
		private EntityName(int i) {
			id = i;
		}
		
		public int getId() {
			return id;
		}
		
		public static EntityName lookupId(int i) {
			return idLookup.get(i);
		}
		static {
			for(EntityName t: values()) {
				idLookup.put(t.getId(), t);
			}
		}		
	}
	
	static ArrayList<Entity> entities = new ArrayList<Entity>();
	static int highestId = 0;
	public static void registerEntity(Entity e) {
		entities.add(e);
		highestId++;
	}

	
	public static void unregisterEntity(Entity e) {
		if(entities.contains(e)) {
			entities.remove(e);
		}
	}
	public static int getHighestId() {
		return highestId;
	}
	public static ArrayList<Entity> getEntities() {
		return entities;
	}
	public static ArrayList<PhysicsAffectedEntity> getPhysicsAffectedEntities() {
		ArrayList<PhysicsAffectedEntity> results = new ArrayList<PhysicsAffectedEntity>();
		for(int i = 0; i < entities.size(); i++) {
			if(entities.get(i) instanceof PhysicsAffectedEntity) {
				results.add((PhysicsAffectedEntity) entities.get(i));
				
			}
		}
		return results;
	}
	public static ArrayList<Player> getPlayers() {
		ArrayList<Player> results = new ArrayList<Player>();
		for(int i = 0; i < entities.size(); i++) {
			if(entities.get(i) instanceof Player) {
				results.add((Player) entities.get(i));
				
			}
		}
		return results;
	}
	public static ArrayList<Entity> getDrawableEntities() {
		ArrayList<Entity> results = new ArrayList<Entity>();
		for(int i = 0; i < entities.size(); i++) {
			if(entities.get(i) instanceof Entity) {
				results.add((Entity) entities.get(i));
				
			}
		}
		return results;
	}


	

}
