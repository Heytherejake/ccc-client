package com.studio878.server.entities;

import com.studio878.server.entities.EntityManager.EntityName;
import com.studio878.server.io.NetPlayer;
import com.studio878.server.packets.Packet08SendMessage;

public class Player extends PhysicsAffectedEntity{
	String name;
	NetPlayer handle;
	boolean hasJumped = false;
	public Player(NetPlayer p, String name, int x, int y, int height, int width) {
		super(x, y, height, width, EntityName.Player);
		this.name = name;
		handle = p;
		MoveSpeed = 2;
		data = name;
	}
	
	public NetPlayer getHandle() {
		return handle;
	}
	
	public String getName() {
		return name;
	}

	public void jump() {
		if(!hasJumped) {
			y -= 3;
			yspeed = -5;
			hasJumped = true;
		}

	}
	


	
	public void sendMessage(String message) {
		handle.getSender().sendPacket(new Packet08SendMessage(handle, message));
	}
	
}
