package com.studio878.server.entities;



import com.studio878.server.block.Block;
import com.studio878.server.entities.EntityManager.EntityName;
import com.studio878.server.world.Grid;

public class Entity{
	int id, x, y, height, width, MoveSpeed;
	Block b;
	EntityName type;
	String data = "";
	public Entity(int x, int y, int height, int width, EntityName type) {
		this.x = x;
		this.y = y;
		this.height = height;
		this.width = width;
		EntityManager.registerEntity(this);
		this.id = EntityManager.getHighestId();
		this.type = type;
	}
	public int getId() {
		return id;
	}
	
	public EntityName getType() {
		return type;
	}
	
	public String getData() {
		return data;
	}
	public boolean isLocationValid(int x, int y, int dir) throws NullPointerException{
		int dx, dy;
		if(dir == 1) {
			dx = (x + MoveSpeed) - ((x+MoveSpeed)%16);
			dy = y - (y%16);
		} else {
			dx = (x-MoveSpeed)- ((x-MoveSpeed)%16);
			dy = y - (y%16); 
		}
		b =  Grid.getClosestBlock(dx, dy + height);
		if(b == null) {
			return false;
		}
		b = b.getBlockAdjacent(0, -1);
		if(dir == 1) {
			b = b.getBlockAdjacent(1,0);
		}
		for(int i = 0; i < ((int) (Math.ceil(height / 16))); i++) {
			if(b != null) {
				if(!b.getType().getPermiability()) {

					return false;
				}
			}
			b = b.getBlockAdjacent(0, -1);
		}
		return true;
	}
	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public void setX(int x) {
		this.x = x;
	}

	public void setY(int y) {
		this.y = y;
	}
}
