package com.studio878.server.entities;

import java.util.ArrayList;

import com.studio878.server.app.Main;
import com.studio878.server.block.Block;
import com.studio878.server.entities.EntityManager.EntityName;
import com.studio878.server.packets.Packet18RemoveEntity;
import com.studio878.server.world.Grid;

public class Grenade extends PhysicsAffectedEntity {

	private boolean hasExploded = false;
	public Grenade(int x, int y) {
		super(x, y, 27, 14, EntityName.Grenade);
		this.data = "null";
	}
	
	public void step() {
		super.step();
		if((yspeed == 0 || xspeed == 0) && !hasExploded) {
			hasExploded = true;
			Block center = Grid.getClosestBlock(x, y);
			ArrayList<Block> blocks = Grid.getCircle(center.getX(), center.getY(), 5);
			for(Block b: blocks) {
				b.destroy(null);
			}
			EntityManager.unregisterEntity(this);
			Main.getDispatch().broadcastPacket(new Packet18RemoveEntity(Main.getDispatch().getClients().get(0), this.id));
		}
	}

}
